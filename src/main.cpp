//
// Created by makrand on 5/13/20.
//
#include <iostream>
#include <DendriteUtils.h>
#include <point.h>
#include <TalyEquation.h>
#include <CHNSNodeData.hpp>
#include <PETSc/Solver/LinearSolver.h>
#include <PETSc/PetscUtils.h>
#include <IO/VTU.h>
#include <Traversal/Analytic.h>
#include <PETSc/IO/petscVTU.h>
#include <CHNSUtils.h>
#include <CHNSBoundaryConditions.h>
#include <PressurePoissonEquation.h>
#include <CHNSIntegrandsGenForm.hpp>
#include <CHEquation.hpp>
#include <SubDA/SubDomain.h>
#include <Boundary/SubDomainBoundary.h>
#include <Checkpoint/Checkpointer.h>
//#include <cmumps_struc.h>
#include <MomentumEquationProjection.h>
#include <VelocityUpdateEquation.h>
#include <search.h>
#include <AnalyticTS.hpp>
#include <EnergyAndMassCalculator.hpp>
#include <InitialConditions.hpp>
#include <PlotOverLine.h>

#ifdef  PROFILING
#define START_TIMER(X) {timerArrayStart[X] = std::chrono::high_resolution_clock::now();}
#define END_TIMER(X) {timerArrayEnd[X] = std::chrono::high_resolution_clock::now(); \
                     timings[X] +=  (static_cast<std::chrono::duration<DENDRITE_REAL>>(timerArrayEnd[X] - timerArrayStart[X])).count();}
#endif
using namespace PETSc;
std::array<DENDRITE_UINT,DIM> PERIODS;
int main(int argc, char *argv[]) {

	dendrite_init (argc, argv);

#ifdef  PROFILING
	enum TIMERS:DENDRITE_UINT{
	  BLOCK1_CH = 0,
	  BLOCK1_NS = 1,
	  BLOCK1_PP = 2,
	  BLOCK1_VU = 3,
    BLOCK2_CH = 4,
    BLOCK2_NS = 5,
    BLOCK2_PP = 6,
    BLOCK2_VU = 7,
    REMESH = 8,
    EUPDATE = 9,
    MAXTIMERS = 10
	};
  static const char* timerNames[] = {"Block1-ch","Block1-ns","Block1-pp","Block1-vu",
                                     "Block2-ch","Block2-ns","Block2-pp","Block2-vu",
                                     "Remesh","EquationUpdate"};
  static_assert(sizeof(timerNames)/sizeof(char*) == TIMERS::MAXTIMERS,
                "sizes dont match");
  std::array<std::chrono::high_resolution_clock::time_point,TIMERS::MAXTIMERS> timerArrayStart;
  std::array<std::chrono::high_resolution_clock::time_point,TIMERS::MAXTIMERS> timerArrayEnd;
  std::array<DENDRITE_REAL ,TIMERS::MAXTIMERS> timings{};
  timings.fill(0.0);
	std::array< PetscLogStage,TIMERS::MAXTIMERS> logs{};
  PetscLogStageRegister(timerNames[TIMERS::BLOCK1_CH], &logs[TIMERS::BLOCK1_CH]);
  PetscLogStageRegister(timerNames[TIMERS::BLOCK1_NS], &logs[TIMERS::BLOCK1_NS]);
  PetscLogStageRegister(timerNames[TIMERS::BLOCK1_PP], &logs[TIMERS::BLOCK1_PP]);
  PetscLogStageRegister(timerNames[TIMERS::BLOCK1_VU], &logs[TIMERS::BLOCK1_VU]);
  PetscLogStageRegister(timerNames[TIMERS::BLOCK2_CH], &logs[TIMERS::BLOCK2_CH]);
  PetscLogStageRegister(timerNames[TIMERS::BLOCK2_NS], &logs[TIMERS::BLOCK2_NS]);
  PetscLogStageRegister(timerNames[TIMERS::BLOCK2_PP], &logs[TIMERS::BLOCK2_PP]);
  PetscLogStageRegister(timerNames[TIMERS::BLOCK2_VU], &logs[TIMERS::BLOCK2_VU]);
  PetscLogStageRegister(timerNames[TIMERS::EUPDATE], &logs[TIMERS::EUPDATE]);
  PetscLogStageRegister(timerNames[TIMERS::REMESH], &logs[TIMERS::REMESH]);
#endif
	int rank = TALYFEMLIB::GetMPIRank ();
	CHNSInputData inputData;

	InitialConditions initialConditions (&inputData);

	if (!(inputData.ReadFromFile ())) {
		if (!rank) {
			throw TALYFEMLIB::TALYException () << "Can't read the config file \n";
		}
		MPI_Abort (MPI_COMM_WORLD, EXIT_FAILURE);
	}

	///================================ Things for periodic ================================================///
	for (int dim = 0; dim < DIM; dim++) {
		PERIODS[dim] = ((1u << m_uiMaxDepth) * inputData.mesh_def.periodicScale[dim]);
	}
	periodic::PCoord<DENDRITE_UINT , DIM>::periods(PERIODS);
	///====================================================================================================///

	///------------------------------ Command line option to restart from a checkpoint -------------------------------////
	bool resume_from_checkpoint = false;
	{
		PetscBool resume = PETSC_FALSE;
		PetscOptionsGetBool (nullptr, nullptr, "-resume_from_checkpoint", &resume, nullptr);
		resume_from_checkpoint = (resume == PETSC_TRUE);
	}
	Checkpointer checkpointer (inputData.numberOfBackups, "CheckPoint");
	/// --------------------------------------------------------------------------------------------------------------////

	inputData.solverOptionsMomentum.apply_to_petsc_options ("-momentum_");
	inputData.solverOptionsPressurePoisson.apply_to_petsc_options ("-pp_");
	inputData.solverOptionsVelocityUpdate.apply_to_petsc_options ("-vupdate_");
	inputData.solverOptionsCH.apply_to_petsc_options ("-ch_");

	TALYFEMLIB::PrintInfo ("Total number of processor = ", TALYFEMLIB::GetMPISize ());
	TALYFEMLIB::PrintInfo ("size of DendroInt ", sizeof (DendroIntL));
	TALYFEMLIB::PrintInfo ("size of PetscInt ", sizeof (PetscInt));

	const DENDRITE_UINT eleOrder = inputData.elemOrder;
	const DENDRITE_UINT levelBase = inputData.mesh_def.refine_lvl_base;
	const bool mfree = inputData.ifMatrixFree;

	TALYFEMLIB::PrintStatus ("eleOrder ", eleOrder);
	TALYFEMLIB::PrintStatus ("BaseLevel ", levelBase);
	TALYFEMLIB::PrintStatus ("Mfree ", mfree);
	TALYFEMLIB::PrintStatus ("DIM =  ", DIM);

	///-------------------------------------------------- Time construct-----------------------------------------------///
	std::vector<DENDRITE_REAL> dt (1, inputData.timeStep);
	std::vector<DENDRITE_REAL> totalT (1, inputData.TotalTime);
	TimeInfo ti (0.0, dt, totalT);
	/// --------------------------------------------------------------------------------------------------------------////

	////--------------------------------------------------- Vectors---------------------------------------------------////

	/// vectors for projection
	Vec prev1VelocitySolutionProj, prev2VelocitySolutionProj, prev3VelocitySolutionProj;
	Vec prev1PressureSolution, prev2PressureSolution, prev3PressureSolution;
	/// vectors for CH
	Vec CH_Solt_pre1, CH_Solt_pre2, CH_Solt_pre3;

	/// Vector for combined CH and VEL used for calculating energies
	Vec chnsCombined;
	/// --------------------------------------------------------------------------------------------------------------////

	///------------------------------------------Creation/loading of mesh----------------------------------------------///
	DomainExtents domainExtents (inputData.mesh_def.fullDADomain, inputData.mesh_def.physDomain);
	DA *octDA = nullptr;
	DistTREE dTree;
	SubDomain subDomain (domainExtents, resume_from_checkpoint);
	SubDomainBoundary subDomainBoundary (&subDomain, octDA, domainExtents);
	/// Add circle/sphere
	if (inputData.mesh_def.add_objects) { /// only add objects if this flag is true
		throw TALYFEMLIB::TALYException () << "Object support not yet added";
	}
	/// carving subda
	std::function<ibm::Partition(const double *, double)> functionToRetain = [&](const double *octCoords, double scale) {
			return (subDomain.functionToRetain (octCoords, scale));
	};

	if (!resume_from_checkpoint) {
		octDA = createSubDA (dTree, functionToRetain, levelBase, eleOrder);
		subDomain.finalize (octDA, dTree.getTreePartFiltered (), domainExtents);
		/// perform initial refinement
		performRefinementSubDAInitial (octDA, dTree, &subDomainBoundary, subDomain, domainExtents, inputData, &initialConditions);
	} else {/// Case with checkpointing
		/// after loading vectors are at t+dt;
		TALYFEMLIB::PrintStatus ("Loading checkpoint");
		std::vector<VecInfo> vecs;

		checkpointer.loadFromCheckpoint (octDA, dTree, functionToRetain, vecs, domainExtents, &ti, false);

		if (octDA->isActive()) {
			prev1VelocitySolutionProj = vecs[0].v;
			prev2VelocitySolutionProj = vecs[1].v;
			prev3VelocitySolutionProj = vecs[2].v;

			prev1PressureSolution = vecs[3].v;
			prev2PressureSolution = vecs[4].v;
			prev3PressureSolution = vecs[5].v;

			CH_Solt_pre1 = vecs[6].v;
			CH_Solt_pre2 = vecs[7].v;
			CH_Solt_pre3 = vecs[8].v;
		}
		TALYFEMLIB::PrintStatus ("Checkpoint Loaded");
	}
	TALYFEMLIB::PrintStatus ("total No of nodes in the mesh = ", octDA->getGlobalNodeSz ());
	/// --------------------------------------------------------------------------------------------------------------////
	subDomain.finalize (octDA, dTree.getTreePartFiltered (), domainExtents);
	const auto &treePartition = dTree.getTreePartFiltered ();
	IO::writeBoundaryElements (octDA, treePartition, "boundary", "subDA", domainExtents);
	SubDomainBoundary boundary (&subDomain, octDA, domainExtents);
	/// Boundary condition
	CHNSBoundaryConditions nsBC (&boundary, &inputData, &ti);
	CHNSBoundaryConditions chBC (&boundary, &inputData, &ti);

	///Gridfield setup
	TalyMesh<CHNSNodeData> talyMesh (octDA->getElementOrder ());

	///Setup integrands
	CHNSIntegrandsGenForm integrandsGenForm (&talyMesh.field, &inputData);

	/// used for projection linear solver
	auto nsEqProjection = new TalyEquation<MomentumEquationProjection, CHNSNodeData> (
			&talyMesh,
			octDA,
			treePartition,
			subDomain.domainExtents (),
			DIM,
			&ti,
			false,
			nullptr,
			&inputData,
			&integrandsGenForm);

	LinearSolver *momentumSolverProjection = setLinearSolver (nsEqProjection, octDA, DIM, mfree);


	/// setup for projection
	auto pressurePoissonEq =
			new TalyEquation<PressurePoissonEquation, CHNSNodeData> (&talyMesh, octDA, treePartition, subDomain
					.domainExtents (), 1, &ti, false, nullptr, &inputData, &integrandsGenForm);
	auto velocityUpdateEq =
			new TalyEquation<VelocityUpdateEquation, CHNSNodeData> (&talyMesh, octDA, treePartition, subDomain
					.domainExtents (), DIM, &ti, false, nullptr, &inputData, &integrandsGenForm);

	LinearSolver *pressurePoissonSolver = setLinearSolver (pressurePoissonEq, octDA, 1, mfree);


	LinearSolver *velocityUpdateSolver = setLinearSolver (velocityUpdateEq, octDA, DIM, mfree);


	auto chEquation = new TalyEquation<CHEquation, CHNSNodeData> (&talyMesh,
	                                                              octDA,
	                                                              treePartition,
	                                                              subDomain.domainExtents (),
	                                                              CHNSNodeData::CH_DOF,
	                                                              &ti,
	                                                              true,
	                                                              &subDomainBoundary,
	                                                              &inputData,
	                                                              &integrandsGenForm);

	std::vector<int> relativeOrder (octDA->getLocalElementSz (), inputData
			.basisRelativeOrderCH); /// integration order for CH
	chEquation->setRelativeOrder (relativeOrder.data ());


	NonlinearSolver *chSolver = setNonLinearSolver (chEquation, octDA, CHNSNodeData::CH_DOF, mfree);


	/// Analytical solution to bc for MMS
	if (inputData.caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS) {
		nsBC.setAnalyticalFunction (InitialConditions::AnalyticalSolutionNS);
		chBC.setAnalyticalFunction (InitialConditions::AnalyticalSolutionCH);
	}

	if (inputData.caseTypeForNavierStokes == CHNSInputData::JET_NS) {
		nsBC.setAnalyticalFunction (InitialConditions::AnalyticalSolutionJET);
	}

	///---------------------------------------------- Boundary condition setup----------------------------------------///
	PrintStatus ("Setting bc for NS");
	momentumSolverProjection->setDirichletBoundaryCondition ([&](const TALYFEMLIB::ZEROPTV &pos, int nodeID) -> Boundary {
			Boundary b;
			nsBC.getMomentumBoundaryCondition (b, pos);
			return b;
	});
	pressurePoissonSolver->setDirichletBoundaryCondition ([&](const TALYFEMLIB::ZEROPTV &pos, int nodeID) -> Boundary {
			Boundary b;
			nsBC.getPressureBoundaryCondition (b, pos);
			return b;
	});
	velocityUpdateSolver->setDirichletBoundaryCondition ([&](const TALYFEMLIB::ZEROPTV &pos, int nodeID) -> Boundary {
			Boundary b;
			return b;
	});

	PrintStatus ("Setting bc for CH");
	chSolver->setDirichletBoundaryCondition ([&](const TALYFEMLIB::ZEROPTV &pos, int nodeID) -> Boundary {
			Boundary b;
			chBC.getCHBoundaryCondition (b, pos);
			return b;
	});
	///----------------------------------------------------------------------------------------------------------------///


	/// --------------------------------------Setting Initial conditions-----------------------------------------------///
#if(DIM == 3)
	static const char *velocity_varname[]{"u", "v", "w"};
	static const char *pressure_varname[]{"p"};
	static const char *ch_varname[]{"phi", "mu"};
#endif
#if(DIM == 2)
	static const char *velocity_varname[]{"u", "v"};
	static const char *pressure_varname[]{"p"};
	static const char *ch_varname[]{"phi", "mu"};
#endif

	if (!resume_from_checkpoint) {
		if (octDA->isActive ()) {
			///For the case of resuming from checkpoint these are loaded in
			octDA->petscCreateVector (prev1VelocitySolutionProj, false, false, DIM);
			octDA->petscCreateVector (prev2VelocitySolutionProj, false, false, DIM);
			octDA->petscCreateVector (prev3VelocitySolutionProj, false, false, DIM);
			octDA->petscCreateVector (prev1PressureSolution, false, false, 1);
			octDA->petscCreateVector (prev2PressureSolution, false, false, 1);
			octDA->petscCreateVector (prev3PressureSolution, false, false, 1);
			initialConditions.setInitialConditionVelocityProjection (octDA, prev1VelocitySolutionProj);
			initialConditions.setInitialConditionPressure (octDA, prev1PressureSolution);

			octDA->petscCreateVector (CH_Solt_pre1, false, false, CHNSNodeData::CH_DOF);
			octDA->petscCreateVector (CH_Solt_pre2, false, false, CHNSNodeData::CH_DOF);
			octDA->petscCreateVector (CH_Solt_pre3, false, false, CHNSNodeData::CH_DOF);

			/// combined vector containing ch and vel
			//octDA->petscCreateVector(chnsCombined, false, false, CHNSNodeData::NS_DOF + CHNSNodeData::CH_DOF);

			initialConditions.setInitialConditionCH (octDA, CH_Solt_pre1);
			/// Set initial conditions of all vectors
			VecSet (prev2VelocitySolutionProj, 0.0);
			VecSet (prev2PressureSolution, 0.0);
			VecSet (prev3VelocitySolutionProj, 0.0);
			VecSet (prev3PressureSolution, 0.0);

			petscVectopvtu (octDA, treePartition, prev1VelocitySolutionProj, "initialConditions", "vel", velocity_varname,
			                subDomain
					                .domainExtents (), false, false, DIM);
			petscVectopvtu (octDA, treePartition, prev1PressureSolution, "initialConditions", "p", pressure_varname, subDomain
					.domainExtents (), false, false, 1);

			initialConditions.setInitialConditionCH (octDA, CH_Solt_pre2);
			initialConditions.setInitialConditionCH (octDA, CH_Solt_pre3);
			petscVectopvtu (octDA, treePartition, CH_Solt_pre1, "initialConditions", "ch", ch_varname,
			                subDomain.domainExtents (), false, false, CHNSNodeData::CH_DOF);
		}
	}
	///----------------------------------------------------------------------------------------------------------------///

	/// ----------------------------------------Sync vectors: For PNP and NS-------------------------------------------///
	const auto resetDAVecs = [&] {
			chEquation->setVectors ({
					                        VecInfo (PLACEHOLDER_GUESS, CHNSNodeData::CH_DOF, CHNSNodeData::PHI),
					                        VecInfo (velocityUpdateSolver->getCurrentSolution (), DIM, CHNSNodeData::VEL_X),
					                        VecInfo (prev1VelocitySolutionProj, DIM, CHNSNodeData::VEL_X_PRE1),
					                        VecInfo (prev2VelocitySolutionProj, DIM, CHNSNodeData::VEL_X_PRE2),
					                        VecInfo (CH_Solt_pre1, CHNSNodeData::CH_DOF, CHNSNodeData::PHI_PRE1),
					                        VecInfo (CH_Solt_pre2, CHNSNodeData::CH_DOF, CHNSNodeData::PHI_PRE2),
					                        VecInfo (CH_Solt_pre3, CHNSNodeData::CH_DOF, CHNSNodeData::PHI_PRE3)
			                        }, SYNC_TYPE::ALL);
			nsEqProjection->setVectors ({
					                            VecInfo (chSolver
							                                     ->getCurrentSolution (), CHNSNodeData::CH_DOF, CHNSNodeData::PHI),
					                            VecInfo (CH_Solt_pre1, CHNSNodeData::CH_DOF, CHNSNodeData::PHI_PRE1),
					                            VecInfo (CH_Solt_pre2, CHNSNodeData::CH_DOF, CHNSNodeData::PHI_PRE2),
					                            VecInfo (CH_Solt_pre3, CHNSNodeData::CH_DOF, CHNSNodeData::PHI_PRE3),
					                            VecInfo (prev1VelocitySolutionProj, DIM, CHNSNodeData::VEL_X_PRE1),
					                            VecInfo (prev1PressureSolution, 1, CHNSNodeData::PRESSURE_PRE1),
					                            VecInfo (prev2VelocitySolutionProj, DIM, CHNSNodeData::VEL_X_PRE2),
					                            VecInfo (prev2PressureSolution, 1, CHNSNodeData::PRESSURE_PRE2),
					                            VecInfo (prev3VelocitySolutionProj, DIM, CHNSNodeData::VEL_X_PRE3),
					                            VecInfo (prev3PressureSolution, 1, CHNSNodeData::PRESSURE_PRE3)
			                            }, SYNC_TYPE::ALL);
			pressurePoissonEq->setVectors ({
					                               VecInfo (chSolver
							                                        ->getCurrentSolution (), CHNSNodeData::CH_DOF, CHNSNodeData::PHI),
					                               VecInfo (CH_Solt_pre1, CHNSNodeData::CH_DOF, CHNSNodeData::PHI_PRE1),
					                               VecInfo (CH_Solt_pre2, CHNSNodeData::CH_DOF, CHNSNodeData::PHI_PRE2),
					                               VecInfo (CH_Solt_pre3, CHNSNodeData::CH_DOF, CHNSNodeData::PHI_PRE3),
					                               VecInfo (momentumSolverProjection
							                                        ->getCurrentSolution (), DIM, CHNSNodeData::VEL_X),
					                               VecInfo (pressurePoissonSolver
							                                        ->getCurrentSolution (), 1, CHNSNodeData::PRESSURE),
					                               VecInfo (prev1VelocitySolutionProj, DIM, CHNSNodeData::VEL_X_PRE1),
					                               VecInfo (prev1PressureSolution, 1, CHNSNodeData::PRESSURE_PRE1),
					                               VecInfo (prev2VelocitySolutionProj, DIM, CHNSNodeData::VEL_X_PRE2),
					                               VecInfo (prev2PressureSolution, 1, CHNSNodeData::PRESSURE_PRE2),
					                               VecInfo (prev3VelocitySolutionProj, DIM, CHNSNodeData::VEL_X_PRE3),
					                               VecInfo (prev3PressureSolution, 1, CHNSNodeData::PRESSURE_PRE3)
			                               }, SYNC_TYPE::ALL);
			velocityUpdateEq->setVectors ({
					                              VecInfo (chSolver
							                                       ->getCurrentSolution (), CHNSNodeData::CH_DOF, CHNSNodeData::PHI),
					                              VecInfo (CH_Solt_pre1, CHNSNodeData::CH_DOF, CHNSNodeData::PHI_PRE1),
					                              VecInfo (CH_Solt_pre2, CHNSNodeData::CH_DOF, CHNSNodeData::PHI_PRE2),
					                              VecInfo (CH_Solt_pre3, CHNSNodeData::CH_DOF, CHNSNodeData::PHI_PRE3),
					                              VecInfo (momentumSolverProjection
							                                       ->getCurrentSolution (), DIM, CHNSNodeData::VEL_X),
					                              VecInfo (pressurePoissonSolver
							                                       ->getCurrentSolution (), 1, CHNSNodeData::PRESSURE),
					                              VecInfo (prev1VelocitySolutionProj, DIM, CHNSNodeData::VEL_X_PRE1),
					                              VecInfo (prev1PressureSolution, 1, CHNSNodeData::PRESSURE_PRE1),
					                              VecInfo (prev2VelocitySolutionProj, DIM, CHNSNodeData::VEL_X_PRE2),
					                              VecInfo (prev2PressureSolution, 1, CHNSNodeData::PRESSURE_PRE2),
					                              VecInfo (prev3VelocitySolutionProj, DIM, CHNSNodeData::VEL_X_PRE3),
					                              VecInfo (prev3PressureSolution, 1, CHNSNodeData::PRESSURE_PRE3)
			                              }, SYNC_TYPE::ALL);
			if (octDA->isActive ()) {
				VecCopy (prev1VelocitySolutionProj, momentumSolverProjection->getCurrentSolution ());
				VecCopy (prev1PressureSolution, pressurePoissonSolver->getCurrentSolution ());
				VecCopy (CH_Solt_pre1, chSolver->getCurrentSolution ());

				SNES chEquation_snes = chSolver->snes ();
				SNESSetOptionsPrefix (chEquation_snes, "ch_");
				SNESSetFromOptions (chEquation_snes);

				KSP velocityUpdate_ksp = velocityUpdateSolver->ksp ();
				KSPSetOptionsPrefix (velocityUpdate_ksp, "vupdate_");
				KSPSetFromOptions (velocityUpdate_ksp);

				KSP pressurePoisson_ksp = pressurePoissonSolver->ksp ();
				KSPSetOptionsPrefix (pressurePoisson_ksp, "pp_");
				KSPSetFromOptions (pressurePoisson_ksp);

				KSP momentumSolverProjection_ksp = momentumSolverProjection->ksp ();
				KSPSetOptionsPrefix (momentumSolverProjection_ksp, "momentum_");
				KSPSetFromOptions (momentumSolverProjection_ksp);
			}
	};

	///Vectors are synched here
	resetDAVecs ();

	///----------------------------------------------------------------------------------------------------------------///

	DENDRITE_REAL chError[CHNSNodeData::CH_DOF];
	DENDRITE_REAL energyAndMass[EnergyAndMassCalculator::noOfTerms]; // 5 terms
	DENDRITE_REAL velocityError[DIM], pressureError; /// for projection
	std::fstream filePtErrorManufacSol;
	if (inputData.caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS) {
		/// Making a tecplot file for Error data
		std::string errorDataFileName = "Error_data.plt";
		if (!resume_from_checkpoint) {
			filePtErrorManufacSol.open (errorDataFileName, std::ios::out);
			filePtErrorManufacSol << "TITLE = \"ErrorData\"" << std::endl;
			filePtErrorManufacSol << "VARIABLES = "
			                         "\"time\"\t\"ErrorU\"\t\"ErrorV\"\t\"ErrorP\"\t"
			                         "\"ErrorPhi\"\t\"ErrorMu"
			                      << std::endl;
		} else {
			filePtErrorManufacSol.open (errorDataFileName, std::ios::out | std::ios::app);
		}
		filePtErrorManufacSol.precision (15);
	}

	/// tecplot file for energy data
	std::fstream filePtEnergy;
	std::fstream fileOutputTimeTracking;
	std::string outputTimeTrackingFileName = "outputTimeTrackingLog.csv";

    if (!rank) {
		/// Making a tecplot file for Energy data
		std::string energyDataFileName = "Energy_data.plt";
		if (!resume_from_checkpoint) {
			filePtEnergy.open (energyDataFileName, std::ios::out);
			filePtEnergy << "TITLE = \"EnergyData\"" << std::endl;
			filePtEnergy
					<< "VARIABLES = \"time\"\t\"TotalEnergy\"\t\"Interface "
					   "energy\"\t\"Bulk energy\"\t\"Kinetic energy\"\t\"Potential "
					   "energy\"\t\" Total Phi \"\t"
					<< std::endl;

			fileOutputTimeTracking.open(outputTimeTrackingFileName, std::ios::out);
			fileOutputTimeTracking << "timestepNumber" << "," << "outputTime" << "," << "outputSpanSetByUser" << "," <<
			"folderName" << std::endl;
			fileOutputTimeTracking.close();
		} else {
			filePtEnergy.open (energyDataFileName, std::ios::out | std::ios::app);
		}
		filePtEnergy.precision (15);
		fileOutputTimeTracking.precision (8);
	}

	int config_nsOrder = inputData.nsOrder;
	int advectionExtrapolationOrder = inputData.advectionExtrapolationOrder;
	KSPConvergedReason convergedReason;
	while (ti.getCurrentTime () < ti.getEndTime ()) {
		ti.print ();
		/// Solving for t
		inputData.nsOrder = config_nsOrder;
		inputData.advectionExtrapolationOrder = advectionExtrapolationOrder;
		if (inputData.timescheme == CHNSInputData::BDF && ti.getTimeStepNumber () == 0) {
			inputData.nsOrder = 1;
			inputData.advectionExtrapolationOrder = 1;
		}
		dt[0] = ti.getCurrentStep ();

		///------------------------------------------------- Solve-------------------------------------------------------///
		if (octDA->isActive ()) {
			if (inputData.solveStrategy == CHNSInputData::SOLENOIDAL_VEL_CH){ // Project velocity after every NS solve

				PrintLogStream (std::cout, "",
				                "/================================ Solving Cahn-Hilliard equations ============================/");
        if (inputData.ifCHSolve) {
#ifdef  PROFILING
        PetscLogStagePush(logs[TIMERS::BLOCK1_CH]);
        START_TIMER(TIMERS::BLOCK1_CH);
#endif
					chSolver->solve ();
				}
#ifdef  PROFILING
        END_TIMER(TIMERS::BLOCK1_CH);
        PetscLogStagePop();
#endif
				PrintLogStream (std::cout, "",
				                "/============================== Done solving Cahn-Hilliard equations ========================/");

				PrintLogStream (std::cout, "",
				                "/=============================== Solving momentum equations =================================/");

        if (inputData.ifNSSolve) {
#ifdef  PROFILING
          PetscLogStagePush(logs[TIMERS::BLOCK1_NS]);
          START_TIMER(TIMERS::BLOCK1_NS);
#endif
					PrintLogStream (std::cout, "",
					                "/=========================== Solving Velocity prediction equations =============================/");
					momentumSolverProjection->solve ();
#ifdef  PROFILING
          END_TIMER(TIMERS::BLOCK1_NS);
          PetscLogStagePop();
#endif
          KSPGetConvergedReason (momentumSolverProjection->ksp (), &convergedReason);
					if (convergedReason < 0) {
						throw std::runtime_error ("Momentum solver diverged");
					}
					PrintLogStream (std::cout, "",
					                "/========================= Done Solving Velocity prediction equations =========================/");
					PrintLogStream (std::cout, "",
					                "/=========================== Solving Pressure-Poisson equations ==========================/");
#ifdef  PROFILING
          PetscLogStagePush(logs[TIMERS::BLOCK1_PP]);
          START_TIMER(TIMERS::BLOCK1_PP);
#endif
					pressurePoissonSolver->solve ();
#ifdef  PROFILING
          END_TIMER(TIMERS::BLOCK1_PP);
          PetscLogStagePop();
#endif
					KSPGetConvergedReason (pressurePoissonSolver->ksp (), &convergedReason);
					if (convergedReason < 0) {
						throw std::runtime_error ("Pressure solver diverged");
					}
					PrintLogStream (std::cout, "",
					                "/======================= Done solving Pressure-Poisson equations =========================/");

					PrintLogStream (std::cout, "",
					                "/=================================== Updating velocities =================================/");
#ifdef  PROFILING
          PetscLogStagePush(logs[TIMERS::BLOCK1_VU]);
          START_TIMER(TIMERS::BLOCK1_VU);
#endif
					velocityUpdateSolver->solve ();
#ifdef  PROFILING
          END_TIMER(TIMERS::BLOCK1_VU);
          PetscLogStagePop();
#endif
					KSPGetConvergedReason (velocityUpdateSolver->ksp (), &convergedReason);
					if (convergedReason < 0) {
						throw std::runtime_error ("Velocity solver diverged");
					}
					PrintLogStream (std::cout, "",
					                "/=============================== Done updating velocities =================================/");
				}

				PrintLogStream (std::cout, "",
				                "/============================= Done solving momentum equations ==============================/");


				PrintLogStream (std::cout, "",
				                "/********************************************************************************************/");
				PrintLogStream (std::cout, "",
				                "/*************************************** SECOND BLOCK ***************************************/");
				PrintLogStream (std::cout, "",
				                "/********************************************************************************************/");

				PrintLogStream (std::cout, "",
				                "/================================ Solving Cahn-Hilliard equations ============================/");
				if (inputData.ifCHSolve) {
#ifdef  PROFILING
          PetscLogStagePush(logs[TIMERS::BLOCK2_CH]);
          START_TIMER(TIMERS::BLOCK2_CH);
#endif
					chSolver->solve ();
#ifdef  PROFILING
          END_TIMER(TIMERS::BLOCK2_CH);
          PetscLogStagePop();
#endif
				}
				PrintLogStream (std::cout, "",
				                "/============================== Done solving Cahn-Hilliard equations ========================/");

				PrintLogStream (std::cout, "",
				                "/=============================== Solving momentum equations =================================/");
				if (inputData.ifNSSolve) {
          PrintLogStream (std::cout, "",
                          "/=========================== Solving Velocity prediction equations =============================/");
#ifdef  PROFILING
          PetscLogStagePush(logs[TIMERS::BLOCK2_NS]);
          START_TIMER(TIMERS::BLOCK2_NS);
#endif
					momentumSolverProjection->solve ();
#ifdef  PROFILING
          END_TIMER(TIMERS::BLOCK2_NS);
          PetscLogStagePop();
#endif
          KSPGetConvergedReason (momentumSolverProjection->ksp (), &convergedReason);
					if (convergedReason < 0) {
						throw std::runtime_error ("Momentum solver diverged");
					}
					PrintLogStream (std::cout, "",
					                "/========================= Done Solving Velocity prediction equations =========================/");
					PrintLogStream (std::cout, "",
					                "/=========================== Solving Pressure-Poisson equations ==========================/");
#ifdef  PROFILING
          PetscLogStagePush(logs[TIMERS::BLOCK2_PP]);
          START_TIMER(BLOCK2_PP);
#endif
					pressurePoissonSolver->solve ();
#ifdef  PROFILING
					END_TIMER(BLOCK2_PP);
          PetscLogStagePop();
#endif
					KSPGetConvergedReason (pressurePoissonSolver->ksp (), &convergedReason);
					if (convergedReason < 0) {
						throw std::runtime_error ("Pressure solver diverged");
					}
					PrintLogStream (std::cout, "",
					                "/======================= Done solving Pressure-Poisson equations =========================/");

					PrintLogStream (std::cout, "",
					                "/=================================== Updating velocities =================================/");
#ifdef  PROFILING
          PetscLogStagePush(logs[TIMERS::BLOCK2_VU]);
          START_TIMER(TIMERS::BLOCK2_VU);
#endif
					velocityUpdateSolver->solve ();
#ifdef  PROFILING
          END_TIMER(TIMERS::BLOCK2_VU);
          PetscLogStagePop();
#endif
					KSPGetConvergedReason (velocityUpdateSolver->ksp (), &convergedReason);
					if (convergedReason < 0) {
						throw std::runtime_error ("Velocity solver diverged");
					}
					PrintLogStream (std::cout, "",
					                "/=============================== Done updating velocities =================================/");
				}

				PrintLogStream (std::cout, "",
				                "/============================= Done solving momentum equations ==============================/");
			}
			else if (inputData.solveStrategy == CHNSInputData::NON_SOLENOIDAL_VEL_CH) {
				///------------------------------------------------------ CH ------------------------------------------------///
				PrintLogStream (std::cout, "",
				                "/================================ Solving Cahn-Hilliard equations ============================/");
				if (inputData.ifCHSolve) {
					chSolver->solve ();
				}
				PrintLogStream (std::cout, "",
				                "/============================== Done solving Cahn-Hilliard equations ========================/");
				///----------------------------------------------------------------------------------------------------------///

				///----------------------------------------------  Vel pred  ------------------------------------------------///
				PrintLogStream (std::cout, "",
				                "/=============================== Solving momentum equations =================================/");
				if (inputData.ifNSSolve) {
					PrintLogStream (std::cout, "",
					                "/=========================== Solving Velocity prediction equations =============================/");
					momentumSolverProjection->solve ();
					KSPGetConvergedReason (momentumSolverProjection->ksp (), &convergedReason);
					if (convergedReason < 0) {
						throw std::runtime_error ("Momentum solver diverged");
					}
					PrintLogStream (std::cout, "",
					                "/========================= Done Solving Velocity prediction equations =========================/");
				}
				PrintLogStream (std::cout, "",
				                "/============================= Done solving momentum equations ==============================/");
				///----------------------------------------------------------------------------------------------------------///


				PrintLogStream (std::cout, "",
				                "/********************************************************************************************/");
				PrintLogStream (std::cout, "",
				                "/*************************************** SECOND BLOCK ***************************************/");
				PrintLogStream (std::cout, "",
				                "/********************************************************************************************/");


				///------------------------------------------------------ CH ------------------------------------------------///
				PrintLogStream (std::cout, "",
				                "/================================ Solving Cahn-Hilliard equations ============================/");
				if (inputData.ifCHSolve) {
					chSolver->solve ();
				}
				PrintLogStream (std::cout, "",
				                "/============================== Done solving Cahn-Hilliard equations ========================/");
				///----------------------------------------------------------------------------------------------------------///

				///-------------------------------------  Vel pred + Projection  --------------------------------------------///
				if (inputData.ifNSSolve) {
					PrintLogStream (std::cout, "",
					                "/=========================== Solving Velocity prediction equations =============================/");
					momentumSolverProjection->solve ();
					KSPGetConvergedReason (momentumSolverProjection->ksp (), &convergedReason);
					if (convergedReason < 0) {
						throw std::runtime_error ("Momentum solver diverged");
					}
					PrintLogStream (std::cout, "",
					                "/========================= Done Solving Velocity prediction equations =========================/");

					PrintLogStream (std::cout, "",
					                "/=========================== Solving Pressure-Poisson equations ==========================/");
					pressurePoissonSolver->solve ();
					KSPGetConvergedReason (pressurePoissonSolver->ksp (), &convergedReason);
					if (convergedReason < 0) {
						throw std::runtime_error ("Pressure solver diverged");
					}
					PrintLogStream (std::cout, "",
					                "/======================= Done solving Pressure-Poisson equations =========================/");

					PrintLogStream (std::cout, "",
					                "/=================================== Updating velocities =================================/");
					velocityUpdateSolver->solve ();
					KSPGetConvergedReason (velocityUpdateSolver->ksp (), &convergedReason);
					if (convergedReason < 0) {
						throw std::runtime_error ("Velocity solver diverged");
					}
					PrintLogStream (std::cout, "",
					                "/=============================== Done updating velocities =================================/");
				}
				///----------------------------------------------------------------------------------------------------------///

			}
			else {
				throw TALYFEMLIB::TALYException() << "incorrect solve strategy selected" ;
			}

			/// solve complete for t
			/// ----------------------------------------------file writing---------------------------------------------------///
			if (ti.getTimeStepNumber () % inputData.OutputSpan == 0) {
				/// writing solution for t+dt
				char folderName[PATH_MAX], fileNameCH[PATH_MAX], fileNameVelocity[PATH_MAX],
						fileNamePressure[PATH_MAX];
				std::snprintf (folderName, sizeof (folderName), "%s_%05d", "results", ti.getTimeStepNumber ());
				std::snprintf (fileNameVelocity, sizeof (fileNameVelocity), "%s_%05d", "vel", ti.getTimeStepNumber ());
				std::snprintf (fileNamePressure, sizeof (fileNamePressure), "%s_%05d", "p", ti.getTimeStepNumber ());
				petscVectopvtu (octDA, treePartition, velocityUpdateSolver->getCurrentSolution (), folderName, fileNameVelocity,
				                velocity_varname, subDomain.domainExtents (), false, false, DIM);
				petscVectopvtu (octDA, treePartition, pressurePoissonSolver
						                ->getCurrentSolution (), folderName, fileNamePressure,
				                pressure_varname, subDomain.domainExtents (), false, false, 1);

				std::snprintf (fileNameCH, sizeof (fileNameCH), "%s_%05d", "ch", ti.getTimeStepNumber ());
				petscVectopvtu (octDA, treePartition, chSolver->getCurrentSolution (), folderName, fileNameCH, ch_varname,
				                subDomain.domainExtents (), false, false, CHNSNodeData::CH_DOF);
				if(rank == 0) {
					fileOutputTimeTracking.open(outputTimeTrackingFileName.c_str(), std::ios::app);
					fileOutputTimeTracking << ti.getTimeStepNumber() << "," << ti.getCurrentTime() << ","
					<< inputData.OutputSpan << "," << folderName << "\n";
					fileOutputTimeTracking.close();
				}
			}
			///--------------------------------------------------------------------------------------------------------------///


			///--------------------------------------PostProcessing stuff at time = t----------------------------------------///
			if (inputData.caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS) {
				{
					VecInfo v (prev1VelocitySolutionProj, DIM, CHNSNodeData::VEL_X);
					AnalyticTS NSAnalytic (octDA, treePartition, v, InitialConditions::AnalyticalSolutionNS,
					                       subDomain.domainExtents (), ti
							                       .getCurrentTime (), ti.getCurrentStep ());
					TALYFEMLIB::PrintStatus ("Velocity error = ");
					NSAnalytic.getL2error ();
					NSAnalytic.getL2error (velocityError);
				}
				{
					VecInfo v (prev1PressureSolution, 1, CHNSNodeData::PRESSURE);
					AnalyticTS NSAnalytic (octDA, treePartition, v, InitialConditions::AnalyticalSolutionNS,
					                       subDomain.domainExtents (), ti
							                       .getCurrentTime (), ti.getCurrentStep ());
					TALYFEMLIB::PrintStatus ("Pressure error = ");
					NSAnalytic.getL2error ();
					NSAnalytic.getL2error (&pressureError);
				}
				{
					VecInfo v (CH_Solt_pre1, CHNSNodeData::CH_DOF, 0);
					AnalyticTS CHAnalytic (octDA, treePartition, v, InitialConditions::AnalyticalSolutionCH,
					                       subDomain.domainExtents (), ti
							                       .getCurrentTime (), ti.getCurrentStep ());
					TALYFEMLIB::PrintStatus ("CH error = ");
					CHAnalytic.getL2error ();
					CHAnalytic.getL2error (chError);
				}
				if (not(rank)) {
					filePtErrorManufacSol << ti.getCurrentTime () << " " << velocityError[0] << " " << velocityError[1] << " "
					                      << " "
					                      << pressureError << " " << chError[0] << " " << chError[1] << " " << "\n";
				}
			}
			/// --------------------------------- Energy and Mass Calculation -----------------------------------------------///
			octDA->petscCreateVector (chnsCombined, false, false, CHNSNodeData::NS_DOF + CHNSNodeData::CH_DOF);
			{
				VecInfo vel (prev1VelocitySolutionProj, DIM, CHNSNodeData::VEL_X);
				VecInfo p (prev1PressureSolution, 1, CHNSNodeData::PRESSURE);
				VecInfo ch (CH_Solt_pre1, CHNSNodeData::CH_DOF, 0);
				VecInfo combinedVec (chnsCombined, CHNSNodeData::NS_DOF + CHNSNodeData::CH_DOF, 0);
				std::vector<VecInfo> combined = {vel, p, ch};
				recombineVec (octDA, combined, combinedVec, true);
				EnergyAndMassCalculator calculator (octDA, treePartition, combinedVec, subDomain.domainExtents (),
				                                    ti.getCurrentTime (), ti.getCurrentStep (), chEquation->equation ()
						                                    ->freeEnergy,
				                                    &inputData);
				TALYFEMLIB::PrintStatus ("Energy and Mass = ");
				calculator.getEnergyAndMass ();
				calculator.getEnergyAndMass (energyAndMass);
				DENDRITE_REAL totalEnergy = energyAndMass[0] + energyAndMass[1] + energyAndMass[2] + energyAndMass[3];
				///***************** calculate and print energy variables *********************************************///
				if (rank == 0) {
					filePtEnergy << std::scientific << std::setprecision (18)
					             << ti.getCurrentTime () << "\t" << totalEnergy << "\t " << energyAndMass[1]
					             << "\t" << energyAndMass[0] << "\t" << energyAndMass[2] << "\t"
					             << energyAndMass[3] << "\t" << energyAndMass[4] << std::endl;
				}
				/// ****************************************************************************************************///
			}
			VecDestroy (&chnsCombined);

		  /// -----------------------------------Plot over line over the central Line---------------------------------///
		  // The below part produces a plot over the mid line of x.
      if((inputData.doPlotOverLine) and ((ti.getTimeStepNumber() % inputData.plotOverLineFrequency) == 0)){
        VecInfo ch (CH_Solt_pre1, CHNSNodeData::CH_DOF, 0);
        PlotOverLine plotOverLine(octDA,treePartition,ch,domainExtents);
        plotOverLine.printToFile(ti,inputData.plotOverLineAsciiWrite);
      }
			///--------------------------------------PostProcessing stuff completed----------------------------------------///
		}
		/// Increment t
		ti.increment ();/// current t = t+dt; /// Global update

		///--------------------------------------------Vector updates ---------------------------------------------------///
		if (octDA->isActive ()) {
			VecCopy (prev2VelocitySolutionProj, prev3VelocitySolutionProj);
			VecCopy (prev1VelocitySolutionProj, prev2VelocitySolutionProj);
			VecCopy (velocityUpdateSolver->getCurrentSolution (), prev1VelocitySolutionProj);

			VecCopy (prev2PressureSolution, prev3PressureSolution);
			VecCopy (prev1PressureSolution, prev2PressureSolution);
			VecCopy (pressurePoissonSolver->getCurrentSolution (), prev1PressureSolution);

			VecCopy (CH_Solt_pre2, CH_Solt_pre3);
			VecCopy (CH_Solt_pre1, CH_Solt_pre2);
			VecCopy (chSolver->getCurrentSolution (), CH_Solt_pre1);
		}
		///--------------------------------------------------------------------------------------------------------------///
		//nsParams.updateNSOrder(inputData.nsOrder);

		////============================================AMR Stuff=======================================================///
		///--------------------------------------- perform refinement---------------------------------------------------///
#ifdef  PROFILING
    PetscLogStagePush(logs[TIMERS::REMESH]);
		START_TIMER(TIMERS::REMESH);
#endif

		std::vector<VecInfo> vectorsForUpdate (9);
		vectorsForUpdate[0] = VecInfo (prev1VelocitySolutionProj, DIM, CHNSNodeData::VEL_X_PRE1);
		vectorsForUpdate[1] = VecInfo (prev2VelocitySolutionProj, DIM, CHNSNodeData::VEL_X_PRE2);
		vectorsForUpdate[2] = VecInfo (prev3VelocitySolutionProj, DIM, CHNSNodeData::VEL_X_PRE3);

		vectorsForUpdate[3] = VecInfo (prev1PressureSolution, 1, CHNSNodeData::PRESSURE_PRE1);
		vectorsForUpdate[4] = VecInfo (prev2PressureSolution, 1, CHNSNodeData::PRESSURE_PRE2);
		vectorsForUpdate[5] = VecInfo (prev3PressureSolution, 1, CHNSNodeData::PRESSURE_PRE3);

		vectorsForUpdate[6] = VecInfo (CH_Solt_pre1, CHNSNodeData::CH_DOF, CHNSNodeData::PHI_PRE1);
		vectorsForUpdate[7] = VecInfo (CH_Solt_pre2, CHNSNodeData::CH_DOF, CHNSNodeData::PHI_PRE2);
		vectorsForUpdate[8] = VecInfo (CH_Solt_pre3, CHNSNodeData::CH_DOF, CHNSNodeData::PHI_PRE3);

		bool meshAdaptedDuringRefine = false;
		meshAdaptedDuringRefine = performMeshAdaptionSubDAIntVecs (octDA, dTree, vectorsForUpdate, &subDomainBoundary, subDomain, domainExtents,
		                                 inputData, true);

		///----------------------------------------------- coarsening --------------------------------------------------///

		bool meshAdaptedDuringCoarsen = false;
		if ((ti.getTimeStepNumber() % inputData.coarseningFrequency) == 0) {
			meshAdaptedDuringCoarsen = performMeshAdaptionSubDAIntVecs (octDA, dTree, vectorsForUpdate, &subDomainBoundary, subDomain, domainExtents,
			                                                            inputData, false);
		}

		///--------------------------------------------Vector assignments-----------------------------------------------///
		prev1VelocitySolutionProj = vectorsForUpdate[0].v;
		prev2VelocitySolutionProj = vectorsForUpdate[1].v;
		prev3VelocitySolutionProj = vectorsForUpdate[2].v;

		prev1PressureSolution = vectorsForUpdate[3].v;
		prev2PressureSolution = vectorsForUpdate[4].v;
		prev3PressureSolution = vectorsForUpdate[5].v;

		CH_Solt_pre1 = vectorsForUpdate[6].v;
		CH_Solt_pre2 = vectorsForUpdate[7].v;
		CH_Solt_pre3 = vectorsForUpdate[8].v;
#ifdef  PROFILING
    END_TIMER(TIMERS::REMESH);
    PetscLogStagePop();
#endif

		TALYFEMLIB::PrintInfo ("Mesh adaption completed: total No of nodes in the mesh = ", octDA->getGlobalNodeSz ());
		///---------------------------------------------Solver updates---------------------------------------------------///
		PrintStatus ("Setting equation objects to the new mesh");

		bool meshAdapted = meshAdaptedDuringRefine or meshAdaptedDuringCoarsen; /// Did mesh changed
#ifdef  PROFILING
    PetscLogStagePush(logs[TIMERS::EUPDATE]);
    START_TIMER(TIMERS::EUPDATE);
#endif
		if(meshAdapted) { // Do this only if the mesh changed
      updateEquationAndSolver(&talyMesh, octDA, treePartition, nsEqProjection, domainExtents, momentumSolverProjection,
                              &ti, nullptr, DIM, &inputData,
                              &integrandsGenForm);



      updateEquationAndSolver(&talyMesh, octDA, treePartition, pressurePoissonEq, domainExtents, pressurePoissonSolver,
                              &ti,
                              nullptr, 1, &inputData, &integrandsGenForm);

      updateEquationAndSolver(&talyMesh, octDA, treePartition, velocityUpdateEq, domainExtents,
                              velocityUpdateSolver, &ti, nullptr, DIM, &inputData, &integrandsGenForm);

      updateEquationAndSolver(&talyMesh, octDA, treePartition, chEquation, domainExtents, chSolver, &ti, &boundary,
                              CHNSNodeData::CH_DOF, &inputData, &integrandsGenForm);
      relativeOrder.resize(octDA->getLocalElementSz());
      std::fill(relativeOrder.begin(), relativeOrder.end(), inputData
        .basisRelativeOrderCH); /// integration order for CH
      chEquation->setRelativeOrder(relativeOrder.data());

      ///Vectors are synched here
      resetDAVecs();


    }
#ifdef  PROFILING
    END_TIMER(TIMERS::EUPDATE);
    PetscLogStagePop();
#endif
		/// Things are synced for t + dt (next time step)

		if (ti.getTimeStepNumber () % inputData.checkpointFrequency == 0) {
			if (octDA->isActive ()) {
				std::vector<VecInfo> vecs;
				vecs.emplace_back (VecInfo (prev1VelocitySolutionProj, DIM, CHNSNodeData::VEL_X_PRE1));
				vecs.emplace_back (VecInfo (prev2VelocitySolutionProj, DIM, CHNSNodeData::VEL_X_PRE2));
				vecs.emplace_back (VecInfo (prev3VelocitySolutionProj, DIM, CHNSNodeData::VEL_X_PRE3));
				vecs.emplace_back (VecInfo (prev1PressureSolution, 1, CHNSNodeData::PRESSURE_PRE1));
				vecs.emplace_back (VecInfo (prev2PressureSolution, 1, CHNSNodeData::PRESSURE_PRE2));
				vecs.emplace_back (VecInfo (prev3PressureSolution, 1, CHNSNodeData::PRESSURE_PRE3));

				vecs.emplace_back (VecInfo (CH_Solt_pre1, CHNSNodeData::CH_DOF, CHNSNodeData::PHI_PRE1));
				vecs.emplace_back (VecInfo (CH_Solt_pre2, CHNSNodeData::CH_DOF, CHNSNodeData::PHI_PRE2));
				vecs.emplace_back (VecInfo (CH_Solt_pre3, CHNSNodeData::CH_DOF, CHNSNodeData::PHI_PRE3));
				checkpointer.storeCheckpoint (octDA, &dTree, vecs, domainExtents, &ti);
				/// If you load this checkpoint you are all set for t + dt solve.
			}
		}
	}
	///-----------------------------------------------Solve loop done -------------------------------------------------///
#ifdef  PROFILING
  TALYFEMLIB::PrintInfo("Timings Info");
	if(not rank) {
    for (int i = 0; i < TIMERS::MAXTIMERS; i++) {
      std::cout << "[TIMERS] " << std::left << std::setw(20) << timerNames[i] << ":" << timings[i] << " s\n";
    }
    std::cout << "\n\n";
  }
	MPI_Barrier(MPI_COMM_WORLD);
#endif
	delete nsEqProjection;
	delete momentumSolverProjection;

	delete pressurePoissonEq;
	delete pressurePoissonSolver;

	delete velocityUpdateSolver;
	delete velocityUpdateEq;

	delete chEquation;
	delete chSolver;
	if (octDA->isActive ()) {
		VecDestroy (&CH_Solt_pre1);
		VecDestroy (&CH_Solt_pre2);
		VecDestroy (&CH_Solt_pre3);

		VecDestroy (&prev1PressureSolution);
		VecDestroy (&prev2PressureSolution);
		VecDestroy (&prev3PressureSolution);
		VecDestroy (&prev1VelocitySolutionProj);
		VecDestroy (&prev2VelocitySolutionProj);
		VecDestroy (&prev3VelocitySolutionProj);
	}
	dendrite_finalize (octDA);
}

