import os
import glob
path = os.path.dirname(os.path.realpath(__file__))
folders = []
mpi = 0
for i in sorted(os.listdir(path)):
    if os.path.isdir(os.path.join(path, i)) and 'results_' in i and 'dendro' not in i:
        folders.append(i)


with open(path + "/vel.visit", 'w+') as visit_file:
    visit_file.write('!NBLOCKS ' + str(1) + '\n')
    for folder in folders:
        visit_file.write(folder + '/vel_' + folder[-5:] + '.pvtu\n')

with open(path + "/p.visit", 'w+') as visit_file:
    visit_file.write('!NBLOCKS ' + str(1) + '\n')
    for folder in folders:
        visit_file.write(folder + '/p_' + folder[-5:] + '.pvtu\n')

with open(path + "/ch.visit", 'w+') as visit_file:
    visit_file.write('!NBLOCKS ' + str(1) + '\n')
    for folder in folders:
        visit_file.write(folder + '/ch_' + folder[-5:] + '.pvtu\n')