function [A] = readBinary(fileName,DIM)
% readBinary reads the binary file dumped by kT
% filename = filename of binary
% DIM = dimension of simulation 2 for 2D and 3 for 3D

 fileID = fopen(fileName,'r');
 [vals, numRead] = fread(fileID, 1E6, '*double');
 time = vals(1)
 A = vals(2:end);
 numEntries = round((numRead - 1)/(DIM + 1));
 A = vals(2:end);
 A = reshape(A,[4,numEntries]);
 A = A';
end

