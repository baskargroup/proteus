## This code is used to output phi =0 isocontours at each timestep in a folder as a series of .pvtp files
## useful for making a video
## file_name.pvd: name of the pvd file which contains contours this is the output (if you want to start from the middle)
## Usage mpirun -np < no of proc > pvbatch isocontourTimeSeries.py file.pvd file_index_to_start_from
## Usage of Frontera and Stampede2: Load paraview-osmesa module for this then
## On Frontera: module load swr ospray qt5 paraview-osmesa
##        ibrun -np < no of proc > swr pvbatch isocontour_video.py file.pvd file_name.pvd
import sys
from paraview.simple import *
print("Here");
fileName = sys.argv[1]
pvdFile = PVDReader(FileName=[fileName])
pvdFile.CellArrays = ['']
pvdFile.PointArrays = ['phi']
# create a new 'Contour'
contour = Contour(Input=pvdFile)
print("Here");
contour.ContourBy = ['phi']
contour.Isosurfaces = [0.0]

print("after Here");
SaveData(sys.argv[2],proxy=contour,WriteTimeSteps=1,DataMode='Binary',EncodeAppendedData=1,CompressorType='LZ4')
