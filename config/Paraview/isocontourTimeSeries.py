## This code is used to output phi =0 isocontours at each timestep and it also prints the bounds of the isocontour in
## a file
## file_index_to_start_from: index of timestep to start from (if you want to start from the middle)
## Usage mpirun -np < no of proc > pvbatch isocontourTimeSeries.py file.pvd file_index_to_start_from
## Usage of Frontera and Stampede2: Load paraview-osmesa module for this then
##        ibrun -np < no of proc > swr pvbatch isocontourTimeSeries.py file.pvd file_index_to_start_from

from paraview.simple import *
import sys

#view = GetRenderView()
print('Here');
FileName = sys.argv[1]
start = 0
if(len(sys.argv) > 2):
    start = int(sys.argv[2])
#pvtuFile=XMLPartitionedUnstructuredGridReader(FileName = [FileName])
pvtuFile=PVDReader(FileName = [FileName])
timestamps = pvtuFile.TimestepValues
del pvtuFile
original_stdout = sys.stdout
boundFile = "boundFiles.txt"
#print('Here');
for i in range(start,len(timestamps)):
    current=timestamps[i]
    filename = "results_"+format(int(current),'05d')+ "/ch_" + format(int(current),'05d') + ".pvtu"
    pvtuFile = XMLPartitionedUnstructuredGridReader(FileName=[filename])
    pvtuFile.CellArrayStatus= []
    pvtuFile.PointArrayStatus = ['phi']
    contour1 = Contour(Input=pvtuFile)
    contour1.ContourBy = ['phi']
    contour1.Isosurfaces = [0.0]
#
    print('Here slice', filename);
    SaveData('phi_isocontour_'+ format(int(current),'05d')+'.pvd',
         proxy=contour1,
#         Writetimestepsasfileseries=1,
         DataMode="Binary")

    dataInfo = contour1.GetDataInformation()
    bounds = dataInfo.DataInformation.GetBounds()
    with open(boundFile,"a") as f:
        sys.stdout = f
        print(current, bounds)
        sys.stdout = original_stdout
        f.close()

    print(current,bounds)
    Delete(contour1)
    Delete(pvtuFile)
    

#contour1

