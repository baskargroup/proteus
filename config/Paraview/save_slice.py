import os
import sys
# trace generated using paraview version 5.7.0
#
# To ensure correct image size when batch processing, please search
# for and uncomment the line `# renderView*.ViewSize = [*,*]`

# import the simple module from the paraview
from paraview.simple import *
# disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'PVD Reader'
try:
  FindSource('PVDReader1')
except NameError:
  pVDReader1 = None
else:
  pVDReader1 = FindSource('PVDReader1')
print(pVDReader1)

case_no = int(sys.argv[1])
start_ts = 0
if len(sys.argv) >= 3:
  start_ts = int(sys.argv[2])
print(case_no, start_ts)
# case_no = 0
x_range = [5.0]
y_range = [5.0]
z_range = [5.0]
current_dir = os.getcwd()
pvd_files = [os.path.join(current_dir, "paraview_p.pvd"),
             os.path.join(current_dir, "paraview_ns.pvd")]
slice_dirs = [os.path.join(current_dir, "slice-data"),
              os.path.join(current_dir, "slice-data")]

case = {"pvd_file": pvd_files[case_no],
        "slice_dir": slice_dirs[case_no],
        "x_range": x_range,
        "y_range": y_range,
        "z_range": z_range, }

ns_ht = "ns"
if "paraview_p" in case["pvd_file"]:
  ns_ht = "p"

print(ns_ht)
if pVDReader1 is None:
  pVDReader1 = PVDReader(FileName=case["pvd_file"])
  pVDReader1.CellArrays = ['mpi_rank', 'cell_level']
  if ns_ht == "ns":
    pVDReader1.PointArrays = ['u', 'v', 'w']
  else:
    pVDReader1.PointArrays = ['p']
else:
  pVDReader1 = FindSource('PVDReader1')

# get animation scene
animationScene1 = GetAnimationScene()

# get the time-keeper
timeKeeper1 = GetTimeKeeper()

# update animation scene based on data timesteps
# animationScene1.UpdateAnimationUsingDataTimeSteps()


#clip1 = Clip(Input=pVDReader1)
#clip1.ClipType = 'Box'
#clip1.ClipType.Position = [3.99, -0.01, 6.99]
#clip1.ClipType.Length = [8.02, 1.02, 2.02]
#clip1 = FindSource('Clip1')

x_slices, y_slices, z_slices = [], [], []

for i, x in enumerate(case["x_range"]):
  print("Generate x_slice at {x}".format(x=x))
  x_slices.append(Slice(Input=pVDReader1))
  x_slices[i].SliceType = 'Plane'
  x_slices[i].SliceOffsetValues = [0.0]
  x_slices[i].SliceType.Origin = [x, 5.0, 1.0]
  x_slices[i].SliceType.Normal = [1.0, 0.0, 0.0]
groupXSlice = GroupDatasets(Input=x_slices)

for i, y in enumerate(case["y_range"]):
  print("Generate y_slice at {y}".format(y=y))
  y_slices.append(Slice(Input=pVDReader1))
  y_slices[i].SliceType = 'Plane'
  y_slices[i].SliceOffsetValues = [0.0]
  y_slices[i].SliceType.Origin = [5.0, y, 1.0]
  y_slices[i].SliceType.Normal = [0.0, 1.0, 0.0]
groupYSlice = GroupDatasets(Input=y_slices)

for i, z in enumerate(case["z_range"]):
  print("Generate z_slice at {z}".format(z=z))
  z_slices.append(Slice(Input=pVDReader1))
  z_slices[i].SliceType = 'Plane'
  z_slices[i].SliceOffsetValues = [0.0]
  z_slices[i].SliceType.Origin = [5.0, 5.0, z]
  z_slices[i].SliceType.Normal = [0.0, 0.0, 1.0]
groupZSlice = GroupDatasets(Input=z_slices)

print("Save x slice")
SaveData(os.path.join(case["slice_dir"], "x_slices", "{type}_x_slice.vtm".format(type=ns_ht)),
         proxy=groupXSlice,
         Writetimestepsasfileseries=1,
         Lasttimestep=-1,
         Timestepstride=1,
         DataMode="Binary",
         CompressorType="LZ4")
print("Save y slice")
SaveData(os.path.join(case["slice_dir"], "y_slices", "{type}_y_slice.vtm".format(type=ns_ht)),
         proxy=groupYSlice,
         Firsttimestep=start_ts,
         Writetimestepsasfileseries=1,
         Lasttimestep=-1,
         Timestepstride=1,
         DataMode="Binary",
         CompressorType="LZ4")
print("Save z slice")
SaveData(os.path.join(case["slice_dir"], "z_slices", "{type}_z_slice.vtm".format(type=ns_ht)),
        proxy=groupZSlice,
        Firsttimestep=start_ts,
        Writetimestepsasfileseries=1,
        Lasttimestep=-1,
        Timestepstride=1,
        DataMode="Binary",
        CompressorType="LZ4")
# SaveData(os.path.join(case["slice_dir"], "x_slices", "{type}_x_slice_{i}.vtm".format(type=ns_ht, i=i)), proxy=groupXSlice, DataMode='Binary',
#          CompressorType='LZ4')
# SaveData(os.path.join(case["slice_dir"], "y_slices", "{type}_y_slice_{i}.vtm".format(type=ns_ht, i=i)), proxy=groupYSlice, DataMode='Binary',
#          CompressorType='LZ4')
# SaveData(os.path.join(case["slice_dir"], "z_slices", "{type}_z_slice_{i}.vtm".format(type=ns_ht, i=i)), proxy=groupZSlice, DataMode='Binary',
#          CompressorType='LZ4')

