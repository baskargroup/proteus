## Compiling PETSc

PETSc has its own build system which is a combination of Python and CMake. To build PETSc, you'll run `./configure` with some options, and then run `make` to build the PETSc library.

PETSc is built on top of a lot of other libraries. Thankfully, the PETSc build system has options to automatically download and install these libraries if desired. This guide recommends the following options for compiling PETSc:

```bash
./configure  --download-metis --download-parmetis --download-scalapack --download-fblaslapack --download-mumps -with-debugging=0 COPTFLAGS='-O3 -march=native -mtune=native' CXXOPTFLAGS='-O3 -march=native -mtune=native' FOPTFLAGS='-O3 -march=native -mtune=native' --download-mpich
```

Explanations for each argument:

* `--download-mpich`: download [MPICH](https://www.mpich.org/). MPI is a popular standard in scientific computing for writing parallel programs, and MPICH is one implementation of the standard. MPICH is maintained by the same people who maintain PETSc.

* `--download-metis` and `--download-parmetis`: download [METIS](http://glaros.dtc.umn.edu/gkhome/metis/metis/overview), a graph partitioning library, as well as its parallel version, ParMETIS. This is used by TalyFEM to do domain decomposition.

* `--download-scalapack` and `--download-fblaslapack`: these are some basic linear algebra routines that PETSc uses to implement some of its built-in solvers. Compiling fblaslapack requires a working Fortran compiler.

* `--download-mumps`: download the MUMPS direct linear solver. This is an alternative to the built-in direct solver, and it works in parallel. It's optional.

How to install PETSc in the terminal:

```bash
# Download and extract PETSc
wget http://ftp.mcs.anl.gov/pub/petsc/release-snapshots/petsc-lite-3.13.4.tar.gz
tar xvf petsc-lite-3.13.4.tar.gz
rm petsc-lite-3.13.4.tar.gz  # delete the archive, we're done with it

# Configure PETSc, downloading and compiling dependencies along the way.
cd petsc-3.13.4
# configure debug mode
./configure --download-mpich --download-metis --download-parmetis --download-scalapack --download-fblaslapack --download-mumps
# configure release mode
./configure  --download-metis --download-parmetis --download-scalapack --download-fblaslapack --download-mumps -with-debugging=0 COPTFLAGS='-O3 -march=native -mtune=native' CXXOPTFLAGS='-O3 -march=native -mtune=native' FOPTFLAGS='-O3 -march=native -mtune=native' --download-mpich

# Write commands to set the PETSC_DIR and PETSC_ARCH environmnt variables to our .bashrc,
# which is automatically executed on login. These environment variables are what TalyFEM
# uses to find the PETSc installation.
echo "export PETSC_DIR=`pwd`" >> ~/.bashrc
# debug mode
echo "export PETSC_ARCH=arch-linux-c-debug" >> ~/.bashrc
# release mode
echo "export PETSC_ARCH=arch-linux-c-opt" >> ~/.bashrc
source ~/.bashrc

# To add mpicc and mpicxx to the default PATH
echo "export PATH=$PETSC_DIR/$PETSC_ARCH/bin:$PATH" >> ~/.bashrc
# Reload .bashrc now so PETSC_DIR and PETSC_ARCH are set.
source ~/.bashrc

# Compile PETSc
# (this will be automatically multi-threaded, so we don't need -j)
make
```

**NOTE:** Some HPC systems already have an MPI implementation available (e.g. Intel MPI or Cray MPI). On these systems you *may* get better performance by using this system default MPI instead of MPICH. You can do this by removing the `--download-mpich` option and specifying to use the system-provided MPI compiler wrapper as the PETSc compiler (e.g. with `--with-cc=mpicc` and `--with-cxx=mpicxx`). PETSc should automatically pick up the MPI directory from the MPI compiler. If it doesn't, you can try to specify it manually with `--with-mpi-dir=/some/path/here`.

**NOTE:** PETSc passes along compiler flags to automatically add its libraries to the [rpath](https://en.wikipedia.org/wiki/Rpath) of your compiled project executable. This is why you don't need to modify `LD_LIBRARY_PATH` in this step.

**NOTE:** For production runs, you can specify `--with-debugging=0` to turn off PETSc's internal debugging features. This may give a performance boost (at the cost of less-comprehendable error messages). Do not do any program development using a version of PETSc compiled this way.

## Compiling libconfig

Libconfig's build system is [Autotools](https://www.gnu.org/software/automake/manual/html_node/Autotools-Introduction.html), which means you'll need to run `./configure` and then `make` to build.

This guide recommends passing ```--prefix=`pwd`/install``` to `./configure`, which will cause `make install` to copy the output files to `[your_libconfig_dir]/install` instead of `/usr`. This way your libconfig install lives completely inside your libconfig folder. This is necessary if you are working on a system where you don't have admin privileges (i.e. an HPC cluster).

```bash
# Download and extract
wget http://hyperrealm.github.io/libconfig/dist/libconfig-1.7.2.tar.gz
tar xvf libconfig-1.7.2.tar.gz
rm libconfig-1.7.2.tar.gz

# Compile and copy output files to libconfig-1.7.2/install
cd libconfig-1.7.2
./configure --prefix=`pwd`/install
make -j8  # compile with 8 threads
make install

# Permanently set $LIBCONFIG_DIR environment variable, which is what TalyFEM uses
# to find your libconfig install. Also set LD_LIBRARY_PATH to include the
# libconfig lib directory to prevent dynamic linking errors with libconfig++.so.
echo "export LIBCONFIG_DIR=`pwd`/install" >> ~/.bashrc
echo "export LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:\$LIBCONFIG_DIR/lib" >> ~/.bashrc
source ~/.bashrc
```

**NOTE:** On some HPC clusters (notably Condo when using the Intel compiler), the `make` step gives a linker error. This is the libconfig example program failing to link with the Intel runtime. This is okay - the libconfig library itself compiles just fine. Just run `make install` and double check that `install/lib` contains some `*.a` files.

