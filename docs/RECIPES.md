## Stampede2 compilation

We are going to be using intel 17 compilers with intel 17 mpi

1. After logging in to stampede2 do the following steps. If default modules are loaded, load/swap the following modules.

    `module load intel/17.0.4 petsc/3.9-cxx`

    This will load intel compilers and mpi. It will also load precompiled optimized petsc on Stampede2.  

    If you do module list it should look like this
    ```
    Currently Loaded Modules:
    1) git/2.24.1   2) autotools/1.1   3) cmake/3.16.1   4) xalt/2.10.2   5) TACC   6) intel/17.0.4   7) libfabric/1.7.0   8) impi/17.0.3   9) python2/2.7.14  10) petsc/3.9-cxx

    ```


2. If your bashrc has been modified so no modules load upon logging in, type the following in Terminal

    `module load intel/17.0.4 petsc/3.9-cxx`

3. Now let us compile Libconfig using instructions from [docs/BUILDING.md](docs/BUILDING.md)

4. Now we have all the dependencies installed. Let us clone proteus. You may need to generate an “app password” on BitBucket and use that instead of your BitBucket password when prompted
    
    `git clone https://bitbucket.org/baskargroup/proteus.git`

5. Checkout the appropriate branch
   
    `git checkout <branch_name>`

6. Now sync submodules (repeat this step every time you do `git pull`)
    `git submodule update --init --recursive`
   
7. Make sure you are on the latest commit
    `git pull`


8. Now create a build directory. Name it according to whether you are compiling in 2D or 3D and whether you are 
compiling in release and debug mode. 
   For example `mkdir build-release-2D`
    Above name suggests that the executable in this folder would be for 2D and with release mode
   
9. Go to the build directory `cd build-release-2D`


10. Run the following cmake command for building proteus in release mode and for 2D computation (if `-DENABLE_2D=Yes` is 
not specified the executable will be compiled for 3D)
    ```
    cmake ..  -DENABLE_2D=Yes -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_FLAGS="-Wno-unknown-pragmas" 
    -DCMAKE_CXX_COMPILER=mpiicpc -DCMAKE_C_COMPILER=mpiicc -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_FLAGS="-xCORE-AVX2 -axCORE-AVX512,MIC-AVX512 -O3 -DNDEBUG"
    ```

    `-xCORE-AVX2 -axCORE-AVX512,MIC-AVX512` are important flags to generate an executable cross compiled for both 
    `skx` and `knl` nodes.

11. Now compile `make -j 8 proteus` 

## Frontera compilation

We are going to be using intel 19 compilers with intel 19 mpi

1. After logging in to frontera do the following steps. If default modules are loaded, load/swap the following modules.

   `module load petsc/3.13 intel/19.0.5 impi/19.0.9`

   This will load intel compilers and mpi. It will also load precompiled optimized petsc on Frontera.

   If you do module list it should look like this
    ```
    Currently Loaded Modules:
    1) git/2.24.1      3) cmake/3.20.3   5) hwloc/1.11.12   7) TACC           9) impi/19.0.9  11) python3/3.7.0
    2) autotools/1.2   4) pmix/3.1.4     6) xalt/2.10.34    8) intel/19.0.5  10) petsc/3.13
    ```


2. If your bashrc has been modified so no modules load upon logging in, type the following in Terminal

   `module load petsc/3.13 intel/19.0.5 impi/19.0.9`

3. Now let us compile Libconfig using instructions from [docs/BUILDING.md](docs/BUILDING.md)

4. Now we have all the dependencies installed. Let us clone proteus. You may need to generate an “app password” on BitBucket and use that instead of your BitBucket password when prompted

   `git clone https://bitbucket.org/baskargroup/proteus.git`

5. Checkout the appropriate branch

   `git checkout <branch_name>`

6. Now sync submodules (repeat this step every time you do `git pull`)
   `git submodule update --init --recursive`

7. Make sure you are on the latest commit
   `git pull`


8. Now create a build directory. Name it according to whether you are compiling in 2D or 3D and whether you are
   compiling in release and debug mode.
   For example `mkdir build-release-2D`
   Above name suggests that the executable in this folder would be for 2D and with release mode

9. Go to the build directory `cd build-release-2D`


10. Run the following cmake command for building proteus in release mode and for 2D computation (if `-DENABLE_2D=Yes` is
    not specified the executable will be compiled for 3D)
    ```
    cmake ..  -DENABLE_2D=Yes -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_FLAGS="-Wno-unknown-pragmas" 
    -DCMAKE_CXX_COMPILER=mpiicpc -DCMAKE_C_COMPILER=mpiicc -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_FLAGS="-O3 -DNDEBUG"
    ```

11. Now compile `make -j 8 proteus`

## Yellowstone compilation (Stanford HPC)

1. We are going to be using intel 19 compilers with intel 18 mpi

    After logging in to yellowstone do the following steps

    `module load cmake/3.15.4 impi/2018.2.199`
   
    `module swap intel/18.0.2.199 intel/19.1.0.166`


2. If you do module list it should look like this
    ```
    Currently Loaded Modules:
    1) cmake/3.15.4   2) gnu7/7.3.0   3) intel/19.1.0.166   4) impi/2018.2.199
    ```
    If your bashrc has been modified so no modules load upon logging in, type the following in Terminal
    `module load cmake/3.15.4 gnu7/7.3.0 intel/19.1.0.166 impi/2018.2.199`

3. Now let us build petsc:
    - Download petsc:
    `wget https://ftp.mcs.anl.gov/pub/petsc/release-snapshots/petsc-3.15.tar.gz --no-check-certificate`
      
    - Extract tar ball `tar -xvf petsc-3.15.tar.gz`
    
    - Go inside petsc directory `cd petsc-3.15.5`
    
    - Use the following configure command for petsc (this will compile petsc in release mode)
        ```
        ./configure  --download-metis --download-parmetis --download-fblaslapack --download-scalapack --download-mumps  --with-cc=mpiicc --with-cxx=mpiicpc --with-fc=mpiifort -with-debugging=0 COPTFLAGS='-O3 -march=native -mtune=native' CXXOPTFLAGS='-O3 -march=native -mtune=native' FOPTFLAGS='-O3 -march=native -mtune=native'
        ```
    - Now compile petsc (this command is also printed by petsc at the end of configure)
      
        `make PETSC_DIR=<current path> PETSC_ARCH=arch-linux2-c-opt all`

    - Make sure petsc compilation is correct 
      
        `make PETSC_DIR=<current path> PETSC_ARCH=arch-linux2-c-opt check`
    - Make sure petsc environment variables are added to the bashrc
        ```
        echo "export PETSC_DIR=`pwd`" >> ~/.bashrc
        echo “export PETSC_ARCH=arch-linux2-c-opt” >> ~/.bashrc
        source ~/.bashrc
        ```

4. Now let us compile Libconfig using instructions from [docs/BUILDING.md](docs/BUILDING.md)

5. Now we have all the dependencies installed. Let us clone proteus. You may need to generate an “app password” on 
   BitBucket and use that instead of your BitBucket password when prompted

   `git clone https://bitbucket.org/baskargroup/proteus.git`

6. Checkout the appropriate branch

   `git checkout <branch_name>`

7. Now sync submodules (repeat this step every time you do `git pull`)
   `git submodule update --init --recursive`

8. Make sure you are on the latest commit
   `git pull`


9. Now create a build directory. Name it according to whether you are compiling in 2D or 3D and whether you are
   compiling in release and debug mode.
   For example `mkdir build-release-2D`
   Above name suggests that the executable in this folder would be for 2D and with release mode

10. Go to the build directory `cd build-release-2D`


11. Run the following cmake command for building proteus in release mode and for 2D computation (if `-DENABLE_2D=Yes` is
    not specified the executable will be compiled for 3D)
    ```
    cmake ..  -DENABLE_2D=Yes -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_FLAGS="-Wno-unknown-pragmas" 
    -DCMAKE_CXX_COMPILER=mpiicpc -DCMAKE_C_COMPILER=mpiicc -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_FLAGS="-xCORE-AVX2 -axCORE-AVX512,MIC-AVX512 -O3 -DNDEBUG"
    ```

    `-xCORE-AVX2 -axCORE-AVX512,MIC-AVX512` are important flags to generate an executable cross compiled for both
    `skx` and `knl` nodes.

12. Now compile `make -j 8 proteus` 