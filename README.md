# About Proteus

Proteus is a two-phase flow simulation framework which solves thermodynamically consistent Cahn-Hilliard
Navier-Stokes equations and optionally second order diffuse interface methods.

It is named after a Greek mythological character who Homer called "Old man of the Sea".  The mythical character of Proteus is famous for changing shape and form and only people who can capture him can get help from his prophetic abilities. Much similar to capturing the shape of the interface between the two phases.  We try to "capture" the interface using fast ocree based adaptive meshing from the Dendro-KT library for predictive simulations.

1. To use Cahn-Hilliard models is following branches have to be chosen
   
      * To use projection based timescheme checkout `master` branch
      * To use fully coupled Cahn-Hilliard Navier Stokes scheme checkout `fully_coupled` branch

2. To use Second Order Diffuse Interface models checkout `allenCahnBasedModelsSeparateCurvature`

tl dr: two-phase simulations with diffuse interface methods; massively parallel; octree based adaptivity; continuous Galerkin Finite Elements; Both projection based and pressure stabilized implementations available; extensively tested and validated. 

For more details read: 

* Makrand A. Khanwale, Kumar Saurabh, Masado Ishii, Hari Sundar, and James A. Rossmanith. “A projection-based, semi-implicit time-stepping approach for the Cahn-Hilliard Navier-Stokes equations on adaptive octree meshes.” In: Submitted to Journal of Computational Physics : arXiv preprint arXiv:2107.05123 (2021). [[ARXIV]](https://arxiv.org/abs/2107.05123)
* Makrand A. Khanwale, Kumar Saurabh, Milinda Fernando, Victor M Calo, James A Rossmanith, Hari Sundar, and Baskar Ganapathysubramanian. “A fully-coupled framework for solving Cahn-Hilliard Navier-Stokes equations: Second-order, energy-stable numerical methods on adaptive octree based meshes”. In: Submitted to Computer Physics Communications : arXiv preprint arXiv:2009.06628 (2020) [[ARXIV]](https://arxiv.org/abs/2009.06628)
* Saurabh, Kumar, Masado Ishii, Milinda Fernando, Boshun Gao, Kendrick Tan, Ming-Chen Hsu, Adarsh Krishnamurthy, Hari Sundar, and Baskar Ganapathysubramanian. "Scalable adaptive PDE solvers in arbitrary domains." In Proceedings of the International Conference for High Performance Computing, Networking, Storage and Analysis, pp. 1-15. 2021. [[ARXIV]](https://arxiv.org/abs/2108.03757)


![phi_jet](docs/phi_jet.gif)

*Primary jet atomization*

![phi_mesh](docs/mesh_jet.gif)

*Mesh adaption using octrees*
## Cloning the repository
```bash
git clone https://bitbucket.org/baskargroup/proteus.git
cd proteus
git submodule update --init --recursive
```
## Dependencies
1. A C++ compiler with C++11 support is required. We aim for compatibility with gcc and icc (Intel C++ compiler).
2. [CMake](https://cmake.org/download/) 3.5 or above
3. [PETSc >= 3.7, upto 3.13.4 tested](https://www.mcs.anl.gov/petsc/), a mature C library of routines for solving
   partial
   differential equations in parallel at large scales. We mostly use it for solving the sparse `Ax=B`
   systems that the finite element method generates in parallel.

4. [libconfig](http://hyperrealm.github.io/libconfig/), a mature C/C++ library for parsing
   config files. Dendrite-kt has built-in utilities for loading simulation parameters from a file,
   and this library is what we use to parse that file.
   At the moment, these config files are tightly coupled with Dendrite-kt, so this dependency is required.

5. [BLAS](http://www.netlib.org/blas/)
   ``` 
   sudo apt install libopenblas-base 
   sudo apt install libopenblas-dev 
   ```
6. [LAPACK](http://www.netlib.org/lapack/)
   ```
   sudo apt install liblapack3 
   sudo apt install liblapack-dev
   ```
7. [LAPACKE](https://www.netlib.org/lapack/lapacke.html)
   ```
   sudo apt install liblapacke-dev 
   ```

## Building Dependencies
* See [docs/BUILDING.md](docs/BUILDING.md) for detailed instructions on how to build proteus dependencies.

* See [docs/RECIPES.md](docs/RECIPES.md) for recommended proteus build recipes for different clusters

## Compilation
```bash
mkdir build;
cd build;
```
### For 2D  Computation
```bash
cmake .. -DCMAKE_BUILD_TYPE=Release -DENABLE_2D=Yes -DCMAKE_C_COMPILER=mpicc -DCMAKE_CXX_COMPILER=mpicxx
make proteus
```

### For 3D  Computation
```bash
cmake .. -DCMAKE_BUILD_TYPE=Release -DENABLE_3D=Yes  -DCMAKE_C_COMPILER=mpicc -DCMAKE_CXX_COMPILER=mpicxx
make proteus
```
## Compile using CLion
We recommand using CLion as IDE. To compile dendrite-kt under CLion environment:

1. add a toolchain in ```File | Settings | Build, Execution, Deployment | Toolchains```,
   set ```C Compiler=$(which mpicc)``` and ```C++ Compiler=$(which mpicxx)```.

2. In ```File | Settings | Build, Execution, Deployment | CMake```, change ```Toolchain``` to the one we just created,
   add environment variable ```PETSC_DIR``` and ```PETSC_ARCH``` in ```environment```, if CMake is complaining about not
   finding ```Libconfig++```, add ```LIBCONFIG_DIR``` and ```LD_LIBRARY_PATH``` also.

3. Add additional ```CMAKE_FLAGS``` in ```CMAKE Options```

Development in CLion will offer multiple choice of ```Toolchain``` and multiple ```Profile``` that makes it easier
for changing build options or switch between different executables.

## Compile under ubuntu-20 focal

1. Change gcc version from 9 to 7

2. Install python2 if not available

## Usage

* We use a text parameter file called `config.txt` to setup parameters of the simulations.  Once the executable is 
built, the simulation can be run from any directory which contains the `config.txt` file with the following command 
`mpirun -n <num proc> /<path>/proteus` 

* For restarting the simulation from a checkpoint one can append a simple flag.  The execute command then looks like 
`mpirun -n <num proc> /<path>/proteus -resume_from_checkpoint`

* Example `config` files from the test cases performed in the papers are in `/config` directory. 


## Contact
If you find any bugs in the library or have questions, check the [BitBucket issue tracker](https://bitbucket.org/baskargroup/proteus/issues?status=new&status=open) to see if it has already been reported. If it hasn't, feel free to open a new issue.

If you have made any improvements to the library you would like to share, please
[open a BitBucket pull request](https://bitbucket.org/baskargroup/proteus/pull-requests/new)

Questions and comments should be sent to developers:

* [Makrand Khanwale](mailto:khanwale@stanford.edu)
* [Kumar Saurabh](mailto:maksbh@iastate.edu) (Dendrite-KT submodule)
* [Masado Ishii](mailto:masado@cs.utah.edu) (Dendro-KT submodule)
* [Hari Sundar](mailto:hari@cs.utah.edu ) (Dendro-KT submodule)
* [Baskar Ganapathysubramanian](mailto:baskarg@iastate.edu)

## Acknowledgements

This work is partially supported via the following grants, which are gratefully acknowledged:

   * NSF 1435587
   * NSF 1855902
   * NSF 1935255
   * NSF 2053760
   * ISU Plant Science Faculty Fellow
   * USDA-NIFA 2021-67021-35329
   
## LICENSE

Released under the GPL V3.0 License, which can be found in the repository in `LICENSE.txt`