//
// Created by makrand on 3/18/21.
//

#pragma once

#include <cmath>

double correctConcentr(double x);

struct CHNSFreeEnergy {
		virtual ~CHNSFreeEnergy(){};
		virtual double f(double phi) = 0;
		virtual double dfdphi(double phi) = 0;
		virtual double d2fdphi2(double phi) = 0;
		virtual double d2fdphi2Ideal(double phi) { return 1.0; }
};

#pragma mark - FH
/**
 * Flory-Huggins free energy
 */
struct FloryHugginsBinaryFreeEnergy : public CHNSFreeEnergy {
		double m1_;
		double m2_;
		double chi12_;

		FloryHugginsBinaryFreeEnergy(double m1, double m2, double chi)
				: m1_(m1), m2_(m2), chi12_(chi) {}

		inline double f(double phi) {
			double phi_c1 = correctConcentr(phi);
			double phi_c2 = correctConcentr(1.0 - phi);
			return (phi / m1_ * log(phi_c1) + phi_c2 / m2_ * log(phi_c2) +
			        chi12_ * phi_c1 * phi_c2);
		}
		inline double dfdphi(double phi) {
			double phi_c1 = correctConcentr(phi);
			double phi_c2 = correctConcentr(1.0 - phi);
			return (1.0 / m1_ * (1.0 + log(phi_c1)) - 1.0 / m2_ * (1.0 + log(phi_c2)) +
			        chi12_ * (1.0 - 2.0 * phi_c1));
		}
		inline double d2fdphi2(double phi) {
			double phi_c1 = correctConcentr(phi);
			double phi_c2 = correctConcentr(1.0 - phi);
			return (1.0 / m1_ * 1.0 / phi_c1 + 1.0 / m2_ * 1.0 / phi_c2 - 2.0 * chi12_);
		}

		inline double d2fdphi2Ideal(double phi) {
			double phi_c1 = correctConcentr(phi);
			double phi_c2 = correctConcentr(1.0 - phi);
			return (1.0 / m1_ * 1.0 / phi_c1 + 1.0 / m2_ * 1.0 / phi_c2);
		}
};

#pragma mark - FH+beta
/**
 * Flory-Huggins free energy with beta correction term
 */
struct FloryHugginsBetaBinaryFreeEnergy : public CHNSFreeEnergy {
		double m1_;
		double m2_;
		double chi12_;
		double beta_;

		FloryHugginsBetaBinaryFreeEnergy(double m1, double m2, double chi,
		                                 double beta)
				: m1_(m1), m2_(m2), chi12_(chi), beta_(beta) {}

		inline double f(double phi) {
			double phi_c1 = correctConcentr(phi);
			double phi_c2 = correctConcentr(1.0 - phi);
			return phi / m1_ * log(phi_c1) + phi_c2 / m2_ * log(phi_c2) +
			       chi12_ * phi_c1 * phi_c2 + beta_ * (1.0 / phi_c1 + 1.0 / phi_c2);
		}
		inline double dfdphi(double phi) {
			double phi_c1 = correctConcentr(phi);
			double phi_c2 = correctConcentr(1.0 - phi);
			return 1.0 / m1_ * (1.0 + log(phi_c1)) - 1.0 / m2_ * (1.0 + log(phi_c2)) +
			       chi12_ * (1.0 - 2.0 * phi_c1) +
			       beta_ * (1.0 / (phi_c2 * phi_c2) - 1.0 / (phi_c1 * phi_c1));
		}
		inline double d2fdphi2(double phi) {
			double phi_c1 = correctConcentr(phi);
			double phi_c2 = correctConcentr(1.0 - phi);
			return 1.0 / m1_ * 1.0 / phi_c1 + 1.0 / m2_ * 1.0 / phi_c2 - 2.0 * chi12_ +
			       beta_ * (2.0 / (phi_c1 * phi_c1 * phi_c1) +
			                2.0 / (phi_c2 * phi_c2 * phi_c2));
		}

		inline double d2fdphi2Ideal(double phi) {
			double phi_c1 = correctConcentr(phi);
			double phi_c2 = correctConcentr(1.0 - phi);
			return (1.0 / m1_ * 1.0 / phi_c1 + 1.0 / m2_ * 1.0 / phi_c2);
		}
};

#pragma mark - Poly01
/**
 * Polynomial Free Energy for:  0 < phi < 1
 *       (A = 3000 from CFP08)
 */
struct PolynomialFreeEnergy01 : public CHNSFreeEnergy {
		double A_;

		PolynomialFreeEnergy01(double A) : A_(A) {}

		inline double f(double phi) {
			return 0.5 * A_ * A_ * phi * phi * (1.0 - phi) * (1.0 - phi);
		}
		inline double dfdphi(double phi) {
			return A_ * A_ * (phi - 3.0 * phi * phi + 2.0 * phi * phi * phi);
		}
		inline double d2fdphi2(double phi) {
			return A_ * A_ * (1.0 - 6.0 * phi + 6.0 * phi * phi);
		}
		inline double d2fdphi2Ideal(double phi) { return 1.0; }
};

#pragma mark - Poly11
/**
 * Polynomial Free Energy for:  -1 < phi < 1
 */
struct PolynomialFreeEnergy_11 : public CHNSFreeEnergy {
		PolynomialFreeEnergy_11() {}

		inline double f(double phi) {
			return 0.25 * (phi * phi - 1.0) * (phi * phi - 1.0);
		}
		inline double dfdphi(double phi) { return (phi * phi * phi - phi); }
		inline double d2fdphi2(double phi) { return (3.0 * phi * phi - 1.0); }
		inline double d2fdphi2Ideal(double phi) { return 1.0; }
};
#pragma mark - Poly/AlphaBeta
/**
 * Polynomial Free Energy with wells/magnitude shifted by alpha/beta terms
 */
struct PolynomialFreeEnergyAlphaBeta : public CHNSFreeEnergy {
		double alpha_;
		double beta_;

		PolynomialFreeEnergyAlphaBeta(double alpha, double beta)
				: alpha_(alpha), beta_(beta) {}

		inline double f(double phi) {
			double c = sqrt(beta_ / alpha_);
			return 0.25 * alpha_ * (phi - c) * (phi - c) * (phi + c) * (phi + c);
		}
		inline double dfdphi(double phi) {
			return alpha_ * phi * phi * phi - beta_ * phi;
		}
		inline double d2fdphi2(double phi) {
			return 3.0 * alpha_ * phi * phi - beta_;
		}

		double d2fdphi2Ideal(double phi) { return 1.0; }
};

#pragma mark - Other

/**
 * Function to correct concentration if out of <0,1> range
 * Only used in Flory-Huggins FE
 *
 * @param x (double) value (phi)
 * @return new phi in <0,1> range
 */
inline double correctConcentr(double x) {
	double epsilon = 1e-10;
	if (x > 1.0)
		return 1.0;
	if (x < epsilon)
		return epsilon;
	return x;
}





