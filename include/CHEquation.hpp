//
// Created by makrand on 3/17/21.
//

#pragma once

#include <talyfem/talyfem.h>
#include <talyfem/stabilizer/tezduyar_upwind.h>
#include "CHNSNodeData.hpp"
#include "CHNSInputData.h"
#include "CHNSIntegrandsGenForm.hpp"


class CHEquation : public NonlinearEquation<CHNSNodeData> {
 private:

	/// For generic integrands support
	CHNSIntegrandsGenForm* integrandsGenForm_;
	CHNSInputData* input_data_;

 public:
	CHNSFreeEnergy* freeEnergy;
	CHEquation(CHNSInputData *idata, CHNSIntegrandsGenForm *integrandsGenForm)
			: NonlinearEquation<CHNSNodeData> (ENABLE_SURFACE_INTEGRATION, false, TALYFEMLIB::kAssembleGaussPoints),
			  integrandsGenForm_ (integrandsGenForm),
			  input_data_(idata){
		/// Set the free energy
		switch (input_data_->free_energy_type) {
		case 1: {
			PrintInfo ("Using Polynomial free energy with coefficient one");
			freeEnergy = new PolynomialFreeEnergy_11 ();
			break;
		}
		case 2: {
			throw TALYException () << "this case of free energy not supported";
		}
		default: {
			PrintInfo ("Using Polynomial free energy with coefficient one");
			freeEnergy = new PolynomialFreeEnergy_11 ();
			break;
		}
		}
	}


	void Integrands(const TALYFEMLIB::FEMElm &fe, TALYFEMLIB::ZeroMatrix<double> &Ae,
	                TALYFEMLIB::ZEROARRAY<double> &be) override {
		assert(false);
	}

	void Integrands_Ae(const TALYFEMLIB::FEMElm &fe, TALYFEMLIB::ZeroMatrix<double> &Ae, double* h) {
		/// Call the appropriate integrands from CHNSIntegrandsGenForm class;
		integrandsGenForm_->getIntegrandsCHAe (fe, Ae, dt_, freeEnergy, t_);
	}

	void Integrands_be(const TALYFEMLIB::FEMElm &fe, TALYFEMLIB::ZEROARRAY<double> &be, double* h) {
		/// Call the appropriate integrands from CHNSIntegrandsGenForm class;
		integrandsGenForm_->getIntegrandsCHbe (fe, be, dt_, freeEnergy, t_);
	}


	virtual void Integrands4side(const FEMElm &fe, int sideInd, ZeroMatrix<double> &Ae, ZEROARRAY<double> &be, double* h) {
		assert(false);
	}

	void Integrands4side_Ae(const TALYFEMLIB::FEMElm &fe, const DENDRITE_UINT sideInd, const DENDRITE_UINT id,TALYFEMLIB::ZeroMatrix<double> &Ae, double* h) {
		integrandsGenForm_->getIntegrands4sideCHAe (fe, sideInd, id, Ae, dt_, t_);
	}

	void Integrands4side_be(const TALYFEMLIB::FEMElm &fe, const DENDRITE_UINT sideInd, const DENDRITE_UINT id, TALYFEMLIB::ZEROARRAY<double> &be, double* h) {
		integrandsGenForm_->getIntegrands4sideCHbe (fe, sideInd, id, be, dt_, t_);
	}

};
