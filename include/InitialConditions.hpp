//
// Created by makrand on 4/16/21.
//


#pragma once

#include <DataTypes.h>
#include <CHNSRefine.hpp>
#include <PETSc/Solver/NonLinearSolver.h>
#include <CHNSInputData.h>
#include <CHNSNodeData.hpp>
#include <Traversal/Analytic.h>
#include "RefineSubDA.h"
#include "CHNSUtils.h"
#include <Coarsen.hpp>
#include <IO/VTU.h>

using namespace PETSc;
class InitialConditions {
 private:
	CHNSInputData* input_data_;
 public:
	InitialConditions(CHNSInputData *input_data) : input_data_(input_data) {}

	void setInitialConditionVelocityProjection(DA *octDA, Vec &Solution);
	void setInitialConditionPressure(DA *octDA, Vec &Solution);
	void setInitialConditionCH(DA *octDA, Vec &Solution);
	double ValueAtInitCH(const ZEROPTV &pt);

	double calcPhiFVIDiffuse(const TALYFEMLIB::ZEROPTV &pt);
	double calcPhiDropDiffuse(const TALYFEMLIB::ZEROPTV &pt);
	double calcPhiDropSplashDiffuse(const TALYFEMLIB::ZEROPTV &pt);
	double calcPhiCapillaryWaveDiffuse(const TALYFEMLIB::ZEROPTV &pt);
	double calcPhiRayleighTaylorDiffuse(const TALYFEMLIB::ZEROPTV &pt);
	double calcPhiEllipseDiffuse(const TALYFEMLIB::ZEROPTV &pt);
	double calcPhiDamBreakDiffuse(const TALYFEMLIB::ZEROPTV &pt);
	double calcPhiJetDiffuse(const TALYFEMLIB::ZEROPTV &pt);
	double calcPhiVerticalSlugDiffuse(const TALYFEMLIB::ZEROPTV &pt);



	/**
	 * @brief returns the value of analytical solution of JET
	 * @param pt position
	 * @param dof
	 * @param time
	 * @return the analytical solution at that current time
	 */
	static double AnalyticalSolutionJET(const TALYFEMLIB::ZEROPTV &pt, int dof, const DENDRITE_REAL time, const
	DENDRITE_REAL timestep) {
		double T_C=1e-6;
		double R = 5e-5;
		const DENDRITE_REAL u_max = 100;
		double xi = 0.1;
		double f = 1.3*u_max/(2*R);
		double u_inlet = 1;
    DENDRITE_REAL u = u_inlet*(1+xi*sin(f*2*M_PI*(time-timestep)*T_C));

#if(DIM == 3)
		if (dof == CHNSNodeData::VEL_X) {
			return (u);
		} else if (dof == CHNSNodeData::VEL_Y) {
			return (0.0);
		} else if (dof == CHNSNodeData::VEL_Z) {
			return (0.0);
		} else {
			throw TALYFEMLIB::TALYException () << "Wrong dof received \n";
		}
#else
		if (dof == CHNSNodeData::VEL_X) {
			return (u);
		} else if (dof == CHNSNodeData::VEL_Y) {
			return (0.0);
		} else {
			throw TALYFEMLIB::TALYException () << "Wrong dof received \n";
		}
#endif
	};

	/**
	 * @brief returns the value of analytical solution of NS
	 * @param pt position
	 * @param dof
	 * @param time
	 * @return the analytical solution at that current time
	 */
	static double AnalyticalSolutionNS(const TALYFEMLIB::ZEROPTV &pt, int dof, const DENDRITE_REAL time, const
	DENDRITE_REAL timestep) {
#if(DIM == 3)
		std::cout << "Analytical solution not supported \n";
#else
		if (dof == CHNSNodeData::VEL_X) {
			return (M_PI * pow (sin (M_PI * pt.x ()), 2) * sin (2.0 * M_PI * pt.y ()) * sin (time));
		} else if (dof == CHNSNodeData::VEL_Y) {
			return (-M_PI * sin (2.0 * M_PI * pt.x ()) * pow (sin (M_PI * pt.y ()), 2) * sin (time));
		} else if (dof == CHNSNodeData::PRESSURE) {
			return (cos (M_PI * pt.x ()) * sin (M_PI * pt.y ()) * sin (time));
		} else {
			throw TALYFEMLIB::TALYException () << "Wrong dof received \n";
		}
#endif
	};

	/**
	 * @brief returns the value of analytical solution of NS
	 * @param pt position
	 * @param dof
	 * @param time
	 * @return the analytical solution at that current time
	 */
	static double AnalyticalSolutionCH(const TALYFEMLIB::ZEROPTV &pt, int dof, const DENDRITE_REAL time, const
	DENDRITE_REAL dt) {
#if(DIM == 3)
		std::cout << "Analytical solution not supported \n";
#else
		if (dof == 0) {
			return cos (M_PI * pt.x ()) * cos (M_PI * pt.y ()) * sin (time);
		} else if (dof == 1) {
			return cos (M_PI * pt.x ()) * cos (M_PI * pt.y ()) * sin (time - (0.5 * dt));
		} else {
			throw TALYFEMLIB::TALYException () << "Wrong dof received \n";
		}
#endif
	};
};

/**
 * @brief sets vector with initial velocity. Must be allocated outside.
 * @param octDA
 * @param [out] Solution the final vector with velocity
 */
void InitialConditions::setInitialConditionVelocityProjection(DA *octDA, Vec &Solution) {
	std::function<void(const double *, double *)> initial_condition = [&](const double *x, double *var) {
			if (input_data_->caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS) {
				var[CHNSNodeData::VEL_X] = InitialConditions::AnalyticalSolutionNS(TALYFEMLIB::ZEROPTV{x[0], x[1], 0.0},
				                                                                   CHNSNodeData::VEL_X, 0.0, 0.0);
				var[CHNSNodeData::VEL_Y] = InitialConditions::AnalyticalSolutionNS(TALYFEMLIB::ZEROPTV{x[0], x[1], 0.0}, CHNSNodeData::VEL_Y, 0.0, 0.0);
			} else if (input_data_->caseTypeForNavierStokes == CHNSInputData::QUIESCENT_BOX
			           or input_data_->caseTypeForNavierStokes == CHNSInputData::FREE_SLIP_QUIESCENT_BOX) {
				var[CHNSNodeData::VEL_X] = 0.0;
				var[CHNSNodeData::VEL_Y] = 0.0;
#if(DIM == 3)
				var[CHNSNodeData::VEL_Z] = 0;
#endif
			} else if (input_data_->caseTypeForNavierStokes == CHNSInputData::PERIODIC_HIT) {
				throw TALYException() << "periodic bc required for PERIODIC_HIT case";
			} else if (input_data_->caseTypeForNavierStokes == CHNSInputData::CAPILLARY_INJECTION_NS) {
				var[CHNSNodeData::VEL_X] = 0.0;
				var[CHNSNodeData::VEL_Y] = 0.0;
#if(DIM == 3)
				var[CHNSNodeData::VEL_Z] = 0;
#endif
			} else if (input_data_->caseTypeForNavierStokes == CHNSInputData::FREE_SLIP_ALL_WALLS_QUIESCENT_BOX) {
				var[CHNSNodeData::VEL_X] = 0.0;
				var[CHNSNodeData::VEL_Y] = 0.0;
#if(DIM == 3)
				var[CHNSNodeData::VEL_Z] = 0;
#endif
			} else if (input_data_->caseTypeForNavierStokes == CHNSInputData::JET_NS) {
				var[CHNSNodeData::VEL_X] = 0.0;
				var[CHNSNodeData::VEL_Y] = 0.0;
#if(DIM == 3)
				var[CHNSNodeData::VEL_Z] = 0;
#endif
			} else if (input_data_->caseTypeForNavierStokes == CHNSInputData::CHANNEL_FLOW) {
				var[CHNSNodeData::VEL_X] = 0.0;
				var[CHNSNodeData::VEL_Y] = 0.0;
#if(DIM == 3)
				var[CHNSNodeData::VEL_Z] = 0;
#endif
			} else if (input_data_->caseTypeForNavierStokes == CHNSInputData::COUETTE_FLOW_BOTH_PLATES_OPPOSITE) {
				var[CHNSNodeData::VEL_X] = 0.0;
				var[CHNSNodeData::VEL_Y] = 0.0;
#if(DIM == 3)
				var[CHNSNodeData::VEL_Z] = 0.0;
#endif
			} else if (input_data_->caseTypeForNavierStokes == CHNSInputData::PERIODIC_CHANNEL_FLOW
			           or input_data_->caseTypeForNavierStokes == CHNSInputData::Y_SHAPED_CHANNEL) {
				throw TALYException() << "Initial condition for this case not implemented yet";
			}
	};
	octDA->petscSetVectorByFunction(Solution, initial_condition, false, false, DIM);
}

/**
 * @brief sets vector with pressure. Must be allocated outside.
 * @param octDA
 * @param [out] Solution the final vector with pressure
 */
void InitialConditions::setInitialConditionPressure(DA *octDA, Vec &Solution) {
	std::function<void(const double *, double *)> initial_condition = [&](const double *x, double *var) {
			if (input_data_->caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS) {
				var[0] = InitialConditions::AnalyticalSolutionNS(TALYFEMLIB::ZEROPTV{x[0], x[1], 0.0}, CHNSNodeData::PRESSURE, 0.0, 0.0);
			} else {
				var[0] = 0.0;
			}
	};
	octDA->petscSetVectorByFunction(Solution, initial_condition, false, false, 1);
}

/**
 * @brief sets vector with initial velocity. Must be allocated outside.
 * @param octDA
 * @param [out] Solution the final vector with velocity
 */
void InitialConditions::setInitialConditionCH(DA *octDA, Vec &Solution) {
	std::function<void(const double *, double *)> initial_condition = [&](const double *x, double *var) {
			/// CH only has 2 dof and should be accessed as var[0], var[1]
			enum CHVarIdx {
					PHI = 0,
					MU = 1,
			};
			if (input_data_->caseTypeCHinit == CHNSInputData::MANUFACTURED_SOL_TEST_CH) {
				var[PHI] = InitialConditions::AnalyticalSolutionCH (TALYFEMLIB::ZEROPTV{x[0], x[1], 0.0}, 0, 0.0, 0.0);
				var[MU] = InitialConditions::AnalyticalSolutionCH (TALYFEMLIB::ZEROPTV{x[0], x[1], 0.0}, 1, 0.0, 0.0);
			} else if (input_data_->caseTypeCHinit == CHNSInputData::FLAT_VERT_INTERFACE) {
				DENDRITE_REAL posX = x[0]*input_data_->mesh_def.sizeOfBox;
				DENDRITE_REAL posY = x[1]*input_data_->mesh_def.sizeOfBox;
				DENDRITE_REAL posZ = x[2]*input_data_->mesh_def.sizeOfBox;
				var[PHI] = this->calcPhiFVIDiffuse(TALYFEMLIB::ZEROPTV{posX, posY, posZ});
				var[MU] = 0.0;
			} else if (input_data_->caseTypeCHinit == CHNSInputData::DROP) {
				DENDRITE_REAL posX = x[0]*input_data_->mesh_def.sizeOfBox;
				DENDRITE_REAL posY = x[1]*input_data_->mesh_def.sizeOfBox;
				DENDRITE_REAL posZ = x[2]*input_data_->mesh_def.sizeOfBox;
				var[PHI] = this->calcPhiDropDiffuse(TALYFEMLIB::ZEROPTV{posX, posY, posZ});
				var[MU] = 0.0;
				//var[PHI] = posX + posY;
				//var[MU] = posX + posY;
			} else if (input_data_->caseTypeCHinit == CHNSInputData::DROPSPLASH) {
				DENDRITE_REAL posX = x[0]*input_data_->mesh_def.sizeOfBox;
				DENDRITE_REAL posY = x[1]*input_data_->mesh_def.sizeOfBox;
				DENDRITE_REAL posZ = x[2]*input_data_->mesh_def.sizeOfBox;
				var[PHI] = this->calcPhiDropSplashDiffuse(TALYFEMLIB::ZEROPTV{posX, posY, posZ});
				var[MU] = 0.0;
			} else if (input_data_->caseTypeCHinit == CHNSInputData::CAPILLARYWAVE) {
				DENDRITE_REAL posX = x[0]*input_data_->mesh_def.sizeOfBox;
				DENDRITE_REAL posY = x[1]*input_data_->mesh_def.sizeOfBox;
				DENDRITE_REAL posZ = x[2]*input_data_->mesh_def.sizeOfBox;
				var[PHI] = this->calcPhiCapillaryWaveDiffuse(TALYFEMLIB::ZEROPTV{posX, posY, posZ});
				var[MU] = 0.0;
			} else if (input_data_->caseTypeCHinit == CHNSInputData::RAYLEIGHTAYLOR) {
				DENDRITE_REAL posX = x[0]*input_data_->mesh_def.sizeOfBox;
				DENDRITE_REAL posY = x[1]*input_data_->mesh_def.sizeOfBox;
				DENDRITE_REAL posZ = x[2]*input_data_->mesh_def.sizeOfBox;
				var[PHI] = this->calcPhiRayleighTaylorDiffuse(TALYFEMLIB::ZEROPTV{posX, posY, posZ});
				var[MU] = 0.0;
			} else if (input_data_->caseTypeCHinit == CHNSInputData::ELLIPSE) {
				DENDRITE_REAL posX = x[0]*input_data_->mesh_def.sizeOfBox;
				DENDRITE_REAL posY = x[1]*input_data_->mesh_def.sizeOfBox;
				DENDRITE_REAL posZ = x[2]*input_data_->mesh_def.sizeOfBox;
				var[PHI] = this->calcPhiEllipseDiffuse(TALYFEMLIB::ZEROPTV{posX, posY, posZ});
				var[MU] = 0.0;
			} else if (input_data_->caseTypeCHinit == CHNSInputData::DAM_BREAK) {
				DENDRITE_REAL posX = x[0]*input_data_->mesh_def.sizeOfBox;
				DENDRITE_REAL posY = x[1]*input_data_->mesh_def.sizeOfBox;
				DENDRITE_REAL posZ = x[2]*input_data_->mesh_def.sizeOfBox;
				var[PHI] = this->calcPhiDamBreakDiffuse(TALYFEMLIB::ZEROPTV{posX, posY, posZ});
				var[MU] = 0.0;
			} else if (input_data_->caseTypeCHinit == CHNSInputData::JET_CH) {
				DENDRITE_REAL posX = x[0]*input_data_->mesh_def.sizeOfBox;
				DENDRITE_REAL posY = x[1]*input_data_->mesh_def.sizeOfBox;
				DENDRITE_REAL posZ = x[2]*input_data_->mesh_def.sizeOfBox;
				var[PHI] = this->calcPhiJetDiffuse(TALYFEMLIB::ZEROPTV{posX, posY, posZ});
				var[MU] = 0.0;
			} else if (input_data_->caseTypeCHinit == CHNSInputData::VERTICAL_SLUG) {
				DENDRITE_REAL posX = x[0]*input_data_->mesh_def.sizeOfBox;
				DENDRITE_REAL posY = x[1]*input_data_->mesh_def.sizeOfBox;
				DENDRITE_REAL posZ = x[2]*input_data_->mesh_def.sizeOfBox;
				var[PHI] = this->calcPhiVerticalSlugDiffuse(TALYFEMLIB::ZEROPTV{posX, posY, posZ});
				var[MU] = 0.0;
			} else {
				var[PHI] = 0.0;
				var[MU] = 0.0;
			}
	};
	octDA->petscSetVectorByFunction (Solution, initial_condition, false, false, CHNSNodeData::CH_DOF);
}

/** Function definition
 * Returns the initial condition value at the given
 * node at time zero.
 * The nodeID is used as a seed in the random number
 * generator for generating
 * parameters for the initial condition
 * @param node_id ID of node where the value will be
 * calculated.
 * @return value of initial condition of phasefield
 * for the given node
 */
double InitialConditions::ValueAtInitCH(const ZEROPTV &pt) {
	double value = 0.0;
	if (input_data_->caseTypeCHinit == CHNSInputData::FLAT_VERT_INTERFACE) {
		value = calcPhiFVIDiffuse(pt);
	} else if (input_data_->caseTypeCHinit == CHNSInputData::DROP) {
		value = calcPhiDropDiffuse(pt);
	} else if (input_data_->caseTypeCHinit == CHNSInputData::DROPSPLASH) {
		value = calcPhiDropSplashDiffuse(pt);
	} else if (input_data_->caseTypeCHinit == CHNSInputData::CAPILLARYWAVE) {
		value = calcPhiCapillaryWaveDiffuse(pt);
	} else if (input_data_->caseTypeCHinit == CHNSInputData::MANUFACTURED_SOL_TEST_CH) {
		/**This is a 2D problem so the initial condition is
		 * phi = cos(pi * x) * cos(pi * y) * sin (t) at t=0 which is zero
		 */
		value = 0.0;
	} else if (input_data_->caseTypeCHinit == CHNSInputData::RAYLEIGHTAYLOR) {
		value = this->calcPhiRayleighTaylorDiffuse(pt);
	} else if (input_data_->caseTypeCHinit == CHNSInputData::ELLIPSE) {
		value = this->calcPhiEllipseDiffuse(pt);
	} else if (input_data_->caseTypeCHinit == CHNSInputData::DAM_BREAK) {
		value = this->calcPhiDamBreakDiffuse(pt);
	} else if (input_data_->caseTypeCHinit == CHNSInputData::JET_CH) {
		value = this->calcPhiJetDiffuse(pt);
	} else if (input_data_->caseTypeCHinit == CHNSInputData::VERTICAL_SLUG) {
		value = this->calcPhiVerticalSlugDiffuse(pt);
	} else {
		throw TALYException() << "Unknown initial condition case";
	}
	return value;
}

/**This function calculates the initial phase field for a flat interface at
 * flat horizontal interface
 * @param pt point at which initial phase field is calculated
 * @return phase field diffused near the flat interface
 */
double InitialConditions::calcPhiFVIDiffuse(const TALYFEMLIB::ZEROPTV &pt) {
	double phiFVIDiffuse; ///< variable for phi

	///This is int
	double delta = input_data_->Cn * input_data_->referenceLengthScaleCn;

	double locationInt = input_data_->vertInterfaceLoc;

	phiFVIDiffuse = tanh((locationInt - pt.y()) / (sqrt(2) * delta));

	return phiFVIDiffuse;
}

/**Function to calculate the phi field diffused near the surface of the sphere
   * @param pt point at which the field is being calculated
   * @return value of phi at the point
   */
double InitialConditions::calcPhiDropDiffuse(const TALYFEMLIB::ZEROPTV &pt) {

	double phiDiffuse = 0.0;

	/// As everything is scaled with droplet diameter, the radius of the
	/// droplet is half
	double radius = input_data_->radiusds;

	///This is interface
	double delta = input_data_->Cn * input_data_->referenceLengthScaleCn;

	/// Get the location of the center of the sphere from the input data
	ZEROPTV center;
	center.x() = input_data_->dropxc;
	center.y() = input_data_->dropyc;
	center.z() = input_data_->dropzc;

	/// Calculate the location of the pt relative to the center of the drop
	ZEROPTV locationRel;
	for (int ndim = 0; ndim < DIM; ndim++) {
		locationRel(ndim) = pt(ndim) - center(ndim);
	}

	/// Calculate square of the relative location
	ZEROPTV locationRelSquared;
	for (int ndim = 0; ndim < DIM; ndim++) {
		locationRelSquared(ndim) = pow(locationRel(ndim), 2);
	}
	phiDiffuse =
			tanh((locationRelSquared(0) + locationRelSquared(1) + locationRelSquared(2) - pow(radius, 2.0)) /(pow(2.0, 0.5) * delta));
//	if (std::fabs(pt.x() - 1.0) < 1e-4 and std::fabs(pt.y() - 1.0) < 1e-4){
//		std::cout << phiDiffuse << std::endl;
//	}
	return phiDiffuse;
}

double InitialConditions::calcPhiJetDiffuse(const TALYFEMLIB::ZEROPTV &pt) {

	double phiDiffuse = 0.0;
	double x = pt.x();
	double y = pt.y();
	double z = pt.z();

	double radius = input_data_->radius_jet;

	/// Get the location of the center of the sphere from the input data
	ZEROPTV center;
	center.x() = input_data_->mesh_def.physDomain.min[0];
	center.y() = (input_data_->mesh_def.physDomain.min[1]+input_data_->mesh_def.physDomain.max[1])/2;

	#if(DIM == 3)
	center.z() = (input_data_->mesh_def.physDomain.min[2]+input_data_->mesh_def.physDomain.max[2])/2;
	bool inside_circle =(fabs(sqrt((y-center.y())*(y-center.y())+(z-center.z())*(z-center.z())))<radius)&&(fabs(x-center.x())<1e-15);
    #endif
	#if(DIM == 2)
	bool inside_circle =(fabs(sqrt((y-center.y())*(y-center.y())))<radius)&&(fabs(x-center.x())<1e-15);
    #endif

	if (inside_circle){
		phiDiffuse = 1;
	} else{
		phiDiffuse = -1;
	}
    
	return phiDiffuse;
}


/**Function to calculate the phi field diffused for a drop about to splash
 * @param pt point at which the field is being calculated
 * @return value of phi at the point
 */
double InitialConditions::calcPhiDropSplashDiffuse(const TALYFEMLIB::ZEROPTV &pt) {

	double phiDropSplash;

	///This is interface
	double delta = input_data_->Cn * input_data_->referenceLengthScaleCn;;

	double locationInt = input_data_->vertInterfaceLoc;

	/// Get the location of the center of the sphere from the input data
	ZEROPTV center;
	center.x() = input_data_->dropxds;
	center.y() = input_data_->dropyds;
	center.z() = input_data_->dropzds;

	/// Calculate the location of the pt relative to the center of the drop
	ZEROPTV locationRel;
	for (int ndim = 0; ndim < DIM; ndim++) {
		locationRel(ndim) = pt(ndim) - center(ndim);
	}

	/// Calculate square of the relative location
	ZEROPTV locationRelSquared;
	for (int ndim = 0; ndim < DIM; ndim++) {
		locationRelSquared(ndim) = pow(locationRel(ndim), 2);
	}

	double radius = input_data_->radiusds;

	phiDropSplash = -tanh((locationRelSquared(0) + locationRelSquared(1) +
	                       locationRelSquared(2) - pow(radius, 2.0)) /
	                      (pow(2.0, 0.5) * delta)) +
	                tanh((locationInt - pt.y()) / (sqrt(2) * delta));

	return phiDropSplash;
}
/**Function to calculate the phi field diffused for capillary wave from
 * Prosperetti et al.
 * @param pt point at which the field is being calculated
 * @return value of phi at the point
 */
double InitialConditions::calcPhiCapillaryWaveDiffuse(const TALYFEMLIB::ZEROPTV &pt) {

	double phiCapillaryWave;
	double amplitude = input_data_->amplitudeInitial;

	///This is interface
	double delta = input_data_->Cn * input_data_->referenceLengthScaleCn;

	double locationInt = input_data_->vertInterfaceLoc;

	phiCapillaryWave =
			tanh((locationInt - pt.y() + (amplitude * cos(2 * M_PI * pt.x()))) /
			     (sqrt(2) * delta));

	return phiCapillaryWave;
}

/**Function to calculate the phi field diffused for initial condition of Rayleigh Taylor instability
 * @param pt point at which the field is being calculated
 * @return value of phi at the point
 */
double InitialConditions::calcPhiRayleighTaylorDiffuse(const ZEROPTV &pt) {

	double phiCapillaryWave = 0.0;
	double amplitude = input_data_->amplitudeInitial;

	/// This is interface
	double delta = input_data_->Cn * input_data_->referenceLengthScaleCn;

	double locationInt = input_data_->vertInterfaceLoc;

	ZEROPTV centerofGaussian = input_data_->gaussianCenter;

	double stdDevGaussian = input_data_->stdDevGaussian;

	/// Gaussian
	double gaussian =
			amplitude * exp(-((((pow((pt.x() - centerofGaussian.x()), 2)) / stdDevGaussian)) +
			                  ((pow((pt.z() - centerofGaussian.z()), 2)) / (stdDevGaussian))));

	/// Reference is Liang(2016 PoF)
	if (input_data_->trigonometricInitialCondition) {
#if(DIM == 3)
		///h(x,y) = 0.05W [cos(kx) + cos(ky)]
		double h = 0.05 * ( (cos( 2 * M_PI * pt.x()) + cos( 2 * M_PI * pt.z()) ) );
//		phiCapillaryWave = tanh ( 2 * ( ( pt.y() - h - (0.5 * input_data_->mesh_def.channel_max[1]) ) / ( sqrt(2) *
//		                                                                                                  delta) ) );
		phiCapillaryWave = tanh ( 2 * ( ( pt.y() - h - (input_data_->vertInterfaceLoc) ) / ( sqrt(2) *delta) ) );
		//PrintStatus("Setting trig RT condition");
#else
		phiCapillaryWave =
				tanh((locationInt - pt.y() + (amplitude * cos(2 * M_PI * pt.x()))) /(sqrt(2) * delta));
#endif
	} else {
		phiCapillaryWave =
				tanh(((locationInt - pt.y()) - gaussian) / (sqrt(2) *delta));
	}
	return phiCapillaryWave;
}


/**Function to calculate the phi field diffused near the surface of the ellipse
 * @param pt point at which the field is being calculated
 * @return value of phi at the point
 */
double InitialConditions::calcPhiEllipseDiffuse(const ZEROPTV &pt) {

	double phiDiffuse = 0.0;
	double xOveryRatio = 0;
	double yOverxRatio = 0;
	/// As everything is scaled with droplet diameter, the radius of the
	/// droplet is half
	double radiusInxDirection = 1.0 * input_data_-> radiusInxDirection;
	double radiusInyDirection = 1.0 * input_data_-> radiusInyDirection;
	double radiusInzDirection = 1.0 * input_data_-> radiusInzDirection;

	///This is interface
	double delta = input_data_->Cn * input_data_->referenceLengthScaleCn;

	/// Get the location of the center of the sphere from the input data
	ZEROPTV center;
	center.x() = input_data_->dropxc;
	center.y() = input_data_->dropyc;
	center.z() = input_data_->dropzc;

#if(DIM == 2) // 2D case
		if (radiusInxDirection > radiusInyDirection) {
			xOveryRatio = radiusInxDirection / radiusInyDirection;
		} else {
			yOverxRatio = radiusInyDirection / radiusInxDirection;
		}
		/// Calculate the location of the pt relative to the center of the drop
		ZEROPTV locationRel;
		for (int ndim = 0; ndim < DIM; ndim++) {
			locationRel(ndim) = pt(ndim) - center(ndim);
		}

		/// Calculate square of the relative location
		ZEROPTV locationRelSquared;
		for (int ndim = 0; ndim < DIM; ndim++) {
			locationRelSquared(ndim) = pow(locationRel(ndim), 2);
		}
		if (xOveryRatio != 0) {
			phiDiffuse = tanh((locationRelSquared(0) + pow(xOveryRatio, 2) * locationRelSquared(1) +
			                   locationRelSquared(2) - pow(xOveryRatio, 2) * pow(radiusInyDirection, 2.0)) /
			                  (pow(2.0, 0.5) * delta));
		} else {
			phiDiffuse = tanh((pow(yOverxRatio, 2) * locationRelSquared(0) + locationRelSquared(1) +
			                   locationRelSquared(2) - pow(yOverxRatio, 2) * pow(radiusInxDirection, 2.0)) /
			                  (pow(2.0, 0.5) * delta));
		}
		return phiDiffuse;
#else // 3D case
		/// Calculate the location of the pt relative to the center of the drop
		ZEROPTV locationRel;
		for (int ndim = 0; ndim < DIM; ndim++) {
			locationRel(ndim) = pt(ndim) - center(ndim);
		}
		/// Calculate square of the relative location
		ZEROPTV locationRelSquared;
		for (int ndim = 0; ndim < DIM; ndim++) {
			locationRelSquared(ndim) = pow(locationRel(ndim), 2);
		}
		phiDiffuse = tanh((locationRelSquared(0) + pow((1.0*radiusInxDirection/(1.0*radiusInyDirection)),2)*locationRelSquared(1) +
		                   + pow((1.0*radiusInxDirection/(1.0*radiusInzDirection)),2)*locationRelSquared(2) - pow(radiusInxDirection, 2.0)) /
		                  (pow(2.0, 0.5) * delta));
		return phiDiffuse;
#endif
}
double InitialConditions::calcPhiDamBreakDiffuse(const ZEROPTV &pt) {
	double phiDamBreakDiffuse; ///< variable for phi

	double x = pt.x();
	double y = pt.y();
	double z = pt.z();
	///This is int
	double delta = input_data_->Cn * input_data_->referenceLengthScaleCn;

	double locationInt = input_data_->vertInterfaceLoc; /// location of the interface

	double locationOfDam = 1.0;

	double functionOfShape = 0.0;
	///- |x/1 - y/2| + | x/1 + y/2| -2
	functionOfShape = -1.0 * ( std::fabs( (x/locationOfDam) - (y/locationInt) )
			+ std::fabs( (x/locationOfDam) + (y/locationInt) ) - 2.0);



	phiDamBreakDiffuse = tanh((functionOfShape) / (sqrt(2) * delta));


	return phiDamBreakDiffuse;
}

double InitialConditions::calcPhiVerticalSlugDiffuse(const ZEROPTV &pt) {
	double phiVerticalSlugDiffuse;

	double delta = input_data_->Cn * input_data_->referenceLengthScaleCn;

	double interfaceLocationLeft = input_data_->leftInterfaceLocation;
	double interfaceLocationRight = input_data_->rightInterfaceLocation;

	phiVerticalSlugDiffuse = -tanh(((pt.x() - interfaceLocationLeft)*(pt.x() - interfaceLocationRight)) / (sqrt(2)*delta));

	return phiVerticalSlugDiffuse;
}
