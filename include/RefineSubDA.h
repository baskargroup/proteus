//
// Created by makrand on 3/17/21.
//

#pragma once
#include <Traversal/Refinement.h>
#include <SubDA/Voxel.h>
#include "Boundary/SubDomainBoundary.h"
class RefineSubDA: public Refinement{
private:
  const SubDomainBoundary* boundaryTypesInformation_;
  const CHNSInputData* inputData_;
public:
  RefineSubDA(DA *octDA, const std::vector<TREENODE> & treePart, const DomainExtents & domain, const CHNSInputData* inputData, const
  SubDomainBoundary* boundaryTypeInfo);
  ot::OCT_FLAGS::Refine getRefineFlags(TALYFEMLIB::FEMElm &fe, const std::vector<TALYFEMLIB::ZEROPTV> &coords) override;
};


RefineSubDA::RefineSubDA(DA *octDA, const std::vector<TREENODE> & treePart, const DomainExtents &domain, const CHNSInputData* inputData,
                         const SubDomainBoundary *boundaryTypeInfo)
        : Refinement (octDA, treePart, domain), inputData_(inputData),
          boundaryTypesInformation_ (boundaryTypeInfo) {
  this->initRefinement();
}

ot::OCT_FLAGS::Refine
RefineSubDA::getRefineFlags(TALYFEMLIB::FEMElm &fe, const std::vector<TALYFEMLIB::ZEROPTV> &coords) {
  /// channel_refinement_level , circle_refinement_level

  bool boundary2Refine = false;
  #if(DIM==2)
  bool bottomWall = ((coords[0](1) == BoundaryTypes::WALL::Y_MINUS)
                     or (coords[1](1) == BoundaryTypes::WALL::Y_MINUS));
  bool topWall = ((coords[2](1) == BoundaryTypes::WALL::Y_PLUS)
                  or (coords[3](1) == BoundaryTypes::WALL::Y_PLUS));
  bool leftWall = ((coords[0](0) == BoundaryTypes::WALL::X_MINUS)
                   or (coords[2](0) == BoundaryTypes::WALL::X_MINUS));
  bool rightWall = ((coords[1](0) == BoundaryTypes::WALL::X_PLUS)
                    or (coords[3](0) == BoundaryTypes::WALL::X_PLUS));
  #endif

  #if(DIM==3)
  bool bottomWall = ((coords[0](1) == BoundaryTypes::WALL::Y_MINUS)
                     or (coords[1](1) == BoundaryTypes::WALL::Y_MINUS));
  bool topWall = ((coords[2](1) == BoundaryTypes::WALL::Y_PLUS)
                  or (coords[3](1) == BoundaryTypes::WALL::Y_PLUS));
  bool leftWall = ((coords[0](0) == BoundaryTypes::WALL::X_MINUS)
                   or (coords[2](0) == BoundaryTypes::WALL::X_MINUS));
  bool rightWall = ((coords[1](0) == BoundaryTypes::WALL::X_PLUS)
                    or (coords[3](0) == BoundaryTypes::WALL::X_PLUS));
  #endif

  if (bottomWall){
    boundary2Refine = true;
  } else if (topWall){
    boundary2Refine = true;
  } else if (leftWall){
    boundary2Refine = true;
  } else if (rightWall){
    boundary2Refine = true;
  }

  if((this->m_BoundaryOctant) and (this->m_level < inputData_->mesh_def.refine_lvl_channel_wall)) {
    return ot::OCT_FLAGS::Refine::OCT_REFINE;
  } else if ((boundary2Refine) and (this->m_level < inputData_->mesh_def.refine_lvl_channel_wall)) {
    return ot::OCT_FLAGS::Refine::OCT_REFINE;
  } else{
    return ot::OCT_FLAGS::Refine::OCT_NO_CHANGE;
  }
}
