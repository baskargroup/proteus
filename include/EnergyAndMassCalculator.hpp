//
// Created by makrand on 4/15/21.
//


#pragma once
#include "Traversal/Traversal.h"
#include <DataTypes.h>
#include <TimeInfo.h>
#include "CHNSInputData.h"

class EnergyAndMassCalculator: Traversal {
	CHNSInputData* input_data_;
	CHNSFreeEnergy *free_energy_;
	DENDRITE_REAL * energyAndMass_ = nullptr;
	DENDRITE_REAL *val_c_ = nullptr;
	DENDRITE_REAL *val_d_ = nullptr;
	DENDRITE_REAL time_;
	DENDRITE_REAL timeStep_;

	void calculateEnergyAndMass(DENDRITE_REAL * globalError);

 public:
	static const DENDRITE_UINT noOfTerms = 5;
	/**
	 * @brief Constructor
	 * @param octDA
	 * @param v vector
	 * @param f analytic function
	 * @param time time
	 */
	EnergyAndMassCalculator(DA *octDA, const std::vector<TREENODE> & treePart, const VecInfo & chns, const DomainExtents &
	domain, const DENDRITE_REAL time, const DENDRITE_REAL timeStep, CHNSFreeEnergy *free_energy, CHNSInputData *inputData)
	:Traversal(octDA,treePart,chns,domain){
		energyAndMass_ = new DENDRITE_REAL[5];
		memset(energyAndMass_,0, sizeof(DENDRITE_REAL)*5);
		val_c_ = new DENDRITE_REAL[chns.ndof];
		val_d_ = new DENDRITE_REAL[chns.ndof*DIM];
		time_ = time;
		timeStep_ = timeStep;
		free_energy_ = free_energy;
		input_data_ = inputData;
		this->traverse();
	}

	/**
	 * @brief Overriding the traversal class operation
	 * @param fe
	 * @param values
	 */
	void traverseOperation(TALYFEMLIB::FEMElm & fe, const PetscScalar * values) override;

	/**
	 * @brief Prints the  L2 error
	 */
	void getEnergyAndMass();

	/**
	 * @brief returns the L2 error
	 * @param error Returns the L2 error. This must be allocated.
	 */
	void getEnergyAndMass(DENDRITE_REAL * error);

	~EnergyAndMassCalculator();

};

void EnergyAndMassCalculator::traverseOperation(TALYFEMLIB::FEMElm &fe, const PetscScalar *values) {
	/// Non dimensional parameters
	const double Cn = input_data_->Cn;
	const double Pe = input_data_->Pe;
	const double We = input_data_->We;
	const double Fr = input_data_->Fr;

	/// Physical parameters
	const double rhoH = input_data_->rhoH;
	const double rhoL = input_data_->rhoL;

	const int nsd = DIM;
	double dphi1_dphi1 = 0.0;
	ZEROPTV dphi;
	ZEROPTV velocity;
	double velNorm;

	enum energyMassTermIdx {
			BULK_ENERGY = 0,
			SURFACE_ENERGY = 1,
			KINETIC_ENERGY = 2,
			POTENTIAL_ENERGY = 3,
			PHI_VOLUME = 4
	};


	const auto bulkEnergy = [&]() {
			/// Bulk energy density is free energy function at every intergration
			/// point. To bulk energy we integrate over the volume
			double val = (1.0 / (We * Cn)) * free_energy_->f(val_c_[CHNSNodeData::PHI]);
			return (val);
	};
	/// surface energy is integral ((Cn/2We) * laplacian(phi) dV)
	/// over the domain
	const auto surfaceEnergy = [&]() {
			/// Calculate grad{phi}
			for (int j = 0; j < nsd; j++) {
				dphi(j) = val_d_[DIM*CHNSNodeData::PHI + j];
			}
			/// Calculate the norm of the grad{phi}
			dphi1_dphi1 = dphi.norm();
			double val = (0.5 * (Cn / We) * (dphi1_dphi1) * (dphi1_dphi1));
			return (val);
	};
	/// Calculate the kinetic energy
	const auto kineticEnergy = [&]() {
			/// Get velocities
			for (int dir = 0; dir < nsd; dir++) {
				velocity(dir) = val_c_[dir];
			}
			/// Calculate the norm of velocity
			velNorm = velocity.norm();
			/// calculate dimensionless mixture density
			double rhoMix = ((rhoH - rhoL) / 2) * val_c_[CHNSNodeData::PHI] + ((rhoH + rhoL) / 2);
			double val = 0.5 * rhoMix * velNorm * velNorm;
			return (val);
	};

	/// phi on the intergration point for mass conservation
	const auto phiVolume = [&]() {
			return (val_c_[CHNSNodeData::PHI]);
	};

	/// Calculate the local potential energy
	/// Calculate the kinetic energy
	const auto potentialEnergy = [&](double y) {
			/// Calculate the height for potential energy as current y coordinate
			/// - lowest y coordinate in the mesh
			double height = y - input_data_->lowestLoc;
			double val = 0.0;
			if (input_data_->Fr == 0.0) {
				val = 0.0;
			} else {
				double rhoMix = ((rhoH - rhoL) / 2) * val_c_[CHNSNodeData::PHI] + ((rhoH + rhoL) / 2);
				val = (1.0 / Fr) * rhoMix * height;
			}
			return (val);
	};


	const int maxTermsInEnergy = 5;
	const DENDRITE_UINT ndof = this->getNdof(); /// degrees of freedom on a quadrature pt
	while (fe.next_itg_pt()) {
		calcValueFEM(fe, ndof, values, val_c_);
		calcValueDerivativeFEM(fe,ndof,values,val_d_);
		energyAndMass_[BULK_ENERGY] += bulkEnergy() * fe.detJxW(); // bulk energy
		energyAndMass_[SURFACE_ENERGY] += surfaceEnergy() * fe.detJxW();
		energyAndMass_[KINETIC_ENERGY] += kineticEnergy() * fe.detJxW();
		energyAndMass_[POTENTIAL_ENERGY] += potentialEnergy(fe.position().y()) * fe.detJxW();
		energyAndMass_[PHI_VOLUME] += phiVolume() * fe.detJxW();
	}
}

void EnergyAndMassCalculator::getEnergyAndMass() {
	DENDRITE_REAL * globalEnergyAndMass = new DENDRITE_REAL[noOfTerms];
	calculateEnergyAndMass(globalEnergyAndMass);
	if (not(TALYFEMLIB::GetMPIRank())) {
		std::cout << "Energy and Mass : ";
		for (int termIndex = 0; termIndex < noOfTerms; termIndex++) {
			std::cout << std::scientific << globalEnergyAndMass[termIndex] << " ";
		}
		std::cout << std::defaultfloat << "\n";
	}

	delete [] globalEnergyAndMass;

}

void EnergyAndMassCalculator::getEnergyAndMass(DENDRITE_REAL *error) {
	calculateEnergyAndMass (error);
}

EnergyAndMassCalculator::~EnergyAndMassCalculator() {
	delete [] energyAndMass_;
	delete [] val_c_;
	delete [] val_d_;
}

void EnergyAndMassCalculator::calculateEnergyAndMass(DENDRITE_REAL *globalError) {
	MPI_Reduce(energyAndMass_, globalError, noOfTerms, MPI_DOUBLE, MPI_SUM, 0, this->m_octDA->getCommActive());
}


