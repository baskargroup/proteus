//
// Created by makrand on 3/17/21.
//

#ifndef CHNS_BLOCK_PROJECTION_CHNSNODEDATA_H
#define CHNS_BLOCK_PROJECTION_CHNSNODEDATA_H

#include <exception>
#include <assert.h>
#include <DataTypes.h>
class CHNSNodeData {
 public:

	/// NS degrees of freedom
	static constexpr unsigned int NS_DOF = DIM + 1;
	/// number of variables for PNP
	static constexpr unsigned int CH_DOF = 2;
	/// number of  variables in the NSHTNodeData
	static constexpr unsigned int NUM_VARS = NS_DOF + CH_DOF;
	/// Store the values of the degrees of freedom at the current timestep (n)
	DENDRITE_REAL u[NUM_VARS];
	/// Store the values at the degrees of freedom at the previous timestep (n-1)
	DENDRITE_REAL u_pre1[NUM_VARS];
	/// Store the values at the degrees of freedom at the second previous timestep (n-2)
	DENDRITE_REAL u_pre2[NUM_VARS];
	/// Store the values at the degrees of freedom at the third previous timestep (n-3)
	DENDRITE_REAL u_pre3[NUM_VARS];

	enum Vars: DENDRITE_UINT {
			/// Current time step
			VEL_X = 0,
			VEL_Y = 1,
#if (DIM == 3)
			VEL_Z         = 2,
#endif
			PRESSURE = DIM,
			PHI = DIM + 1,
			MU = DIM + 2,

			/// n-1 timestep
			VEL_X_PRE1 = NUM_VARS + 0,
			VEL_Y_PRE1 = NUM_VARS + 1,
#if (DIM == 3)
			VEL_Z_PRE1     = NUM_VARS + 2,
#endif
			PRESSURE_PRE1 = NUM_VARS + DIM,
			PHI_PRE1 = NUM_VARS + DIM + 1,
			MU_PRE1 = NUM_VARS + DIM + 2,

			/// n-2 timestep
			VEL_X_PRE2 = (2 * NUM_VARS) + 0,
			VEL_Y_PRE2 = (2 * NUM_VARS) + 1,
#if (DIM == 3)
			VEL_Z_PRE2    = 2*NUM_VARS + 2,
#endif
			PRESSURE_PRE2 = (2 * NUM_VARS) + DIM,
			PHI_PRE2 = (2 * NUM_VARS) + DIM + 1,
			MU_PRE2 = (2 * NUM_VARS) + DIM + 2,

			/// n -3 timestep
			VEL_X_PRE3 = (3 * NUM_VARS) + 0,
			VEL_Y_PRE3 = (3 * NUM_VARS) + 1,
#if (DIM == 3)
			VEL_Z_PRE3    = 3*NUM_VARS + 2,
#endif
			PRESSURE_PRE3 = (3 * NUM_VARS) + DIM,
			PHI_PRE3 = (3 * NUM_VARS) + DIM + 1,
			MU_PRE3 = (3 * NUM_VARS) + DIM + 2,
	};

	CHNSNodeData() {
		std::memset (u, 0, sizeof (DENDRITE_REAL) * NUM_VARS);
		std::memset (u_pre1, 0, sizeof (DENDRITE_REAL) * NUM_VARS);
		std::memset (u_pre2, 0, sizeof (DENDRITE_REAL) * NUM_VARS);
		std::memset (u_pre3, 0, sizeof (DENDRITE_REAL) * NUM_VARS);
	}
	/**
	 * Returns reference to the given value in the object
	 *
	 * @param index the index of the desired item
	 * @return reference to the desired data item
	 */
	inline double &value(int index) {
		assert (index >= 0 && index < NUM_VARS * 4);
		if (index >= 0 && index < NUM_VARS) {
			return u[index];
		}
		/// If the index is greater than NUM_VARS then assign the numbers to u_n
		if (index >= NUM_VARS && index < NUM_VARS * 2) {
			return u_pre1[index - NUM_VARS];
		}
		if (index >= NUM_VARS * 2 && index < NUM_VARS * 3) {
			return u_pre2[index - 2 * NUM_VARS];
		}
		if (index >= NUM_VARS * 3 && index < NUM_VARS * 4) {
			return u_pre3[index - 3 * NUM_VARS];
		}
		TALYFEMLIB::TALYException () << "Invalid variable index!";
	}

	inline double value(int index) const {
		return const_cast<CHNSNodeData *>(this)->value (index);
	}

	/**
	 * Returns the name of the given data value in the object
	 * @param index the index of the desired item nsame
	 * @return name of the specified data item
	 */
	static const char *name(int index) {
		switch (index) {
		case VEL_X: return "vel_x";
		case VEL_Y: return "vel_y";
#if(DIM == 3)
			case VEL_Z: return "vel_z";
#endif
		case PRESSURE: return "pressure";
		case PHI: return "phi";
		case MU: return "mu";
		default: throw std::runtime_error ("Invalid CHNSNodeData index");
		}
		return nullptr;
	}

	/**
	 * Returns the number of the data items in the object
	 * @return number of the data items in the object
	 */
	static int valueno() {
		return NUM_VARS;
	}
};
#endif //CHNS_BLOCK_PROJECTION_CHNSNODEDATA_H
