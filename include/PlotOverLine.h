//
// Created by maksbh on 7/5/21.
//

#ifndef PROTEUS_PLOTOVERLINE_H
#define PROTEUS_PLOTOVERLINE_H
#include <Traversal/Traversal.h>
struct valueCoordTuple{
  DENDRITE_REAL coordinate[DIM];
  DENDRITE_REAL phiVal;
  static MPI_Datatype dataType(){
    static bool         first = true;
    static MPI_Datatype _datatype;
    if (first){
      first = false;
      MPI_Type_contiguous(sizeof(valueCoordTuple), MPI_BYTE, &_datatype);
      MPI_Type_commit(&_datatype);
    }
    return _datatype;
  }
};

class PlotOverLine: public Traversal{

  std::vector<valueCoordTuple> valuesOnCenterlineLocalProcess;
  DENDRITE_REAL Xline[DIM - 1];
public:
  PlotOverLine(DA *octDA, const std::vector<TREENODE> & treePart, const VecInfo & chns, const DomainExtents &domain);
  void traverseOperation(TALYFEMLIB::FEMElm &fe, const PetscScalar *values) override;
  void printToFile(const TimeInfo & ti,bool ifAsciiWrite);
};
PlotOverLine::PlotOverLine(DA *octDA, const std::vector<TREENODE> & treePart, const VecInfo & chns, const DomainExtents &domain)
:Traversal(octDA,treePart,chns,domain){
  valuesOnCenterlineLocalProcess.clear();
  for(int dim = 0; dim < DIM - 1; dim++){
    Xline[dim] = domain.physicalDADomain.min[dim] + (domain.physicalDADomain.max[dim] - domain.physicalDADomain.min[dim])/2.0;
  }


  this->traverse();
}

void PlotOverLine::traverseOperation(TALYFEMLIB::FEMElm &fe, const PetscScalar *values) {

   const DENDRITE_UINT nPe = this->m_octDA->getNumNodesPerElement();
   const DENDRITE_UINT eleOrder = this->m_octDA->getElementOrder();
   const auto & coords = this->m_coords;
   // Just check if th bottom most point lie on the X = Lx/2
   bool pointOnLine = true;
   for(int dim = 0; dim < DIM - 1; dim++){
     pointOnLine = pointOnLine and (FEQUALS(coords[dim],Xline[dim]));
   }
   if(pointOnLine){
     // If bottom most lie on the line, then we can push two points into the vector.
     // We are only looking at the points on lower coordinate ends to prevent too many duplication
//     for(int i = 0; i < eleOrder+1; i++){
       const DENDRITE_UINT nodeID = 0; // This is valid for 3D also as we only care about Z-.
       valueCoordTuple tuple{};
       std::memcpy(tuple.coordinate,&coords[DIM*nodeID], sizeof(DENDRITE_REAL)*DIM);
       tuple.phiVal = values[nodeID*this->getNdof()];
       valuesOnCenterlineLocalProcess.push_back(tuple);
//     }
   }
}
void PlotOverLine::printToFile(const TimeInfo & ti, bool ifAsciiWrite ){
  DENDRITE_UINT rank = this->m_octDA->getRankActive();
  DENDRITE_UINT npes = this->m_octDA->getNpesActive();
  /// find out the number of elements which will be sent to proc = 0
  int numData = valuesOnCenterlineLocalProcess.size();

  std::vector<int> localEachProcSizeNumNodes;

  if (rank == 0) {
    localEachProcSizeNumNodes.resize(npes);
  }
  /// Get the sizes from each process to zeroeth process
  MPI_Gather(&numData, 1, MPI_INT, localEachProcSizeNumNodes.data(), 1, MPI_INT, 0, this->m_octDA->getCommActive());

  /// Add everything up
  PetscInt totalGlobalSize = std::accumulate (localEachProcSizeNumNodes.begin(), localEachProcSizeNumNodes.end(), 0);

  /// Allocate a global chunk of memory for MPI to push into

  std::vector<valueCoordTuple> globalValuesCoordTuple;
  if (rank == 0) {
    /// resize using the total number of nodes being gathered
    globalValuesCoordTuple.resize(totalGlobalSize);
 	}
  /// MPI estimate of displacement (nothing to do with physical displacement)
  std::vector<int> displacement(npes, 0);

  // Prefix sum
  if (rank == 0) {
    for (int i = 1; i < displacement.size(); i++) {
      displacement[i] = displacement[i - 1] + localEachProcSizeNumNodes[i - 1];
    }
  }

  /// Now gather everything
  MPI_Gatherv(valuesOnCenterlineLocalProcess.data (),valuesOnCenterlineLocalProcess.size (),valueCoordTuple::dataType(),
              globalValuesCoordTuple.data (),localEachProcSizeNumNodes.data (),displacement.data (),valueCoordTuple::dataType(),
              0, this->m_octDA->getCommActive());

  if (rank == 0) {
    if (ifAsciiWrite) {
      static constexpr int precisionNumber = 6;
      std::string timeStepNumber = std::string(precisionNumber - std::to_string(ti.getTimeStepNumber()).length(), '0') +
                                   std::to_string(ti.getTimeStepNumber());
      std::string fileName = "dataOverLine_" + timeStepNumber + ".dat";
      int defalultPrecision = std::cout.precision();
      std::ofstream fout(fileName.c_str());
      fout << "# Time = " << std::setprecision(10) << ti.getCurrentTime() << std::setprecision(defalultPrecision)
           << "\n";
      for (const auto &tuple:globalValuesCoordTuple) {
        for(int dim = 0; dim < DIM; dim++){
          fout << tuple.coordinate[dim] << " ";
        }
        fout << tuple.phiVal << "\n";

      }

      fout.close();
    } else {
      static constexpr int precisionNumber = 6;
      std::string timeStepNumber = std::string(precisionNumber - std::to_string(ti.getTimeStepNumber()).length(), '0') +
                                   std::to_string(ti.getTimeStepNumber());
      std::string fileName = "dataOverLine_" + timeStepNumber + ".dat";
      FILE *outfile = fopen(fileName.c_str(), "w");
      double time = ti.getCurrentTime();
      fwrite(&time, sizeof(double), 1, outfile);
      fwrite(globalValuesCoordTuple.data(), sizeof(valueCoordTuple), globalValuesCoordTuple.size(), outfile);
      fclose(outfile);
    }
  }
}
#endif //PROTEUS_PLOTOVERLINE_H
