//
// Created by makrand on 3/17/21.
//

#ifndef DENDRITEKT_VELOCITYUPDATE_H
#define DENDRITEKT_VELOCITYUPDATE_H
#include <talyfem/fem/cequation.h>
#include "CHNSNodeData.hpp"
#include "CHNSInputData.h"
#include "CHNSUtils.h"
#include <VMSparams.h>
class VelocityUpdateEquation: public TALYFEMLIB::CEquation<CHNSNodeData> {
 private:
	const CHNSInputData *idata_;
	CHNSIntegrandsGenForm *integrandsGenForm_;


 public:
	explicit VelocityUpdateEquation(const CHNSInputData *idata, CHNSIntegrandsGenForm *integrandsGenForm)
			: TALYFEMLIB::CEquation<CHNSNodeData> (false, TALYFEMLIB::kAssembleGaussPoints) {
		idata_ = idata;
		integrandsGenForm_ = integrandsGenForm;
	}


	void Solve(double dt, double t) override {
		assert(false);
	}

	void Integrands(const TALYFEMLIB::FEMElm &fe, TALYFEMLIB::ZeroMatrix<double> &Ae,
	                TALYFEMLIB::ZEROARRAY<double> &be) override {
		assert(false);
	}

	void Integrands_Ae(const TALYFEMLIB::FEMElm &fe, TALYFEMLIB::ZeroMatrix<double> &Ae, double* h) {
		integrandsGenForm_->getIntegrandsVelocityUpdateAe(fe, Ae, dt_, t_);
	}

	void Integrands_be(const TALYFEMLIB::FEMElm &fe, TALYFEMLIB::ZEROARRAY<double> &be, double* h) {
		integrandsGenForm_->getIntegrandsVelocityUpdatebe(fe, be, dt_, t_);
	}

};
#endif //DENDRITEKT_VELOCITYUPDATE_H
