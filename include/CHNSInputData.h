//
// Created by makrand on 3/17/21.
//

#pragma once
#include <talyfem/input_data/input_data.h>
#include <talyfem/talyfem.h>
#include <DataTypes.h>
#include <point.h>
#include <DendriteUtils.h>
#include <map>
#include <vector>
#include <string>

/* Function maximum definition */
/* x, y and z are parameters */
double maximum(ZEROPTV pt) {

	DENDRITE_REAL max = pt.x(); /* assume x is the largest */

	if (pt.y() > max) { /* if y is larger than max, assign y to max */
		max = pt.y();
	} /* end if */

	if (pt.z() > max) { /* if z is larger than max, assign z to max */
		max = pt.z();
	} /* end if */

	return max; /* max is the largest value */
} /* end function maximum */

/// Meshdef struct for subDA
struct MeshDef {
		/// Rectangular wake Region
		struct WAKE_REGION {
				DENDRITE_REAL start[DIM];
				DENDRITE_REAL end[DIM];
				DENDRITE_UINT refineLvl;
		};
		std::vector<WAKE_REGION> wakeRefineRegions;
		DomainInfo fullDADomain; /// The domain from which its carved out.
		DomainInfo physDomain;/// The actual SubDAdomain

		TALYFEMLIB::ZEROPTV channel_max; ///< end of domain

		/// Usage: dimensions which are non-periodic are set to zero.  Periodic dimensions are set to ratio of their
		// length to largest length (in some direction).  Periodic dimension is always set to a power of 2.
		/// e.g for a 2d mesh with dimensions [4,2],
		/// 1) setting periodicScale to [0.0,0.5] would set Y periodicity
		/// 2) setting periodicScale to [1.0, 0.0] would set X periodicity
		/// 3) setting periodicScale to [1.0, 0.5] would set both X and Y periodicity
		/// 4) setting periodicScale to [0.0, 0.0] would no periodicity
		/// Note that the values passed are the ratios to the largest dimension, and powers of 2.
		TALYFEMLIB::ZEROPTV periodicScale; ///< Variable for specifying periodicity, iniliazed to zero for non-periodicity


		///< Dendro options
		DENDRITE_UINT refine_lvl_base;
		DENDRITE_UINT refine_lvl_channel_wall;
		DENDRITE_UINT refine_lvl_object;
		DENDRITE_UINT refine_lvl_interface;
		DENDRITE_REAL refine_interface_tol; ///< if abs(phi) < interface_tol, refine to
		/// refine_lvl_interface

		bool refine_walls = false; ///< whether or not fx_refine refines at channel_min/channel_max
		bool add_objects = false;
		bool refine_object = false;
		bool wake_refine = false;
		TALYFEMLIB::ZEROPTV object_centre;
		DENDRITE_REAL object_radius;
		DENDRITE_REAL sizeOfBox;


		bool refineLightUniform = false; ///< whether to refine uniformly in the light phase
		/// whether to pad the refinement to prevent hanging nodes
		bool refinementPadding = false;


		void read_from_config(const libconfig::Setting &root) {

			refine_lvl_base = (unsigned int) root["refine_lvl_base"];
			refine_lvl_interface = (unsigned int)root["refine_lvl_interface"];
			refine_interface_tol = (double)root["refine_interface_tol"];

			channel_max = TALYFEMLIB::ZEROPTV ((double) root["max"][0], (double) root["max"][1],
			                                   (double) root["max"][2]);
			PrintStatus ("channel_max: ", channel_max);

			if (!root.lookupValue ("refine_walls", refine_walls)) {
				refine_walls = false;
			} else {
				refine_walls = root["refine_walls"];
			}
			if (!root.lookupValue ("add_object", add_objects)) {
				add_objects = false;
			} else {
				add_objects = root["add_object"];
			}

			if (root.exists("periodicBoundariesAndScale")) {
				periodicScale = TALYFEMLIB::ZEROPTV ((double) root["periodicBoundariesAndScale"][0], (double) root["periodicBoundariesAndScale"][1],
				                                   (double) root["periodicBoundariesAndScale"][2]);
			} else {
				periodicScale = {0.0, 0.0, 0.0};
			}

			if (add_objects) {
				object_centre = TALYFEMLIB::ZEROPTV ((double) root["object_centre"][0], (double) root["object_centre"][1],
				                                     (double) root["object_centre"][2]);
				PrintStatus ("object_centre: ", object_centre);
				object_radius = (double) root["object_radius"];
				TALYFEMLIB::PrintStatus ("object_centre: ", object_radius);

				if (!root.lookupValue ("refine_object", refine_object)) {
					refine_object = false;
				} else {
					refine_object = root["refine_object"];
				}
			}

			if (refine_walls) {
				refine_lvl_channel_wall = (unsigned int) root["refine_lvl_channel_wall"];
				if (refine_lvl_base < 1 || refine_lvl_base >= 31 ||
				    refine_lvl_channel_wall < 1 || refine_lvl_channel_wall >= 31) {
					TALYFEMLIB::PrintWarning ("Invalid refine_ level - should be 1 < refine_lvl <= 31.");
				}
			}
			if (!refine_walls) {
				refine_lvl_channel_wall = refine_lvl_base;
			}
			if (!root.lookupValue ("wake_refine", wake_refine)) {
				wake_refine = false;
			} else {
				wake_refine = root["wake_refine"];
			}
			if (wake_refine) {
				const auto &cfg_wake = root["wake_refine_regions"];
				wakeRefineRegions.resize (cfg_wake.getLength ());
				for (int i = 0; i < cfg_wake.getLength (); i++) {
					for (int d = 0; d < DIM; d++) {
						wakeRefineRegions[i].start[d] = (DENDRITE_REAL) cfg_wake[i]["start"][d];
						wakeRefineRegions[i].end[d] = (DENDRITE_REAL) cfg_wake[i]["end"][d];
					}
					wakeRefineRegions[i].refineLvl = (DENDRITE_REAL) cfg_wake[i]["refine_lvl"];
				}

			}


			if (refine_object) {
				refine_lvl_object = (unsigned int) root["refine_lvl_object"];
				if (refine_lvl_base < 1 || refine_lvl_base >= 31 ||
				    refine_lvl_object < 1 || refine_lvl_object >= 31) {
					TALYFEMLIB::PrintWarning ("Invalid refine_ level - should be 1 < refine_lvl <= 31.");
				}
			}
			if (!refine_object) {
				refine_lvl_object = refine_lvl_base;
			}

			if (refine_lvl_base > refine_lvl_channel_wall) {
				TALYFEMLIB::PrintWarning ("refine_lvl_base > refine_lvl_channel_wall");
			}

			TALYFEMLIB::PrintStatus ("refine_lvl_base: ", refine_lvl_base);
			TALYFEMLIB::PrintStatus ("refine_lvl_channel_wall: ", refine_lvl_channel_wall);

			sizeOfBox = maximum (channel_max);

			fullDADomain.min.fill (0.0);
			fullDADomain.max.fill (sizeOfBox);

			physDomain.min.fill (0.0);
			physDomain.max[0] = channel_max[0];
			physDomain.max[1] = channel_max[1];
			#if (DIM == 3)
			physDomain.max[2] = channel_max[2];
			#endif

		}
};

class CHNSInputData: public TALYFEMLIB::InputData {
 public:
    static  constexpr int nsd = DIM;

	enum TIME_STEPPING: u_short {
			BDF = 0,
			THETA = 1
	};
	enum SOLVE_STRATEGY: u_short {
			SOLENOIDAL_VEL_CH = 0,
			NON_SOLENOIDAL_VEL_CH = 1
	};

	enum typeNSProjectionsolver { SemiImplicit = 0, IMEX = 1 };

	/// Declare enum to store the type of NS case
	/// Casetype for velocity cases
	enum Casetype {
			PERIODIC_HIT = 0,
			CHANNEL_FLOW = 1,
			QUIESCENT_BOX = 2,
			MANUFACTURED_SOL_TEST_NS = 3,
			PERIODIC_CHANNEL_FLOW = 4,
			Y_SHAPED_CHANNEL = 5,
			CAPILLARY_INJECTION_NS =  6,
			JACOBIAN_TEST = 7,
			FREE_SLIP_QUIESCENT_BOX = 8,
			FREE_SLIP_ALL_WALLS_QUIESCENT_BOX = 9,
			JET_NS = 10,
			COUETTE_FLOW_BOTH_PLATES_OPPOSITE = 11
	};

	/// Casetype for initial conditions of the phase field
	enum CasetypeCHinit {
			FLAT_VERT_INTERFACE = 0,
			DROP = 1,
			DROPSPLASH = 2,
			CAPILLARYWAVE = 3,
			MANUFACTURED_SOL_TEST_CH = 4,
			CAPILLARY_INJECTION_CH = 5,
			ELLIPSE = 6,
			RAYLEIGHTAYLOR = 7,
			DAM_BREAK = 8,
			JET_CH = 9,
			VERTICAL_SLUG = 10 ///< A slug which touches both top and bottom wall, and has rectangular shape
	};

	/// Declare enum to store the type of NS case
	Casetype caseTypeForNavierStokes;

	/// Declare enum to store the type of initial phase field
	CasetypeCHinit caseTypeCHinit;

	/// Setup the meshDef object for subDA parameters
	MeshDef mesh_def;

	DENDRITE_UINT elemOrder;
	DENDRITE_UINT basisRelativeOrderCH = 1;
	bool ifMatrixFree = false;
	DENDRITE_UINT nsOrder;
	/// Order of Cahn-Hilliard Solver
	DENDRITE_UINT chOrder;
	bool ifUseRotationalForm = false;

	///< Checkpointing options
	DENDRITE_UINT checkpointFrequency;
	DENDRITE_UINT numberOfBackups;

	/// Controlling coarsening Frequencies
	DENDRITE_UINT coarseningFrequency = 1;


	/// All non-dimensional numbers

	DENDRITE_REAL Re;       // Reynolds number
	DENDRITE_REAL ReLambda; // Taylor length scale based Re no (see Pope (2000))
	DENDRITE_REAL Pe;       // Diffusional Peclet number. (see Simsek et al. (2017))
	DENDRITE_REAL We;       // Weber number
	DENDRITE_REAL Cn;       // Cahn number
	DENDRITE_REAL Fr;       // Froude number

	/// Physical properties of the problem
	DENDRITE_REAL rhoH; /// specific gravity of heavier fluid
	DENDRITE_REAL rhoL; /// specific gravity of lighter fluid
	DENDRITE_REAL etaH; /// normalized viscosity of heavier fluid (normalized by
	/// viscosity of heavier fluid)
	DENDRITE_REAL etaL; /// normalized viscosity of lighter fluid (normalized by
	/// viscosity of heavier fluid)

	bool ifdphidphiForcing = true;

	bool rescale_pressure = true;


	/// VMS model parameters
	DENDRITE_REAL Ci_f;
	DENDRITE_REAL scale;

	DENDRITE_REAL timeStep;
	DENDRITE_REAL TotalTime;


	SolverOptions solverOptionsMomentum,solverOptionsPressurePoisson, solverOptionsVelocityUpdate, solverOptionsCH;
	DENDRITE_UINT pExtrapOrder = 2;
	DENDRITE_UINT manufacSolType = 2;
	bool ifRhoNuImplicit = true;
	bool ifFullyImplicitCH = true;

	bool pureCH = false;
	bool pureNS = false;

	bool ifCHSolve = true;
	bool ifNSSolve = true;


	///whether to use tau with nonDim numbers
	bool tauWithNonDimNumbers = false;
	/// whether to use timestep in tau definition
	bool tauWithTimestep = true;

	/// Order of extrapolation
	DENDRITE_UINT advectionExtrapolationOrder = 2;

	/// Integrands type
	DENDRITE_UINT integrandType;
	/// Time scheme
	TIME_STEPPING timescheme;
	/// Whether to use linearNS
	bool ifLinearNS = true;

	SOLVE_STRATEGY solveStrategy = CHNSInputData::SOLENOIDAL_VEL_CH;

	bool ifProjection = false;
	typeNSProjectionsolver solverTypeNSProjection;
	/// Whether to use Temam's formulation
	DENDRITE_REAL Ctemam = 0.0;

	/// File writing options
	DENDRITE_UINT OutputSpan; // number of timesteps between each data write (NOT time)

	/// location of the lowest point on the mesh for potential energy
	DENDRITE_REAL lowestLoc;

	/** Cahn Hilliard parameters*/
	DENDRITE_UINT free_energy_type = 1;
	DENDRITE_REAL referenceLengthScaleCn = 1.0;

	/// Declarations for casesetup parameters

	/// locations of the drop if initial condition for CH is drop
	DENDRITE_REAL dropxc;
	DENDRITE_REAL dropyc;
	DENDRITE_REAL dropzc;

	/// locations of the drop if initial condition for CH is horizontal interface
	DENDRITE_REAL vertInterfaceLoc;

	/// Parameters for the drop splash case
	DENDRITE_REAL vertInterfaceLocDS; ///< location of the interface

	/// Jet Case
	DENDRITE_REAL radius_jet;
	DENDRITE_REAL velocity_jet;

	/// location of the center of the drop
	DENDRITE_REAL dropxds;
	DENDRITE_REAL dropyds;
	DENDRITE_REAL dropzds;

	DENDRITE_REAL radiusds;

	/// Parameters for the capillary wave case
	DENDRITE_REAL amplitudeInitial;

	/// Initial conditions for 2D turbulence
	DENDRITE_UINT numberOfVortices;
	DENDRITE_REAL strengthOfVortices;
	DENDRITE_REAL widthOfVortices;

	///  Parameters for rayleighTaylor case
	ZEROPTV gaussianCenter = {0.0, 0.0,  0.0};
	double stdDevGaussian = 0.0;
	bool trigonometricInitialCondition = true;

	/// Parameters for the ellipse case
	double radiusInxDirection = 0.0;
	double radiusInyDirection = 0.0;
	double radiusInzDirection = 0.0;

	/// Parameters for the vertical slug case
	DENDRITE_REAL leftInterfaceLocation = 0.0;
	DENDRITE_REAL rightInterfaceLocation = 0.0;

	/// Parameters for the couette flow opposite plates
	DENDRITE_REAL topPlateVelocity = 1.0; ///< x-velocity of the top plate
	DENDRITE_REAL bottomPlateVelocity = -1.0; ///< y-velocity of the bottom plate

	bool doPlotOverLine = false;
	int plotOverLineFrequency = 1;
	bool plotOverLineAsciiWrite = false;

	/// Function for reading type of Navier Stokes solver
	static typeNSProjectionsolver read_NSsolver(libconfig::Setting &root, const char *name) {
		std::string str = "semiImplicit";
		// If nothing specified stays NavierStokes
		root.lookupValue (name, str);

		if (str == "semiImplicit") {
			return SemiImplicit;
		} else if (str == "imex") {
			return IMEX;
		} else {
			throw TALYFEMLIB::TALYException () << "Must specity imex or semiImplicit for projection integrands: " << name <<str;
		}
	}

	static TIME_STEPPING readTimeStepper(libconfig::Setting &root,
	                                     const char *name) {
		std::string str;
		/// If nothing specified
		if (root.lookupValue (name, str)) {
			if (str == "CN") {
				return TIME_STEPPING::THETA;
			} else if (str == "BDF") {
				return TIME_STEPPING::BDF;
			} else {
				throw TALYFEMLIB::TALYException () << "Unknown time stepping for NS: " << name << str;
			}
		} else {
			throw TALYFEMLIB::TALYException () << "Must specify timeStepping: CN or BE";
		}
	}

	static SOLVE_STRATEGY readSolveStrategy(libconfig::Setting &root, const char *name) {
		std::string str = "solenoidalVelCH";

		if (str == "solenoidalVelCH") {
			return SOLVE_STRATEGY::SOLENOIDAL_VEL_CH;
		} else if (str == "nonSolenoidalVelCH") {
			return SOLVE_STRATEGY::NON_SOLENOIDAL_VEL_CH;
		} else {
			throw TALYFEMLIB::TALYException () << "Unknown Solve Strategy: " << name << str;
		}
	}

	/// Function for reading type of Navier Stokes case
	static Casetype read_caseNS(libconfig::Setting &root, const char *name) {
		std::string str = "quiescentBox";
		// If nothing specified stays NavierStokes
		root.lookupValue(name, str);

		if (str == "periodicHIT") {
			return PERIODIC_HIT;
		} else if (str == "channelFLow") {
			return CHANNEL_FLOW;
		} else if (str == "quiescentBox") {
			return QUIESCENT_BOX;
		} else if (str == "manufacturedSolutionsTestNS") {
			return MANUFACTURED_SOL_TEST_NS;
		} else if (str == "periodicChannelFlow") {
			return PERIODIC_CHANNEL_FLOW;
		} else if (str == "yShapedChannelFlow"){
			return Y_SHAPED_CHANNEL;
		} else if (str == "capillaryInjectionNS"){
			return CAPILLARY_INJECTION_NS;
		} else if (str == "jacobianTest"){
			return JACOBIAN_TEST;
		} else if (str == "freeSlipSideQuiescentBox") {
			return FREE_SLIP_QUIESCENT_BOX;
		} else if (str == "freeSlipAllWallsQuiescentBox") {
			return FREE_SLIP_ALL_WALLS_QUIESCENT_BOX;
		} else if (str == "JET"){
			return JET_NS;
		} else if (str == "coetteFlowBothPlatesOpposite") {
			return COUETTE_FLOW_BOTH_PLATES_OPPOSITE;
		} else {
			throw TALYException() << "Unknown case name for NS:" << name << str;
		}
	}

	/// Function for reading type of PhaseField initial condition
	static CasetypeCHinit read_caseCHinit(libconfig::Setting &root,
	                                      const char *name) {
		std::string str = "FLAT_VERT_INTERFACE";

		root.lookupValue(name, str);

		if (str == "flatVertInterface") {
			return FLAT_VERT_INTERFACE;
		} else if (str == "drop") {
			return DROP;
		} else if (str == "dropSplash") {
			return DROPSPLASH;
		} else if (str == "capillaryWave") {
			return CAPILLARYWAVE;
		} else if (str == "manufacturedSolutionsTestCH") {
			return MANUFACTURED_SOL_TEST_CH;
		} else if (str == "capillaryInjectionCH"){
			return CAPILLARY_INJECTION_CH;
		} else if (str == "ellipse") {
			return ELLIPSE;
		} else if (str == "rayleighTaylor"){
			return RAYLEIGHTAYLOR;
		} else if (str == "damBreak") {
			return DAM_BREAK;
		} else if (str == "JET"){
			return JET_CH;
		} else if (str == "verticalSlug") {
			return VERTICAL_SLUG;
		} else {
			throw TALYException() << "Unknown case name:" << name << str;
		}
	}

	bool ReadFromFile(const std::string &filename = std::string ("config.txt")) {
		ReadConfigFile (filename);

		elemOrder = 1;
		ReadValue ("elemOrder" , elemOrder);
		basisRelativeOrderCH = 1;
		ReadValue ("basisRelativeOrderCH" , basisRelativeOrderCH);
		ifMatrixFree = false;
		ReadValue ("ifMatrixFree", ifMatrixFree);

		/// SubDA (channel parameters)
		mesh_def.read_from_config(cfg.getRoot()["mesh"]);

		solverTypeNSProjection = read_NSsolver (cfg.getRoot(), "typeNSProjectionsolver"); // read which projection solver

		/// Read the caseType for Nav Stokes
		caseTypeForNavierStokes = read_caseNS(cfg.getRoot(), "caseType");

		/// Read the caseType for Nav Stokes
		solveStrategy = readSolveStrategy(cfg.getRoot(), "solveStrategy");

		/// Read the caseType for PNP
		caseTypeCHinit = read_caseCHinit(cfg.getRoot(), "caseTypeCHinit");

		/// File writing
		if (ReadValue("OutputSpan", OutputSpan)) {}

		// Checkpointing options
		checkpointFrequency = 1.0;
		numberOfBackups = 2.0;
		ReadValue("checkpointFrequency", checkpointFrequency);
		ReadValue("checkpointNumberOfBackups", numberOfBackups);

		ReadValue ("coarseningFrequency",coarseningFrequency);

		/// VMS model parameters
		ReadValueRequired ("Ci_f", Ci_f);
		ReadValueRequired ("scale", scale);

		ReadValueRequired ("nsOrder", nsOrder);
		solverOptionsMomentum = read_solver_options (cfg, "solver_options_momentum");
		solverOptionsPressurePoisson = read_solver_options (cfg, "solver_options_pp");
		solverOptionsVelocityUpdate = read_solver_options (cfg, "solver_options_vupdate");
		solverOptionsCH = read_solver_options(cfg, "solver_options_ch");

		if (ReadValue ("ifLinearNS", ifLinearNS)) {}

		if (ReadValue ("ifNSSolve",ifNSSolve)){}
		if (ReadValue ("ifCHSolve", ifCHSolve)){}


		ifUseRotationalForm = false;
		if (ReadValue ("ifUseRotationalForm", ifUseRotationalForm)) {}
		pExtrapOrder = 1;
		if (ReadValue ("PressureExtrapolationOrder", pExtrapOrder)) {}
		advectionExtrapolationOrder = 2;
		ReadValue("advectionExtrapolationOrder", advectionExtrapolationOrder);

		timescheme = BDF; // default method
		std::string time_scheme_name ("bdf");
		if (ReadValue ("timeScheme", time_scheme_name)) {
			if (time_scheme_name == "theta") {
				timescheme = THETA;
			}
		}
		ReadValue ("dt",timeStep);
		ReadValue ("totalTime",TotalTime);

		if (ReadValue("ifProjection", ifProjection)) {
		}
		ReadValue("Ctemam_", Ctemam);
		ReadValue("ifRhoNuImplicit", ifRhoNuImplicit);
    //
		/// Get the coordinates of sphere if the initial condition for CH is a drop
		if (caseTypeCHinit == CHNSInputData::DROP) {
			/// Coordinates of center for drop in a channel
			ReadValueRequired("dropxc", dropxc);
			ReadValueRequired("dropyc", dropyc);
			ReadValueRequired("dropzc", dropzc);
			ReadValueRequired("radiusds", radiusds);
		} else if (caseTypeCHinit == CHNSInputData::FLAT_VERT_INTERFACE) {
			/// Coordinates of location of the flat interface
			ReadValueRequired("vertInterfaceLoc", vertInterfaceLoc);
		} else if (caseTypeCHinit == CHNSInputData::DAM_BREAK) {
			/// Coordinates of location of the flat interface
			ReadValueRequired("vertInterfaceLoc", vertInterfaceLoc);
		} else if (caseTypeCHinit == CHNSInputData::DROPSPLASH) {
			/// Coordinates of location of the drop center and the flat interface

			ReadValueRequired("verInterfaceLocDS", vertInterfaceLocDS);

			ReadValueRequired("dropxds", dropxds);

			ReadValueRequired("dropyds", dropyds);

			ReadValueRequired("dropzds", dropzds);

			ReadValueRequired("radiusds", radiusds);
		} else if (caseTypeCHinit == CHNSInputData::CAPILLARYWAVE) {
			/// Get the initial amplitude of the interface
			ReadValueRequired("amplitudeInitial", amplitudeInitial);
			ReadValueRequired("vertInterfaceLoc", vertInterfaceLoc);
		} else if (caseTypeCHinit == CHNSInputData::RAYLEIGHTAYLOR) {
			/// Get the initial amplitude of the interface
			ReadValue ("trigonometricInitialCondition", trigonometricInitialCondition);
#if (DIM == 2) // 2d case requires input, 3D case is standard
			ReadValueRequired("amplitudeInitial", amplitudeInitial);
			ReadValueRequired("vertInterfaceLoc", vertInterfaceLoc);
#endif
			ReadValueRequired("vertInterfaceLoc", vertInterfaceLoc);
			if (!trigonometricInitialCondition){
				gaussianCenter = ZEROPTV((double)cfg.getRoot()["gaussianCenter"][0], (double)cfg.getRoot()["gaussianCenter"][1],
				                         (double)cfg.getRoot()["gaussianCenter"][2]);
				ReadValueRequired("stdDevGaussian", stdDevGaussian);
			}
		} else if (caseTypeCHinit == CHNSInputData::ELLIPSE) {
			ReadValueRequired("dropxc", dropxc);
			ReadValueRequired("dropyc", dropyc);
			ReadValueRequired("dropzc", dropzc);
			ReadValueRequired("radiusInxDirection", radiusInxDirection);
			ReadValueRequired("radiusInyDirection", radiusInyDirection);
			ReadValueRequired("radiusInzDirection", radiusInzDirection);
		} else if (caseTypeCHinit == CHNSInputData::JET_CH){
 			ReadValueRequired("radius_jet", radius_jet);
 			ReadValueRequired("velocity_jet", velocity_jet);
		} else if (caseTypeCHinit == CHNSInputData::VERTICAL_SLUG) {
			ReadValueRequired("leftInterfaceLocation", leftInterfaceLocation);
			ReadValueRequired("rightInterfaceLocation", rightInterfaceLocation);
		} else if (caseTypeForNavierStokes == CHNSInputData::COUETTE_FLOW_BOTH_PLATES_OPPOSITE) {
			/// couette flow parameters
			ReadValueRequired("bottomPlateVelocity", bottomPlateVelocity);
			ReadValueRequired("topPlateVelocity", topPlateVelocity);
		}

		/// CH parameters
		if (ReadValue("free_energy_type", free_energy_type)) {}
		if (ReadValue("chOrder", chOrder)) {}
		if (ReadValue("nsOrder", nsOrder)){}
		if (ReadValue("referenceLengthScaleCn", referenceLengthScaleCn)) {}
		// ======
		if (ReadValue("pExtrapOrder", pExtrapOrder)) {
		}
		/// Read which type of manufactured solutions to use: 1: is non-zero slip vel, 2: is zero slip vel (H^1_0)
		ReadValue("manufacSolType", manufacSolType);

		ReadValue("ifRhoNuImplicit", ifRhoNuImplicit);

		ReadValue("ifFullyImplicitCH", ifFullyImplicitCH);


		/// Non dimensional parameters for CHNS
		if (caseTypeForNavierStokes == CHNSInputData::PERIODIC_HIT) {
			ReadValueRequired("Re_lambda", ReLambda);
		} else {
			ReadValueRequired("Re", Re);
		}
		if (ReadValue("Pe", Pe)) {
		}
		if (ReadValue("We", We)) {
		}
		if (ReadValue("Cn", Cn)) {
		}
		if (ReadValue("Fr", Fr)) {
		}

		/// Physical properties
		if (ReadValue("rhoH", rhoH)) {
		}
		if (ReadValue("rhoL", rhoL)) {
		}
		if (ReadValue("etaH", etaH)) {
		}
		if (ReadValue("etaL", etaL)) {
		}
		/// For energy calculations provide the lowest point 'y' point in the mesh
		/// ( useful for potential energy calculation

		if (ReadValue("ifdphidphiForcing", ifdphidphiForcing)){}

		if (ReadValue("lowestLoc", lowestLoc)) {
		}

		/// Read whether to rescale pressure or not
		ReadValue("rescalePressure", rescale_pressure);

		/// Read whether to use tau with Nondim numbers
		ReadValue("tauWithNonDimNumbers", tauWithNonDimNumbers);
		
		/// Read whether to use tau with timestep term
		ReadValue("tauWithTimestep", tauWithTimestep);
		
		/// if we want to decouple CH and analyse pure CH
		ReadValue("pureCH", pureCH);

		/// if we want to decouple NS and analyse pure NS
		ReadValue("pureNS", pureNS);

		if(ReadValue("doPlotOverLine",doPlotOverLine)){
      if(ReadValue("plotOverLineFrequency",plotOverLineFrequency)){}
      if(ReadValue("plotOverLineAsciiWrite",plotOverLineAsciiWrite)){}
		}
		
		return true;
	}
};
