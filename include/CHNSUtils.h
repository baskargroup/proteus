//
// Created by makrand on 3/17/21.
//

#pragma once

#include <DataTypes.h>
#include <CHNSRefine.hpp>
#include <PETSc/Solver/NonLinearSolver.h>
#include <CHNSInputData.h>
#include <CHNSNodeData.hpp>
#include <Traversal/Analytic.h>
#include "RefineSubDA.h"
#include <Coarsen.hpp>
#include <IO/VTU.h>

using namespace PETSc;
/**
 * @brief perform Refinement for initial mesh generation
 * @param [in,out] octDA returns new DA after refinement
 * @param treeNode TreeNodes
 * @param inputData inputData
 */
void performRefinementSubDAInitial(DA *&octDA, DistTREE &distTree, SubDomainBoundary *subDomainBoundary, SubDomain &subDA,
                                   DomainExtents &domainExtents,
                                   const CHNSInputData &inputData,
                                   InitialConditions * initialConditions) {
	while (true) {
		CHNSRefine refine (octDA, distTree.getTreePartFiltered (), domainExtents, subDomainBoundary, initialConditions,
		                   inputData);
		DA *newDA = refine.getRefineSubDA (distTree);
		if (newDA == NULL) {
			newDA = refine.getForceRefineSubDA (distTree);
			std::swap (newDA, octDA);
			delete newDA;
			break;
		}
		std::swap (newDA, octDA);
		delete newDA;
		subDA.finalize (octDA, distTree.getTreePartFiltered (), domainExtents);
	}
}

/**
 * @brief perform Refinement/Coarsening from a vector of a field
 * @param [in,out] octDA returns new DA after refinement
 * @param treeNode TreeNodes
 * @param inputData inputData
 */
void performMeshAdaptionSubDA(DA *&octDA,
														DistTREE &distTree,
														VecInfo & vec_in,
														SubDomainBoundary *subDomainBoundary,
														SubDomain &subDA,
														DomainExtents &domainExtents,
														const CHNSInputData &inputData,
														bool refineOrNot) {
	DENDRITE_UINT counter = 0;
	if (refineOrNot){
		PrintInfo ("Refining based on phi");
	} else {
		PrintInfo ("Coarsening based on phi");
	}
	while (true) {
		CHNSRefine refine(octDA,
										distTree.getTreePartFiltered (),
										vec_in,
										domainExtents,
										subDomainBoundary,
										inputData,
										refineOrNot);
		//if (refineOrNot){
		//	refine.printefineFlags(("refineFlags"+std::to_string(counter)).c_str(),"refine",domainExtents);
		//} else {
		//	refine.printefineFlags(("refineFlags"+std::to_string(counter)).c_str(),"coarsen",domainExtents);
		//}
		DA *newDA = refine.getRefineSubDA (distTree);
		if (newDA == NULL) {
			newDA = refine.getForceRefineSubDA (distTree);
			refine.petscIntergridTransfer(newDA, distTree, vec_in.v, CHNSNodeData::CH_DOF);
			std::swap (newDA, octDA);
			delete newDA;
			counter++;
			break;
		}
		//IO::writeBoundaryElements (newDA, distTree.getTreePartFiltered(), "coarsened", "coarseDA", domainExtents);
		refine.petscIntergridTransfer(newDA, distTree, vec_in.v, CHNSNodeData::CH_DOF);
		std::swap (newDA, octDA);
		delete newDA;
		subDA.finalize (octDA, distTree.getTreePartFiltered (), domainExtents);
		counter++;
	}
}

/**
 * @brief perform Refinement/Coarsening from a vector of a field
 * @param [in,out] octDA returns new DA after refinement
 * @param treeNode TreeNodes
 * @param inputData inputData
 */
bool performMeshAdaptionSubDAIntVecs(DA *&octDA,
                              DistTREE &distTree,
                              std::vector<VecInfo> & vecs_in,
                              SubDomainBoundary *subDomainBoundary,
                              SubDomain &subDA,
                              DomainExtents &domainExtents,
                              const CHNSInputData &inputData,
                              bool refineOrNot) {
	DENDRITE_UINT counter = 0;
	if (refineOrNot){
		PrintInfo ("Refining based on phi");
	} else {
		PrintInfo ("Coarsening based on phi");
	}
	bool meshAdapted = false;

	while (true) {
		CHNSRefine meshAdaption(octDA,
		                        distTree.getTreePartFiltered (),
		                        vecs_in[6],
		                        domainExtents,
		                        subDomainBoundary,
		                        inputData,
		                        refineOrNot);
		//if (refineOrNot){
		//	meshAdaption.printefineFlags(("refineFlags"+std::to_string(counter)).c_str(),"meshAdaption",domainExtents);
		//} else {
		//	meshAdaption.printefineFlags(("refineFlags"+std::to_string(counter)).c_str(),"coarsen",domainExtents);
		//}
		DA *newDA = meshAdaption.getRefineSubDA (distTree);
		if (newDA == NULL) {
//			newDA = meshAdaption.getForceRefineSubDA (distTree);
//			meshAdaption.initPetscIntergridTransfer();
//			meshAdaption.petscIntergridTransfer(newDA, distTree, vecs_in[0].v, DIM);
//			meshAdaption.petscIntergridTransfer(newDA, distTree, vecs_in[1].v, DIM);
//			meshAdaption.petscIntergridTransfer(newDA, distTree, vecs_in[2].v, DIM);
//
//			meshAdaption.petscIntergridTransfer(newDA, distTree, vecs_in[3].v, 1);
//			meshAdaption.petscIntergridTransfer(newDA, distTree, vecs_in[4].v, 1);
//			meshAdaption.petscIntergridTransfer(newDA, distTree, vecs_in[5].v, 1);
//
//			meshAdaption.petscIntergridTransfer(newDA, distTree, vecs_in[6].v, CHNSNodeData::CH_DOF);
//			meshAdaption.petscIntergridTransfer(newDA, distTree, vecs_in[7].v, CHNSNodeData::CH_DOF);
//			meshAdaption.petscIntergridTransfer(newDA, distTree, vecs_in[8].v, CHNSNodeData::CH_DOF);
//			meshAdaption.finializeIntergridTransfer();
//			std::swap (newDA, octDA);
//			delete newDA;
//			counter++;
			break;
		}

		//IO::writeBoundaryElements (newDA, distTree.getTreePartFiltered(), "coarsened", "coarseDA", domainExtents);
		//meshAdaption.petscIntergridTransfer(newDA, distTree, vec_in.v, CHNSNodeData::CH_DOF);
		meshAdaption.initPetscIntergridTransfer();
		meshAdaption.petscIntergridTransfer(newDA, distTree, vecs_in[0].v, DIM);
		meshAdaption.petscIntergridTransfer(newDA, distTree, vecs_in[1].v, DIM);
		meshAdaption.petscIntergridTransfer(newDA, distTree, vecs_in[2].v, DIM);

		meshAdaption.petscIntergridTransfer(newDA, distTree, vecs_in[3].v, 1);
		meshAdaption.petscIntergridTransfer(newDA, distTree, vecs_in[4].v, 1);
		meshAdaption.petscIntergridTransfer(newDA, distTree, vecs_in[5].v, 1);

		meshAdaption.petscIntergridTransfer(newDA, distTree, vecs_in[6].v, CHNSNodeData::CH_DOF);
		meshAdaption.petscIntergridTransfer(newDA, distTree, vecs_in[7].v, CHNSNodeData::CH_DOF);
		meshAdaption.petscIntergridTransfer(newDA, distTree, vecs_in[8].v, CHNSNodeData::CH_DOF);
		//meshAdaption.petscIntergridTransfer (newDA, distTree, vecs_in);
		meshAdaption.finializeIntergridTransfer();
		std::swap (newDA, octDA);
		delete newDA;
		subDA.finalize (octDA, distTree.getTreePartFiltered (), domainExtents);
		counter++;
    meshAdapted = true;
	}
  return meshAdapted;
}



