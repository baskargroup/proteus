//
// Created by makrand on 3/17/21.
//
#pragma once
#include <talyfem/fem/cequation.h>
#include "CHNSNodeData.hpp"
#include "CHNSInputData.h"
#include "CHNSUtils.h"
#include "CHNSIntegrandsGenForm.hpp"
#include <VMSparams.h>
class PressurePoissonEquation: public TALYFEMLIB::CEquation<CHNSNodeData> {

 private:
	const CHNSInputData *idata_;
	CHNSIntegrandsGenForm *integrandsGenForm_;

 public:
	explicit PressurePoissonEquation(const CHNSInputData *idata, CHNSIntegrandsGenForm *integrandsGenForm)
			: TALYFEMLIB::CEquation<CHNSNodeData> (false, TALYFEMLIB::kAssembleGaussPoints) {
		idata_ = idata;
		integrandsGenForm_ = integrandsGenForm;
	}


	void Solve(double dt, double t) override {
		assert(false);
	}

	void Integrands(const TALYFEMLIB::FEMElm &fe, TALYFEMLIB::ZeroMatrix<double> &Ae,
	                TALYFEMLIB::ZEROARRAY<double> &be) override {
		assert(false);
	}

	void Integrands_Ae(const TALYFEMLIB::FEMElm &fe, TALYFEMLIB::ZeroMatrix<double> &Ae, double* h) {
		integrandsGenForm_->getIntegrandsPPAe(fe, Ae, dt_, t_);
	}

	void Integrands_be(const TALYFEMLIB::FEMElm &fe, TALYFEMLIB::ZEROARRAY<double> &be, double* h) {
		integrandsGenForm_->getIntegrandsPPbe(fe, be, dt_, t_);
	}
};
