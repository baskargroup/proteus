//
// Created by makrand on 3/17/21.
//
#pragma once
#include <Traversal/Refinement.h>
#include <CHNSInputData.h>
#include "InitialConditions.hpp"
class CHNSRefine: public Refinement{
	SubDomainBoundary * m_subDomainBoundary{};
	const CHNSInputData & inputData_;
	InitialConditions * initialConditions_;
	bool refine_;
	bool intialState_;
 public:
	/**
	 * @brief constructor
	 * @param da The old DA that you want to refine.
	 * @param domainInfo Information of physical domain
	 * @param numLocalElements number of local ekements
	 * @param boundaryRefinementLevel level of Boundary refinement
	 */
	CHNSRefine(DA *da,
	           const std::vector<TREENODE> & treenode,
	           const DomainExtents & domainExtents,
	           SubDomainBoundary * subDomainBoundary,
	           InitialConditions * initialConditionCH,
	           const CHNSInputData & inputData);

	/// Constructor for value based refinement
	CHNSRefine(DA *da,
	           const std::vector<TREENODE> & treenode,
	           VecInfo & vec_in,
	           const DomainExtents & domainExtents,
	           SubDomainBoundary * subDomainBoundary,
	           const CHNSInputData & idata_in,
	           bool refineOrCoarsen);

	/**
	 * @brief The refinement flags per element by element. If nothing is returned, its set to NO_CHANGE.
	 * @param fe
	 * @param coords vector of coords
	 * @return the flags for each elements
	 */
	ot::OCT_FLAGS::Refine getRefineFlags(TALYFEMLIB::FEMElm & fe, const std::vector<TALYFEMLIB::ZEROPTV>&coords, const PetscScalar * values)
	override;

	/**
	 * @brief The refinement flags per element by element. If nothing is returned, its set to NO_CHANGE.
	 * @param fe
	 * @param coords vector of coords
	 * @return the flags for each elements
	 */
	ot::OCT_FLAGS::Refine getRefineFlags(TALYFEMLIB::FEMElm & fe, const std::vector<TALYFEMLIB::ZEROPTV>&coords) override;

	DENDRITE_UINT getRequiredLevel( const std::vector<TALYFEMLIB::ZEROPTV>&coords, const PetscScalar * values) const;
	DENDRITE_UINT getRequiredLevel( const std::vector<TALYFEMLIB::ZEROPTV>&coords) const;
};


CHNSRefine::CHNSRefine(DA *da, const std::vector<TREENODE> & treenode, const DomainExtents & domainExtents, SubDomainBoundary *
subDomainBoundary, InitialConditions * initialConditionCH, const CHNSInputData & inputData)
		: Refinement(da,treenode,domainExtents),
		m_subDomainBoundary(subDomainBoundary),
		initialConditions_(initialConditionCH),
		inputData_(inputData){
	refine_ = true;
	intialState_ = true;
	this->initRefinement();
}

CHNSRefine::CHNSRefine(DA *da,
                       const std::vector<TREENODE> & treenode,
                       VecInfo & vec_in,
                       const DomainExtents & domainExtents,
                       SubDomainBoundary * subDomainBoundary,
                       const CHNSInputData & idata_in,
                       bool refineOrCoarsen) :
		Refinement(da, treenode, vec_in, domainExtents),
		m_subDomainBoundary(subDomainBoundary),
		inputData_(idata_in),
		refine_(refineOrCoarsen){
	this->initRefinement();
}


DENDRITE_UINT CHNSRefine::getRequiredLevel(const std::vector<TALYFEMLIB::ZEROPTV>&coords, const PetscScalar * values) const{
	enum Regions: DENDRITE_UINT {
			OBJECT = 0,
			WALLS = 1,
			WAKE = 2,
			INTERFACE = 3,
			MAX = 4
	};
	DENDRITE_UINT  refineRegions[Regions::MAX];
	std::fill(refineRegions,refineRegions+Regions::MAX, inputData_.mesh_def.refine_lvl_base);

	if(this->m_BoundaryOctant) {
		DENDRITE_UINT  id;
		bool isObjectBoundary = false;
		for (const auto &coord: coords) {
			m_subDomainBoundary->generateBoundaryFlags(coord, id);
			isObjectBoundary = m_subDomainBoundary->checkBoundaryType(BoundaryTypes::VOXEL::SPHERE) or (m_subDomainBoundary->checkBoundaryType(BoundaryTypes::VOXEL::CIRCLE))
			                   or m_subDomainBoundary->checkBoundaryType(BoundaryTypes::VOXEL::BOX);
			if (isObjectBoundary) {
				break;
			}
		}
		if(isObjectBoundary){
			refineRegions[OBJECT] = std::max(refineRegions[OBJECT], inputData_.mesh_def.refine_lvl_object);
		}
		else{
			refineRegions[WALLS] = std::max(refineRegions[WALLS], inputData_.mesh_def.refine_lvl_channel_wall);
		}
	}
	for(const auto & wakeRegions: inputData_.mesh_def.wakeRefineRegions){
		for (const auto &coord: coords) {
			bool inWake = true;
			for(int d = 0; d < DIM; d++){
				if(not(coord[d] >= wakeRegions.start[d] and coord[d] <= wakeRegions.end[d])){
					inWake = false;
					break;
				}
			}
			if(inWake){
				refineRegions[WAKE] = std::max(refineRegions[WAKE],wakeRegions.refineLvl);
			}
		}
	}
	/// Calculate phi at each node
	std::vector<double> phi(coords.size());
	DENDRITE_UINT numNodes = coords.size();
	for (DENDRITE_UINT i = 0; i < numNodes; i++ ) {
		phi[i] = values[i * CHNSNodeData::CH_DOF];
	}
	bool refine_phi = false;
	if (!inputData_.mesh_def.refineLightUniform) {
		// refine_ if any nodes are below the phi interface tolerance (abs(phi) <
		// interface_tol) or if the sign changes
		refine_phi =
				std::any_of(phi.begin(), phi.end(),
				            [&](double d) {
						            return (
								            std::fabs(d) < inputData_.mesh_def.refine_interface_tol
						            );
				            }) ||
				((*std::min_element(phi.begin(), phi.end()) > 0) !=
				 (*std::max_element(phi.begin(), phi.end()) > 0));
	} else {
		// refine_ everywhere for phi less than tolerance
		refine_phi =
				std::any_of(phi.begin(), phi.end(),
				            [&](double d) {
						            return (
								            d < inputData_.mesh_def.refine_interface_tol
						            );
				            }) ||
				((*std::min_element(phi.begin(), phi.end()) > 0) !=
				 (*std::max_element(phi.begin(), phi.end()) > 0));
	}
	if(refine_phi){
		refineRegions[INTERFACE] = std::max(refineRegions[INTERFACE],inputData_.mesh_def.refine_lvl_interface);
	}
	return *std::max_element(refineRegions,refineRegions+Regions::MAX);
}

DENDRITE_UINT CHNSRefine::getRequiredLevel(const std::vector<TALYFEMLIB::ZEROPTV>&coords) const{
	enum Regions: DENDRITE_UINT {
			OBJECT = 0,
			WALLS = 1,
			WAKE = 2,
			INTERFACE = 3,
			MAX = 4
	};
	DENDRITE_UINT  refineRegions[Regions::MAX];
	if (intialState_) {
		std::fill(refineRegions,refineRegions+Regions::MAX, inputData_.mesh_def.refine_lvl_base);
		if(this->m_BoundaryOctant) {
			DENDRITE_UINT  id;
			bool isObjectBoundary = false;
			for (const auto &coord: coords) {
				m_subDomainBoundary->generateBoundaryFlags(coord, id);
				isObjectBoundary = m_subDomainBoundary->checkBoundaryType(BoundaryTypes::VOXEL::SPHERE) or (m_subDomainBoundary->checkBoundaryType(BoundaryTypes::VOXEL::CIRCLE))
				                   or m_subDomainBoundary->checkBoundaryType(BoundaryTypes::VOXEL::BOX);
				if (isObjectBoundary) {
					break;
				}
			}
			if(isObjectBoundary){
				refineRegions[OBJECT] = std::max(refineRegions[OBJECT], inputData_.mesh_def.refine_lvl_object);
			}
			else{
				refineRegions[WALLS] = std::max(refineRegions[WALLS], inputData_.mesh_def.refine_lvl_channel_wall);
			}
		}
		for(const auto & wakeRegions: inputData_.mesh_def.wakeRefineRegions){
			for (const auto &coord: coords) {
				bool inWake = true;
				for(int d = 0; d < DIM; d++){
					if(not(coord[d] >= wakeRegions.start[d] and coord[d] <= wakeRegions.end[d])){
						inWake = false;
						break;
					}
				}
				if(inWake){
					refineRegions[WAKE] = std::max(refineRegions[WAKE],wakeRegions.refineLvl);
				}
			}
		}

		/// Calculate phi at each node
		std::vector<double> phi;
		phi.resize (coords.size());
		DENDRITE_UINT n = 0;
		for (const auto &coord: coords) {
				phi[n] = initialConditions_->ValueAtInitCH(coord);
				n++;
		}
		bool refine_phi = false;
		if (!inputData_.mesh_def.refineLightUniform) {
			// refine_ if any nodes are below the phi interface tolerance (abs(phi) <
			// interface_tol) or if the sign changes
			refine_phi =
					std::any_of(phi.begin(), phi.end(),
					            [&](double d) {
							            return (
									            std::fabs(d) < inputData_.mesh_def.refine_interface_tol
							            );
					            }) ||
					((*std::min_element(phi.begin(), phi.end()) > 0) !=
					 (*std::max_element(phi.begin(), phi.end()) > 0));
		} else {
			// refine_ everywhere for phi less than tolerance
			refine_phi =
					std::any_of(phi.begin(), phi.end(),
					            [&](double d) {
							            return (
									            d < inputData_.mesh_def.refine_interface_tol
							            );
					            }) ||
					((*std::min_element(phi.begin(), phi.end()) > 0) !=
					 (*std::max_element(phi.begin(), phi.end()) > 0));
		}
		if(refine_phi){
			refineRegions[INTERFACE] = std::max(refineRegions[INTERFACE],inputData_.mesh_def.refine_lvl_interface);
		}
		return *std::max_element(refineRegions,refineRegions+Regions::MAX);
	} else {
		throw TALYException() << "This function should only be called for setting up initial refinement";
	}
}

ot::OCT_FLAGS::Refine CHNSRefine::getRefineFlags(TALYFEMLIB::FEMElm & fe, const
std::vector<TALYFEMLIB::ZEROPTV>&coords, const PetscScalar * values){
	DENDRITE_UINT reqLevel;
	if (refine_) {
		reqLevel = getRequiredLevel(coords, values);
		if(this->m_level < reqLevel){
			return ot::OCT_FLAGS::OCT_REFINE;
		}
		return ot::OCT_FLAGS::OCT_NO_CHANGE;
	} else { /// Coarsen
		reqLevel = getRequiredLevel(coords, values);
		if(reqLevel < this->m_level) {
			return ot::OCT_FLAGS::OCT_COARSEN;
		}
		return ot::OCT_FLAGS::OCT_NO_CHANGE;
	}
}

/**
 * This is for initial refinement, it only is for refining the mesh
 * @param fe
 * @param coords
 * @return
 */
ot::OCT_FLAGS::Refine CHNSRefine::getRefineFlags(TALYFEMLIB::FEMElm & fe, const
std::vector<TALYFEMLIB::ZEROPTV>&coords){
	DENDRITE_UINT reqLevel;
	reqLevel = getRequiredLevel(coords);

	if(this->m_level < reqLevel){
		return ot::OCT_FLAGS::OCT_REFINE;
	}
	return ot::OCT_FLAGS::OCT_NO_CHANGE;
}
