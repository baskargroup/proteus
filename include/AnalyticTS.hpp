//
// Created by makrand on 3/31/21.
//


#pragma once
#include "Traversal/Traversal.h"
#include <DataTypes.h>
#include <TimeInfo.h>

class AnalyticTS: Traversal{
	DENDRITE_REAL * L2error_ = nullptr;
	DENDRITE_REAL *val_c_ = nullptr;
	typedef std::function<double(const TALYFEMLIB::ZEROPTV & pos, const DENDRITE_UINT dof, const DENDRITE_REAL time,
	                             const DENDRITE_REAL timestep)> AnalyticFunctionTimestep;
	AnalyticFunctionTimestep analFunction_;
	AnalyticType analyticType;
	DENDRITE_REAL time_;
	DENDRITE_REAL timeStep_;
	DENDRITE_UINT sdof_;

	void calculateL2error(DENDRITE_REAL * globalError);
 public:
	/**
	 * @brief Constructor
	 * @param octDA
	 * @param v vector
	 * @param f analytic function
	 * @param time time
	 */
	AnalyticTS(DA *octDA, const std::vector<TREENODE> & treePart,const VecInfo & v, const
	AnalyticFunctionTimestep &f,  const DomainExtents & domain, const DENDRITE_REAL time, const DENDRITE_REAL timeStep)
			:Traversal(octDA,treePart,v,domain){
		analyticType = AnalyticType::L2ERROR;
		analFunction_ = f;
		L2error_ = new DENDRITE_REAL[v.ndof];
		memset(L2error_,0, sizeof(DENDRITE_REAL)*v.ndof);
		val_c_ = new DENDRITE_REAL[v.ndof];
		time_ = time;
		timeStep_ = timeStep;
		sdof_ = v.nodeDataIndex;
		this->traverse();

	}

	/**
	 * @brief Overriding the traversal class operation
	 * @param fe
	 * @param values
	 */
	void traverseOperation(TALYFEMLIB::FEMElm & fe, const PetscScalar * values) override;

	/**
	 * @brief Prints the  L2 error
	 */
	void getL2error();

	/**
	 * @brief returns the L2 error
	 * @param error Returns the L2 error. This must be allocated.
	 */
	void getL2error(DENDRITE_REAL * error);

	~AnalyticTS();
};



void AnalyticTS::traverseOperation(TALYFEMLIB::FEMElm &fe, const PetscScalar *values) {

	const DENDRITE_UINT ndof = this->getNdof();
	while (fe.next_itg_pt()) {
		calcValueFEM(fe, ndof, values, val_c_);
		for (DENDRITE_UINT dof = 0; dof < ndof; dof++) {
			DENDRITE_REAL val_a = analFunction_(fe.position(), dof + sdof_, time_, timeStep_);
			L2error_[dof] += (val_c_[dof] - val_a) * (val_c_[dof] - val_a) * fe.detJxW();
		}
	}
}

void AnalyticTS::getL2error() {
	DENDRITE_REAL * globalL2Eror = new DENDRITE_REAL[this->getNdof()];
	calculateL2error(globalL2Eror);
	if (not(TALYFEMLIB::GetMPIRank())) {
		std::cout << "L2Error : ";
		for (int dof = 0; dof < this->getNdof(); dof++) {
			std::cout << std::scientific << globalL2Eror[dof] << " ";
		}
		std::cout << std::defaultfloat << "\n";
	}

	delete [] globalL2Eror;

}

void AnalyticTS::getL2error(DENDRITE_REAL *error) {
	calculateL2error(error);
}

AnalyticTS::~AnalyticTS() {
	if(analyticType == AnalyticType::L2ERROR){
		delete [] L2error_;
		delete [] val_c_;

	}
}

void AnalyticTS::calculateL2error(DENDRITE_REAL *globalL2Error) {
	MPI_Reduce(L2error_,globalL2Error,this->getNdof(),MPI_DOUBLE,MPI_SUM,0,this->m_octDA->getCommActive());
	if(TALYFEMLIB::GetMPIRank() == 0){
		for(int dof = 0; dof < this->getNdof(); dof++) {
			globalL2Error[dof] = sqrt(globalL2Error[dof]);
		}
	}
}



