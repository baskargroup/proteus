//
// Created by makrand on 3/17/21.
//
#pragma once

class MomentumEquationProjection : public TALYFEMLIB::CEquation<CHNSNodeData> {

 private:
	const CHNSInputData *idata_;
	CHNSIntegrandsGenForm* integrandsGenForm_;

 public:
	explicit MomentumEquationProjection(const CHNSInputData *idata, CHNSIntegrandsGenForm *integrandsGenForm)
			: TALYFEMLIB::CEquation<CHNSNodeData> (false, TALYFEMLIB::kAssembleGaussPoints) {
		idata_ = idata;
		integrandsGenForm_ = integrandsGenForm;
	}


	void Solve(double dt, double t) override {
		assert(false);
	}

	void Integrands(const TALYFEMLIB::FEMElm &fe, TALYFEMLIB::ZeroMatrix<double> &Ae,
	                TALYFEMLIB::ZEROARRAY<double> &be) override {
		assert(false);
	}

	void Integrands4side(const TALYFEMLIB::FEMElm &fe, int sideInd, TALYFEMLIB::ZeroMatrix<double> &Ae, TALYFEMLIB::ZEROARRAY<double> &be, double* h) {
		assert(false);
	}


	void Integrands_Ae(const TALYFEMLIB::FEMElm &fe, TALYFEMLIB::ZeroMatrix<double> &Ae, double* h) {
		/// Call the appropriate integrands from CHNSIntegrandsGenForm class;
		integrandsGenForm_->getIntegrandsVelocityPredictionAe(fe, Ae, dt_, t_);
	}

	void Integrands_be(const TALYFEMLIB::FEMElm &fe, TALYFEMLIB::ZEROARRAY<double> &be, double* h) {
		/// Call the appropriate integrands from CHNSIntegrandsGenForm class;
		integrandsGenForm_->getIntegrandsVelocityPredictionbe(fe, be, dt_, t_);
	}

	void Integrands4side_Ae(const TALYFEMLIB::FEMElm &fe, const DENDRITE_UINT sideInd, const DENDRITE_UINT id, TALYFEMLIB::ZeroMatrix<double> &Ae, double* h) {
		throw std::runtime_error("Unreachable statement");
	}

	void Integrands4side_be(const TALYFEMLIB::FEMElm &fe, const DENDRITE_UINT sideInd, const DENDRITE_UINT id, TALYFEMLIB::ZEROARRAY<double> &be, double* h) {
		throw std::runtime_error("Unreachable statement");
	}
};




