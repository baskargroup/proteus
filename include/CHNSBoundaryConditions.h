//
// Created by makrand on 3/17/21.
//

#pragma once

#include <TimeInfo.h>
#include <PETSc/BoundaryConditions.h>
#include <DataTypes.h>
#include <Boundary/SubDomainBoundary.h>
#include "CHNSInputData.h"
#include "CHNSNodeData.hpp"

enum BoundaryCase : bool{
		MOMENTUM = true,
		PRESSURE = false
};

enum TypeOfNSSolver : bool{
		PSPG = true,
		PROJECTION = false
};

/**
 * Class for NS Boundary conditions.
 */
class CHNSBoundaryConditions{
private:
  SubDomainBoundary *boundaries_;
  const CHNSInputData *inputData_; /// Input Data
  const TimeInfo *ti_; /// Time Info
	typedef std::function<double(const TALYFEMLIB::ZEROPTV & pos, const DENDRITE_UINT dof, const DENDRITE_REAL time,
	                     const DENDRITE_REAL timestep)> AnalyticFunctionTimestep;
  AnalyticFunctionTimestep  analyticFunction_ = nullptr; /// Analytical function for bc setup for MMS

  /// =============================== Functions for NS bc ===========================================================///
	/**
	 * @brief returns the boundary condition for velocity and pressure for different cases
	 * @param [out] b Boundary
	 * @param pos position
	 */
	template<BoundaryCase eqType>
	void returnMMS_NSBoundary(PETSc::Boundary &b, const TALYFEMLIB::ZEROPTV &pos);

	template<BoundaryCase eqType>
	void returnChannelFlowNSBoundary(PETSc::Boundary &b, const TALYFEMLIB::ZEROPTV &pos);

	template<BoundaryCase eqType>
	void returnQuiescentBoxNS(PETSc::Boundary &b, const TALYFEMLIB::ZEROPTV &pos);

	template<BoundaryCase eqType>
	void returnFreeSlipQuiescentBoxNS(PETSc::Boundary &b, const TALYFEMLIB::ZEROPTV &pos);

	template<BoundaryCase eqType>
	void returnFreeSlipAllWallsQuiescentBoxNS(PETSc::Boundary &b, const TALYFEMLIB::ZEROPTV &pos);

	template<BoundaryCase eqType>
	void returnJetNS(PETSc::Boundary & b, const TALYFEMLIB::ZEROPTV &pos);

	template<BoundaryCase eqType>
	void returnCouetteFlowOppositePlatesNS(PETSc::Boundary &b, const TALYFEMLIB::ZEROPTV &pos);

	/// ============================== Functions for CH bc ===========================================================///
	/// CH bc functions
  void returnChannelCH(PETSc::Boundary & b, const TALYFEMLIB::ZEROPTV & pos);

  void returnJetCH(PETSc::Boundary & b, const TALYFEMLIB::ZEROPTV & pos);

public:
  /**
   * @brief constructor
   * @param idata input Data
   * @param ti Time Info
   */
  CHNSBoundaryConditions(SubDomainBoundary * boundary, const CHNSInputData * idata, const TimeInfo * ti);

  /** returnMMSpressureBoundary
   *  @brief sets the Analytical function for MMS case.
   *  @param f Analytical function
   */
  void setAnalyticalFunction(const AnalyticFunctionTimestep & f);

	/**
	 * @brief  returns the boundary condition for velocity (Used for projection solvers)
	 * @param [out] b  boundary condition for pressure
	 * @param pos position
	 */
	void getMomentumBoundaryCondition(PETSc::Boundary &b, const TALYFEMLIB::ZEROPTV &pos);

	/**
	 * @brief  returns the boundary condition for pressure (Used for projection solvers, where pressure is separate)
	 * @param [out] b  boundary condition for pressure
	 * @param pos position
	 */
	void getPressureBoundaryCondition(PETSc::Boundary &b, const TALYFEMLIB::ZEROPTV &pos);

  /**
   * @brief returns the boundary condition for all degrees of freedom for PNP
   * @param [out] b  boundary condition for all deg of freedom
   * @param pos position
   */
  void getCHBoundaryCondition(PETSc::Boundary &b, const TALYFEMLIB::ZEROPTV &pos);
};
CHNSBoundaryConditions::CHNSBoundaryConditions(SubDomainBoundary *boundary, const CHNSInputData *idata, const TimeInfo *ti) {
  inputData_ = idata;
  ti_ = ti; // use to implement time-dependent BC
  boundaries_ = boundary;
}

void CHNSBoundaryConditions::setAnalyticalFunction(const AnalyticFunctionTimestep & f){

  analyticFunction_ = f;

}

void CHNSBoundaryConditions::getMomentumBoundaryCondition(Boundary &b, const ZEROPTV &pos) {
	if (inputData_->caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS) {
		if ((analyticFunction_ == nullptr)) {
			throw TALYFEMLIB::TALYException () << "Please set the analytical solution ";
		}
		returnMMS_NSBoundary<BoundaryCase::MOMENTUM>(b, pos);
	} else if (inputData_->caseTypeForNavierStokes == CHNSInputData::CHANNEL_FLOW) {
		returnChannelFlowNSBoundary<BoundaryCase::MOMENTUM>(b, pos);
	} else if (inputData_->caseTypeForNavierStokes == CHNSInputData::QUIESCENT_BOX) {
		returnQuiescentBoxNS<BoundaryCase::MOMENTUM> (b, pos);
	} else if (inputData_->caseTypeForNavierStokes == CHNSInputData::FREE_SLIP_QUIESCENT_BOX) {
		returnFreeSlipQuiescentBoxNS<BoundaryCase::MOMENTUM> (b, pos);
	} else if (inputData_->caseTypeForNavierStokes == CHNSInputData::FREE_SLIP_ALL_WALLS_QUIESCENT_BOX) {
		returnFreeSlipAllWallsQuiescentBoxNS<BoundaryCase::MOMENTUM> (b, pos);
	} else if (inputData_->caseTypeForNavierStokes == CHNSInputData::JET_NS) {
		if ((analyticFunction_ == nullptr)) {
			throw TALYFEMLIB::TALYException () << "Please set the analytical solution ";
		}
		returnJetNS<BoundaryCase::MOMENTUM> (b, pos);
	} else if (inputData_->caseTypeForNavierStokes == CHNSInputData::COUETTE_FLOW_BOTH_PLATES_OPPOSITE) {
		returnCouetteFlowOppositePlatesNS<BoundaryCase::MOMENTUM>(b, pos);
	} else {
		TALYException () << "Boundary condition for this case not implemented yet";
	}
}

void CHNSBoundaryConditions::getPressureBoundaryCondition(PETSc::Boundary &b, const TALYFEMLIB::ZEROPTV &pos){
	if (inputData_->caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS) {
		if ((analyticFunction_ == nullptr)) {
			throw TALYFEMLIB::TALYException () << "Please set the analytical solution ";
		}
		returnMMS_NSBoundary<BoundaryCase::PRESSURE>(b, pos);
	} else if (inputData_->caseTypeForNavierStokes == CHNSInputData::CHANNEL_FLOW) {
		returnChannelFlowNSBoundary<BoundaryCase::PRESSURE>(b, pos);
	} else if (inputData_->caseTypeForNavierStokes == CHNSInputData::QUIESCENT_BOX) {
		returnQuiescentBoxNS<BoundaryCase::PRESSURE> (b, pos);
	} else if (inputData_->caseTypeForNavierStokes == CHNSInputData::FREE_SLIP_QUIESCENT_BOX) {
		returnFreeSlipQuiescentBoxNS<BoundaryCase::PRESSURE>(b, pos);
	} else if (inputData_->caseTypeForNavierStokes == CHNSInputData::FREE_SLIP_ALL_WALLS_QUIESCENT_BOX) {
		returnFreeSlipAllWallsQuiescentBoxNS<BoundaryCase::PRESSURE> (b, pos);
	} else if (inputData_->caseTypeForNavierStokes == CHNSInputData::JET_NS) {
		returnJetNS<BoundaryCase::PRESSURE> (b, pos);
	} else if (inputData_->caseTypeForNavierStokes == CHNSInputData::COUETTE_FLOW_BOTH_PLATES_OPPOSITE) {
		returnCouetteFlowOppositePlatesNS<BoundaryCase::PRESSURE> (b, pos);
	} else {
		TALYException () << "Boundary condition for this case not implemented yet";
	}
}

void CHNSBoundaryConditions::getCHBoundaryCondition(PETSc::Boundary &b, const TALYFEMLIB::ZEROPTV &pos){
  //unsigned int id;
  //std::cout << boundaries_->getBoundaryType(pos,id) << " " << id << "\n";

  if (inputData_->caseTypeForNavierStokes == CHNSInputData::CHANNEL_FLOW
      and inputData_->caseTypeCHinit == CHNSInputData::DROP) {
    returnChannelCH(b, pos);
  } else if (inputData_->caseTypeCHinit == CHNSInputData::JET_CH) {
    returnJetCH(b, pos);
  } else if (inputData_->caseTypeCHinit == CHNSInputData::MANUFACTURED_SOL_TEST_CH
          or inputData_->caseTypeCHinit == CHNSInputData::FLAT_VERT_INTERFACE
          or inputData_->caseTypeCHinit == CHNSInputData::DROP
          or inputData_->caseTypeCHinit == CHNSInputData::DROPSPLASH
          or inputData_->caseTypeCHinit == CHNSInputData::CAPILLARYWAVE
          or inputData_->caseTypeCHinit == CHNSInputData::ELLIPSE
          or inputData_->caseTypeCHinit == CHNSInputData::RAYLEIGHTAYLOR
          or inputData_->caseTypeCHinit == CHNSInputData::DAM_BREAK
          or inputData_->caseTypeCHinit == CHNSInputData::VERTICAL_SLUG){
  	// Do nothing so Neumann conditions
  	return;
  } else {
    throw TALYFEMLIB::TALYException() << "support for these boundary conditions not yet implemented";
  }
}

# pragma mark NS-boundary conditions

template<BoundaryCase eqType>
void CHNSBoundaryConditions::returnMMS_NSBoundary(PETSc::Boundary &b, const TALYFEMLIB::ZEROPTV &pos){
	const DENDRITE_REAL & x = pos.x();
	const DENDRITE_REAL & y = pos.y();

	if (eqType == BoundaryCase::MOMENTUM) {
		b.addDirichlet (CHNSNodeData::VEL_X, analyticFunction_ (pos, CHNSNodeData::VEL_X,
		                                                        ti_->getCurrentTime () + ti_->getCurrentStep (), ti_->getCurrentStep()));
		b.addDirichlet (CHNSNodeData::VEL_Y, analyticFunction_ (pos, CHNSNodeData::VEL_Y,
		                                                        ti_->getCurrentTime () + ti_->getCurrentStep (), ti_->getCurrentStep()));
	} else {
		/// Apply corner condition for pressure
		if ((FEQUALS(x, 0.0)) and (FEQUALS(y, 0.0))) {
			b.addDirichlet (0, 0.0);
		}
	}
}

template <BoundaryCase eqType>
void CHNSBoundaryConditions::returnChannelFlowNSBoundary(PETSc::Boundary &b, const TALYFEMLIB::ZEROPTV &pos){
#if(DIM == 3)
	const DENDRITE_REAL & x = pos.x();
	const DENDRITE_REAL & y = pos.y();
	DENDRITE_UINT id;
	boundaries_->generateBoundaryFlags(pos,id); // Generates the boundary flag
	bool inlet = boundaries_->checkBoundaryType(BoundaryTypes::WALL::X_MINUS);
	bool outlet = boundaries_->checkBoundaryType(BoundaryTypes::WALL::X_PLUS);
	bool walls = (boundaries_->checkBoundaryType(BoundaryTypes::WALL::Y_MINUS))
	             or (boundaries_->checkBoundaryType(BoundaryTypes::WALL::Y_PLUS))
	             or (boundaries_->checkBoundaryType(BoundaryTypes::WALL::Z_MINUS))
	             or (boundaries_->checkBoundaryType(BoundaryTypes::WALL::Z_PLUS));
	if (eqType == BoundaryCase::MOMENTUM){
		if(inlet){
			b.addDirichlet(CHNSNodeData::VEL_X,1.0);
			b.addDirichlet(CHNSNodeData::VEL_Y,0.0);
			b.addDirichlet(CHNSNodeData::VEL_Z,0.0);
			return;
		}
		if(walls){
			b.addDirichlet(CHNSNodeData::VEL_Y,0.0);
			b.addDirichlet(CHNSNodeData::VEL_Z,0.0);
			b.addDirichlet(CHNSNodeData::VEL_Z,0.0);
			return;
		}
	}
	else{
		if(outlet){
			// Pressure will always be 0. The dof for pressure equation is 0.
			b.addDirichlet(0,0.0);
			return;
		}
	}
#endif
#if(DIM == 2)
	const DENDRITE_REAL & x = pos.x();
	const DENDRITE_REAL & y = pos.y();
	DENDRITE_UINT id;
	boundaries_->generateBoundaryFlags(pos,id);
	bool inlet = boundaries_->checkBoundaryType(BoundaryTypes::WALL::X_MINUS);
	bool outlet = boundaries_->checkBoundaryType(BoundaryTypes::WALL::X_PLUS);
	bool walls = (boundaries_->checkBoundaryType(BoundaryTypes::WALL::Y_MINUS))
	             or (boundaries_->checkBoundaryType(BoundaryTypes::WALL::Y_PLUS));

	if (eqType == BoundaryCase::MOMENTUM){
		if(inlet){
			double H = inputData_->mesh_def.physDomain.max[1] - inputData_->mesh_def.physDomain.min[1];
			const DENDRITE_REAL u_max = 1.0;
			DENDRITE_REAL u = 4*u_max * y*(H-y)/(H*H);

			b.addDirichlet(CHNSNodeData::VEL_X, u);
			b.addDirichlet(CHNSNodeData::VEL_Y,0.0);
			return;
		}
		if(walls){
			b.addDirichlet(CHNSNodeData::VEL_X,0.0);
			b.addDirichlet(CHNSNodeData::VEL_Y,0.0);
			return;
		}
	}
	else{
		if(outlet){
			// Pressure will always be 0. The dof for pressure equation is 0.
			b.addDirichlet(0,0.0);
			return;
		}
	}
#endif
}

/// bottom half domain
template <BoundaryCase eqType>
void CHNSBoundaryConditions::returnQuiescentBoxNS(PETSc::Boundary &b, const TALYFEMLIB::ZEROPTV &pos){
	const DENDRITE_REAL & x = pos.x();
	const DENDRITE_REAL & y = pos.y();
	DENDRITE_UINT id;
	boundaries_->generateBoundaryFlags(pos,id);
#if(DIM == 3)
	bool corner = boundaries_->checkBoundaryType(BoundaryTypes::WALL::X_MINUS)
	              and boundaries_->checkBoundaryType(BoundaryTypes::WALL::Y_MINUS)
	              and boundaries_->checkBoundaryType(BoundaryTypes::WALL::Z_MINUS);
	bool walls = (
			boundaries_->checkBoundaryType(BoundaryTypes::WALL::X_MINUS)
			or (boundaries_->checkBoundaryType(BoundaryTypes::WALL::X_PLUS))
			or (boundaries_->checkBoundaryType(BoundaryTypes::WALL::Y_MINUS))
			or (boundaries_->checkBoundaryType(BoundaryTypes::WALL::Y_PLUS))
			or (boundaries_->checkBoundaryType(BoundaryTypes::WALL::Z_MINUS))
			or (boundaries_->checkBoundaryType(BoundaryTypes::WALL::Z_PLUS))
	);
	if (eqType == BoundaryCase::MOMENTUM){
		if(walls) {
			b.addDirichlet (CHNSNodeData::VEL_X, 0.0);
			b.addDirichlet (CHNSNodeData::VEL_Y, 0.0);
			b.addDirichlet (CHNSNodeData::VEL_Z,0.0);
			return;
		}
	} else {
		if (corner) {
			// Pressure will always be 0. The dof for pressure equation is 0.
			b.addDirichlet(0,0.0);
			return;
		}
	}
#endif
#if(DIM == 2)
	bool walls = (boundaries_->checkBoundaryType(BoundaryTypes::WALL::Y_MINUS))
	             or (boundaries_->checkBoundaryType(BoundaryTypes::WALL::Y_PLUS))
	             or (boundaries_->checkBoundaryType(BoundaryTypes::WALL::X_MINUS))
	             or (boundaries_->checkBoundaryType(BoundaryTypes::WALL::X_PLUS));
	bool corner = boundaries_->checkBoundaryType(BoundaryTypes::WALL::X_MINUS)
	              and boundaries_->checkBoundaryType(BoundaryTypes::WALL::Y_MINUS);
	if (eqType == BoundaryCase::MOMENTUM) {
		if (walls) {
			b.addDirichlet (CHNSNodeData::VEL_X, 0.0);
			b.addDirichlet (CHNSNodeData::VEL_Y, 0.0);
			return;
		}
	} else { /// pressure bc for projection
		/// inlet and outlet p bc
		if (corner) {
			b.addDirichlet (0, 0.0);
			return;
		}
	}
#endif
}


template <BoundaryCase eqType>
void CHNSBoundaryConditions::returnFreeSlipQuiescentBoxNS(PETSc::Boundary &b, const TALYFEMLIB::ZEROPTV &pos) {
	const DENDRITE_REAL & x = pos.x();
	const DENDRITE_REAL & y = pos.y();
	DENDRITE_UINT id;
	boundaries_->generateBoundaryFlags(pos,id);
#if(DIM == 3)
	bool corner = boundaries_->checkBoundaryType(BoundaryTypes::WALL::X_MINUS)
	              and boundaries_->checkBoundaryType(BoundaryTypes::WALL::Y_MINUS)
	              and boundaries_->checkBoundaryType(BoundaryTypes::WALL::Z_MINUS);
	bool topAndBottomWalls = (
			(boundaries_->checkBoundaryType(BoundaryTypes::WALL::Y_MINUS))
			or (boundaries_->checkBoundaryType(BoundaryTypes::WALL::Y_PLUS))
	);

	bool sideWalls = (
			boundaries_->checkBoundaryType(BoundaryTypes::WALL::X_MINUS)
			or (boundaries_->checkBoundaryType(BoundaryTypes::WALL::X_PLUS))
			or (boundaries_->checkBoundaryType(BoundaryTypes::WALL::Z_MINUS))
			or (boundaries_->checkBoundaryType(BoundaryTypes::WALL::Z_PLUS))
	);
	if (eqType == BoundaryCase::MOMENTUM){
		if(topAndBottomWalls) {
			b.addDirichlet (CHNSNodeData::VEL_X, 0.0);
			b.addDirichlet (CHNSNodeData::VEL_Y, 0.0);
			b.addDirichlet (CHNSNodeData::VEL_Z,0.0);
			return;
		}
		if(sideWalls) {
			b.addDirichlet (CHNSNodeData::VEL_X, 0.0);
			//b.addDirichlet (CHNSNodeData::VEL_Y, 0.0);
			b.addDirichlet (CHNSNodeData::VEL_Z,0.0);
			return;
		}
	} else {
		if (corner) {
			// Pressure will always be 0. The dof for pressure equation is 0.
			b.addDirichlet(0,0.0);
			return;
		}
	}
#endif
#if (DIM==2)
	bool topAndBottomWalls = (boundaries_->checkBoundaryType(BoundaryTypes::WALL::Y_MINUS))
	             or (boundaries_->checkBoundaryType(BoundaryTypes::WALL::Y_PLUS));
	bool sideWalls = (boundaries_->checkBoundaryType(BoundaryTypes::WALL::X_MINUS))
	                 or (boundaries_->checkBoundaryType(BoundaryTypes::WALL::X_PLUS));
	bool corner = boundaries_->checkBoundaryType(BoundaryTypes::WALL::X_MINUS)
	              and boundaries_->checkBoundaryType(BoundaryTypes::WALL::Y_MINUS);
	if (eqType == BoundaryCase::MOMENTUM) {
		if (topAndBottomWalls) {
			b.addDirichlet (CHNSNodeData::VEL_X, 0.0);
			b.addDirichlet (CHNSNodeData::VEL_Y, 0.0);
			return;
		}
		if (sideWalls) {
			b.addDirichlet (CHNSNodeData::VEL_X, 0.0);
			//b.addDirichlet (CHNSNodeData::VEL_Y, 0.0); /// No bc for y-velocity (Neumann zero)
			return;
		}
	} else { /// pressure bc for projection
		/// corner p bc
		if (corner) {
			b.addDirichlet (0, 0.0);
			return;
		}
	}
#endif
}

template<BoundaryCase eqType>
void CHNSBoundaryConditions::returnFreeSlipAllWallsQuiescentBoxNS(Boundary &b, const ZEROPTV &pos) {
    const DENDRITE_REAL & x = pos.x();
    const DENDRITE_REAL & y = pos.y();

    DENDRITE_UINT id;
    boundaries_->generateBoundaryFlags(pos,id);
#if(DIM == 3)
    bool corner = boundaries_->checkBoundaryType(BoundaryTypes::WALL::X_MINUS)
	              and boundaries_->checkBoundaryType(BoundaryTypes::WALL::Y_MINUS)
	              and boundaries_->checkBoundaryType(BoundaryTypes::WALL::Z_MINUS);
	bool topAndBottomWalls = (
			(boundaries_->checkBoundaryType(BoundaryTypes::WALL::Y_MINUS))
			or (boundaries_->checkBoundaryType(BoundaryTypes::WALL::Y_PLUS))
	);

	bool leftAndRightWalls = (
			boundaries_->checkBoundaryType(BoundaryTypes::WALL::X_MINUS)
			or (boundaries_->checkBoundaryType(BoundaryTypes::WALL::X_PLUS))
	);

	bool FrontAndBackWalls = (
			 (boundaries_->checkBoundaryType(BoundaryTypes::WALL::Z_MINUS))
			or (boundaries_->checkBoundaryType(BoundaryTypes::WALL::Z_PLUS))
	);

	if (eqType == BoundaryCase::MOMENTUM){
		if(topAndBottomWalls) {
			//b.addDirichlet (CHNSNodeData::VEL_X, 0.0);
			b.addDirichlet (CHNSNodeData::VEL_Y, 0.0);
			//b.addDirichlet (CHNSNodeData::VEL_Z,0.0);
			return;
		}
		if(leftAndRightWalls) {
			b.addDirichlet (CHNSNodeData::VEL_X, 0.0);
			//b.addDirichlet (CHNSNodeData::VEL_Y, 0.0);
			//b.addDirichlet (CHNSNodeData::VEL_Z,0.0);
			return;
		}
		if(FrontAndBackWalls) {
			//b.addDirichlet (CHNSNodeData::VEL_X, 0.0);
			//b.addDirichlet (CHNSNodeData::VEL_Y, 0.0);
			b.addDirichlet (CHNSNodeData::VEL_Z,0.0);
			return;
		}
	} else {
		if (corner) {
			// Pressure will always be 0. The dof for pressure equation is 0.
			b.addDirichlet(0,0.0);
			return;
		}
	}
#endif
#if (DIM==2)
    bool topAndBottomWalls = (boundaries_->checkBoundaryType(BoundaryTypes::WALL::Y_MINUS))
                             or (boundaries_->checkBoundaryType(BoundaryTypes::WALL::Y_PLUS));
    bool sideWalls = (boundaries_->checkBoundaryType(BoundaryTypes::WALL::X_MINUS))
                     or (boundaries_->checkBoundaryType(BoundaryTypes::WALL::X_PLUS));
    bool corner = boundaries_->checkBoundaryType(BoundaryTypes::WALL::X_MINUS)
                  and boundaries_->checkBoundaryType(BoundaryTypes::WALL::Y_MINUS);
    if (eqType == BoundaryCase::MOMENTUM) {
        if (topAndBottomWalls) {
            //b.addDirichlet (CHNSNodeData::VEL_X, 0.0);
            b.addDirichlet (CHNSNodeData::VEL_Y, 0.0);
            return;
        }
        if (sideWalls) {
            b.addDirichlet (CHNSNodeData::VEL_X, 0.0);
            //b.addDirichlet (CHNSNodeData::VEL_Y, 0.0); /// No bc for y-velocity (Neumann zero)
            return;
        }
    } else { /// pressure bc for projection
        /// corner p bc
        if (corner) {
            b.addDirichlet (0, 0.0);
            return;
        }
    }
#endif
}


template<BoundaryCase eqType>
void CHNSBoundaryConditions::returnJetNS(Boundary &b, const ZEROPTV &pos) {
    const DENDRITE_REAL & x = pos.x();
    const DENDRITE_REAL & y = pos.y();
    const DENDRITE_REAL & z = pos.z();
    DENDRITE_UINT id;
    
	boundaries_->generateBoundaryFlags(pos,id);

#if(DIM == 3)
    bool corner = boundaries_->checkBoundaryType(BoundaryTypes::WALL::X_MINUS)
	              and boundaries_->checkBoundaryType(BoundaryTypes::WALL::Y_MINUS)
	              and boundaries_->checkBoundaryType(BoundaryTypes::WALL::Z_MINUS);
	bool topAndBottomWalls = (
			(boundaries_->checkBoundaryType(BoundaryTypes::WALL::Y_MINUS))
			or (boundaries_->checkBoundaryType(BoundaryTypes::WALL::Y_PLUS))
	);

	bool sideWalls = (
			 (boundaries_->checkBoundaryType(BoundaryTypes::WALL::Z_MINUS))
			or (boundaries_->checkBoundaryType(BoundaryTypes::WALL::Z_PLUS))
	);
    
	bool leftWall =boundaries_->checkBoundaryType(BoundaryTypes::WALL::X_MINUS);
	bool RightWall =(boundaries_->checkBoundaryType(BoundaryTypes::WALL::X_PLUS));

	double x_min=inputData_->mesh_def.physDomain.min[0];
	double y_middle=(inputData_->mesh_def.physDomain.min[1]+inputData_->mesh_def.physDomain.max[1])/2;
	double z_middle=(inputData_->mesh_def.physDomain.min[2]+inputData_->mesh_def.physDomain.max[2])/2;

	bool inside_circle= (fabs(sqrt((x-x_min)*(x-x_min)+(y-y_middle)*(y-y_middle)+(z-z_middle)*(z-z_middle)))<inputData_->radius_jet) && leftWall;
	bool out_circle= (fabs(sqrt((x-x_min)*(x-x_min)+(y-y_middle)*(y-y_middle)+(z-z_middle)*(z-z_middle)))>=inputData_->radius_jet) && leftWall;

	if (eqType == BoundaryCase::MOMENTUM){
		if(inside_circle) {
			b.addDirichlet (CHNSNodeData::VEL_X, analyticFunction_ (pos, CHNSNodeData::VEL_X,
		                                                        ti_->getCurrentTime () + ti_->getCurrentStep (), ti_->getCurrentStep()));
			b.addDirichlet (CHNSNodeData::VEL_Y, 0.0);
			b.addDirichlet (CHNSNodeData::VEL_Z,0.0);
			return;
		} 
		
		if (out_circle){
			b.addDirichlet (CHNSNodeData::VEL_X, 0.0);
			b.addDirichlet (CHNSNodeData::VEL_Y, 0.0);
			b.addDirichlet (CHNSNodeData::VEL_Z,0.0);
			return;
		}
	} else {

		if(topAndBottomWalls) {
			b.addDirichlet(0,0.0);
			return;
		}

		if(sideWalls) {
			b.addDirichlet(0,0.0);
			return;
		}

     	if (RightWall) {
			b.addDirichlet(0,0.0);
			return;
		}   

	}
#endif
#if (DIM==2)

    bool corner = boundaries_->checkBoundaryType(BoundaryTypes::WALL::X_MINUS)
	              and boundaries_->checkBoundaryType(BoundaryTypes::WALL::Y_MINUS);
	bool topAndBottomWalls = (
			(boundaries_->checkBoundaryType(BoundaryTypes::WALL::Y_MINUS))
			or (boundaries_->checkBoundaryType(BoundaryTypes::WALL::Y_PLUS))
	);

	bool leftWall =boundaries_->checkBoundaryType(BoundaryTypes::WALL::X_MINUS);
	bool RightWall =(boundaries_->checkBoundaryType(BoundaryTypes::WALL::X_PLUS));

	double x_min=inputData_->mesh_def.physDomain.min[0];
	double y_middle=(inputData_->mesh_def.physDomain.min[1]+inputData_->mesh_def.physDomain.max[1])/2;

	bool inside_circle= (fabs(y-y_middle)<inputData_->radius_jet) && leftWall;
	bool out_circle= (fabs(y-y_middle)>=inputData_->radius_jet) && leftWall;

    if (eqType == BoundaryCase::MOMENTUM) {
		
		if(inside_circle) {
			b.addDirichlet (CHNSNodeData::VEL_X, analyticFunction_ (pos, CHNSNodeData::VEL_X,
		                                                        ti_->getCurrentTime () + ti_->getCurrentStep (), ti_->getCurrentStep()));
			b.addDirichlet (CHNSNodeData::VEL_Y, 0.0);
			return;
		} 

		if (out_circle){
			b.addDirichlet (CHNSNodeData::VEL_X, 0.0);
			b.addDirichlet (CHNSNodeData::VEL_Y, 0.0);
			return;
		}	

    } else { /// pressure bc for projection

		if(topAndBottomWalls) {
            b.addDirichlet (0, 0.0);
            return;
		}

		if (RightWall) {
            b.addDirichlet (0, 0.0);
            return;
		}

	}
#endif
}

template<BoundaryCase eqType>
void CHNSBoundaryConditions::returnCouetteFlowOppositePlatesNS(Boundary &b, const ZEROPTV &pos) {
	const DENDRITE_REAL & x = pos.x();
	const DENDRITE_REAL & y = pos.y();
	DENDRITE_UINT id;
	boundaries_->generateBoundaryFlags(pos,id);
#if(DIM == 3)
	/// Periodicity in x and z
	bool topWall = boundaries_->checkBoundaryType(BoundaryTypes::WALL::Y_PLUS);
	bool bottomWall = boundaries_->checkBoundaryType(BoundaryTypes::WALL::Y_MINUS);
	if (eqType == BoundaryCase::MOMENTUM){
		if(topWall) {
			b.addDirichlet (CHNSNodeData::VEL_X, inputData_->topPlateVelocity);
			b.addDirichlet (CHNSNodeData::VEL_Y, 0.0);
			b.addDirichlet (CHNSNodeData::VEL_Z,0.0);
			return;
		} else if (bottomWall) {
			b.addDirichlet (CHNSNodeData::VEL_X, inputData_->bottomPlateVelocity);
			b.addDirichlet (CHNSNodeData::VEL_Y, 0.0);
			b.addDirichlet (CHNSNodeData::VEL_Z,0.0);
			return;
		}
	} /// No boundary condition for pressure applied anywhere
#endif
#if(DIM == 2)
	/// Periodicity in x direction
	bool corner = boundaries_->checkBoundaryType(BoundaryTypes::WALL::X_MINUS)
			and boundaries_->checkBoundaryType(BoundaryTypes::WALL::Y_MINUS);
	bool topWall = boundaries_->checkBoundaryType(BoundaryTypes::WALL::Y_PLUS);
	bool bottomWall = boundaries_->checkBoundaryType(BoundaryTypes::WALL::Y_MINUS);
	if (eqType == BoundaryCase::MOMENTUM) {
		if (topWall) {
			b.addDirichlet (CHNSNodeData::VEL_X, inputData_->topPlateVelocity);
			b.addDirichlet (CHNSNodeData::VEL_Y, 0.0);
			return;
		}
		if (bottomWall) {
			b.addDirichlet (CHNSNodeData::VEL_X, inputData_->bottomPlateVelocity);
			b.addDirichlet (CHNSNodeData::VEL_Y, 0.0);
			return;
		}
	} else {
		/// Apply corner condition for pressure
		if ((FEQUALS(x, 1.0)) and (FEQUALS(y, 0.0))) {
			b.addDirichlet (0, 0.0);
			return;
		}
	}
#endif
}

#pragma mark CH-boundary conditions

void CHNSBoundaryConditions::returnChannelCH(PETSc::Boundary &b, const TALYFEMLIB::ZEROPTV &pos) {
  enum CH_DOF {
    PHI = 0,
    MU = 1,
  };

  DENDRITE_UINT objectID;
  boundaries_->generateBoundaryFlags(pos,objectID);
  bool leftWall = boundaries_->checkBoundaryType(BoundaryTypes::WALL::X_MINUS);
	if (leftWall) {
		b.addDirichlet (PHI, 1.0);
		b.addDirichlet (MU, 0.0);
		return;
	}
}

void CHNSBoundaryConditions::returnJetCH(PETSc::Boundary &b, const TALYFEMLIB::ZEROPTV &pos) {
  enum CH_DOF {
    PHI = 0,
    MU = 1,
  };

  DENDRITE_UINT objectID;
  boundaries_->generateBoundaryFlags(pos,objectID);

  const DENDRITE_REAL & x = pos.x();
  const DENDRITE_REAL & y = pos.y();
  const DENDRITE_REAL & z = pos.z();


	bool topAndBottomWalls = (
			(boundaries_->checkBoundaryType(BoundaryTypes::WALL::Y_MINUS))
			or (boundaries_->checkBoundaryType(BoundaryTypes::WALL::Y_PLUS))
	);
	#if(DIM == 3)
	bool sideWalls = (
			boundaries_->checkBoundaryType(BoundaryTypes::WALL::Z_MINUS)
			or (boundaries_->checkBoundaryType(BoundaryTypes::WALL::X_PLUS)) 
			or (boundaries_->checkBoundaryType(BoundaryTypes::WALL::Z_PLUS))
	);
	#endif

	#if(DIM == 2)
	bool sideWalls = boundaries_->checkBoundaryType(BoundaryTypes::WALL::X_PLUS);
	#endif

	bool leftWall = boundaries_->checkBoundaryType(BoundaryTypes::WALL::X_MINUS);

	double x_min=inputData_->mesh_def.physDomain.min[0];
	double y_middle=(inputData_->mesh_def.physDomain.min[1]+inputData_->mesh_def.physDomain.max[1])/2;


	#if(DIM == 3)
	double z_middle=(inputData_->mesh_def.physDomain.min[2]+inputData_->mesh_def.physDomain.max[2])/2;
	bool inside_circle= (fabs(sqrt((x-x_min)*(x-x_min)+(y-y_middle)*(y-y_middle)+(z-z_middle)*(z-z_middle)))<inputData_->radius_jet) && leftWall;
	bool other_sides= (fabs(sqrt((x-x_min)*(x-x_min)+(y-y_middle)*(y-y_middle)+(z-z_middle)*(z-z_middle)))>=inputData_->radius_jet);
	#endif

	#if(DIM == 2)
	bool inside_circle= (fabs(y-y_middle)<inputData_->radius_jet) && leftWall;
	bool other_sides= (fabs(y-y_middle)>=inputData_->radius_jet);
	#endif


    if(inside_circle) {
		b.addDirichlet (PHI, 1.0);
		b.addDirichlet (MU, 0.0);   
		return;
	} 


    if(other_sides) {
		b.addDirichlet (PHI, -1.0);
		b.addDirichlet (MU, 0.0);   
		return;
	}


}




