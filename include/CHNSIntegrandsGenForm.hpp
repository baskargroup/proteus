//
// Created by makrand on 3/17/21.
//

#pragma once

#include <talyfem/talyfem.h>
#include <link.h>
#include "CHNSInputData.h"
#include "CHNSNodeData.hpp"
#include "CHNSFreeEnergy.hpp"
#include <fstream>

#define CHECK_INF_NAN

class CHNSIntegrandsGenForm{
protected:
  struct VMSParams {
    DENDRITE_REAL tauM;
    DENDRITE_REAL tauC;
		DENDRITE_REAL tauPhi;

    VMSParams()
            : tauM (1.0), tauC (1.0), tauPhi(1.0) {}
  };

private:
  GridField<CHNSNodeData> *p_data_;
  CHNSInputData *input_data_;

  // this flag is responsible for switching the contribution of dp_prev, especially in PP and VU
  DENDRITE_REAL dp_prev_flag;
  // only applicable to the fine scale of the previous step in theta method
  DENDRITE_REAL res_prev_flag;
  // whether fine scale contribution goes into PP and VU
  DENDRITE_REAL stab_in_ppe_vu;
  // flag to switch on/off rotational form terms
  DENDRITE_REAL rot_form_flag;
  // Rotational form: flag for terms on the boundary
  DENDRITE_REAL rot_form_boundary_flag;
  // Theta method coefficient
  DENDRITE_REAL theta;
  // BDF coefficient vector
  std::vector<DENDRITE_REAL> bdf_c;
  // pressure extrapolation order
  DENDRITE_UINT pExtrapOrder;
  // pressure extrapolation coefficients
  std::vector<DENDRITE_REAL> p_extrap_c;
	// Ctemam_ is an alias for 0.5, originating from the Temam term = 0.5(div V)V
	DENDRITE_REAL Ctemam_;

	DENDRITE_REAL mobility;

public:
#pragma mark Constructor
  /// Class constructor
  CHNSIntegrandsGenForm(GridField<CHNSNodeData> *gf, CHNSInputData *inputData)
          : p_data_ (gf), input_data_ (inputData), bdf_c (3, 0.0), p_extrap_c (4, 0.0) {
  }

#pragma mark Internal-functions

  void setIntegrandsFlags() {
	  mobility = 1.0;
    if (input_data_->nsOrder == 1) {
      dp_prev_flag = 0.0;
    } else if (input_data_->nsOrder == 2) {
      dp_prev_flag = 1.0;
    }
	  Ctemam_ = input_data_->Ctemam;

    //dp_prev_flag = 1.0;
    res_prev_flag = 0.0;
    stab_in_ppe_vu = 1.0;

    rot_form_flag = 0.0;
    if (input_data_->ifUseRotationalForm) {
      rot_form_flag = 1.0;
    }

    rot_form_boundary_flag = 1.0;

    this->pExtrapOrder = input_data_->pExtrapOrder;

    if (input_data_->nsOrder == 1) {
      pExtrapOrder = 0;
      rot_form_flag = 0.0;
      theta = 1.0;
      bdf_c[0] = 1.0;
      bdf_c[1] = -1.0;
      bdf_c[2] = 0.0;
    } else if (input_data_->nsOrder == 2) {
      theta = 0.5;
      bdf_c[0] = 1.5;
      bdf_c[1] = -2.0;
      bdf_c[2] = 0.5;
    }

    if (pExtrapOrder == 0) {
      p_extrap_c[0] = 0.0;///< for t_current = t_n
      p_extrap_c[1] = 0.0;///< for t_n-1
      p_extrap_c[2] = 0.0;///< for t_n-2
      p_extrap_c[3] = 0.0;///< for t_n-3
    } else if (pExtrapOrder == 1) {
      p_extrap_c[0] = 0.0;///< for t_current = t_n
      p_extrap_c[1] = 1.0;///< for t_n-1
      p_extrap_c[2] = 0.0;///< for t_n-2
      p_extrap_c[3] = 0.0;///< for t_n-3
    } else if (pExtrapOrder == 2) {
      p_extrap_c[0] = 0.0;///< for t_current = t_n
      p_extrap_c[1] = 2.0;///< for t_n-1
      p_extrap_c[2] = -1.0;///< for t_n-2
      p_extrap_c[3] = 0.0;///< for t_n-3
    }
  }

	/**
 * @brief This function calculates the explicit approximation of \rho(\phi^{k+\half}) with \rho
 * (\tilde{\phi}^{k+\half}), where \tilde{\phi}^{k+\half} = (3\phi^{k} - \phi^{k-1})/2 for theta schemes
 * Or \tilde{\phi}^{k+1} = (2\phi^{k} - \phi^{k-1}) for bdf schemes
 * Note: We pull back the phi first to ensure positivity of the density field.
 * @param phi_list: a vector of prev \phi
 * @return calculated density
 */
	inline double calcExplicitDensity(const std::vector<double> &phi_list) {
		double densityExplicitAvg = 0.0;

		/// Make the bounds on phi used for evaluation for mixture density and viscosity
		std::vector<double> pulledBackPhi;
		pulledBackPhi.resize(phi_list.size());
		for (int i = 0; i < phi_list.size(); i++) {
			if (phi_list[i] >= 1.0){
				pulledBackPhi[i] = 1.0;
			} else if (phi_list[i] <= -1.0){
				pulledBackPhi[i] = -1.0;
			} else{
				pulledBackPhi[i] = phi_list[i];
			}
		}
		/// calculate alpha and beta (coefficients in mixture density)
		double alpha = (input_data_->rhoH - input_data_->rhoL) / 2.0;
		double beta = (input_data_->rhoH + input_data_->rhoL) / 2.0;

		double phiTildePulledBack = 1.0;
		if (input_data_->timescheme == CHNSInputData::THETA){
			phiTildePulledBack = 0.5 * (3.0 * pulledBackPhi[0] - pulledBackPhi[1]);
		} else if (input_data_->timescheme == CHNSInputData::BDF) {
			phiTildePulledBack = (2.0 * pulledBackPhi[0] - pulledBackPhi[1]);
		}
		densityExplicitAvg = alpha * phiTildePulledBack + beta;

		return densityExplicitAvg;
	}
	/**
	 * This function calculates the explicit approximation of \rho(\phi^{k+\half}) with \rho(\tilde{\phi}^{k+\half}),
	 * where \tilde{\phi}^{k+\half} = (3\phi^{k} - \phi^{k-1})/2 for theta schemes
	 * Or \tilde{\phi}^{k+1} = (2\phi^{k} - \phi^{k-1}) for bdf schemes
	 * Note: In this version we do not pull back phi, this is for manufactured solutions testing
	 * @param phi_list: a vector of prev \phi
	 * @return calculated density
	 */
	inline double calcExplicitDensityNoPullBack(const std::vector<double> &phi_list) {
		double densityExplicitAvg = 0.0;

		/// calculate alpha and beta (coefficients in mixture density)
		double alpha = (input_data_->rhoH - input_data_->rhoL) / 2.0;
		double beta = (input_data_->rhoH + input_data_->rhoL) / 2.0;

		double phiTilde = 1.0;
		if (input_data_->timescheme == CHNSInputData::THETA){
			phiTilde = 0.5 * (3.0 * phi_list[0] - phi_list[1]);
		} else if (input_data_->timescheme == CHNSInputData::BDF) {
			phiTilde = (2.0 * phi_list[0] - phi_list[1]);
		}

		densityExplicitAvg = alpha * phiTilde + beta;

		return densityExplicitAvg;
	}

	/**
	 * This function calculates the explicit approximation of \nu(\phi^{k+\half}) with \nu(\tilde{\phi}^{k+\half}),
	 * where \tilde{\phi}^{k+\half} = (3\phi^{k} - \phi^{k-1})/2 for theta schemes
	 * Or \tilde{\phi}^{k+1} = (2\phi^{k} - \phi^{k-1}) for bdf schemes
	 * Note: We pull back the phi first to ensure positivity of the viscosity field.
	 * @param phi_list: a vector of prev \phi
	 * @return calculated density
	 */
	inline double calcExplicitViscosity(const std::vector<double> &phi_list) {
		double viscosityExplicitAvg = 0.0;

		/// Make the bounds on phi used for evaluation for mixture density and viscosity
		std::vector<double> pulledBackPhi;
		pulledBackPhi.resize(phi_list.size());
		for (int i = 0; i < phi_list.size(); i++) {
			if (phi_list[i] >= 1.0){
				pulledBackPhi[i] = 1.0;
			} else if (phi_list[i] <= -1.0){
				pulledBackPhi[i] = -1.0;
			} else{
				pulledBackPhi[i] = phi_list[i];
			}
		}
		/// calculate alpha and beta (coefficients in mixture density)
		double alpha = (input_data_->etaH - input_data_->etaL) / 2.0;
		double beta = (input_data_->etaH + input_data_->etaL) / 2.0;

		double phiTildePulledBack = 1.0;
		if (input_data_->timescheme == CHNSInputData::THETA){
			phiTildePulledBack = 0.5 * (3.0 * pulledBackPhi[0] - pulledBackPhi[1]);
		} else if (input_data_->timescheme == CHNSInputData::BDF) {
			phiTildePulledBack = (2.0 * pulledBackPhi[0] - pulledBackPhi[1]);
		}
		viscosityExplicitAvg = alpha * phiTildePulledBack + beta;

		return viscosityExplicitAvg;
	}

	/**
	 * @brief This function calculates the explicit approximation of any field. This could be an approximation to
	 * A^{k+1} or A^{k+1/2} depending on the scheme (BDF or Theta)
	 * @param field_list: a vector of fields at prev timesteps, this list list is expected to be sorted (latest to oldest)
	 * @return calculated density
	 */
	inline double calcExplicitApproxField(const std::vector<double> &field_list) {
		double fieldExplicitAvg = 0.0;
		if (input_data_->timescheme == CHNSInputData::THETA){
			fieldExplicitAvg = 0.5 * (3.0 * field_list[0] - field_list[1]);
		} else if (input_data_->timescheme == CHNSInputData::BDF) {
			fieldExplicitAvg = (2.0 * field_list[0] - field_list[1]);
		}
		return fieldExplicitAvg;
	}

	/**
	 * @brief This function calculates the explicit approximation of avg of any field A^{k+\half} with
	 * \tilde{A}^{k+\half}), where \tilde{A}^{k+\half} = (3A^{k} - A^{k-1})/2 for theta schemes
	 * Or explicit approximation of any field A^{k+1} with  \tilde{A}^{k+\half}), where \tilde{A}^{k+1} = (2A^{k} -
	 * A^{k-1}) for bdf schemes
	 * @param field_list: a vector of fields at prev timestes, this list list is expected to be sorted (latest to oldest)
	 * @return calculated density
	 */
	inline double calcExplicitField(const std::vector<double> &field_list) {
		double fieldExplicitAvg = 0.0;
		if (input_data_->timescheme == CHNSInputData::THETA){
			fieldExplicitAvg = (2.0 * field_list[0] - field_list[1]);
		} else if (input_data_->timescheme == CHNSInputData::BDF) {
			fieldExplicitAvg = 0.5 * (3.0 * field_list[0] - field_list[1]);
		}
		return fieldExplicitAvg;
	}

	/** This function returns the linear extrapolated (explicit approximation of v^{k + \half}) velocity calculated from
	 * velocities from previous steps for theta schemes
	 * Or explicit approximation of any field v^{k + 1} with  \tilde{v}^{k+1}), where \tilde{v}^{k+1} = (2v^{k} -
	 * v^{k-1}) for bdf schemes
	 * @param vel_pre_list This vector should contain velocities at t_n, t_(n-1), t_(n-2) and so on in that order
	 * @return explicit velocity
	 */
	inline ZEROPTV calcExplicitVelocity(std::vector<ZEROPTV> &vel_pre_list) {
		ZEROPTV u_star;
		int nsd = DIM;
		for (int i = 0; i < nsd; i++) {
			if (input_data_->timescheme == CHNSInputData::THETA){
				u_star(i) = 0.5 * (3.0 * vel_pre_list[0](i) - vel_pre_list[1](i));
			} else if (input_data_->timescheme == CHNSInputData::BDF) {
				u_star(i) = (2.0 * vel_pre_list[0](i) - vel_pre_list[1](i));
			}
		}
		return u_star;
	}



#pragma mark Integrands-interface-functions NS

	/**
	 * Returns the linear system (LHS part) of the integrands for projection solvers
	 * @param fe FEMElm object for looping over elements and gauss points
	 * @param Ae Empty Ae matrix to be filled in the method
	 * @param dt timestep
	 * @param t current time
	 * @param freeEnergy: freeEnergy object to calculate free energy on the fly
	 */
	void getIntegrandsVelocityPredictionAe(const FEMElm &fe, ZeroMatrix<double> &Ae, double dt, double t) {
		setIntegrandsFlags ();
		if (input_data_->timescheme == CHNSInputData::THETA) {
			if (input_data_->solverTypeNSProjection == CHNSInputData::SemiImplicit){
				if (input_data_->ifRhoNuImplicit) {
					CHNSsemiImplicitThetaSecondOrder_ConsvEqAdded_ImplicitDensityAe(fe, Ae, dt, t);
				} else {
					CHNSsemiImplicitThetaSecondOrder_ConsvEqAddedAe(fe, Ae, dt, t);
				}
			} else if (input_data_->solverTypeNSProjection == CHNSInputData::IMEX) {
				throw TALYException () << "IMEX NS scheme not implemented yet for THETA";
			} else {
				throw TALYException () << "This type of NS scheme not implemented yet for BDF";
			}
		} else if (input_data_->timescheme == CHNSInputData::BDF) {
			throw TALYException () << "BDF schemes not implemented yet";
		} else {
			throw TALYException () << "Time scheme not implemented. Should be either \"theta\" or \"bdf\"";
		}
	}

	/**
	 * Returns the linear system (RHS part) of the integrands
	 * @param fe FEMElm object for looping over elements and gauss points
	 * @param be Empty be vector to be filled in the method
	 * @param dt timestep
	 * @param t current time
	 * @param freeEnergy: freeEnergy object to calculate free energy on the fly
	 */
	void getIntegrandsVelocityPredictionbe(const FEMElm &fe, ZEROARRAY<double> &be, double dt, double t) {
		setIntegrandsFlags ();
		if (input_data_->timescheme == CHNSInputData::THETA) {
			if (input_data_->solverTypeNSProjection == CHNSInputData::SemiImplicit){
				if (input_data_->ifRhoNuImplicit) {
					CHNSsemiImplicitThetaSecondOrder_ConsvEqAdded_ImplicitDensitybe(fe, be, dt, t);
				} else {
					CHNSsemiImplicitThetaSecondOrder_ConsvEqAddedbe(fe, be, dt, t);
				}
			} else if (input_data_->solverTypeNSProjection == CHNSInputData::IMEX) {
				throw TALYException () << "IMEX NS scheme not implemented yet for THETA";
			} else {
				throw TALYException () << "This type of NS scheme not implemented yet for THETA";
			}
		} else if (input_data_->timescheme == CHNSInputData::BDF) {
			if (input_data_->solverTypeNSProjection == CHNSInputData::SemiImplicit){
				throw TALYException () << "BDF schemes not implemented yet";
			} else if (input_data_->solverTypeNSProjection == CHNSInputData::IMEX) {
				throw TALYException () << "BDF schemes not implemented yet";
			} else {
				throw TALYException () << "This type of NS scheme not implemented yet for BDF";
			}
		} else {
			throw TALYException () << "Time scheme not implemented. Should be either \"theta\" or \"bdf\"";
		}
	}


	/** Function to return the integrands for Pressure Poisson (Just Ae)
	 * @param fe FEMElm object for looping over elements and gauss points
	 * @param Ae Empty Ae matrix to be filled in the method
	 */
	void getIntegrandsPPAe(const FEMElm &fe, ZeroMatrix<double> &Ae, const double dt, const double t) {
		setIntegrandsFlags ();
		NSIntegrandsPressurePoissonAe(fe, Ae, dt, t);
	}

	/** Function to return the integrands for Pressure Poisson (Just be)
	 * @param fe FEMElm object for looping over elements and gauss points
	 * @param be Empty be vector to be filled in the method
	 */
	void getIntegrandsPPbe(const FEMElm &fe, ZEROARRAY<double> &be, const double dt, const double t) {
		setIntegrandsFlags ();
		NSIntegrandsPressurePoissonbe(fe, be, dt, t);
	}

  /** Function to return the integrands for boundary terms for Pressure Poisson (only contributions to RHS)
	 * @param fe FEMElm object for looping over elements and gauss points
	 * @param be Empty be matrix to be filled in the method
	 */
  void getIntegrands4sidePPbe(const FEMElm &fe, int sideInd, ZEROARRAY<double> &be, const double dt, const double t) {
    setIntegrandsFlags ();
  }

	/** Function to return the integrands for Velocity update (Just Ae)
	 * @param fe FEMElm object for looping over elements and gauss points
	 * @param Ae Empty Ae matrix to be filled in the method
	 */
	void getIntegrandsVelocityUpdateAe(const FEMElm &fe, ZeroMatrix<double> &Ae, const double dt, const double t) {
		setIntegrandsFlags ();
		NSIntegrandsVelocityUpdateAe (fe, Ae, dt, t);
	}

	/** Function to return the integrands for Velocity update (Just be)
	 * @param fe FEMElm object for looping over elements and gauss points
	 * @param be Empty be matrix to be filled in the method
	 */
	void getIntegrandsVelocityUpdatebe(const FEMElm &fe, ZEROARRAY<double> &be, const double dt, const double t) {
		setIntegrandsFlags ();
		NSIntegrandsVelocityUpdatebe(fe, be, dt, t);
	}


#pragma mark Integrands-interface-functions CH

  /**
	 * Returns the linear system (LHS part) of the integrands
	 * @param fe FEMElm object for looping over elements and gauss points
	 * @param Ae Empty Ae matrix to be filled in the method
	 * @param dt timestep
	 * @param t current time
	 * @param freeEnergy: freeEnergy object to calculate free energy on the fly
	 */
  void getIntegrandsCHAe(const FEMElm &fe, ZeroMatrix<double> &Ae, double dt, CHNSFreeEnergy *freeEnergy, double t) {
    setIntegrandsFlags ();
	  if (input_data_->chOrder == 1) {
		  throw TALYException () << "This order CH formulation not implemented";
	  } else if (input_data_->chOrder == 2) {
		  /// Crank Nicholson conservative implementation
		  if (input_data_->timescheme == CHNSInputData::BDF) {
			  CHNSIntegrandsCHConsvBDF2Ae(fe, Ae, dt, freeEnergy, t);
		  } else if (input_data_->timescheme == CHNSInputData::THETA) {
		  	if (input_data_->ifFullyImplicitCH) {
				  CHNSIntegrandsCHConsvCrankNicholsonFullyImplicitAe(fe, Ae, dt, freeEnergy, t);
		  	} else {
		  		CHNSIntegrandsCHConsvCrankNicholsonAe(fe, Ae, dt, freeEnergy, t);
		  	}
		  } else {
			  throw TALYException () << "This timescheme not implemented";
		  }
	  }
  }


  /**
	 * Returns the linear system (RHS part) of the integrands
	 * @param fe FEMElm object for looping over elements and gauss points
	 * @param be Empty be vector to be filled in the method
	 * @param dt timestep
	 * @param t current time
	 * @param freeEnergy: freeEnergy object to calculate free energy on the fly
	 */
  void getIntegrandsCHbe(const FEMElm &fe, ZEROARRAY<double> &be, double dt, CHNSFreeEnergy *freeEnergy, double t) {
    setIntegrandsFlags ();
	  if (input_data_->chOrder == 1) {
		  throw TALYException () << "This order CH formulation not implemented";
	  } else if (input_data_->chOrder == 2) {
		  /// Crank Nicholson conservative implementation
		  if (input_data_->timescheme == CHNSInputData::BDF) {
			  CHNSIntegrandsCHConsvBDF2be(fe, be, dt, freeEnergy, t);
		  } else if (input_data_->timescheme == CHNSInputData::THETA) {
		  	if (input_data_->ifFullyImplicitCH){
				  CHNSIntegrandsCHConsvCrankNicholsonFullyImplicitbe(fe, be, dt, freeEnergy, t);
		  	} else {
				  CHNSIntegrandsCHConsvCrankNicholsonbe(fe, be, dt, freeEnergy, t);
		  	}
		  } else {
			  throw TALYException () << "This timescheme not implemented";
		  }
	  }
  }


  /** Function to return the integrands for boundary terms for Pressure Poisson (only contributions to RHS)
	 * @param fe FEMElm object for looping over elements and gauss points
	 * @param be Empty be matrix to be filled in the method
	 */
  void getIntegrands4sideCHAe(const FEMElm &fe, int sideInd, DENDRITE_UINT id, ZeroMatrix<double> &Ae, const double dt, const double t) {
    setIntegrandsFlags ();
    //throw TALYException () << "Integrands4side not implemented";
  }

  /** Function to return the integrands for boundary terms for Pressure Poisson (only contributions to RHS)
 * @param fe FEMElm object for looping over elements and gauss points
 * @param be Empty be matrix to be filled in the method
 */
  void getIntegrands4sideCHbe(const FEMElm &fe, const DENDRITE_UINT sideInd, const DENDRITE_UINT id, ZEROARRAY<double> &be, const double dt, const double t) {
    setIntegrandsFlags ();
	  //throw TALYException () << "Integrands4side not implemented";
  }


#pragma mark tau-calculation
	/*
	 * This is a function to calculate tau for given velocity, J, viscosity, density, useful for linearized
	 * (semi-Implicit NS)
	 * @param fe finite element object
	 * @param vel velocity for which tau needs to be calculated
	 * @param J J for which tau needs to be calculated
	 * @param phi this can be used to control tau locally according to phase field (Note: phi has to be pulled back
	 * here to ensure positivity)
	 * @param density
	 * @param viscosity
	 * @param Ci_f Coefficient to control diffusive parameter in tau
	 * @param dt timestep
	 * @return
	 */
	VMSParams calc_tau_vel(const FEMElm &fe,
	                       ZEROPTV &vel, ZEROPTV &J,
	                       double phi, double density, double viscosity,
	                       double Ci_f, double dt) const {

		double dt_ = dt;
		double Re;
		/// Get all the non dimensional numbers for the problem from input_data_
		Re = this->input_data_->Re; ///< Reynolds number
		double Pe = this->input_data_->Pe; /// Diffusional Peclet number
		double We = this->input_data_->We; /// Weber number
		double Cn = this->input_data_->Cn; /// Cahn number
		double Fr = this->input_data_->Fr; /// Froude number

		/// Physical properties
		double rhoH = input_data_->rhoH;
		double rhoL = input_data_->rhoL;
		double etaH = input_data_->etaH;
		double etaL = input_data_->etaL;
		const int nsd = DIM;

		double scale = input_data_->scale;
		VMSParams params;

		double coeff_diffusion = 1.0/Re;
		double coeff_massFlux = 1.0/(Cn * Pe);

		/// Get the cofactor matrix from FEELm class
		ZeroMatrix<double> ksiX;
		ksiX.redim(nsd, nsd);
		for (int i = 0; i < nsd; i++) {
			for (int j = 0; j < nsd; j++) {
				ksiX(i, j) = fe.cof(j, i) / fe.jacc();
			}
		}

		/// G_{ij} in equation 65 in Bazilevs et al. (2007)
		ZeroMatrix<double> Ge;
		Ge.redim(nsd, nsd);
		for (int i = 0; i < nsd; i++) {
			for (int j = 0; j < nsd; j++) {
				Ge(i, j) = 0.0;
				for (int k = 0; k < nsd; k++)
					Ge(i, j) += ksiX(k, i) * ksiX(k, j);
			}
		}

		/// Equation 67 in Bazilevs et al. (2007)
		double u_Gu = 0.0;
		for (int i = 0; i < nsd; i++) {
			for (int j = 0; j < nsd; j++) {
				u_Gu += vel(i) * Ge(i, j) * vel(j);
			}
		}

		/// Last term in Equation 63 in Bazilevs et al. (2007)
		double G_G_u = 0.0;
		for (int i = 0; i < nsd; i++) {
			for (int j = 0; j < nsd; j++) {
				//G_G_u += Ci_f * ((etaMix * etaMix) / ((rhoMix * rhoMix * Re * Re))) * Ge(i, j) * Ge(i, j);
				if (input_data_->tauWithNonDimNumbers) {
					G_G_u += Ci_f * ((viscosity * viscosity * coeff_diffusion * coeff_diffusion) / ((density * density))) * Ge(i, j) * Ge(i,j);
				} else {
					G_G_u += Ci_f * ((viscosity * viscosity) / ((density * density))) * Ge(i, j) * Ge(i,j);
				}
			}
		}

		/// Adding a scaling term based on mass flux (just for debugging not tested
		/// completely)
		double J_Gu = 0.0;
		for (int i = 0; i < nsd; i++) {
			for (int j = 0; j < nsd; j++) {
				if (input_data_->tauWithNonDimNumbers) {
					J_Gu += (coeff_massFlux / (density)) * (J(i) * Ge(i, j) * vel(j));
				} else {
					J_Gu += (1.0 / (density)) * (J(i) * Ge(i, j) * vel(j));
				}
			}
		}

		/// Calculate the tau_M based on the projections calculated, Equation 63
		/// in
		/// Bazilevs et al. (2007), here we use scale as a parameter to tune tauM,
		/// in the paper it is set to 1.
		double s = 1.0;
		double tauM = 0.0;

		//if (input_data_->caseTypeCHinit == CHNSInputData::ELLIPSE) {
		/// different function for Ali's high density ratio case
		/// high s value outside and s = 1 inside
		//	s = ((1.0 + phi)* 0.5 * input_data_->sInTauM) + 1.0;
		//} else {
		//	s = ((1.0 - phi)* 0.5 * input_data_->sInTauM) + 1.0;
		//}
		if (input_data_->tauWithTimestep) {
			params.tauM = (input_data_->scale) / (sqrt(s*((4.0 / (dt_ * dt_)) + J_Gu + u_Gu + G_G_u)));
		} else {
			params.tauM = (input_data_->scale) / (sqrt(s*(J_Gu + u_Gu + G_G_u)));
		}
		//params.tauM = (input_data_->scale) / (sqrt(s*(J_Gu + u_Gu + G_G_u)));

		/// Equation 68 in Bazilevs et al. (2007)
		ZEROARRAY<double> ge;
		ge.redim(nsd);
		for (int i = 0; i < nsd; i++) {
			ge(i) = 0.0;
			for (int j = 0; j < nsd; j++)
				ge(i) += ksiX(j, i);
		}

		/// Equation 69 Bazilevs et al. (2007)
		double g_g = 0.0;
		for (int i = 0; i < nsd; i++) {
			g_g += ge(i) * ge(i);
		}


		///Calculate phase field tau
		params.tauPhi = (input_data_->scale) / (sqrt((4.0 / (dt_ * dt_)) + u_Gu));

		params.tauC = 1.0 / (params.tauM * g_g);

		return params;
	}


#pragma mark NS-integrands-linearized-Semi-Implicit-projection
	///****************************************************************************************************************///
	///******************Implementations with custom Theta scheme for the NS block*************************************///
	///****************************************************************************************************************///
	/** This is a variational multiscale solver for CHNS with a custom IMEX scheme for time integration, using the
	 * variable density momentum residual.
	 * We have proven this scheme to be unconditionally energy stable.
	 * @param fe FEMElm object for looping over elements and gauss points
	 * @param Ae Empty Ae matrix to be filled in the method
	 * @param dt timestep
	 * @param t current time
	 * @return
	 */
	void
	CHNSsemiImplicitThetaSecondOrderAe(const FEMElm &fe, ZeroMatrix<double> &Ae, const double dt,const double t) {

		//validateVector(be);
		//PrintInfo ("position = ", fe.position (), ", iso_position = ", fe.itg_pt (), ", num_pt = ", fe
		//		.n_itg_pts (), ", current_itg_pt = ", fe.cur_itg_pt_num ());
		//PrintInfo ("Printing be");
		//if (GetMPIRank () == 0) {
		//	for (int i = 0; i < be.size (); i++) {
		//		std::cout << be (i) << " ";
		//		std::cout << "\n";
		//	}
		//}

		double dt_ = dt;
		double t_ = t + dt_; ///< current time is elapsed + dt
		int degOfFreedom = DIM;
		int nsd = DIM;
		double Ci_f = this->input_data_->Ci_f;
		int n_basis_functions = fe.nbf (); // # of basis functions
		double detJxW = fe.detJxW ();

		double Re = this->input_data_->Re; ///< Reynolds number

		double Pe = this->input_data_->Pe; /// Diffusional Peclet number
		double We = this->input_data_->We; /// Weber number
		double Cn = this->input_data_->Cn; /// Cahn number
		double Fr = this->input_data_->Fr; /// Froude number

		/// Physical properties
		double rhoH = input_data_->rhoH;
		double rhoL = input_data_->rhoL;
		double etaH = input_data_->etaH;
		double etaL = input_data_->etaL;

		/// calculate alpha and beta (coefficients in mixture density)
		double alpha = (rhoH - rhoL) / 2.0;
		double beta = (rhoH + rhoL) / 2.0;
		/// calculate gamma and kappa (coefficients in mixture density)
		double gamma = (etaH - etaL) / 2.0;
		double kappa = (etaH + etaL) / 2.0;

		/// Get phi to calculate mixture density of the form given in Abels et al.
		double phiPrev1 = this->p_data_->valueFEM (fe, CHNSNodeData::PHI_PRE1);
		double phiPrev2 = this->p_data_->valueFEM (fe, CHNSNodeData::PHI_PRE2);
		std::vector<double> phi_pre_list = {phiPrev1, phiPrev2};
		double phiExplicitAvg = calcExplicitApproxField(phi_pre_list);

		/// Calculate the explicit approximations of averaged Density and viscosity
		//double explicitAvgDensity = calcExplicitDensity(phi_pre_list);
		/// Mixture density explicit
		double explicitAvgDensity = 1.0;
		//double rhoExplicitAvg = calcExplicitDensity(phi_list);
		if (input_data_->caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS && input_data_->manufacSolType == 2) {
			explicitAvgDensity = calcExplicitDensityNoPullBack(phi_pre_list);
		} else {
			explicitAvgDensity = calcExplicitDensity(phi_pre_list);
		}
		double explicitAvgViscosity = calcExplicitViscosity(phi_pre_list);

		/// Calculate bunch of coeff
		double Coe_diff = 0.0;
		double Coe_diff_without_eta = 0.0;
		double Coe_conv = 0.0;
		double Coe_crossTerm1 = 0.0;
		double Coe_massFlux = 0.0;
		double Coe_fineScaleMassFlux = 0.0;
		double Coe_pressure = 0.0;
		double Coe_gravity = 0.0;
		double Coe_forcing_equiv = 0.0;
		double Coe_forcing = 0.0;
		double Coe_diff_phi = 0.0;
		double Coe_mu = 0.0;
		if (Re >= 1) {
			Coe_diff = explicitAvgViscosity / Re;
			Coe_diff_without_eta = 1.0 / Re;
			Coe_conv = explicitAvgDensity;
			Coe_crossTerm1 = 0.5;
			Coe_massFlux = 1.0 / Pe;
			Coe_fineScaleMassFlux = 0.5 / (Pe * explicitAvgDensity);
			if (input_data_->rescale_pressure) {
				Coe_pressure = 1.0;
			} else {
				Coe_pressure = 1.0 / We;
			}
			Coe_forcing_equiv = 1.0 / (Cn * We);
			Coe_forcing = Cn /We;
			Coe_gravity = 1.0;
			Coe_diff_phi = 1.0 / (Pe * Cn);
			Coe_mu = 1.0;
			//Coe_diff_phi = 1.0/(Pe);
			//Coe_mu = Cn;
		} else if (Re < 1) {
			Coe_diff = explicitAvgViscosity;
			Coe_diff_without_eta = 1 / Re;
			Coe_conv = explicitAvgDensity * Re;
			Coe_crossTerm1 = Re;
			Coe_massFlux = Re / Pe;
			Coe_fineScaleMassFlux = Re / (Pe * explicitAvgDensity);
			if (input_data_->rescale_pressure) {
				Coe_pressure = Re;
			} else {
				Coe_pressure = Re / We;
			}
			Coe_forcing_equiv = Re / (Cn * We);
			Coe_forcing = (Re * Cn) /We;
			Coe_gravity = Re;
			Coe_diff_phi = 1.0 / (Pe * Cn);
			Coe_mu = 1.0;
		}


		/// field variables at the previous timestep (level k), where k+1 is current level.


		ZEROPTV u_pre1, u_pre2;
		for (int i = 0; i < nsd; i++) {
			/// u_pre here is solenoidal and VEL_X_SOL, .. are accessed here
			u_pre1 (i) = p_data_->valueFEM (fe, (CHNSNodeData::VEL_X_PRE1 + i));
			/// u_pre2 here is solenoidal and VEL_X_SOL, .. are accessed here
			u_pre2 (i) = p_data_->valueFEM (fe, (CHNSNodeData::VEL_X_PRE2 + i));
		}
		/// explicit approximation of u^{k+\half}
		std::vector<ZEROPTV> vel_pre_list = {u_pre1, u_pre2};
		ZEROPTV uExplicitAvgVelocity = calcExplicitVelocity (vel_pre_list);

		/// Define velocity gradient tensor
		ZeroMatrix<double> duPrev1, duPrev2;
		duPrev1.redim (nsd, nsd);
		duPrev2.redim (nsd, nsd);
		for (int i = 0; i < nsd; i++) {
			for (int j = 0; j < nsd; j++) {
				///both of these are solenoidal
				duPrev1 (i, j) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::VEL_X_PRE1 + i, j);//< timelevel k
				duPrev2 (i, j) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::VEL_X_PRE2 + i, j);//< timelevel k-1
			}
		}

		/// Get gradient of phasefield
		ZEROPTV dphi, dphiPrev1, dphiPrev2;
		for (int i = 0; i < nsd; i++) {
			dphi (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PHI, i);
			dphiPrev1 (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PHI_PRE1, i);
			dphiPrev2 (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PHI_PRE2, i);
		}
		std::vector<ZEROPTV> gradPhi_prev_list = {dphiPrev1, dphiPrev2};
		ZEROPTV gradPhiExplicitAvg = calcExplicitVelocity (gradPhi_prev_list);

		/// Calculate the laplacian of velocity
		ZEROPTV d2uPrev1;
		/// PS: This assumes the first three degrees are always velocity, it is wise to keep it that way
		for (int dir1 = 0; dir1 < nsd; dir1++) {
			/// Summing over three directions of velocity as it is laplacian
			for (int dir2 = 0; dir2 < nsd; dir2++) {
				d2uPrev1 (dir1) += p_data_->value2DerivativeFEM (fe, CHNSNodeData::VEL_X_PRE1 + dir1, dir2, dir2);
			}
		}


		double p_prev = p_data_->valueFEM (fe, CHNSNodeData::PRESSURE_PRE1);
		double p_prev2 = p_data_->valueFEM (fe, CHNSNodeData::PRESSURE_PRE2);
		/// See general formulation given in equation 3.13, 3.14 in Guermond et al. 2006
		/// This controls how many time points of pressure are use to approximate grad{p} term in the velocity predictor.
		double pStar = p_extrap_c[1] * p_prev + p_extrap_c[2] * p_prev2;
		//pStar = this->p_data_->valueFEM (fe, CHNSNodeData::PRESSURE_MANUFAC);

		/// Get gradient of pressure at previous timestep and calculate the gradient of pressure according to 3.14 in
		// Guermond 2006
		ZEROPTV dpPrev, dpPrev2, dpStar;
		for (int i = 0; i < nsd; i++) {
			dpPrev (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PRESSURE_PRE1, i);
			dpPrev2 (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PRESSURE_PRE2, i);
			dpStar (i) = p_extrap_c[1] * dpPrev (i) + p_extrap_c[2] * dpPrev2 (i);
			//dpStar(i) = this->p_data_->valueDerivativeFEM(fe, CHNSNodeData::PRESSURE_MANUFAC, i);
		}


		/// Calculate the Diffusional fluxes from Cahn Hilliard (J in Abels et al.)
		ZEROPTV JExplicitAvg, JExact;
		ZEROPTV gradMu, gradmuPrev1, gradmuPrev2, gradmuExact;
		for (int i = 0; i < nsd; i++) {
			gradMu (i) = p_data_->valueDerivativeFEM (fe, CHNSNodeData::MU, i);
			gradmuPrev1 (i) = p_data_->valueDerivativeFEM (fe, CHNSNodeData::MU_PRE1, i);
			gradmuPrev2 (i) = p_data_->valueDerivativeFEM (fe, CHNSNodeData::MU_PRE2, i);
		}
		std::vector<ZEROPTV> gradMu_prev_list = {gradmuPrev1, gradmuPrev2};
		ZEROPTV gradMuExplicitAvg = calcExplicitVelocity (gradMu_prev_list);
		/// We can compute explicit approximation of J^{k+\half} similar to velocity
		for (int i = 0; i < nsd; i++) {
			JExplicitAvg (i) = ((rhoL - rhoH) / 2) * mobility * gradMuExplicitAvg (i);
			JExact (i) = ((rhoL - rhoH) / 2) * mobility * gradmuExact (i);
		}

		/// -------------------------------------body forces ------------------------------------------------------///

		/// Contribution of gravity in terms of Froude number
		ZEROPTV gravity (0.0, 0.0, 0.0);
		gravity.x () = 0.0;
		/// if Fr is specified to be zero in the config file turn gravity off
		if (Fr < 1e-10) {
			gravity.y () = 0;
		} else {
			gravity.y () = -(Coe_gravity / Fr);
		}
		gravity.z () = 0.0;

		/// External forcing: In the current implementation this term is non-zero
		/// only in the case of Manufactured solutions test of CHNS and periodic
		/// channel case
		ZEROPTV forcingNS (0.0, 0.0, 0.0);
		if (input_data_->caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS) {
			forcingNS = calcForcingManufacturedSolutionNS (fe, t_);
		} else if (input_data_->caseTypeForNavierStokes == CHNSInputData::PERIODIC_CHANNEL_FLOW) {
			throw TALYException () << "Not yet implemented forcing for periodic pressure driven flow needs to be added";
		} else {
			forcingNS = {0.0, 0.0, 0.0};
		}

		/// External forcing at t = k-1: In the current implementation this term is non-zero
		/// only in the case of Manufactured solutions test of CHNS and periodic
		/// channel case
		ZEROPTV forcingNSprev1 (0.0, 0.0, 0.0);
		if (input_data_->caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS) {
			forcingNSprev1 = calcForcingManufacturedSolutionNS (fe, t_ - dt_);
		} else if (input_data_->caseTypeForNavierStokes == CHNSInputData::PERIODIC_CHANNEL_FLOW) {
			throw TALYException () << "Not yet implemented forcing for periodic pressure driven flow needs to be added";
		} else {
			forcingNS = {0.0, 0.0, 0.0};
		}


		///---------------------------------VMS parameter settings---------------------------------------------------///
		VMSParams vms_params_current = calc_tau_vel (fe, uExplicitAvgVelocity, JExplicitAvg, phiExplicitAvg,
		                                             explicitAvgDensity, explicitAvgViscosity, Ci_f, dt);
		double tauM = vms_params_current.tauM;
		///---------------------------------VMS parameters calculated------------------------------------------------///

		for (int a = 0; a < n_basis_functions; a++) {
			///-------------------------------------Calculating the Elemental operator-------------------------------------///

			/// Auxiliary term useful for calculating things in jacobian (\partial_j{w_i}, v_j)
			double crossTermVelocityPart = 0.0;
			for (int j = 0; j < nsd; j++) {
				crossTermVelocityPart += fe.dN (a, j) * uExplicitAvgVelocity (j);
			}
			double massFluxCrossContrib = 0.0;
			for (int j = 0; j < nsd; j++) {
				massFluxCrossContrib += fe.dN (a, j) * JExplicitAvg (j);
			}


			/// crosstermPrefix =  0.5 * \partial_j{w_i} \tilde{v_j}^{k+\half} \tau_M :  summed over j for each i
			double crosstermPrefix = Coe_crossTerm1 * crossTermVelocityPart * tauM;

			/// massFluxCrossTermPrefix = Coe_fineScaleMassFlux * \partial_j{w_i} \tilde{J_j}^{k+\half} \tau_M :
			/// summed over j for each i
			/// Where, Coe_fineScaleMassFlux = 0.5/(Pe \tilde{\rho}^{k+1/2})
			///        massFluxCrossContrib  = \partial_j{w_i} \tilde{J_j}^{k+\half} = fe.dN(a, j) * JExplicitAvg(j)
			double massFluxCrossTermPrefix = Coe_fineScaleMassFlux * massFluxCrossContrib * tauM;

			for (int b = 0; b < n_basis_functions; b++) {
				/// Convection term averaged: u_j \partial_j{ \delta{v_i} }
				double convAvg = 0.0;
				for (int j = 0; j < nsd; j++) {
					convAvg += 0.5 * uExplicitAvgVelocity (j) * fe.dN (b, j);
				}

				///J_j \partial_j{ \delta{v_i} }
				double massFlux = 0.0;
				for (int j = 0; j < nsd; j++) {
					massFlux += 0.5 * JExplicitAvg (j) * fe.dN (b, j);
				}

				/// Contribution of laplacian of velocity(diffusion) to the diagonal
				/// \partial_j{ \partial_j{ \delta{v_i} } }
				double diffJ = 0.0;
				for (int j = 0; j < nsd; j++) {
					diffJ += 0.5 * fe.d2N (b, j, j);
				}

				/// \partial_j{ \phi } \partial_j{ v_i }
				double diffJCrossterm = 0.0;
				for (int j = 0; j < nsd; j++) {
					diffJCrossterm += 0.5 * gradPhiExplicitAvg (j) * fe.dN (b, j);
				}

				double convConsv = 0.0;
				for (int j = 0; j < nsd; j++) {
					convConsv += fe.dN (a, j) * uExplicitAvgVelocity (j) * 0.5 * fe.N (b);
				}

				double massFluxConsv = 0.0;
				for (int j = 0; j < nsd; j++) {
					massFluxConsv += fe.dN (a, j) * JExplicitAvg (j) * 0.5 * fe.N (b);
				}


				/// Adding terms to the Elemental matrix.
				for (int i = 0; i < nsd; i++) {
					Ae ((degOfFreedom * a) + i, (degOfFreedom * b) + i) +=
							///(w_i, \tilde{\rho}^{k+\half} \partial_t(v_i))
							/// Note: Coe_conv = \tilde{\rho}^{k+\half}
							(fe.N (a) * Coe_conv * ((fe.N (b)) / dt_)) * detJxW
							//+(fe.N(a) * uExplicitAvgVelocity(i) * alpha * ((fe.N(b))/ dt_)) * detJxW
							///(w_i, \tilde{\rho}^{k+\half} \tilde{v_j}^{k+\half} 0.5 * \partial_j(v_i)
							///since convAvg = 0.5 * \tilde{v_j}^{k+\half} \partial_j{v_i} for a fixed i
							+ (fe.N(a) * Coe_conv * convAvg) * detJxW
							//- (Coe_conv * convConsv) * detJxW
							///(1/Pe)(w_i, \tilde{v_j}^{k+\half} \partial_j(v_i))
							/// since massFluxAvg = 0.5 * \tilde{J_j}^{k+\half} \partial_j(v_i) for a fixed i
							+ (fe.N(a) * Coe_massFlux * massFlux) * detJxW;
					//- (Coe_massFlux * massFluxConsv) * detJxW;

					for (int j = 0; j < nsd; j++) {
						///Diffusion term
						Ae ((degOfFreedom * a) + i, (degOfFreedom * b) + i) +=
								///(\partial_jw_i, 0.5 * (eta(\phi)/Re)\eta \partial_j{v_i^{k+1}})
								/// The other half of corresponding to above term given by
								/// (\partial_jw_i, 0.5 * (eta(\phi)/Re)\eta\partial_j{v_i^{k}}) will go in be
								(fe.dN (a, j) * Coe_diff * 0.5 * fe.dN (b, j)) * detJxW;
					}
				}

				//debug: test for NAN and inf
				//validateMatrix(Ae);
				/// crossTerm 1: This term is written as,
				/// (\partial_j{w_i}, 0.5 * \tilde{v_j}^{k+\half} \tau_M R_M(\phi, v_i,p)),
				/// (only implicit terms on left hand side)

				///Note:
				/// crosstermPrefix =  0.5 * \partial_j{w_i} \tilde{v_j}^{k+\half} \tau_M :  summed over j for each i
				/// double crosstermPrefix = Coe_crossTerm1 * crossTermVelocityPart * tauM;
				for (int i = 0; i < nsd; i++) {
					Ae ((degOfFreedom * a) + i, (degOfFreedom * b) + i) +=
							///(\partial_j{w_i}, \tilde{v_j}^{k+\half} 0.5 * tau_M \rho \partial_t{ \delta{v_i} }),
							/// since crosstermVelocityAvg = \partial_j{w_i} \tilde{v_j}^{k+\half}
							/// 0.5 comes from Crank-Nicholson type averaging
							(crosstermPrefix * Coe_conv * ((fe.N (b)) / dt_)) * detJxW
							///(\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M \rho v_j \partial_j{\delta v_i}),
							/// since convAvg = 0.5 * \tilde{v_j}^{k+\half} \partial_j{v_i} for a fixed i
							+ (crosstermPrefix * Coe_conv * convAvg) * detJxW
							///(\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M (1/Pe) \tilde{J_j}^{k+\half} \partial_j{v_i})
							/// since massFlux = 0.5 * \tilde{J_j}^{k+\half} \partial_j(v_i) for a fixed i
							+ (crosstermPrefix * Coe_massFlux * massFlux) * detJxW
							///(\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M (1/Re) \eta \partial_j{ \partial_j{ v_i } })
							/// Note: this is only one part of the term
							- (crosstermPrefix * Coe_diff * diffJ) * detJxW
							///(\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M (1/Re) \gamma \partial_j{ \phi } \partial_j{ v_i } )
							/// since diffJCrossterm1 = \partial_j{ \phi } \partial_j{ v_i } for a fixed i
							- (crosstermPrefix * Coe_diff_without_eta * gamma * diffJCrossterm) * detJxW;
				}

				//debug: test for NAN and inf
				//validateMatrix (Ae);
				/// Mass flux cross term: ((1/Pe)\partial_j{w_i}, (\tilde{J_j}^{k+\half}/\rho)\tau_M R_M(\phi,v_i,p))

				///Note:
				/// massFluxCrossTermPrefix = Coe_fineScaleMassFlux * \partial_j{w_i} \tilde{J_j}^{k+\half} \tau_M :
				/// summed over j for each i
				/// Where, Coe_fineScaleMassFlux = 0.5/(Pe \tilde{\rho}^{k+1/2})
				///        massFluxCrossContrib  = \partial_j{w_i} \tilde{J_j}^{k+\half} = fe.dN(a, j) * JExplicitAvg(j)
				///double massFluxCrossTermPrefix = Coe_fineScaleMassFlux * massFluxCrossContrib * tauM;
				for (int i = 0; i < nsd; i++) { /// "i" is the row number
					Ae ((degOfFreedom * a) + i, (degOfFreedom * b) + i) +=
							///(0.5/(Pe \rho))(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M \rho \partial_t{ \delta{v_i} }),
							/// since massFluxCrossContrib = \partial_j{w_i}(\tilde{J_j}^{k+\half}
							(massFluxCrossTermPrefix * Coe_conv * ((fe.N (b)) / dt_)) * detJxW
							///(0.5/(Pe \rho))(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M \rho \tilde{v_j}^{k+\half}
							/// \partial_j{v_i}),
							/// since convAvg = \tilde{v_j}^{k+\half} \partial_j{v_i} for a fixed i
							+ (massFluxCrossTermPrefix * Coe_conv * convAvg) * detJxW
							///(0.5/(Pe^2 \rho))(\partial_j{w_i}, \partial_j{mu} tau_M(-\gamma/\rho) J_j \partial_j{\delta v_i})
							/// since massFlux = 0.5 * \tilde{J_j}^{k+\half} \partial_j{v_i} for a fixed i
							+ (massFluxCrossTermPrefix * Coe_massFlux * massFlux) * detJxW
							///(0.5/Pe\rho)(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M(\eta/Re)\partial_j{\partial_j{v_i}})
							- (massFluxCrossTermPrefix * Coe_diff * diffJ) * detJxW
							///(0.5/Pe\rho)(\partial_j{w_i}, \partial_j{mu} tau_M (1/Re)\gamma\partial_j{\phi}\partial_j{v_i})
							/// since diffJCrossterm = \partial_j{ \phi } \partial_j{ \delta v_i } for a fixed i
							- (massFluxCrossTermPrefix * Coe_diff_without_eta * gamma * diffJCrossterm) * detJxW;
				}

				//debug: test for NAN and inf
				//validateMatrix (Ae);
			}
			///---------------------------------Done with Elemental operator Matrix----------------------------------------///
		}

	}

	///****************************************************************************************************************///
	///******************Implementations with custom Theta scheme for the NS block*************************************///
	///****************************************************************************************************************///
	/** This is a variational multiscale solver for CHNS with a custom IMEX scheme for time integration, using the
	 * variable density momentum residual.
	 * We have proven this scheme to be unconditionally energy stable.
	 * @param fe FEMElm object for looping over elements and gauss points
	 * @param Ae Empty Ae matrix to be filled in the method
	 * @param dt timestep
	 * @param t current time
	 * @return
	 */
	void
	CHNSsemiImplicitThetaSecondOrderbe(const FEMElm &fe, ZEROARRAY<double> &be, const double dt, const double t) {

		//validateVector(be);
		//PrintInfo ("position = ", fe.position (), ", iso_position = ", fe.itg_pt (), ", num_pt = ", fe
		//		.n_itg_pts (), ", current_itg_pt = ", fe.cur_itg_pt_num ());
		//PrintInfo ("Printing be");
		//if (GetMPIRank () == 0) {
		//	for (int i = 0; i < be.size (); i++) {
		//		std::cout << be (i) << " ";
		//		std::cout << "\n";
		//	}
		//}

		double dt_ = dt;
		double t_ = t + dt_; ///< current time is elapsed + dt
		int degOfFreedom = DIM;
		int nsd = DIM;
		double Ci_f = this->input_data_->Ci_f;
		int n_basis_functions = fe.nbf (); // # of basis functions
		double detJxW = fe.detJxW ();


		/// Get all the non dimensional numbers for the problem from input_data_
		double Re = this->input_data_->Re; ///< Reynolds number
		double Pe = this->input_data_->Pe; /// Diffusional Peclet number
		double We = this->input_data_->We; /// Weber number
		double Cn = this->input_data_->Cn; /// Cahn number
		double Fr = this->input_data_->Fr; /// Froude number

		/// Physical properties
		double rhoH = input_data_->rhoH;
		double rhoL = input_data_->rhoL;
		double etaH = input_data_->etaH;
		double etaL = input_data_->etaL;

		/// calculate alpha and beta (coefficients in mixture density)
		double alpha = (rhoH - rhoL) / 2.0;
		double beta = (rhoH + rhoL) / 2.0;
		/// calculate gamma and kappa (coefficients in mixture density)
		double gamma = (etaH - etaL) / 2.0;
		double kappa = (etaH + etaL) / 2.0;

		/// Get phi to calculate mixture density of the form given in Abels et al.
		double phi = this->p_data_->valueFEM (fe, CHNSNodeData::PHI);
		double phiPrev1 = this->p_data_->valueFEM (fe, CHNSNodeData::PHI_PRE1);
		double phiPrev2 = this->p_data_->valueFEM (fe, CHNSNodeData::PHI_PRE2);
		std::vector<double> phi_pre_list = {phiPrev1, phiPrev2};
		double phiExplicitAvg = calcExplicitApproxField(phi_pre_list);

		/// Calculate the explicit approximations of averaged Density and viscosity
		//double explicitAvgDensity = calcExplicitDensity(phi_pre_list);
		/// Mixture density explicit
		double explicitAvgDensity = 1.0;
		//double rhoExplicitAvg = calcExplicitDensity(phi_list);
		if (input_data_->caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS && input_data_->manufacSolType == 2) {
			explicitAvgDensity = calcExplicitDensityNoPullBack(phi_pre_list);
		} else {
			explicitAvgDensity = calcExplicitDensity(phi_pre_list);
		}
		double explicitAvgViscosity = calcExplicitViscosity(phi_pre_list);

		/// Calculate bunch of coeff
		double Coe_diff = 0.0;
		double Coe_diff_without_eta = 0.0;
		double Coe_conv = 0.0;
		double Coe_crossTerm1 = 0.0;
		double Coe_massFlux = 0.0;
		double Coe_fineScaleMassFlux = 0.0;
		double Coe_pressure = 0.0;
		double Coe_gravity = 0.0;
		double Coe_forcing_equiv = 0.0;
		double Coe_forcing = 0.0;
		double Coe_diff_phi = 0.0;
		double Coe_mu = 0.0;
		if (Re >= 1) {
			Coe_diff = explicitAvgViscosity / Re;
			Coe_diff_without_eta = 1.0 / Re;
			Coe_conv = explicitAvgDensity;
			Coe_crossTerm1 = 0.5;
			Coe_massFlux = 1.0 / Pe;
			Coe_fineScaleMassFlux = 0.5 / (Pe * explicitAvgDensity);
			if (input_data_->rescale_pressure) {
				Coe_pressure = 1.0;
			} else {
				Coe_pressure = 1.0 / We;
			}
			Coe_forcing_equiv = 1.0 / (Cn * We);
			Coe_forcing = Cn /We;
			Coe_gravity = 1.0;
			Coe_diff_phi = 1.0 / (Pe * Cn);
			Coe_mu = 1.0;
			//Coe_diff_phi = 1.0/(Pe);
			//Coe_mu = Cn;
		} else if (Re < 1) {
			Coe_diff = explicitAvgViscosity;
			Coe_diff_without_eta = 1 / Re;
			Coe_conv = explicitAvgDensity * Re;
			Coe_crossTerm1 = Re;
			Coe_massFlux = Re / Pe;
			Coe_fineScaleMassFlux = Re / (Pe * explicitAvgDensity);
			if (input_data_->rescale_pressure) {
				Coe_pressure = Re;
			} else {
				Coe_pressure = Re / We;
			}
			Coe_forcing_equiv = Re / (Cn * We);
			Coe_forcing = (Re * Cn) /We;
			Coe_gravity = Re;
			Coe_diff_phi = 1.0 / (Pe * Cn);
			Coe_mu = 1.0;
		}


		/// field variables at the previous timestep (level k), where k+1 is current level.


		ZEROPTV u_pre1, u_pre2;
		for (int i = 0; i < nsd; i++) {
			/// u_pre here is solenoidal and VEL_X_SOL, .. are accessed here
			u_pre1 (i) = p_data_->valueFEM (fe, (CHNSNodeData::VEL_X_PRE1 + i));
			/// u_pre2 here is solenoidal and VEL_X_SOL, .. are accessed here
			u_pre2 (i) = p_data_->valueFEM (fe, (CHNSNodeData::VEL_X_PRE2 + i));
		}
		/// explicit approximation of u^{k+\half}
		std::vector<ZEROPTV> vel_pre_list = {u_pre1, u_pre2};
		ZEROPTV uExplicitAvgVelocity = calcExplicitVelocity (vel_pre_list);

		/// Define velocity gradient tensor
		ZeroMatrix<double> duPrev1, duPrev2;
		duPrev1.redim (nsd, nsd);
		duPrev2.redim (nsd, nsd);
		for (int i = 0; i < nsd; i++) {
			for (int j = 0; j < nsd; j++) {
				///both of these are solenoidal
				duPrev1 (i, j) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::VEL_X_PRE1 + i, j);//< timelevel k
				duPrev2 (i, j) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::VEL_X_PRE2 + i, j);//< timelevel k-1
			}
		}

		/// Get gradient of phasefield
		ZEROPTV dphi, dphiPrev1, dphiPrev2;
		for (int i = 0; i < nsd; i++) {
			dphi (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PHI, i);
			dphiPrev1 (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PHI_PRE1, i);
			dphiPrev2 (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PHI_PRE2, i);
		}
		std::vector<ZEROPTV> gradPhi_prev_list = {dphiPrev1, dphiPrev2};
		ZEROPTV gradPhiExplicitAvg = calcExplicitVelocity (gradPhi_prev_list);

		/// Calculate the laplacian of velocity
		ZEROPTV d2uPrev1;
		/// PS: This assumes the first three degrees are always velocity, it is wise to keep it that way
		for (int dir1 = 0; dir1 < nsd; dir1++) {
			/// Summing over three directions of velocity as it is laplacian
			for (int dir2 = 0; dir2 < nsd; dir2++) {
				d2uPrev1 (dir1) += p_data_->value2DerivativeFEM (fe, CHNSNodeData::VEL_X_PRE1 + dir1, dir2, dir2);
			}
		}


		double p_prev = p_data_->valueFEM (fe, CHNSNodeData::PRESSURE_PRE1);
		double p_prev2 = p_data_->valueFEM (fe, CHNSNodeData::PRESSURE_PRE2);
		/// See general formulation given in equation 3.13, 3.14 in Guermond et al. 2006
		/// This controls how many time points of pressure are use to approximate grad{p} term in the velocity predictor.
		double pStar = p_extrap_c[1] * p_prev + p_extrap_c[2] * p_prev2;
		//pStar = this->p_data_->valueFEM (fe, CHNSNodeData::PRESSURE_MANUFAC);

		/// Get gradient of pressure at previous timestep and calculate the gradient of pressure according to 3.14 in
		// Guermond 2006
		ZEROPTV dpPrev, dpPrev2, dpStar;
		for (int i = 0; i < nsd; i++) {
			dpPrev (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PRESSURE_PRE1, i);
			dpPrev2 (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PRESSURE_PRE2, i);
			dpStar (i) = p_extrap_c[1] * dpPrev (i) + p_extrap_c[2] * dpPrev2 (i);
			//dpStar(i) = this->p_data_->valueDerivativeFEM(fe, CHNSNodeData::PRESSURE_MANUFAC, i);
		}


		/// Calculate the Diffusional fluxes from Cahn Hilliard (J in Abels et al.)
		double mu = p_data_->valueFEM (fe, CHNSNodeData::MU);
		ZEROPTV JExplicitAvg, JExact;
		ZEROPTV gradMu, gradmuPrev1, gradmuPrev2, gradmuExact;
		for (int i = 0; i < nsd; i++) {
			gradMu (i) = p_data_->valueDerivativeFEM (fe, CHNSNodeData::MU, i);
			gradmuPrev1 (i) = p_data_->valueDerivativeFEM (fe, CHNSNodeData::MU_PRE1, i);
			gradmuPrev2 (i) = p_data_->valueDerivativeFEM (fe, CHNSNodeData::MU_PRE2, i);
		}
		std::vector<ZEROPTV> gradMu_prev_list = {gradmuPrev1, gradmuPrev2};
		ZEROPTV gradMuExplicitAvg = calcExplicitVelocity (gradMu_prev_list);
		/// We can compute explicit approximation of J^{k+\half} similar to velocity
		for (int i = 0; i < nsd; i++) {
			JExplicitAvg (i) = ((rhoL - rhoH) / 2) * mobility * gradMuExplicitAvg (i);
			JExact (i) = ((rhoL - rhoH) / 2) * mobility * gradmuExact (i);
		}

		/// -------------------------------------body forces ------------------------------------------------------///

		/// Contribution of gravity in terms of Froude number
		ZEROPTV gravity (0.0, 0.0, 0.0);
		gravity.x () = 0.0;
		/// if Fr is specified to be zero in the config file turn gravity off
		if (Fr < 1e-10) {
			gravity.y () = 0;
		} else {
			gravity.y () = -(Coe_gravity / Fr);
		}
		gravity.z () = 0.0;

		/// External forcing: In the current implementation this term is non-zero
		/// only in the case of Manufactured solutions test of CHNS and periodic
		/// channel case
		ZEROPTV forcingNS (0.0, 0.0, 0.0);
		if (input_data_->caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS) {
			forcingNS = calcForcingManufacturedSolutionNS (fe, t_);
		} else if (input_data_->caseTypeForNavierStokes == CHNSInputData::PERIODIC_CHANNEL_FLOW) {
			throw TALYException () << "Not yet implemented forcing for periodic pressure driven flow needs to be added";
		} else {
			forcingNS = {0.0, 0.0, 0.0};
		}

		/// External forcing at t = k-1: In the current implementation this term is non-zero
		/// only in the case of Manufactured solutions test of CHNS and periodic
		/// channel case
		ZEROPTV forcingNSprev1 (0.0, 0.0, 0.0);
		if (input_data_->caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS) {
			forcingNSprev1 = calcForcingManufacturedSolutionNS (fe, t_ - dt_);
		} else if (input_data_->caseTypeForNavierStokes == CHNSInputData::PERIODIC_CHANNEL_FLOW) {
			throw TALYException () << "Not yet implemented forcing for periodic pressure driven flow needs to be added";
		} else {
			forcingNS = {0.0, 0.0, 0.0};
		}

		double pureNSflag = 1.0;
		if (input_data_->pureNS) {
			pureNSflag = 0.0;
		}

		///---------------------------------VMS parameter settings---------------------------------------------------///
		VMSParams vms_params_current = calc_tau_vel (fe, uExplicitAvgVelocity, JExplicitAvg, phiExplicitAvg,
		                                             explicitAvgDensity, explicitAvgViscosity, Ci_f, dt);
		double tauM = vms_params_current.tauM;
		///---------------------------------VMS parameters calculated------------------------------------------------///

		for (int a = 0; a < n_basis_functions; a++) {
			///-------------------------------------Calculating the Elemental operator-------------------------------------///

			/// Auxiliary term useful for calculating things in jacobian (\partial_j{w_i}, v_j)
			double crossTermVelocityPart = 0.0;
			for (int j = 0; j < nsd; j++) {
				crossTermVelocityPart += fe.dN (a, j) * uExplicitAvgVelocity (j);
			}
			double massFluxCrossContrib = 0.0;
			for (int j = 0; j < nsd; j++) {
				massFluxCrossContrib += fe.dN (a, j) * JExplicitAvg (j);
			}


			/// crosstermPrefix =  0.5 * \partial_j{w_i} \tilde{v_j}^{k+\half} \tau_M :  summed over j for each i
			double crosstermPrefix = Coe_crossTerm1 * crossTermVelocityPart * tauM;

			/// massFluxCrossTermPrefix = Coe_fineScaleMassFlux * \partial_j{w_i} \tilde{J_j}^{k+\half} \tau_M :
			/// summed over j for each i
			/// Where, Coe_fineScaleMassFlux = 0.5/(Pe \tilde{\rho}^{k+1/2})
			///        massFluxCrossContrib  = \partial_j{w_i} \tilde{J_j}^{k+\half} = fe.dN(a, j) * JExplicitAvg(j)
			double massFluxCrossTermPrefix = Coe_fineScaleMassFlux * massFluxCrossContrib * tauM;
			///----------------------------------------------Start be------------------------------------------------------///
			/// All the terms involved in the equation
			ZEROPTV convFine, massFluxFine, normal_NS, NScontrib;


			ZEROPTV convectionCoarse;
			ZEROPTV massFluxCoarse;
			ZEROPTV diff;
			ZEROPTV diffChainTerm;
			ZEROPTV CHNSCouple;
			for (int i = 0; i < nsd; i++) {
				convectionCoarse (i) = 0.0;
				diff (i) = 0.0;
				for (int j = 0; j < nsd; j++) {
					convectionCoarse(i) += Coe_conv * uExplicitAvgVelocity(j) * 0.5 * duPrev1(i, j); ///< coarse advection
					//convectionCoarse (i) += fe.dN (a, j) * Coe_conv * uExplicitAvgVelocity (j) * 0.5 * u_pre1 (i);
					massFluxCoarse(i) += Coe_massFlux * (JExplicitAvg(j) * 0.5 * duPrev1(i, j)); ///< coarse massflux
					//massFluxCoarse (i) += Coe_massFlux * fe.dN (a, j) * JExplicitAvg (j) * 0.5 * u_pre1 (i);
					diff (i) += fe.dN (a, j) * Coe_diff * 0.5 * duPrev1 (i, j);///< Diffusion term
					diffChainTerm (i) += (Coe_diff_without_eta * gamma * 0.5 * gradPhiExplicitAvg (j) * duPrev1 (i, j));
					//CHNSCouple(i) +=  (0.5 * (Cn/We) * fe.dN(a, j) * 0.5 * dphiPrev1(j) * gradPhiExplicitAvg(i) ) * detJxW
					//		+ (0.5 * (Cn/We) * fe.dN(a, j) * 0.5 * gradPhiExplicitAvg(j) * dphiPrev1(i)) * detJxW;
					//CHNSCouple(i) +=  -((Cn/We) * fe.dN(a, j) * 0.5 * (dphi(i) + dphiPrev1(i)) * 0.5 * (dphi(j) + dphiPrev1(j)
					//		))* detJxW;
				}
				/// (w_i, \tilde{phi}^{k+1/2} \partial_i(mu^{k+1/2}))
				//CHNSCouple (i) += (Coe_forcing_equiv * fe.N (a) * phiExplicitAvg * gradMu(i)) * detJxW;
			}
			if(input_data_->ifdphidphiForcing) {
				for (int i = 0; i < nsd; i++) {
					CHNSCouple(i) = 0.0;
					for (int j = 0; j < nsd; j++) {
						CHNSCouple(i) +=
								-(Coe_forcing * fe.dN(a, j) * pureNSflag * 0.5 * (dphi(i) + dphiPrev1(i)) * 0.5 * (dphi(j) +dphiPrev1(j)))* detJxW;
//						CHNSCouple(i) +=
//								-0.5 * (Coe_forcing * fe.dN(a, j) * pureNSflag * gradPhiExplicitAvg(i) * 0.5 * (dphi(j) + dphiPrev1(j))) * detJxW
//								- 0.5 * ( Coe_forcing * fe.dN(a, j) * pureNSflag * 0.5 * (dphi(i) +dphiPrev1(i)) * gradPhiExplicitAvg(j) ) * detJxW;
//						CHNSCouple(i) +=
//								-(Coe_forcing * fe.dN(a, j) * pureNSflag * gradPhiExplicitAvg(i) * gradPhiExplicitAvg(j)) * detJxW;
					}
				}
			} else {
				for (int i = 0; i < nsd; i++) {
					CHNSCouple (i) += (Coe_forcing_equiv * pureNSflag * fe.N (a) * phiExplicitAvg * gradMu(i)) * detJxW;
					for (int j = 0; j < nsd; j++) {
						CHNSCouple (i) +=
								Coe_forcing_equiv * pureNSflag * fe.dN(a, i) * mu * 0.5 * (phi  + phiPrev1) * detJxW
								- Coe_forcing * pureNSflag * 0.5 * fe.dN(a, i) * 0.25 * (dphi(j) + dphiPrev1(j)) * (dphi(j) + dphiPrev1(j))*detJxW;
					}
					CHNSCouple (i) += Coe_forcing_equiv * pureNSflag * fe.N(a) *
					                  ( pow((0.5 * (phi + phiPrev1)), 3) - (0.5 *(phi +phiPrev1)) ) * 0.5 * (dphi(i) + dphiPrev1(i)) * detJxW;
				}
			}



			/// Terms of Normal Navier Stokes without any additional VMS terms
			for (int i = 0; i < nsd; i++) { /// "i" is the row number
				normal_NS (i) =
						///(w_i, \tilde{\rho}^{k+1/2} \partial_t{v_i})
						(fe.N (a) * Coe_conv * (u_pre1 (i) / dt_)) * detJxW
						//+ (fe.N(a) * uExplicitAvgVelocity(i) * alpha * (phiPrev1 / dt_)) * detJxW
						///(w_i, \rho v_j \partial_j{v_i})
						- (fe.N(a) * convectionCoarse(i)) * detJxW
						//+ (convectionCoarse (i)) * detJxW
						///1/Pe(w_i, J_j \partial_j{v_i})
						- (fe.N(a) * massFluxCoarse(i)) * detJxW
						//+ (massFluxCoarse (i)) * detJxW
						///1/We(w_i, \partial_i{pStar}) : see Guermond et al. 2006 : eq 3.14 for details
						+ (Coe_pressure * (fe.dN (a, i) * pStar)) * detJxW
						///1/Re(\partial_j{w_i}, \eta \partial_j{v_i})
						- ((diff (i)) * detJxW)
						///(w_i, \rho {gravity{i}})
						+ (fe.N (a) * explicitAvgDensity * gravity (i)) * detJxW
						///(w_i, forcingNS(i))
						+ (fe.N (a) * 0.5 * (forcingNS (i) + forcingNSprev1 (i))) * detJxW;

				/// convection fine scale terms
				convFine (i) =
						///0.5 * (\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M \rho \partial_t{v_i^{k}} ),
						(crosstermPrefix * Coe_conv * (u_pre1 (i) / dt_)) * detJxW
						///0.5 * (\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M convAvg(i)),
						/// where convAvg = 0.5 * \tilde{v_j}^{k+\half} \partial_j{v_i^{k}} summer over j for a fixed i
						- (crosstermPrefix * convectionCoarse (i)) * detJxW
						///0.5 * (\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M massFlux(i)),
						/// where massFlux = 0.5 * \tilde{J_j}^{k+\half} \partial_j(v_i) summer over j for a fixed i
						- (crosstermPrefix * massFluxCoarse (i)) * detJxW
						///0.5 * (\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M (1/We * \partial_i(p^k) ))
						- (crosstermPrefix * (Coe_pressure * dpStar (i))) * detJxW
						///0.5 * (\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M (1/Re) \tilde{\eta}^{k+1/2}
						///                                                                  \partial_j(\partial_j (v_i^k)) )
						+ (crosstermPrefix * (Coe_diff * 0.5 * d2uPrev1 (i))) * detJxW
						///0.5 * (\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M (\gamma/Re)\partial_j(\tilde{\eta}^{k+1/2})
						///                                                                                  \partial_j (v_i^k)))
						+ (crosstermPrefix * diffChainTerm (i)) * detJxW
						///0.5 * (\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M (1/Fr) * \tilde{\rho}^{k+1/2} g_i)
						+ (crosstermPrefix * explicitAvgDensity * gravity (i)) * detJxW
						///0.5 * (\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M f_i^{k+1/2})
						+ (crosstermPrefix * 0.5 * (forcingNS (i) + forcingNSprev1 (i))) * detJxW;

				/// mass flux fine scale terms
				massFluxFine (i) =
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M  \rho\partial_t{v_i^{k}} ),
						(massFluxCrossTermPrefix * Coe_conv * (u_pre1 (i) / dt_)) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M convAvg(i)),
						/// where convAvg = 0.5 * \tilde{v_j}^{k+\half} \partial_j{v_i^{k}} summer over j for a fixed i
						- (massFluxCrossTermPrefix * convectionCoarse (i)) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M massFlux(i)),
						/// where massFlux = 0.5 * \tilde{J_j}^{k+\half} \partial_j(v_i) summer over j for a fixed i
						- (massFluxCrossTermPrefix * massFluxCoarse (i)) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M (1/We * \partial_i(p^k) ))
						- (massFluxCrossTermPrefix * (Coe_pressure * dpStar (i))) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M (1/Re) \tilde{\eta}^{k+1/2}
						///                                                                  0.5 *\partial_j(\partial_j(v_i^k)) )
						+ (massFluxCrossTermPrefix * (Coe_diff * 0.5 * d2uPrev1 (i))) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M (\gamma/Re)\partial_j(\tilde{\eta}^{k+1/2})
						///                                                                           0.5 * \partial_j(v_i^k)))
						+ (massFluxCrossTermPrefix * diffChainTerm (i)) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M (1/Fr) * \tilde{\rho}^{k+1/2} g_i)
						+ (massFluxCrossTermPrefix * explicitAvgDensity * gravity (i)) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M f_i^{k+1/2})
						+ (massFluxCrossTermPrefix * 0.5 * (forcingNS (i) + forcingNSprev1 (i))) * detJxW;

				//NScontrib(i) =    normal_NS(i) + convFine(i) + massFluxFine(i) + CHNSCouple(i);
				NScontrib (i) = normal_NS (i)
				                + convFine (i)
				                + massFluxFine (i)
				                - CHNSCouple (i);
			}
			//validateVector (be);

			/// Adding all the calculated terms to the vector
			for (int i = 0; i < nsd; i++) {
				be ((degOfFreedom * a) + i) += NScontrib (i);
			}
		}
	}


	///****************************************************************************************************************///
	///******************Implementations with custom Theta scheme for the NS block*************************************///
	///****************************************************************************************************************///
	/** This is a variational multiscale solver for CHNS with a custom IMEX scheme for time integration, using the
	 * variable density momentum residual.
	 * We have proven this scheme to be unconditionally energy stable.
	 * @param fe FEMElm object for looping over elements and gauss points
	 * @param Ae Empty Ae matrix to be filled in the method
	 * @param dt timestep
	 * @param t current time
	 * @return
	 */
	void
	CHNSsemiImplicitThetaSecondOrder_ConsvEqAddedAe(const FEMElm &fe, ZeroMatrix<double> &Ae, const double dt, const
	double t) {

		//validateVector(be);
		//PrintInfo ("position = ", fe.position (), ", iso_position = ", fe.itg_pt (), ", num_pt = ", fe
		//		.n_itg_pts (), ", current_itg_pt = ", fe.cur_itg_pt_num ());
		//PrintInfo ("Printing be");
		//if (GetMPIRank () == 0) {
		//	for (int i = 0; i < be.size (); i++) {
		//		std::cout << be (i) << " ";
		//		std::cout << "\n";
		//	}
		//}

		double dt_ = dt;
		double t_ = t + dt_; ///< current time is elapsed + dt
		int degOfFreedom = DIM;
		int nsd = DIM;
		double Ci_f = this->input_data_->Ci_f;
		int n_basis_functions = fe.nbf (); // # of basis functions
		double detJxW = fe.detJxW ();


		/// Get all the non dimensional numbers for the problem from input_data_
		double Re = this->input_data_->Re; ///< Reynolds number
		double Pe = this->input_data_->Pe; /// Diffusional Peclet number
		double We = this->input_data_->We; /// Weber number
		double Cn = this->input_data_->Cn; /// Cahn number
		double Fr = this->input_data_->Fr; /// Froude number

		/// Physical properties
		double rhoH = input_data_->rhoH;
		double rhoL = input_data_->rhoL;
		double etaH = input_data_->etaH;
		double etaL = input_data_->etaL;

		/// calculate alpha and beta (coefficients in mixture density)
		double alpha = (rhoH - rhoL) / 2.0;
		double beta = (rhoH + rhoL) / 2.0;
		/// calculate gamma and kappa (coefficients in mixture density)
		double gamma = (etaH - etaL) / 2.0;
		double kappa = (etaH + etaL) / 2.0;

		/// Get phi to calculate mixture density of the form given in Abels et al.
		double phi = this->p_data_->valueFEM (fe, CHNSNodeData::PHI);
		double phiPrev1 = this->p_data_->valueFEM (fe, CHNSNodeData::PHI_PRE1);
		double phiPrev2 = this->p_data_->valueFEM (fe, CHNSNodeData::PHI_PRE2);
		std::vector<double> phi_pre_list = {phiPrev1, phiPrev2};
		double phiExplicitAvg = calcExplicitApproxField(phi_pre_list);


		double phiForPropertyCalculation = 0.0;
		if (phi >= 1.0) {
			phiForPropertyCalculation = 1.0;
		} else if (phi <= -1.0) {
			phiForPropertyCalculation = -1.0;
		} else {
			phiForPropertyCalculation = phi;
		}

		double phiForPropertyCalculationPrev = 0.0;
		if (phiPrev1 >= 1.0) {
			phiForPropertyCalculationPrev = 1.0;
		} else if (phiPrev1 <= -1.0) {
			phiForPropertyCalculationPrev = -1.0;
		} else {
			phiForPropertyCalculationPrev = phiPrev1;
		}

		/// Calculate the explicit approximations of averaged Density and viscosity
		//double explicitAvgDensity = calcExplicitDensity(phi_pre_list);
		/// Mixture density explicit
		double explicitAvgDensity = 1.0;
		//double rhoExplicitAvg = calcExplicitDensity(phi_list);
		if (input_data_->caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS && input_data_->manufacSolType == 2) {
			explicitAvgDensity = calcExplicitDensityNoPullBack(phi_pre_list);
		} else {
			explicitAvgDensity = calcExplicitDensity(phi_pre_list);
		}
		double explicitAvgViscosity = calcExplicitViscosity(phi_pre_list);

		double Ctemam = input_data_->Ctemam;

		/// Calculate bunch of coeff
		double Coe_diff = 0.0;
		double Coe_diff_without_eta = 0.0;
		double Coe_conv = 0.0;
		double Coe_crossTerm1 = 0.0;
		double Coe_massFlux = 0.0;
		double Coe_fineScaleMassFlux = 0.0;
		double Coe_pressure = 0.0;
		double Coe_gravity = 0.0;
		double Coe_forcing_equiv = 0.0;
		if (Re >= 1) {
			Coe_diff = explicitAvgViscosity / Re;
			Coe_diff_without_eta = 1.0 / Re;
			Coe_conv = explicitAvgDensity;
			Coe_crossTerm1 = 0.5;
			Coe_massFlux = 1.0 / (Cn * Pe);
			Coe_fineScaleMassFlux = 0.5 / (Cn * Pe * explicitAvgDensity);
			if (input_data_->rescale_pressure) {
				Coe_pressure = 1.0;
			} else {
				Coe_pressure = 1.0 / We;
			}
			Coe_forcing_equiv = 1.0 / (Cn * We);
			Coe_gravity = 1.0;
		} else if (Re < 1) {
			Coe_diff = explicitAvgViscosity;
			Coe_diff_without_eta = 1 / Re;
			Coe_conv = explicitAvgDensity * Re;
			Coe_crossTerm1 = Re;
			Coe_massFlux = Re / (Cn * Pe);
			Coe_fineScaleMassFlux = Re / (Cn * Pe * explicitAvgDensity);
			if (input_data_->rescale_pressure) {
				Coe_pressure = Re;
			} else {
				Coe_pressure = Re / We;
			}
			Coe_forcing_equiv = Re / (Cn * We);
			Coe_gravity = Re;
		}


		/// field variables at the previous timestep (level k), where k+1 is current level.


		ZEROPTV u_pre1, u_pre2;
		for (int i = 0; i < nsd; i++) {
			/// u_pre here is solenoidal and VEL_X_SOL, .. are accessed here
			u_pre1 (i) = p_data_->valueFEM (fe, (CHNSNodeData::VEL_X_PRE1 + i));
			/// u_pre2 here is solenoidal and VEL_X_SOL, .. are accessed here
			u_pre2 (i) = p_data_->valueFEM (fe, (CHNSNodeData::VEL_X_PRE2 + i));
		}
		/// explicit approximation of u^{k+\half}
		std::vector<ZEROPTV> vel_pre_list = {u_pre1, u_pre2};
		ZEROPTV uExplicitAvgVelocity = calcExplicitVelocity (vel_pre_list);

		/// Define velocity gradient tensor
		ZeroMatrix<double> duPrev1, duPrev2;
		duPrev1.redim (nsd, nsd);
		duPrev2.redim (nsd, nsd);
		for (int i = 0; i < nsd; i++) {
			for (int j = 0; j < nsd; j++) {
				///both of these are solenoidal
				duPrev1 (i, j) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::VEL_X_PRE1 + i, j);//< timelevel k
				duPrev2 (i, j) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::VEL_X_PRE2 + i, j);//< timelevel k-1
			}
		}

		/// Get gradient of phasefield
		ZEROPTV dphi, dphiPrev1, dphiPrev2;
		for (int i = 0; i < nsd; i++) {
			dphi (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PHI, i);
			dphiPrev1 (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PHI_PRE1, i);
			dphiPrev2 (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PHI_PRE2, i);
		}
		std::vector<ZEROPTV> gradPhi_prev_list = {dphiPrev1, dphiPrev2};
		ZEROPTV gradPhiExplicitAvg = calcExplicitVelocity (gradPhi_prev_list);

		/// ----------------------------------Conservation terms calculation ------------------------------------------///
		double div_u_prev1 = 0.0;
		double div_u_prev2 = 0.0;
		for (int i = 0; i < nsd; i ++ ){
			div_u_prev1 += duPrev1 (i, i);
			div_u_prev2 += duPrev2 (i, i);
		}
		std::vector<double>div_u_prev_list = {div_u_prev1, div_u_prev2};
		double div_u_explicitAvg = calcExplicitField(div_u_prev_list);
		///( \tilde{rho}^{k + 1/2} * \partial_j( \tilde{v_j^{k + 1/2}} ) )
		double rhoMultipliedWithDiv_u_explicitAvg = explicitAvgDensity * div_u_explicitAvg;

		///( \tilde{v_j^{k + 1/2}} * \partial_j(\tilde{rho}^{k + 1/2}) )
		double div_rhoExplicitAvgDottedWithExplicitVelocity = 0.0;
		for (int i = 0; i < nsd; i++) {
			div_rhoExplicitAvgDottedWithExplicitVelocity += alpha * gradPhiExplicitAvg(i) * uExplicitAvgVelocity(i);
		}

		/**
		 * \partial_j(\tilde{rho}^{k + 1/2} v_j^{k + 1/2}) =
		 * ( \tilde{rho}^{k + 1/2} * \partial_j( \tilde{v_j^{k + 1/2}} ) ) + ( \tilde{v_j^{k + 1/2}} * \partial_j(\tilde{rho}^{k + 1/2}) )
		 */
		double divOfRhoExplicitMultipliedWithExplicitVelocity = div_rhoExplicitAvgDottedWithExplicitVelocity +
		                                                        rhoMultipliedWithDiv_u_explicitAvg;
		///-------------------------------------------------------------------------------------------------------------///

		/// Calculate the laplacian of velocity
		ZEROPTV d2uPrev1;
		/// PS: This assumes the first three degrees are always velocity, it is wise to keep it that way
		for (int dir1 = 0; dir1 < nsd; dir1++) {
			/// Summing over three directions of velocity as it is laplacian
			for (int dir2 = 0; dir2 < nsd; dir2++) {
				d2uPrev1 (dir1) += p_data_->value2DerivativeFEM (fe, CHNSNodeData::VEL_X_PRE1 + dir1, dir2, dir2);
			}
		}


		double p_prev = p_data_->valueFEM (fe, CHNSNodeData::PRESSURE_PRE1);
		double p_prev2 = p_data_->valueFEM (fe, CHNSNodeData::PRESSURE_PRE2);
		/// See general formulation given in equation 3.13, 3.14 in Guermond et al. 2006
		/// This controls how many time points of pressure are use to approximate grad{p} term in the velocity predictor.
		double pStar = p_extrap_c[1] * p_prev + p_extrap_c[2] * p_prev2;
		//pStar = this->p_data_->valueFEM (fe, CHNSNodeData::PRESSURE_MANUFAC);

		/// Get gradient of pressure at previous timestep and calculate the gradient of pressure according to 3.14 in
		// Guermond 2006
		ZEROPTV dpPrev, dpPrev2, dpStar;
		for (int i = 0; i < nsd; i++) {
			dpPrev (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PRESSURE_PRE1, i);
			dpPrev2 (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PRESSURE_PRE2, i);
			dpStar (i) = p_extrap_c[1] * dpPrev (i) + p_extrap_c[2] * dpPrev2 (i);
			//dpStar(i) = this->p_data_->valueDerivativeFEM(fe, CHNSNodeData::PRESSURE_MANUFAC, i);
		}


		/// Calculate the Diffusional fluxes from Cahn Hilliard (J in Abels et al.)
		ZEROPTV JExplicitAvg;
		ZEROPTV gradMu, gradmuPrev1, gradmuPrev2;
		double laplacianMuPrev1 = 0.0;
		double laplacianMuPrev2 = 0.0;
		for (int i = 0; i < nsd; i++) {
			gradMu (i) = p_data_->valueDerivativeFEM (fe, CHNSNodeData::MU, i);
			gradmuPrev1 (i) = p_data_->valueDerivativeFEM (fe, CHNSNodeData::MU_PRE1, i);
			gradmuPrev2 (i) = p_data_->valueDerivativeFEM (fe, CHNSNodeData::MU_PRE2, i);

			laplacianMuPrev1 += p_data_->value2DerivativeFEM (fe, CHNSNodeData::MU_PRE1, i, i);
			laplacianMuPrev2 += p_data_->value2DerivativeFEM (fe, CHNSNodeData::MU_PRE2, i, i);
		}
		std::vector<ZEROPTV> gradMu_prev_list = {gradmuPrev1, gradmuPrev2};
		ZEROPTV gradMuExplicitAvg = calcExplicitVelocity (gradMu_prev_list);
		/// We can compute explicit approximation of J^{k+\half} similar to velocity
		for (int i = 0; i < nsd; i++) {
			JExplicitAvg (i) = ((rhoL - rhoH) / 2) * mobility * gradMuExplicitAvg (i);
		}
		double divJprev1 = ((rhoL - rhoH) / 2) * mobility * laplacianMuPrev1;
		double divJprev2 = ((rhoL - rhoH) / 2) * mobility * laplacianMuPrev2;
		std::vector<double> divJ_prevList = {divJprev1, divJprev2};
		//// \partial_j{\tilde(J^{k + 1/2})}
		double div_J_explicit_avg = calcExplicitField(divJ_prevList);


		/// -------------------------------------body forces ------------------------------------------------------///

		/// Contribution of gravity in terms of Froude number
		ZEROPTV gravity (0.0, 0.0, 0.0);
		gravity.x () = 0.0;
		/// if Fr is specified to be zero in the config file turn gravity off
		if (Fr < 1e-10) {
			gravity.y () = 0;
		} else {
			gravity.y () = -(Coe_gravity / Fr);
		}
		gravity.z () = 0.0;

		/// External forcing: In the current implementation this term is non-zero
		/// only in the case of Manufactured solutions test of CHNS and periodic
		/// channel case
		ZEROPTV forcingNS (0.0, 0.0, 0.0);
		if (input_data_->caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS) {
			forcingNS = calcForcingManufacturedSolutionNS (fe, t_);
		} else if (input_data_->caseTypeForNavierStokes == CHNSInputData::PERIODIC_CHANNEL_FLOW) {
			throw TALYException () << "Not yet implemented forcing for periodic pressure driven flow needs to be added";
		} else {
			forcingNS = {0.0, 0.0, 0.0};
		}

		/// External forcing at t = k-1: In the current implementation this term is non-zero
		/// only in the case of Manufactured solutions test of CHNS and periodic
		/// channel case
		ZEROPTV forcingNSprev1 (0.0, 0.0, 0.0);
		if (input_data_->caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS) {
			forcingNSprev1 = calcForcingManufacturedSolutionNS (fe, t_ - dt_);
		} else if (input_data_->caseTypeForNavierStokes == CHNSInputData::PERIODIC_CHANNEL_FLOW) {
			throw TALYException () << "Not yet implemented forcing for periodic pressure driven flow needs to be added";
		} else {
			forcingNS = {0.0, 0.0, 0.0};
		}


		///---------------------------------VMS parameter settings---------------------------------------------------///
		VMSParams vms_params_current = calc_tau_vel (fe, uExplicitAvgVelocity, JExplicitAvg, phiExplicitAvg,
		                                             explicitAvgDensity, explicitAvgViscosity, Ci_f, dt);
		double tauM = vms_params_current.tauM;
		///---------------------------------VMS parameters calculated------------------------------------------------///

		double pureNSflag = 1.0;
		if (input_data_->pureNS) {
			pureNSflag = 0.0;
		}

		for (int a = 0; a < n_basis_functions; a++) {
			///-------------------------------------Calculating the Elemental operator-------------------------------------///

			/// Auxiliary term useful for calculating things in jacobian (\partial_j{w_i}, v_j)
			double crossTermVelocityPart = 0.0;
			for (int j = 0; j < nsd; j++) {
				crossTermVelocityPart += fe.dN (a, j) * uExplicitAvgVelocity (j);
			}
			double massFluxCrossContrib = 0.0;
			for (int j = 0; j < nsd; j++) {
				massFluxCrossContrib += fe.dN (a, j) * JExplicitAvg (j);
			}


			/// crosstermPrefix =  0.5 * \partial_j{w_i} \tilde{v_j}^{k+\half} \tau_M :  summed over j for each i
			double crosstermPrefix = Coe_crossTerm1 * crossTermVelocityPart * tauM;

			/// massFluxCrossTermPrefix = Coe_fineScaleMassFlux * \partial_j{w_i} \tilde{J_j}^{k+\half} \tau_M :
			/// summed over j for each i
			/// Where, Coe_fineScaleMassFlux = 0.5/(Pe \tilde{\rho}^{k+1/2})
			///        massFluxCrossContrib  = \partial_j{w_i} \tilde{J_j}^{k+\half} = fe.dN(a, j) * JExplicitAvg(j)
			double massFluxCrossTermPrefix = Coe_fineScaleMassFlux * massFluxCrossContrib * tauM;

			for (int b = 0; b < n_basis_functions; b++) {
				/// Convection term averaged: u_j \partial_j{ \delta{v_i} }
				double convAvg = 0.0;
				for (int j = 0; j < nsd; j++) {
					convAvg += 0.5 * uExplicitAvgVelocity (j) * fe.dN (b, j);
				}

				///J_j \partial_j{ \delta{v_i} }
				double massFlux = 0.0;
				for (int j = 0; j < nsd; j++) {
					massFlux += 0.5 * JExplicitAvg (j) * fe.dN (b, j);
				}

				/// Contribution of laplacian of velocity(diffusion) to the diagonal
				/// \partial_j{ \partial_j{ \delta{v_i} } }
				double diffJ = 0.0;
				for (int j = 0; j < nsd; j++) {
					diffJ += 0.5 * fe.d2N (b, j, j);
				}

				/// \partial_j{ \phi } \partial_j{ v_i }
				double diffJCrossterm = 0.0;
				for (int j = 0; j < nsd; j++) {
					diffJCrossterm += 0.5 * gradPhiExplicitAvg (j) * fe.dN (b, j);
				}

				double convConsv = 0.0;
				for (int j = 0; j < nsd; j++) {
					convConsv += fe.dN (a, j) * uExplicitAvgVelocity (j) * 0.5 * fe.N (b);
				}

				double massFluxConsv = 0.0;
				for (int j = 0; j < nsd; j++) {
					massFluxConsv += fe.dN (a, j) * JExplicitAvg (j) * 0.5 * fe.N (b);
				}


				/// Adding terms to the Elemental matrix.
				for (int i = 0; i < nsd; i++) {
					Ae ((degOfFreedom * a) + i, (degOfFreedom * b) + i) +=
							///(w_i, \tilde{\rho}^{k+\half} \partial_t(v_i))
							/// Note: Coe_conv = \tilde{\rho}^{k+\half}
							(fe.N (a) * Coe_conv * ((fe.N (b)) / dt_)) * detJxW
							//+(fe.N(a) * uExplicitAvgVelocity(i) * alpha * ((fe.N(b))/ dt_)) * detJxW
							///(w_i, \tilde{\rho}^{k+\half} \tilde{v_j}^{k+\half} 0.5 * \partial_j(v_i)
							///since convAvg = 0.5 * \tilde{v_j}^{k+\half} \partial_j{v_i} for a fixed i
							+ (fe.N(a) * Coe_conv * convAvg) * detJxW
							//- (Coe_conv * convConsv) * detJxW
							///(1/Pe)(w_i, \tilde{v_j}^{k+\half} \partial_j(v_i))
							/// since massFluxAvg = 0.5 * \tilde{J_j}^{k+\half} \partial_j(v_i) for a fixed i
							+ (fe.N(a) * Coe_massFlux * massFlux) * detJxW;
					//- (Coe_massFlux * massFluxConsv) * detJxW;

					for (int j = 0; j < nsd; j++) {
						///Diffusion term
						Ae ((degOfFreedom * a) + i, (degOfFreedom * b) + i) +=
								///(\partial_jw_i, 0.5 * (eta(\phi)/Re)\eta \partial_j{v_i^{k+1}})
								/// The other half of corresponding to above term given by
								/// (\partial_jw_i, 0.5 * (eta(\phi)/Re)\eta\partial_j{v_i^{k}}) will go in be
								(fe.dN (a, j) * Coe_diff * 0.5 * fe.dN (b, j)) * detJxW;
					}
				}
				//debug: test for NAN and inf
				//validateMatrix(Ae);
				/// crossTerm 1: This term is written as,
				/// (\partial_j{w_i}, 0.5 * \tilde{v_j}^{k+\half} \tau_M R_M(\phi, v_i,p)),
				/// (only implicit terms on left hand side)

				///Note:
				/// crosstermPrefix =  0.5 * \partial_j{w_i} \tilde{v_j}^{k+\half} \tau_M :  summed over j for each i
				/// double crosstermPrefix = Coe_crossTerm1 * crossTermVelocityPart * tauM;
				for (int i = 0; i < nsd; i++) {
					Ae ((degOfFreedom * a) + i, (degOfFreedom * b) + i) +=
							///(\partial_j{w_i}, \tilde{v_j}^{k+\half} 0.5 * tau_M \rho \partial_t{ \delta{v_i} }),
							/// since crosstermVelocityAvg = \partial_j{w_i} \tilde{v_j}^{k+\half}
							/// 0.5 comes from Crank-Nicholson type averaging
							(crosstermPrefix * Coe_conv * ((fe.N (b)) / dt_)) * detJxW
							///(\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M \rho v_j \partial_j{\delta v_i}),
							/// since convAvg = 0.5 * \tilde{v_j}^{k+\half} \partial_j{v_i} for a fixed i
							+ (crosstermPrefix * Coe_conv * convAvg) * detJxW
							///(\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M (1/Pe) \tilde{J_j}^{k+\half} \partial_j{v_i})
							/// since massFlux = 0.5 * \tilde{J_j}^{k+\half} \partial_j(v_i) for a fixed i
							+ (crosstermPrefix * Coe_massFlux * massFlux) * detJxW
							///(\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M (1/Re) \eta \partial_j{ \partial_j{ v_i } })
							/// Note: this is only one part of the term
							- (crosstermPrefix * Coe_diff * diffJ) * detJxW
							///(\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M (1/Re) \gamma \partial_j{ \phi } \partial_j{ v_i } )
							/// since diffJCrossterm1 = \partial_j{ \phi } \partial_j{ v_i } for a fixed i
							- (crosstermPrefix * Coe_diff_without_eta * gamma * diffJCrossterm) * detJxW;
				}
				//debug: test for NAN and inf
				//validateMatrix (Ae);
				/// Mass flux cross term: ((1/Pe)\partial_j{w_i}, (\tilde{J_j}^{k+\half}/\rho)\tau_M R_M(\phi,v_i,p))
				///Note:
				/// massFluxCrossTermPrefix = Coe_fineScaleMassFlux * \partial_j{w_i} \tilde{J_j}^{k+\half} \tau_M :
				/// summed over j for each i
				/// Where, Coe_fineScaleMassFlux = 0.5/(Pe \tilde{\rho}^{k+1/2})
				///        massFluxCrossContrib  = \partial_j{w_i} \tilde{J_j}^{k+\half} = fe.dN(a, j) * JExplicitAvg(j)
				///double massFluxCrossTermPrefix = Coe_fineScaleMassFlux * massFluxCrossContrib * tauM;
				for (int i = 0; i < nsd; i++) { /// "i" is the row number
					Ae ((degOfFreedom * a) + i, (degOfFreedom * b) + i) +=
							///(0.5/(Pe \rho))(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M \rho \partial_t{ \delta{v_i} }),
							/// since massFluxCrossContrib = \partial_j{w_i}(\tilde{J_j}^{k+\half}
							(massFluxCrossTermPrefix * Coe_conv * ((fe.N (b)) / dt_)) * detJxW
							///(0.5/(Pe \rho))(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M \rho \tilde{v_j}^{k+\half}
							/// \partial_j{v_i}),
							/// since convAvg = \tilde{v_j}^{k+\half} \partial_j{v_i} for a fixed i
							+ (massFluxCrossTermPrefix * Coe_conv * convAvg) * detJxW
							///(0.5/(Pe^2 \rho))(\partial_j{w_i}, \partial_j{mu} tau_M(-\gamma/\rho) J_j \partial_j{\delta v_i})
							/// since massFlux = 0.5 * \tilde{J_j}^{k+\half} \partial_j{v_i} for a fixed i
							+ (massFluxCrossTermPrefix * Coe_massFlux * massFlux) * detJxW
							///(0.5/Pe\rho)(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M(\eta/Re)\partial_j{\partial_j{v_i}})
							- (massFluxCrossTermPrefix * Coe_diff * diffJ) * detJxW
							///(0.5/Pe\rho)(\partial_j{w_i}, \partial_j{mu} tau_M (1/Re)\gamma\partial_j{\phi}\partial_j{v_i})
							/// since diffJCrossterm = \partial_j{ \phi } \partial_j{ \delta v_i } for a fixed i
							- (massFluxCrossTermPrefix * Coe_diff_without_eta * gamma * diffJCrossterm) * detJxW;
				}

				for (int i = 0; i < nsd; i++) {
					Ae ((degOfFreedom * a) + i, (degOfFreedom * b) + i) +=
							pureNSflag * (fe.N(a) * Ctemam * ((phiForPropertyCalculation - phiForPropertyCalculationPrev)/dt_) * 0.5 *
							              fe.N(b)) * detJxW
							///(w_i, Ctemam_ \partial_j(\tilde{v_j}^{k+\half}) 0.5 * v_i^{k+1} ),
							/// 0.5 comes from Crank-Nicholson type averaging
							+ (fe.N(a) * Ctemam * divOfRhoExplicitMultipliedWithExplicitVelocity * 0.5 * fe.N(b)) * detJxW
							///(w_i, Ctemam_ \partial_j(\tilde{J_j}^{k+\half}) 0.5 * v_i),
							/// 0.5 comes from Crank-Nicholson type averaging
							+ (fe.N(a) * Ctemam * div_J_explicit_avg * 0.5 * fe.N(b)) * detJxW;
				}


				///(w_i, Ctemam_ \partial_j(\tilde{\rho}^{k + 1/2}\tilde{v_j}^{k+\half}) (\tau_M/\rho) R_M(\phi,v_i,p))
				for (int i = 0; i < nsd; i++) { /// "i" is the row number
					Ae ((degOfFreedom * a) + i, (degOfFreedom * b) + i) +=
							///(w_i, Ctemam_ \partial_j(\tilde{rho}^{k + 1/2} v_j^{k + 1/2}) (tau_M/\rho) \rho \partial_t{ \delta{v_i} }),
							/// since massFluxCrossContrib = \partial_j{w_i}(\tilde{J_j}^{k+\half}
							- (fe.N(a) * Ctemam * divOfRhoExplicitMultipliedWithExplicitVelocity
							   * (tauM/explicitAvgDensity) * 0.5 * Coe_conv * ((fe.N (b)) / dt_)) * detJxW
							///(w_i, Ctemam_ \partial_j(\tilde{rho}^{k + 1/2} v_j^{k + 1/2}) (tau_M/\rho) \rho \tilde{v_j}^{k+\half} \partial_j{v_i}),
							/// since convAvg = \tilde{v_j}^{k+\half} \partial_j{v_i} for a fixed i
							- (fe.N(a) * Ctemam * divOfRhoExplicitMultipliedWithExplicitVelocity * (tauM/explicitAvgDensity) * 0.5 * Coe_conv * convAvg) * detJxW
							///(w_i, Ctemam_ \partial_j(\tilde{rho}^{k + 1/2} v_j^{k + 1/2}) (tau_M/\rho) (-\gamma/\rho) J_j\partial_j{v_i})
							/// since massFlux = 0.5 * \tilde{J_j}^{k+\half} \partial_j{v_i} for a fixed i
							- (fe.N(a) * Ctemam * divOfRhoExplicitMultipliedWithExplicitVelocity * (tauM/explicitAvgDensity) * 0.5 * Coe_massFlux * massFlux) * detJxW
							///(w_i, Ctemam_ \partial_j(\tilde{rho}^{k + 1/2} v_j^{k + 1/2}) (tau_M/\rho) (\eta/Re)\partial_j{\partial_j{v_i}})
							+ (fe.N(a) * Ctemam * divOfRhoExplicitMultipliedWithExplicitVelocity * (tauM/explicitAvgDensity) * 0.5 * Coe_diff * diffJ) * detJxW
							///(w_i,Ctemam_ \partial_j(\tilde{rho}^{k + 1/2} v_j^{k + 1/2}) (tau_M/\rho) (1/Re)\gamma\partial_j{\phi}\partial_j{v_i})
							/// since diffJCrossterm = \partial_j{ \phi } \partial_j{ \delta v_i } for a fixed i
							+ (fe.N(a) * Ctemam * divOfRhoExplicitMultipliedWithExplicitVelocity * (tauM/explicitAvgDensity) * 0.5 * gamma * diffJCrossterm) * detJxW;
				}

				///(w_i, Ctemam_ \partial_j(\tilde{J_j}^{k + 1/2}) (\tau_M/\rho) R_M(\phi,v_i,p))
				for (int i = 0; i < nsd; i++) { /// "i" is the row number
					Ae ((degOfFreedom * a) + i, (degOfFreedom * b) + i) +=
							///(1/Pe)(w_i, Ctemam_ \partial_j(\tilde{J_j}^{k + 1/2}) (tau_M/\rho) \rho \partial_t{ \delta{v_i} }),
							/// since massFluxCrossContrib = \partial_j{w_i}(\tilde{J_j}^{k+\half}
							- (fe.N(a) * Ctemam * Coe_massFlux * div_J_explicit_avg
							   * (tauM/explicitAvgDensity) * 0.5 * Coe_conv * ((fe.N (b)) / dt_)) * detJxW
							///(1/Pe)(w_i, Ctemam_ \partial_j(\tilde{J_j}^{k + 1/2}) (tau_M/\rho) \rho \tilde{v_j}^{k+\half}
							/// \partial_j{v_i}),
							/// since convAvg = \tilde{v_j}^{k+\half} \partial_j{v_i} for a fixed i
							- (fe.N(a) * Ctemam * Coe_massFlux * div_J_explicit_avg * (tauM/explicitAvgDensity) * 0.5 * Coe_conv * convAvg) * detJxW
							///(1/Pe)(w_i, Ctemam_ \partial_j(\tilde{J_j}^{k + 1/2}) (tau_M/\rho)(-\gamma/\rho) J_j \partial_j{\delta v_i})
							/// since massFlux = 0.5 * \tilde{J_j}^{k+\half} \partial_j{v_i} for a fixed i
							- (fe.N(a) * Ctemam * Coe_massFlux * div_J_explicit_avg * (tauM/explicitAvgDensity) * 0.5 * Coe_massFlux * massFlux) * detJxW
							///(1/Pe)(w_i, Ctemam_ \partial_j(\tilde{J_j}^{k + 1/2}) (tau_M/\rho)(\eta/Re)\partial_j{\partial_j{v_i}})
							+ (fe.N(a) * Ctemam * Coe_massFlux * div_J_explicit_avg * (tauM/explicitAvgDensity) * 0.5 * Coe_diff * diffJ) * detJxW
							///(1/Pe)(w_i, Ctemam_ \partial_j(\tilde{J_j}^{k + 1/2}) (tau_M/\rho) (1/Re)\gamma\partial_j{\phi}\partial_j{v_i})
							/// since diffJCrossterm = \partial_j{ \phi } \partial_j{ \delta v_i } for a fixed i
							+ (fe.N(a) * Ctemam * Coe_massFlux * div_J_explicit_avg * (tauM/explicitAvgDensity) * 0.5 * gamma * diffJCrossterm) * detJxW;
				}

				//debug: test for NAN and inf
				//validateMatrix (Ae);
			}
			///---------------------------------Done with Elemental operator Matrix----------------------------------------///
		}
	}

	///****************************************************************************************************************///
	///******************Implementations with custom Theta scheme for the NS block*************************************///
	///****************************************************************************************************************///
	/** This is a variational multiscale solver for CHNS with a custom IMEX scheme for time integration, using the
	 * variable density momentum residual.
	 * We have proven this scheme to be unconditionally energy stable.
	 * @param fe FEMElm object for looping over elements and gauss points
	 * @param Ae Empty Ae matrix to be filled in the method
	 * @param dt timestep
	 * @param t current time
	 * @return
	 */
	void
	CHNSsemiImplicitThetaSecondOrder_ConsvEqAddedbe(const FEMElm &fe, ZEROARRAY<double> &be, const double dt, const
	double t) {

		//validateVector(be);
		//PrintInfo ("position = ", fe.position (), ", iso_position = ", fe.itg_pt (), ", num_pt = ", fe
		//		.n_itg_pts (), ", current_itg_pt = ", fe.cur_itg_pt_num ());
		//PrintInfo ("Printing be");
		//if (GetMPIRank () == 0) {
		//	for (int i = 0; i < be.size (); i++) {
		//		std::cout << be (i) << " ";
		//		std::cout << "\n";
		//	}
		//}

		double dt_ = dt;
		double t_ = t + dt_; ///< current time is elapsed + dt

		int degOfFreedom = DIM;
		int nsd = DIM;
		double Ci_f = this->input_data_->Ci_f;
		int n_basis_functions = fe.nbf (); // # of basis functions
		double detJxW = fe.detJxW ();

		double Re = this->input_data_->Re;           ///<Reynolds number
		double Pe = this->input_data_->Pe; /// Diffusional Peclet number
		double We = this->input_data_->We; /// Weber number
		double Cn = this->input_data_->Cn; /// Cahn number
		double Fr = this->input_data_->Fr; /// Froude number

		/// Physical properties
		double rhoH = input_data_->rhoH;
		double rhoL = input_data_->rhoL;
		double etaH = input_data_->etaH;
		double etaL = input_data_->etaL;

		/// calculate alpha and beta (coefficients in mixture density)
		double alpha = (rhoH - rhoL) / 2.0;
		double beta = (rhoH + rhoL) / 2.0;
		/// calculate gamma and kappa (coefficients in mixture density)
		double gamma = (etaH - etaL) / 2.0;
		double kappa = (etaH + etaL) / 2.0;


		/// Get phi to calculate mixture density of the form given in Abels et al.
		double phi = this->p_data_->valueFEM (fe, CHNSNodeData::PHI);
		double phiPrev1 = this->p_data_->valueFEM (fe, CHNSNodeData::PHI_PRE1);
		double phiPrev2 = this->p_data_->valueFEM (fe, CHNSNodeData::PHI_PRE2);
		std::vector<double> phi_pre_list = {phiPrev1, phiPrev2};
		double phiExplicitAvg = calcExplicitApproxField(phi_pre_list);


		double phiForPropertyCalculation = 0.0;
		if (phi >= 1.0) {
			phiForPropertyCalculation = 1.0;
		} else if (phi <= -1.0) {
			phiForPropertyCalculation = -1.0;
		} else {
			phiForPropertyCalculation = phi;
		}

		double phiForPropertyCalculationPrev = 0.0;
		if (phiPrev1 >= 1.0) {
			phiForPropertyCalculationPrev = 1.0;
		} else if (phiPrev1 <= -1.0) {
			phiForPropertyCalculationPrev = -1.0;
		} else {
			phiForPropertyCalculationPrev = phiPrev1;
		}

		/// Calculate the explicit approximations of averaged Density and viscosity
		//double explicitAvgDensity = calcExplicitDensity(phi_pre_list);
		/// Mixture density explicit
		double explicitAvgDensity = 1.0;
		//double rhoExplicitAvg = calcExplicitDensity(phi_list);
		if (input_data_->caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS && input_data_->manufacSolType == 2) {
			explicitAvgDensity = calcExplicitDensityNoPullBack(phi_pre_list);
		} else {
			explicitAvgDensity = calcExplicitDensity(phi_pre_list);
		}
		double explicitAvgViscosity = calcExplicitViscosity(phi_pre_list);

		double Ctemam = input_data_->Ctemam;

		/// Calculate bunch of coeff
		double Coe_diff = 0.0;
		double Coe_diff_without_eta = 0.0;
		double Coe_conv = 0.0;
		double Coe_crossTerm1 = 0.0;
		double Coe_massFlux = 0.0;
		double Coe_fineScaleMassFlux = 0.0;
		double Coe_pressure = 0.0;
		double Coe_gravity = 0.0;
		double Coe_forcing_equiv = 0.0;
		double Coe_forcing = 0.0;
		if (Re >= 1) {
			Coe_diff = explicitAvgViscosity / Re;
			Coe_diff_without_eta = 1.0 / Re;
			Coe_conv = explicitAvgDensity;
			Coe_crossTerm1 = 0.5;
			Coe_massFlux = 1.0 / (Cn * Pe);
			Coe_fineScaleMassFlux = 0.5 / (Cn * Pe * explicitAvgDensity);
			if (input_data_->rescale_pressure) {
				Coe_pressure = 1.0;
			} else {
				Coe_pressure = 1.0 / We;
			}
			Coe_forcing_equiv = 1.0 / (Cn * We);
			Coe_forcing = Cn /We;
			Coe_gravity = 1.0;
		} else if (Re < 1) {
			Coe_diff = explicitAvgViscosity;
			Coe_diff_without_eta = 1 / Re;
			Coe_conv = explicitAvgDensity * Re;
			Coe_crossTerm1 = Re;
			Coe_massFlux = Re / (Cn * Pe);
			Coe_fineScaleMassFlux = Re / (Cn * Pe * explicitAvgDensity);
			if (input_data_->rescale_pressure) {
				Coe_pressure = Re;
			} else {
				Coe_pressure = Re / We;
			}
			Coe_forcing_equiv = Re / (Cn * We);
			Coe_forcing = (Re * Cn) /We;
			Coe_gravity = Re;
		}


		/// field variables at the previous timestep (level k), where k+1 is current level.


		ZEROPTV u_pre1, u_pre2;
		for (int i = 0; i < nsd; i++) {
			/// u_pre here is solenoidal and VEL_X_SOL, .. are accessed here
			u_pre1 (i) = p_data_->valueFEM (fe, (CHNSNodeData::VEL_X_PRE1 + i));
			/// u_pre2 here is solenoidal and VEL_X_SOL, .. are accessed here
			u_pre2 (i) = p_data_->valueFEM (fe, (CHNSNodeData::VEL_X_PRE2 + i));
		}
		/// explicit approximation of u^{k+\half}
		std::vector<ZEROPTV> vel_pre_list = {u_pre1, u_pre2};
		ZEROPTV uExplicitAvgVelocity = calcExplicitVelocity (vel_pre_list);

		/// Define velocity gradient tensor
		ZeroMatrix<double> duPrev1, duPrev2;
		duPrev1.redim (nsd, nsd);
		duPrev2.redim (nsd, nsd);
		for (int i = 0; i < nsd; i++) {
			for (int j = 0; j < nsd; j++) {
				///both of these are solenoidal
				duPrev1 (i, j) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::VEL_X_PRE1 + i, j);//< timelevel k
				duPrev2 (i, j) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::VEL_X_PRE2 + i, j);//< timelevel k-1
			}
		}



		/// Get gradient of phasefield
		ZEROPTV dphi, dphiPrev1, dphiPrev2;
		for (int i = 0; i < nsd; i++) {
			dphi (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PHI, i);
			dphiPrev1 (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PHI_PRE1, i);
			dphiPrev2 (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PHI_PRE2, i);
		}
		std::vector<ZEROPTV> gradPhi_prev_list = {dphiPrev1, dphiPrev2};
		ZEROPTV gradPhiExplicitAvg = calcExplicitVelocity (gradPhi_prev_list);

		/// ----------------------------------Conservation terms calculation ------------------------------------------///
		double div_u_prev1 = 0.0;
		double div_u_prev2 = 0.0;
		for (int i = 0; i < nsd; i ++ ){
			div_u_prev1 += duPrev1 (i, i);
			div_u_prev2 += duPrev2 (i, i);
		}
		std::vector<double>div_u_prev_list = {div_u_prev1, div_u_prev2};
		double div_u_explicitAvg = calcExplicitField(div_u_prev_list);
		///( \tilde{rho}^{k + 1/2} * \partial_j( \tilde{v_j^{k + 1/2}} ) )
		double rhoMultipliedWithDiv_u_explicitAvg = explicitAvgDensity * div_u_explicitAvg;

		///( \tilde{v_j^{k + 1/2}} * \partial_j(\tilde{rho}^{k + 1/2}) )
		double div_rhoExplicitAvgDottedWithExplicitVelocity = 0.0;
		for (int i = 0; i < nsd; i++) {
			div_rhoExplicitAvgDottedWithExplicitVelocity += alpha * gradPhiExplicitAvg(i) * uExplicitAvgVelocity(i);
		}

		/**
		 * \partial_j(\tilde{rho}^{k + 1/2} v_j^{k + 1/2}) =
		 * ( \tilde{rho}^{k + 1/2} * \partial_j( \tilde{v_j^{k + 1/2}} ) ) + ( \tilde{v_j^{k + 1/2}} * \partial_j(\tilde{rho}^{k + 1/2}) )
		 */
		double divOfRhoExplicitMultipliedWithExplicitVelocity = div_rhoExplicitAvgDottedWithExplicitVelocity +
		                                                        rhoMultipliedWithDiv_u_explicitAvg;
		///-------------------------------------------------------------------------------------------------------------///

		/// Calculate the laplacian of velocity
		ZEROPTV d2uPrev1;
		/// PS: This assumes the first three degrees are always velocity, it is wise to keep it that way
		for (int dir1 = 0; dir1 < nsd; dir1++) {
			/// Summing over three directions of velocity as it is laplacian
			for (int dir2 = 0; dir2 < nsd; dir2++) {
				d2uPrev1 (dir1) += p_data_->value2DerivativeFEM (fe, CHNSNodeData::VEL_X_PRE1 + dir1, dir2, dir2);
			}
		}


		double p_prev = p_data_->valueFEM (fe, CHNSNodeData::PRESSURE_PRE1);
		double p_prev2 = p_data_->valueFEM (fe, CHNSNodeData::PRESSURE_PRE2);
		/// See general formulation given in equation 3.13, 3.14 in Guermond et al. 2006
		/// This controls how many time points of pressure are use to approximate grad{p} term in the velocity predictor.
		double pStar = p_extrap_c[1] * p_prev + p_extrap_c[2] * p_prev2;
		//pStar = this->p_data_->valueFEM (fe, CHNSNodeData::PRESSURE_MANUFAC);

		/// Get gradient of pressure at previous timestep and calculate the gradient of pressure according to 3.14 in
		// Guermond 2006
		ZEROPTV dpPrev, dpPrev2, dpStar;
		for (int i = 0; i < nsd; i++) {
			dpPrev (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PRESSURE_PRE1, i);
			dpPrev2 (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PRESSURE_PRE2, i);
			dpStar (i) = p_extrap_c[1] * dpPrev (i) + p_extrap_c[2] * dpPrev2 (i);
			//dpStar(i) = this->p_data_->valueDerivativeFEM(fe, CHNSNodeData::PRESSURE_MANUFAC, i);
		}


		/// Calculate the Diffusional fluxes from Cahn Hilliard (J in Abels et al.)
		double mu = p_data_->valueFEM (fe, CHNSNodeData::MU);
		ZEROPTV JExplicitAvg, JExact;
		ZEROPTV gradMu, gradmuPrev1, gradmuPrev2, gradmuExact;
		double laplacianMuPrev1 = 0.0;
		double laplacianMuPrev2 = 0.0;
		for (int i = 0; i < nsd; i++) {
			gradMu (i) = p_data_->valueDerivativeFEM (fe, CHNSNodeData::MU, i);
			gradmuPrev1 (i) = p_data_->valueDerivativeFEM (fe, CHNSNodeData::MU_PRE1, i);
			gradmuPrev2 (i) = p_data_->valueDerivativeFEM (fe, CHNSNodeData::MU_PRE2, i);

			laplacianMuPrev1 += p_data_->value2DerivativeFEM (fe, CHNSNodeData::MU_PRE1, i, i);
			laplacianMuPrev2 += p_data_->value2DerivativeFEM (fe, CHNSNodeData::MU_PRE2, i, i);
		}
		std::vector<ZEROPTV> gradMu_prev_list = {gradmuPrev1, gradmuPrev2};
		ZEROPTV gradMuExplicitAvg = calcExplicitVelocity (gradMu_prev_list);
		/// We can compute explicit approximation of J^{k+\half} similar to velocity
		for (int i = 0; i < nsd; i++) {
			JExplicitAvg (i) = ((rhoL - rhoH) / 2) * mobility * gradMuExplicitAvg (i);
			JExact (i) = ((rhoL - rhoH) / 2) * mobility * gradmuExact (i);
		}
		double divJprev1 = ((rhoL - rhoH) / 2) * mobility * laplacianMuPrev1;
		double divJprev2 = ((rhoL - rhoH) / 2) * mobility * laplacianMuPrev2;
		std::vector<double> divJ_prevList = {divJprev1, divJprev2};
		//// \partial_j{\tilde(J^{k + 1/2})}
		double div_J_explicit_avg = calcExplicitField(divJ_prevList);

		/// -------------------------------------body forces ------------------------------------------------------///

		/// Contribution of gravity in terms of Froude number
		ZEROPTV gravity (0.0, 0.0, 0.0);
		gravity.x () = 0.0;
		/// if Fr is specified to be zero in the config file turn gravity off
		if (Fr < 1e-10) {
			gravity.y () = 0;
		} else {
			gravity.y () = -(Coe_gravity / Fr);
		}
		gravity.z () = 0.0;

		/// External forcing: In the current implementation this term is non-zero
		/// only in the case of Manufactured solutions test of CHNS and periodic
		/// channel case
		ZEROPTV forcingNS (0.0, 0.0, 0.0);
		if (input_data_->caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS) {
			forcingNS = calcForcingManufacturedSolutionNS (fe, t_);
		} else if (input_data_->caseTypeForNavierStokes == CHNSInputData::PERIODIC_CHANNEL_FLOW) {
			throw TALYException () << "Not yet implemented forcing for periodic pressure driven flow needs to be added";
		} else {
			forcingNS = {0.0, 0.0, 0.0};
		}

		/// External forcing at t = k-1: In the current implementation this term is non-zero
		/// only in the case of Manufactured solutions test of CHNS and periodic
		/// channel case
		ZEROPTV forcingNSprev1 (0.0, 0.0, 0.0);
		if (input_data_->caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS) {
			forcingNSprev1 = calcForcingManufacturedSolutionNS (fe, t_ - dt_);
		} else if (input_data_->caseTypeForNavierStokes == CHNSInputData::PERIODIC_CHANNEL_FLOW) {
			throw TALYException () << "Not yet implemented forcing for periodic pressure driven flow needs to be added";
		} else {
			forcingNS = {0.0, 0.0, 0.0};
		}


		///---------------------------------VMS parameter settings---------------------------------------------------///
		VMSParams vms_params_current = calc_tau_vel (fe, uExplicitAvgVelocity, JExplicitAvg, phiExplicitAvg,
		                                             explicitAvgDensity, explicitAvgViscosity, Ci_f, dt);
		double tauM = vms_params_current.tauM;
		///---------------------------------VMS parameters calculated------------------------------------------------///

		double pureNSflag = 1.0;
		if (input_data_->pureNS) {
			pureNSflag = 0.0;
		}

		for (int a = 0; a < n_basis_functions; a++) {
			///-------------------------------------Calculating the Elemental operator-------------------------------------///

			/// Auxiliary term useful for calculating things in jacobian (\partial_j{w_i}, v_j)
			double crossTermVelocityPart = 0.0;
			for (int j = 0; j < nsd; j++) {
				crossTermVelocityPart += fe.dN (a, j) * uExplicitAvgVelocity (j);
			}
			double massFluxCrossContrib = 0.0;
			for (int j = 0; j < nsd; j++) {
				massFluxCrossContrib += fe.dN (a, j) * JExplicitAvg (j);
			}


			/// crosstermPrefix =  0.5 * \partial_j{w_i} \tilde{v_j}^{k+\half} \tau_M :  summed over j for each i
			double crosstermPrefix = Coe_crossTerm1 * crossTermVelocityPart * tauM;

			/// massFluxCrossTermPrefix = Coe_fineScaleMassFlux * \partial_j{w_i} \tilde{J_j}^{k+\half} \tau_M :
			/// summed over j for each i
			/// Where, Coe_fineScaleMassFlux = 0.5/(Pe \tilde{\rho}^{k+1/2})
			///        massFluxCrossContrib  = \partial_j{w_i} \tilde{J_j}^{k+\half} = fe.dN(a, j) * JExplicitAvg(j)
			double massFluxCrossTermPrefix = Coe_fineScaleMassFlux * massFluxCrossContrib * tauM;
			///---------------------------------Done with Elemental operator Matrix----------------------------------------///

			///----------------------------------------------Start be------------------------------------------------------///
			/// All the terms involved in the equation
			ZEROPTV convFine, massFluxFine, normal_NS, NScontrib, conservationTermsNScoarse;
			ZEROPTV conservationTermsNSfineMomentum, conservationTermsNSfineMassFlux;


			ZEROPTV convectionCoarse;
			ZEROPTV massFluxCoarse;
			ZEROPTV diff;
			ZEROPTV diffChainTerm;
			ZEROPTV CHNSCouple;
			for (int i = 0; i < nsd; i++) {
				convectionCoarse (i) = 0.0;
				diff (i) = 0.0;
				for (int j = 0; j < nsd; j++) {
					convectionCoarse(i) += Coe_conv * uExplicitAvgVelocity(j) * 0.5 * duPrev1(i, j); ///< coarse advection
					//convectionCoarse (i) += fe.dN (a, j) * Coe_conv * uExplicitAvgVelocity (j) * 0.5 * u_pre1 (i);
					massFluxCoarse(i) += Coe_massFlux * (JExplicitAvg(j) * 0.5 * duPrev1(i, j)); ///< coarse massflux
					//massFluxCoarse (i) += Coe_massFlux * fe.dN (a, j) * JExplicitAvg (j) * 0.5 * u_pre1 (i);
					diff (i) += fe.dN (a, j) * Coe_diff * 0.5 * duPrev1 (i, j);///< Diffusion term
					diffChainTerm (i) += (Coe_diff_without_eta * gamma * 0.5 * gradPhiExplicitAvg (j) * duPrev1 (i, j));
					//CHNSCouple(i) +=  -((Cn/We) * fe.dN(a, j) * 0.5 * (dphi(i) + dphiPrev1(i)) * 0.5 * (dphi(j) + dphiPrev1(j)
					//		))* detJxW;
					//CHNSCouple(i) +=  -((Cn/We) * fe.dN(a, j) * dphiExact(i) * dphiExact(j))* detJxW;
				}
				/// (w_i, \tilde{phi}^{k+1/2} \partial_i(mu^{k+1/2}))
				//CHNSCouple (i) += (Coe_forcing_equiv * pureNSflag * fe.N (a) * phiExplicitAvg * gradMu(i)) * detJxW;
				//CHNSCouple (i) += (Coe_forcing_equiv * pureNSflag * fe.N (a) * 0.5 * (phi + phiPrev1) * gradMu(i)) * detJxW;
			}
			if(input_data_->ifdphidphiForcing) {
				for (int i = 0; i < nsd; i++) {
					CHNSCouple(i) = 0.0;
					for (int j = 0; j < nsd; j++) {
						CHNSCouple(i) +=
								-(Coe_forcing * fe.dN(a, j) * pureNSflag * 0.5 * (dphi(i) + dphiPrev1(i)) * 0.5 * (dphi(j) +dphiPrev1(j)))* detJxW;
//						CHNSCouple(i) +=
//								-0.5 * (Coe_forcing * fe.dN(a, j) * pureNSflag * gradPhiExplicitAvg(i) * 0.5 * (dphi(j) + dphiPrev1(j))) * detJxW
//								- 0.5 * ( Coe_forcing * fe.dN(a, j) * pureNSflag * 0.5 * (dphi(i) +dphiPrev1(i)) * gradPhiExplicitAvg(j) ) * detJxW;
//						CHNSCouple(i) +=
//								-(Coe_forcing * fe.dN(a, j) * pureNSflag * gradPhiExplicitAvg(i) * gradPhiExplicitAvg(j)) * detJxW;
					}
				}
			} else {
				for (int i = 0; i < nsd; i++) {
					CHNSCouple (i) += (Coe_forcing_equiv * pureNSflag * fe.N (a) * phiExplicitAvg * gradMu(i)) * detJxW;
					for (int j = 0; j < nsd; j++) {
						CHNSCouple (i) +=
								Coe_forcing_equiv * pureNSflag * fe.dN(a, i) * mu * 0.5 * (phi  + phiPrev1) * detJxW
								- Coe_forcing * pureNSflag * 0.5 * fe.dN(a, i) * 0.25 * (dphi(j) + dphiPrev1(j)) * (dphi(j) + dphiPrev1(j))*detJxW;
					}
					CHNSCouple (i) += Coe_forcing_equiv * pureNSflag * fe.N(a) *
					                  ( pow((0.5 * (phi + phiPrev1)), 3) - (0.5 *(phi +phiPrev1)) ) * 0.5 * (dphi(i) + dphiPrev1(i)) * detJxW;
				}
			}



			/// Terms of Normal Navier Stokes without any additional VMS terms
			for (int i = 0; i < nsd; i++) { /// "i" is the row number
				normal_NS (i) =
						///(w_i, \tilde{\rho}^{k+1/2} \partial_t{v_i})
						(fe.N (a) * Coe_conv * (u_pre1 (i) / dt_)) * detJxW
						//+ (fe.N(a) * uExplicitAvgVelocity(i) * alpha * (phiPrev1 / dt_)) * detJxW
						///(w_i, \rho v_j \partial_j{v_i})
						- (fe.N(a) * convectionCoarse(i)) * detJxW
						//+ (convectionCoarse (i)) * detJxW
						///1/Pe(w_i, J_j \partial_j{v_i})
						- (fe.N(a) * massFluxCoarse(i)) * detJxW
						//+ (massFluxCoarse (i)) * detJxW
						///1/We(w_i, \partial_i{pStar}) : see Guermond et al. 2006 : eq 3.14 for details
						+ (Coe_pressure * (fe.dN (a, i) * pStar)) * detJxW
						///1/Re(\partial_j{w_i}, \eta \partial_j{v_i})
						- ((diff (i)) * detJxW)
						///(w_i, \rho {gravity{i}})
						+ (fe.N (a) * explicitAvgDensity * gravity (i)) * detJxW
						///(w_i, forcingNS(i))
						+ (fe.N (a) * 0.5 * (forcingNS (i) + forcingNSprev1 (i))) * detJxW;

				conservationTermsNScoarse(i) =
						(pureNSflag * fe.N(a) * Ctemam * ((phiForPropertyCalculation - phiForPropertyCalculationPrev)/dt_) * 0.5 * u_pre1(i)) * detJxW
						+ (fe.N(a) * Ctemam * Coe_massFlux * div_J_explicit_avg * 0.5 * u_pre1(i)) * detJxW
						+ (fe.N(a) * Ctemam * divOfRhoExplicitMultipliedWithExplicitVelocity * 0.5 * u_pre1(i)) * detJxW;
				/// convection fine scale terms
				convFine (i) =
						///0.5 * (\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M \rho \partial_t{v_i^{k}} ),
						(crosstermPrefix * Coe_conv * (u_pre1 (i) / dt_)) * detJxW
						///0.5 * (\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M convAvg(i)),
						/// where convAvg = 0.5 * \tilde{v_j}^{k+\half} \partial_j{v_i^{k}} summer over j for a fixed i
						- (crosstermPrefix * convectionCoarse (i)) * detJxW
						///0.5 * (\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M massFlux(i)),
						/// where massFlux = 0.5 * \tilde{J_j}^{k+\half} \partial_j(v_i) summer over j for a fixed i
						- (crosstermPrefix * massFluxCoarse (i)) * detJxW
						///0.5 * (\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M (1/We * \partial_i(p^k) ))
						- (crosstermPrefix * (Coe_pressure * dpStar (i))) * detJxW
						///0.5 * (\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M (1/Re) \tilde{\eta}^{k+1/2}
						///                                                                  \partial_j(\partial_j (v_i^k)) )
						+ (crosstermPrefix * (Coe_diff * 0.5 * d2uPrev1 (i))) * detJxW
						///0.5 * (\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M (\gamma/Re)\partial_j(\tilde{\eta}^{k+1/2})
						///                                                                                  \partial_j (v_i^k)))
						+ (crosstermPrefix * diffChainTerm (i)) * detJxW
						///0.5 * (\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M (1/Fr) * \tilde{\rho}^{k+1/2} g_i)
						+ (crosstermPrefix * explicitAvgDensity * gravity (i)) * detJxW
						///0.5 * (\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M f_i^{k+1/2})
						+ (crosstermPrefix * 0.5 * (forcingNS (i) + forcingNSprev1 (i))) * detJxW;

				/// mass flux fine scale terms
				massFluxFine (i) =
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M  \rho\partial_t{v_i^{k}} ),
						(massFluxCrossTermPrefix * Coe_conv * (u_pre1 (i) / dt_)) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M convAvg(i)),
						/// where convAvg = 0.5 * \tilde{v_j}^{k+\half} \partial_j{v_i^{k}} summer over j for a fixed i
						- (massFluxCrossTermPrefix * convectionCoarse (i)) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M massFlux(i)),
						/// where massFlux = 0.5 * \tilde{J_j}^{k+\half} \partial_j(v_i) summer over j for a fixed i
						- (massFluxCrossTermPrefix * massFluxCoarse (i)) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M (1/We * \partial_i(p^k) ))
						- (massFluxCrossTermPrefix * (Coe_pressure * dpStar (i))) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M (1/Re) \tilde{\eta}^{k+1/2}
						///                                                                  0.5 *\partial_j(\partial_j(v_i^k)) )
						+ (massFluxCrossTermPrefix * (Coe_diff * 0.5 * d2uPrev1 (i))) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M (\gamma/Re)\partial_j(\tilde{\eta}^{k+1/2})
						///                                                                           0.5 * \partial_j(v_i^k)))
						+ (massFluxCrossTermPrefix * diffChainTerm (i)) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M (1/Fr) * \tilde{\rho}^{k+1/2} g_i)
						+ (massFluxCrossTermPrefix * explicitAvgDensity * gravity (i)) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M f_i^{k+1/2})
						+ (massFluxCrossTermPrefix * 0.5 * (forcingNS (i) + forcingNSprev1 (i))) * detJxW;

				conservationTermsNSfineMomentum(i) = ///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M  \rho\partial_t{v_i^{k}} ),
						- (fe.N(a) * Ctemam * divOfRhoExplicitMultipliedWithExplicitVelocity
						   * (tauM/explicitAvgDensity) * 0.5 * Coe_conv * (u_pre1 (i) / dt_)) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M convAvg(i)),
						/// where convAvg = 0.5 * \tilde{v_j}^{k+\half} \partial_j{v_i^{k}} summer over j for a fixed i
						+ (fe.N(a) * Ctemam * divOfRhoExplicitMultipliedWithExplicitVelocity
						   * (tauM/explicitAvgDensity) * 0.5 * convectionCoarse (i)) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M massFlux(i)),
						/// where massFlux = 0.5 * \tilde{J_j}^{k+\half} \partial_j(v_i) summer over j for a fixed i
						+ (fe.N(a) * Ctemam * divOfRhoExplicitMultipliedWithExplicitVelocity
						   * (tauM/explicitAvgDensity) * 0.5 * massFluxCoarse (i)) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M (1/We * \partial_i(p^k) ))
						+ (fe.N(a) * Ctemam * divOfRhoExplicitMultipliedWithExplicitVelocity
						   * (tauM/explicitAvgDensity) * 0.5 * (Coe_pressure * dpStar (i))) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M (1/Re) \tilde{\eta}^{k+1/2}
						///                                                                  0.5 *\partial_j(\partial_j(v_i^k)) )
						- (fe.N(a) * Ctemam * divOfRhoExplicitMultipliedWithExplicitVelocity
						   * (tauM/explicitAvgDensity) * 0.5 * (Coe_diff * 0.5 * d2uPrev1 (i))) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M (\gamma/Re)\partial_j(\tilde{\eta}^{k+1/2})
						///                                                                           0.5 * \partial_j(v_i^k)))
						- (fe.N(a) * Ctemam * divOfRhoExplicitMultipliedWithExplicitVelocity
						   * (tauM/explicitAvgDensity) * 0.5 * diffChainTerm (i)) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M (1/Fr) * \tilde{\rho}^{k+1/2} g_i)
						- (fe.N(a) * Ctemam * divOfRhoExplicitMultipliedWithExplicitVelocity
						   * (tauM/explicitAvgDensity) * 0.5 * explicitAvgDensity * gravity (i)) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M f_i^{k+1/2})
						- (fe.N(a) * Ctemam * divOfRhoExplicitMultipliedWithExplicitVelocity
						   * (tauM/explicitAvgDensity) * 0.5 * 0.5 * (forcingNS (i) + forcingNSprev1 (i))) * detJxW;

				conservationTermsNSfineMassFlux(i) = ///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M\rho\partial_t{v_i^{k}} ),
						- (fe.N(a) * Ctemam * Coe_massFlux * div_J_explicit_avg
						   * (tauM/explicitAvgDensity) * 0.5 * Coe_conv * (u_pre1 (i) / dt_)) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M convAvg(i)),
						/// where convAvg = 0.5 * \tilde{v_j}^{k+\half} \partial_j{v_i^{k}} summer over j for a fixed i
						+ (fe.N(a) * Ctemam * Coe_massFlux * div_J_explicit_avg
						   * (tauM/explicitAvgDensity) * 0.5 * convectionCoarse (i)) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M massFlux(i)),
						/// where massFlux = 0.5 * \tilde{J_j}^{k+\half} \partial_j(v_i) summer over j for a fixed i
						+ (fe.N(a) * Ctemam * Coe_massFlux * div_J_explicit_avg
						   * (tauM/explicitAvgDensity) * 0.5 * massFluxCoarse (i)) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M (1/We * \partial_i(p^k) ))
						+ (fe.N(a) * Ctemam * Coe_massFlux * div_J_explicit_avg
						   * (tauM/explicitAvgDensity) * 0.5 * (Coe_pressure * dpStar (i))) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M (1/Re) \tilde{\eta}^{k+1/2}
						///                                                                  0.5 *\partial_j(\partial_j(v_i^k)) )
						- (fe.N(a) * Ctemam * Coe_massFlux * div_J_explicit_avg
						   * (tauM/explicitAvgDensity) * 0.5 * (Coe_diff * 0.5 * d2uPrev1 (i))) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M (\gamma/Re)\partial_j(\tilde{\eta}^{k+1/2})
						///                                                                           0.5 * \partial_j(v_i^k)))
						- (fe.N(a) * Ctemam * Coe_massFlux * div_J_explicit_avg
						   * (tauM/explicitAvgDensity) * 0.5 * diffChainTerm (i)) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M (1/Fr) * \tilde{\rho}^{k+1/2} g_i)
						- (fe.N(a) * Ctemam * Coe_massFlux * div_J_explicit_avg
						   * (tauM/explicitAvgDensity) * 0.5 * explicitAvgDensity * gravity (i)) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M f_i^{k+1/2})
						- (fe.N(a) * Ctemam * Coe_massFlux * div_J_explicit_avg
						   * (tauM/explicitAvgDensity) * 0.5 * 0.5 * (forcingNS (i) + forcingNSprev1 (i))) * detJxW;

				//NScontrib(i) =    normal_NS(i) + convFine(i) + massFluxFine(i) + CHNSCouple(i);
				NScontrib (i) = normal_NS (i)
				                + convFine (i)
				                + massFluxFine (i)
				                + conservationTermsNScoarse(i)
				                + conservationTermsNSfineMomentum (i)
				                + conservationTermsNSfineMassFlux (i)
				                - CHNSCouple (i);
			}
			//validateVector (be);

			/// Adding all the calculated terms to the vector
			for (int i = 0; i < nsd; i++) {
				be ((degOfFreedom * a) + i) += NScontrib (i);
			}
		}
	}

	///****************************************************************************************************************///
	///******************Implementations with custom Theta scheme for the NS block*************************************///
	///****************************************************************************************************************///
	/** This is a variational multiscale solver for CHNS with a custom IMEX scheme for time integration, using the
	 * variable density momentum residual.
	 * We have proven this scheme to be unconditionally energy stable.
	 * @param fe FEMElm object for looping over elements and gauss points
	 * @param Ae Empty Ae matrix to be filled in the method
	 * @param dt timestep
	 * @param t current time
	 * @return
	 */
	void CHNSsemiImplicitThetaSecondOrder_ConsvEqAdded_ImplicitDensityAe(const FEMElm &fe, ZeroMatrix<double> &Ae, const
	                                                                   double dt, const double t) {

		//validateVector(be);
		//PrintInfo ("position = ", fe.position (), ", iso_position = ", fe.itg_pt (), ", num_pt = ", fe
		//		.n_itg_pts (), ", current_itg_pt = ", fe.cur_itg_pt_num ());
		//PrintInfo ("Printing be");
		//if (GetMPIRank () == 0) {
		//	for (int i = 0; i < be.size (); i++) {
		//		std::cout << be (i) << " ";
		//		std::cout << "\n";
		//	}
		//}

		double dt_ = dt;
		double t_ = t + dt_; ///< current time is elapsed + dt
		int degOfFreedom = DIM;
		int nsd = DIM;
		double Ci_f = this->input_data_->Ci_f;
		int n_basis_functions = fe.nbf (); // # of basis functions
		double detJxW = fe.detJxW ();


		double Re = this->input_data_->Re; ///< Reynolds number
		double Pe = this->input_data_->Pe; /// Diffusional Peclet number
		double We = this->input_data_->We; /// Weber number
		double Cn = this->input_data_->Cn; /// Cahn number
		double Fr = this->input_data_->Fr; /// Froude number

		/// Physical properties
		double rhoH = input_data_->rhoH;
		double rhoL = input_data_->rhoL;
		double etaH = input_data_->etaH;
		double etaL = input_data_->etaL;

		/// calculate alpha and beta (coefficients in mixture density)
		double alpha = (rhoH - rhoL) / 2.0;
		double beta = (rhoH + rhoL) / 2.0;
		/// calculate gamma and kappa (coefficients in mixture density)
		double gamma = (etaH - etaL) / 2.0;
		double kappa = (etaH + etaL) / 2.0;

		/// Get phi to calculate mixture density of the form given in Abels et al.
		double phi = this->p_data_->valueFEM (fe, CHNSNodeData::PHI);
		double phiPrev1 = this->p_data_->valueFEM (fe, CHNSNodeData::PHI_PRE1);
		double phiPrev2 = this->p_data_->valueFEM (fe, CHNSNodeData::PHI_PRE2);
		std::vector<double> phi_pre_list = {phiPrev1, phiPrev2};
		double phiExplicitAvg = calcExplicitApproxField(phi_pre_list);


		double phiForPropertyCalculation = 0.0;
		if (phi >= 1.0) {
			phiForPropertyCalculation = 1.0;
		} else if (phi <= -1.0) {
			phiForPropertyCalculation = -1.0;
		} else {
			phiForPropertyCalculation = phi;
		}

		/// Make the bounds on phi used for evaluation for mixture density and viscosity
		double phiForPropertyCalculationPrev = 0.0;
		if (phiPrev1 >= 1.0){
			phiForPropertyCalculationPrev = 1.0;
		} else if (phiPrev1 <= -1.0){
			phiForPropertyCalculationPrev = -1.0;
		} else{
			phiForPropertyCalculationPrev = phiPrev1;
		}

		double rhoMix,rhoMixPrev;
		if (input_data_->caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS && input_data_->manufacSolType == 2) {
			rhoMix = ((rhoH - rhoL) / 2) * phi + ((rhoH + rhoL) / 2);
			rhoMixPrev = ((rhoH - rhoL) / 2) * phiPrev1 + ((rhoH + rhoL) / 2);
		} else {
			rhoMix = ((rhoH - rhoL) / 2) * phiForPropertyCalculation + ((rhoH + rhoL) / 2);
			rhoMixPrev = ((rhoH - rhoL) / 2) * phiForPropertyCalculationPrev + ((rhoH + rhoL) / 2);
		}

		double implicitAvgDensity = 0.5 * (rhoMix + rhoMixPrev);

		/// Mixture viscosity based on last SNES iteration
		double etaMix = ((etaH - etaL) / 2) * phiForPropertyCalculation + ((etaH + etaL) / 2);
		double etaMixPrev = ((etaH - etaL) / 2) * phiForPropertyCalculationPrev + ((etaH + etaL) / 2);
		double implicitAvgViscosity = 0.5 * (etaMix + etaMixPrev);

		/// Calculate the explicit approximations of averaged Density and viscosity
		//double explicitAvgDensity = calcExplicitDensity(phi_pre_list);
		/// Mixture density explicit
		double explicitAvgDensity = 1.0;
		//double rhoExplicitAvg = calcExplicitDensity(phi_list);
		if (input_data_->caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS && input_data_->manufacSolType == 2) {
			explicitAvgDensity = calcExplicitDensityNoPullBack(phi_pre_list);
		} else {
			explicitAvgDensity = calcExplicitDensity(phi_pre_list);
		}
		double explicitAvgViscosity = calcExplicitViscosity(phi_pre_list);

		//double Ctemam = input_data_->Ctemam;

		/// Calculate bunch of coeff
		double Coe_diff = 0.0;
		double Coe_diff_without_eta = 0.0;
		double Coe_conv = 0.0;
		double Coe_crossTerm1 = 0.0;
		double Coe_massFlux = 0.0;
		double Coe_fineScaleMassFlux = 0.0;
		double Coe_pressure = 0.0;
		double Coe_gravity = 0.0;
		double Coe_forcing_equiv = 0.0;
		if (Re >= 1) {
			Coe_diff = implicitAvgViscosity / Re;
			Coe_diff_without_eta = 1.0 / Re;
			Coe_conv = implicitAvgDensity;
			Coe_crossTerm1 = 0.5;
			Coe_massFlux = 1.0 / (Cn * Pe);
			Coe_fineScaleMassFlux = 0.5 / (Cn * Pe * implicitAvgDensity);
			if (input_data_->rescale_pressure) {
				Coe_pressure = 1.0;
			} else {
				Coe_pressure = 1.0 / We;
			}
			Coe_forcing_equiv = 1.0 / (Cn * We);
			Coe_gravity = 1.0;
		} else if (Re < 1) {
			Coe_diff = implicitAvgViscosity;
			Coe_diff_without_eta = 1 / Re;
			Coe_conv = implicitAvgDensity * Re;
			Coe_crossTerm1 = Re;
			Coe_massFlux = Re / (Cn * Pe);
			Coe_fineScaleMassFlux = Re / (Cn * Pe * implicitAvgDensity);
			if (input_data_->rescale_pressure) {
				Coe_pressure = Re;
			} else {
				Coe_pressure = Re / We;
			}
			Coe_forcing_equiv = Re / (Cn * We);
			Coe_gravity = Re;
		}


		/// field variables at the previous timestep (level k), where k+1 is current level.


		ZEROPTV u_pre1, u_pre2;
		for (int i = 0; i < nsd; i++) {
			/// u_pre here is solenoidal and VEL_X_SOL, .. are accessed here
			u_pre1 (i) = p_data_->valueFEM (fe, (CHNSNodeData::VEL_X_PRE1 + i));
			/// u_pre2 here is solenoidal and VEL_X_SOL, .. are accessed here
			u_pre2 (i) = p_data_->valueFEM (fe, (CHNSNodeData::VEL_X_PRE2 + i));
		}
		/// explicit approximation of u^{k+\half}
		std::vector<ZEROPTV> vel_pre_list = {u_pre1, u_pre2};
		ZEROPTV uExplicitAvgVelocity = calcExplicitVelocity (vel_pre_list);

		/// Define velocity gradient tensor
		ZeroMatrix<double> duPrev1, duPrev2;
		duPrev1.redim (nsd, nsd);
		duPrev2.redim (nsd, nsd);
		for (int i = 0; i < nsd; i++) {
			for (int j = 0; j < nsd; j++) {
				///both of these are solenoidal
				duPrev1 (i, j) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::VEL_X_PRE1 + i, j);//< timelevel k
				duPrev2 (i, j) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::VEL_X_PRE2 + i, j);//< timelevel k-1
			}
		}



		/// Get gradient of phasefield
		ZEROPTV dphi, dphiPrev1, dphiPrev2, dphiExact;
		for (int i = 0; i < nsd; i++) {
			dphi (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PHI, i);
			dphiPrev1 (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PHI_PRE1, i);
			dphiPrev2 (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PHI_PRE2, i);
		}
		std::vector<ZEROPTV> gradPhi_prev_list = {dphiPrev1, dphiPrev2};
		ZEROPTV gradPhiExplicitAvg = calcExplicitVelocity (gradPhi_prev_list);

		/// ----------------------------------Conservation terms calculation ------------------------------------------///
		double div_u_prev1 = 0.0;
		double div_u_prev2 = 0.0;
		for (int i = 0; i < nsd; i ++ ){
			div_u_prev1 += duPrev1 (i, i);
			div_u_prev2 += duPrev2 (i, i);
		}
		std::vector<double>div_u_prev_list = {div_u_prev1, div_u_prev2};
		double div_u_explicitAvg = calcExplicitField(div_u_prev_list);
		///( \tilde{rho}^{k + 1/2} * \partial_j( \tilde{v_j^{k + 1/2}} ) )
		double rhoMultipliedWithDiv_u_explicitAvg = implicitAvgDensity * div_u_explicitAvg;

		///( \tilde{v_j^{k + 1/2}} * \partial_j(\tilde{rho}^{k + 1/2}) )
		double div_rhoExplicitAvgDottedWithExplicitVelocity = 0.0;
		for (int i = 0; i < nsd; i++) {
			div_rhoExplicitAvgDottedWithExplicitVelocity += alpha * gradPhiExplicitAvg(i) * uExplicitAvgVelocity(i);
		}

		/**
		 * \partial_j(\tilde{rho}^{k + 1/2} v_j^{k + 1/2}) =
		 * ( \tilde{rho}^{k + 1/2} * \partial_j( \tilde{v_j^{k + 1/2}} ) ) + ( \tilde{v_j^{k + 1/2}} * \partial_j(\tilde{rho}^{k + 1/2}) )
		 */
		double divOfRhoExplicitMultipliedWithExplicitVelocity = div_rhoExplicitAvgDottedWithExplicitVelocity +
		                                                        rhoMultipliedWithDiv_u_explicitAvg;
		///-------------------------------------------------------------------------------------------------------------///

		/// Calculate the laplacian of velocity
		ZEROPTV d2uPrev1;
		/// PS: This assumes the first three degrees are always velocity, it is wise to keep it that way
		for (int dir1 = 0; dir1 < nsd; dir1++) {
			/// Summing over three directions of velocity as it is laplacian
			for (int dir2 = 0; dir2 < nsd; dir2++) {
				d2uPrev1 (dir1) += p_data_->value2DerivativeFEM (fe, CHNSNodeData::VEL_X_PRE1 + dir1, dir2, dir2);
			}
		}


		double p_prev = p_data_->valueFEM (fe, CHNSNodeData::PRESSURE_PRE1);
		double p_prev2 = p_data_->valueFEM (fe, CHNSNodeData::PRESSURE_PRE2);
		/// See general formulation given in equation 3.13, 3.14 in Guermond et al. 2006
		/// This controls how many time points of pressure are use to approximate grad{p} term in the velocity predictor.
		double pStar = p_extrap_c[1] * p_prev + p_extrap_c[2] * p_prev2;
		//pStar = this->p_data_->valueFEM (fe, CHNSNodeData::PRESSURE_MANUFAC);

		/// Get gradient of pressure at previous timestep and calculate the gradient of pressure according to 3.14 in
		// Guermond 2006
		ZEROPTV dpPrev, dpPrev2, dpStar;
		for (int i = 0; i < nsd; i++) {
			dpPrev (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PRESSURE_PRE1, i);
			dpPrev2 (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PRESSURE_PRE2, i);
			dpStar (i) = p_extrap_c[1] * dpPrev (i) + p_extrap_c[2] * dpPrev2 (i);
			//dpStar(i) = this->p_data_->valueDerivativeFEM(fe, CHNSNodeData::PRESSURE_MANUFAC, i);
		}


		/// Calculate the Diffusional fluxes from Cahn Hilliard (J in Abels et al.)
		ZEROPTV JExplicitAvg;
		ZEROPTV gradMu, gradmuPrev1, gradmuPrev2;
		double laplacianMuPrev1 = 0.0;
		double laplacianMuPrev2 = 0.0;
		for (int i = 0; i < nsd; i++) {
			gradMu (i) = p_data_->valueDerivativeFEM (fe, CHNSNodeData::MU, i);
			gradmuPrev1 (i) = p_data_->valueDerivativeFEM (fe, CHNSNodeData::MU_PRE1, i);
			gradmuPrev2 (i) = p_data_->valueDerivativeFEM (fe, CHNSNodeData::MU_PRE2, i);

			laplacianMuPrev1 += p_data_->value2DerivativeFEM (fe, CHNSNodeData::MU_PRE1, i, i);
			laplacianMuPrev2 += p_data_->value2DerivativeFEM (fe, CHNSNodeData::MU_PRE2, i, i);
		}
		std::vector<ZEROPTV> gradMu_prev_list = {gradmuPrev1, gradmuPrev2};
		ZEROPTV gradMuExplicitAvg = calcExplicitVelocity (gradMu_prev_list);
		/// We can compute explicit approximation of J^{k+\half} similar to velocity
		for (int i = 0; i < nsd; i++) {
			JExplicitAvg (i) = ((rhoL - rhoH) / 2) * mobility * gradMuExplicitAvg (i);
		}
		double divJprev1 = ((rhoL - rhoH) / 2) * mobility * laplacianMuPrev1;
		double divJprev2 = ((rhoL - rhoH) / 2) * mobility * laplacianMuPrev2;
		std::vector<double> divJ_prevList = {divJprev1, divJprev2};
		//// \partial_j{\tilde(J^{k + 1/2})}
		double div_J_explicit_avg = calcExplicitField(divJ_prevList);


		/// -------------------------------------body forces ------------------------------------------------------///

		/// Contribution of gravity in terms of Froude number
		ZEROPTV gravity (0.0, 0.0, 0.0);
		gravity.x () = 0.0;
		/// if Fr is specified to be zero in the config file turn gravity off
		if (Fr < 1e-10) {
			gravity.y () = 0;
		} else {
			gravity.y () = -(Coe_gravity / Fr);
		}
		gravity.z () = 0.0;

		/// External forcing: In the current implementation this term is non-zero
		/// only in the case of Manufactured solutions test of CHNS and periodic
		/// channel case
		ZEROPTV forcingNS (0.0, 0.0, 0.0);
		if (input_data_->caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS) {
			forcingNS = calcForcingManufacturedSolutionNS (fe, t_);
		} else if (input_data_->caseTypeForNavierStokes == CHNSInputData::PERIODIC_CHANNEL_FLOW) {
			throw TALYException () << "Not yet implemented forcing for periodic pressure driven flow needs to be added";
		} else {
			forcingNS = {0.0, 0.0, 0.0};
		}

		/// External forcing at t = k-1: In the current implementation this term is non-zero
		/// only in the case of Manufactured solutions test of CHNS and periodic
		/// channel case
		ZEROPTV forcingNSprev1 (0.0, 0.0, 0.0);
		if (input_data_->caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS) {
			forcingNSprev1 = calcForcingManufacturedSolutionNS (fe, t_ - dt_);
		} else if (input_data_->caseTypeForNavierStokes == CHNSInputData::PERIODIC_CHANNEL_FLOW) {
			throw TALYException () << "Not yet implemented forcing for periodic pressure driven flow needs to be added";
		} else {
			forcingNS = {0.0, 0.0, 0.0};
		}


		///---------------------------------VMS parameter settings---------------------------------------------------///
		VMSParams vms_params_current = calc_tau_vel (fe, uExplicitAvgVelocity, JExplicitAvg, phiExplicitAvg,
		                                             implicitAvgDensity, implicitAvgViscosity, Ci_f, dt);
		double tauM = vms_params_current.tauM;
		///---------------------------------VMS parameters calculated------------------------------------------------///

		double pureNSflag = 1.0;
		if (input_data_->pureNS) {
			pureNSflag = 0.0;
		}

		for (int a = 0; a < n_basis_functions; a++) {
			///-------------------------------------Calculating the Elemental operator-------------------------------------///

			/// Auxiliary term useful for calculating things in jacobian (\partial_j{w_i}, v_j)
			double crossTermVelocityPart = 0.0;
			for (int j = 0; j < nsd; j++) {
				crossTermVelocityPart += fe.dN (a, j) * uExplicitAvgVelocity (j);
			}
			double massFluxCrossContrib = 0.0;
			for (int j = 0; j < nsd; j++) {
				massFluxCrossContrib += fe.dN (a, j) * JExplicitAvg (j);
			}


			/// crosstermPrefix =  0.5 * \partial_j{w_i} \tilde{v_j}^{k+\half} \tau_M :  summed over j for each i
			double crosstermPrefix = Coe_crossTerm1 * crossTermVelocityPart * tauM;

			/// massFluxCrossTermPrefix = Coe_fineScaleMassFlux * \partial_j{w_i} \tilde{J_j}^{k+\half} \tau_M :
			/// summed over j for each i
			/// Where, Coe_fineScaleMassFlux = 0.5/(Pe \tilde{\rho}^{k+1/2})
			///        massFluxCrossContrib  = \partial_j{w_i} \tilde{J_j}^{k+\half} = fe.dN(a, j) * JExplicitAvg(j)
			double massFluxCrossTermPrefix = Coe_fineScaleMassFlux * massFluxCrossContrib * tauM;

			for (int b = 0; b < n_basis_functions; b++) {
				/// Convection term averaged: u_j \partial_j{ \delta{v_i} }
				double convAvg = 0.0;
				for (int j = 0; j < nsd; j++) {
					convAvg += 0.5 * uExplicitAvgVelocity (j) * fe.dN (b, j);
				}

				///J_j \partial_j{ \delta{v_i} }
				double massFlux = 0.0;
				for (int j = 0; j < nsd; j++) {
					massFlux += 0.5 * JExplicitAvg (j) * fe.dN (b, j);
				}

				/// Contribution of laplacian of velocity(diffusion) to the diagonal
				/// \partial_j{ \partial_j{ \delta{v_i} } }
				double diffJ = 0.0;
				for (int j = 0; j < nsd; j++) {
					diffJ += 0.5 * fe.d2N (b, j, j);
				}

				/// \partial_j{ \phi } \partial_j{ v_i }
				double diffJCrossterm = 0.0;
				for (int j = 0; j < nsd; j++) {
					diffJCrossterm += 0.5 * gradPhiExplicitAvg (j) * fe.dN (b, j);
				}

				double convConsv = 0.0;
				for (int j = 0; j < nsd; j++) {
					convConsv += fe.dN (a, j) * uExplicitAvgVelocity (j) * 0.5 * fe.N (b);
				}

				double massFluxConsv = 0.0;
				for (int j = 0; j < nsd; j++) {
					massFluxConsv += fe.dN (a, j) * JExplicitAvg (j) * 0.5 * fe.N (b);
				}


				/// Adding terms to the Elemental matrix.
				for (int i = 0; i < nsd; i++) {
					Ae ((degOfFreedom * a) + i, (degOfFreedom * b) + i) +=
							///(w_i, \tilde{\rho}^{k+\half} \partial_t(v_i))
							/// Note: Coe_conv = \tilde{\rho}^{k+\half}
							(fe.N (a) * Coe_conv * ((fe.N (b)) / dt_)) * detJxW
							//+(fe.N(a) * uExplicitAvgVelocity(i) * alpha * ((fe.N(b))/ dt_)) * detJxW
							///(w_i, \tilde{\rho}^{k+\half} \tilde{v_j}^{k+\half} 0.5 * \partial_j(v_i)
							///since convAvg = 0.5 * \tilde{v_j}^{k+\half} \partial_j{v_i} for a fixed i
							+ (fe.N(a) * Coe_conv * convAvg) * detJxW
							//- (Coe_conv * convConsv) * detJxW
							///(1/Pe)(w_i, \tilde{v_j}^{k+\half} \partial_j(v_i))
							/// since massFluxAvg = 0.5 * \tilde{J_j}^{k+\half} \partial_j(v_i) for a fixed i
							+ (fe.N(a) * Coe_massFlux * massFlux) * detJxW;
					//- (Coe_massFlux * massFluxConsv) * detJxW;

					for (int j = 0; j < nsd; j++) {
						///Diffusion term
						Ae ((degOfFreedom * a) + i, (degOfFreedom * b) + i) +=
								///(\partial_jw_i, 0.5 * (eta(\phi)/Re)\eta \partial_j{v_i^{k+1}})
								/// The other half of corresponding to above term given by
								/// (\partial_jw_i, 0.5 * (eta(\phi)/Re)\eta\partial_j{v_i^{k}}) will go in be
								(fe.dN (a, j) * Coe_diff * 0.5 * fe.dN (b, j)) * detJxW;
					}
				}
				//debug: test for NAN and inf
				//validateMatrix(Ae);
				/// crossTerm 1: This term is written as,
				/// (\partial_j{w_i}, 0.5 * \tilde{v_j}^{k+\half} \tau_M R_M(\phi, v_i,p)),
				/// (only implicit terms on left hand side)

				///Note:
				/// crosstermPrefix =  0.5 * \partial_j{w_i} \tilde{v_j}^{k+\half} \tau_M :  summed over j for each i
				/// double crosstermPrefix = Coe_crossTerm1 * crossTermVelocityPart * tauM;
				for (int i = 0; i < nsd; i++) {
					Ae ((degOfFreedom * a) + i, (degOfFreedom * b) + i) +=
							///(\partial_j{w_i}, \tilde{v_j}^{k+\half} 0.5 * tau_M \rho \partial_t{ \delta{v_i} }),
							/// since crosstermVelocityAvg = \partial_j{w_i} \tilde{v_j}^{k+\half}
							/// 0.5 comes from Crank-Nicholson type averaging
							(crosstermPrefix * Coe_conv * ((fe.N (b)) / dt_)) * detJxW
							///(\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M \rho v_j \partial_j{\delta v_i}),
							/// since convAvg = 0.5 * \tilde{v_j}^{k+\half} \partial_j{v_i} for a fixed i
							+ (crosstermPrefix * Coe_conv * convAvg) * detJxW
							///(\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M (1/Pe) \tilde{J_j}^{k+\half} \partial_j{v_i})
							/// since massFlux = 0.5 * \tilde{J_j}^{k+\half} \partial_j(v_i) for a fixed i
							+ (crosstermPrefix * Coe_massFlux * massFlux) * detJxW
							///(\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M (1/Re) \eta \partial_j{ \partial_j{ v_i } })
							/// Note: this is only one part of the term
							- (crosstermPrefix * Coe_diff * diffJ) * detJxW
							///(\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M (1/Re) \gamma \partial_j{ \phi } \partial_j{ v_i } )
							/// since diffJCrossterm1 = \partial_j{ \phi } \partial_j{ v_i } for a fixed i
							- (crosstermPrefix * Coe_diff_without_eta * gamma * diffJCrossterm) * detJxW;
				}
				//debug: test for NAN and inf
				//validateMatrix (Ae);
				/// Mass flux cross term: ((1/Pe)\partial_j{w_i}, (\tilde{J_j}^{k+\half}/\rho)\tau_M R_M(\phi,v_i,p))
				///Note:
				/// massFluxCrossTermPrefix = Coe_fineScaleMassFlux * \partial_j{w_i} \tilde{J_j}^{k+\half} \tau_M :
				/// summed over j for each i
				/// Where, Coe_fineScaleMassFlux = 0.5/(Pe \tilde{\rho}^{k+1/2})
				///        massFluxCrossContrib  = \partial_j{w_i} \tilde{J_j}^{k+\half} = fe.dN(a, j) * JExplicitAvg(j)
				///double massFluxCrossTermPrefix = Coe_fineScaleMassFlux * massFluxCrossContrib * tauM;
				for (int i = 0; i < nsd; i++) { /// "i" is the row number
					Ae ((degOfFreedom * a) + i, (degOfFreedom * b) + i) +=
							///(0.5/(Pe \rho))(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M \rho \partial_t{ \delta{v_i} }),
							/// since massFluxCrossContrib = \partial_j{w_i}(\tilde{J_j}^{k+\half}
							(massFluxCrossTermPrefix * Coe_conv * ((fe.N (b)) / dt_)) * detJxW
							///(0.5/(Pe \rho))(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M \rho \tilde{v_j}^{k+\half}
							/// \partial_j{v_i}),
							/// since convAvg = \tilde{v_j}^{k+\half} \partial_j{v_i} for a fixed i
							+ (massFluxCrossTermPrefix * Coe_conv * convAvg) * detJxW
							///(0.5/(Pe^2 \rho))(\partial_j{w_i}, \partial_j{mu} tau_M(-\gamma/\rho) J_j \partial_j{\delta v_i})
							/// since massFlux = 0.5 * \tilde{J_j}^{k+\half} \partial_j{v_i} for a fixed i
							+ (massFluxCrossTermPrefix * Coe_massFlux * massFlux) * detJxW
							///(0.5/Pe\rho)(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M(\eta/Re)\partial_j{\partial_j{v_i}})
							- (massFluxCrossTermPrefix * Coe_diff * diffJ) * detJxW
							///(0.5/Pe\rho)(\partial_j{w_i}, \partial_j{mu} tau_M (1/Re)\gamma\partial_j{\phi}\partial_j{v_i})
							/// since diffJCrossterm = \partial_j{ \phi } \partial_j{ \delta v_i } for a fixed i
							- (massFluxCrossTermPrefix * Coe_diff_without_eta * gamma * diffJCrossterm) * detJxW;
				}

				for (int i = 0; i < nsd; i++) {
					Ae ((degOfFreedom * a) + i, (degOfFreedom * b) + i) +=
							pureNSflag * (fe.N(a) * Ctemam_ * ((phiForPropertyCalculation - phiForPropertyCalculationPrev)/dt_) * 0.5 *
							              fe.N(b)) * detJxW
							///(w_i, Ctemam_ \partial_j(\tilde{v_j}^{k+\half}) 0.5 * v_i^{k+1} ),
							/// 0.5 comes from Crank-Nicholson type averaging
							+ (fe.N(a) * Ctemam_ * divOfRhoExplicitMultipliedWithExplicitVelocity * 0.5 * fe.N(b)) * detJxW
							///(w_i, Ctemam_ \partial_j(\tilde{J_j}^{k+\half}) 0.5 * v_i),
							/// 0.5 comes from Crank-Nicholson type averaging
							+ (fe.N(a) * Ctemam_ * div_J_explicit_avg * 0.5 * fe.N(b)) * detJxW;
				}


				///(w_i, Ctemam_ \partial_j(\tilde{\rho}^{k + 1/2}\tilde{v_j}^{k+\half}) (\tau_M/\rho) R_M(\phi,v_i,p))
				for (int i = 0; i < nsd; i++) { /// "i" is the row number
					Ae ((degOfFreedom * a) + i, (degOfFreedom * b) + i) +=
							///(w_i, Ctemam_ \partial_j(\tilde{rho}^{k + 1/2} v_j^{k + 1/2}) (tau_M/\rho) \rho \partial_t{ \delta{v_i} }),
							/// since massFluxCrossContrib = \partial_j{w_i}(\tilde{J_j}^{k+\half}
							- (fe.N(a) * Ctemam_ * divOfRhoExplicitMultipliedWithExplicitVelocity
							   * (tauM/implicitAvgDensity) * 0.5 * Coe_conv * ((fe.N (b)) / dt_)) * detJxW
							///(w_i, Ctemam_ \partial_j(\tilde{rho}^{k + 1/2} v_j^{k + 1/2}) (tau_M/\rho) \rho \tilde{v_j}^{k+\half} \partial_j{v_i}),
							/// since convAvg = \tilde{v_j}^{k+\half} \partial_j{v_i} for a fixed i
							- (fe.N(a) * Ctemam_ * divOfRhoExplicitMultipliedWithExplicitVelocity * (tauM/implicitAvgDensity) * 0.5
							* Coe_conv * convAvg) * detJxW
							///(w_i, Ctemam_ \partial_j(\tilde{rho}^{k + 1/2} v_j^{k + 1/2}) (tau_M/\rho) (-\gamma/\rho) J_j\partial_j{v_i})
							/// since massFlux = 0.5 * \tilde{J_j}^{k+\half} \partial_j{v_i} for a fixed i
							- (fe.N(a) * Ctemam_ * divOfRhoExplicitMultipliedWithExplicitVelocity * (tauM/implicitAvgDensity) * 0.5 *
							   Coe_massFlux * massFlux) * detJxW
							///(w_i, Ctemam_ \partial_j(\tilde{rho}^{k + 1/2} v_j^{k + 1/2}) (tau_M/\rho) (\eta/Re)\partial_j{\partial_j{v_i}})
							+ (fe.N(a) * Ctemam_ * divOfRhoExplicitMultipliedWithExplicitVelocity * (tauM/implicitAvgDensity) * 0.5 *
							   Coe_diff * diffJ) * detJxW
							///(w_i,Ctemam_ \partial_j(\tilde{rho}^{k + 1/2} v_j^{k + 1/2}) (tau_M/\rho) (1/Re)\gamma\partial_j{\phi}\partial_j{v_i})
							/// since diffJCrossterm = \partial_j{ \phi } \partial_j{ \delta v_i } for a fixed i
							+ (fe.N(a) * Ctemam_ * divOfRhoExplicitMultipliedWithExplicitVelocity * (tauM/implicitAvgDensity) * 0.5 *
							   gamma * diffJCrossterm) * detJxW;
				}

				///(w_i, Ctemam_ \partial_j(\tilde{J_j}^{k + 1/2}) (\tau_M/\rho) R_M(\phi,v_i,p))
				for (int i = 0; i < nsd; i++) { /// "i" is the row number
					Ae ((degOfFreedom * a) + i, (degOfFreedom * b) + i) +=
							///(1/Pe)(w_i, Ctemam_ \partial_j(\tilde{J_j}^{k + 1/2}) (tau_M/\rho) \rho \partial_t{ \delta{v_i} }),
							/// since massFluxCrossContrib = \partial_j{w_i}(\tilde{J_j}^{k+\half}
							- (fe.N(a) * Ctemam_ * Coe_massFlux * div_J_explicit_avg
							   * (tauM/implicitAvgDensity) * 0.5 * Coe_conv * ((fe.N (b)) / dt_)) * detJxW
							///(1/Pe)(w_i, Ctemam_ \partial_j(\tilde{J_j}^{k + 1/2}) (tau_M/\rho) \rho \tilde{v_j}^{k+\half}
							/// \partial_j{v_i}),
							/// since convAvg = \tilde{v_j}^{k+\half} \partial_j{v_i} for a fixed i
							- (fe.N(a) * Ctemam_ * Coe_massFlux * div_J_explicit_avg * (tauM/implicitAvgDensity) * 0.5 * Coe_conv *
							   convAvg) * detJxW
							///(1/Pe)(w_i, Ctemam_ \partial_j(\tilde{J_j}^{k + 1/2}) (tau_M/\rho)(-\gamma/\rho) J_j \partial_j{\delta v_i})
							/// since massFlux = 0.5 * \tilde{J_j}^{k+\half} \partial_j{v_i} for a fixed i
							- (fe.N(a) * Ctemam_ * Coe_massFlux * div_J_explicit_avg * (tauM/implicitAvgDensity) * 0.5 * Coe_massFlux
							   * massFlux) * detJxW
							///(1/Pe)(w_i, Ctemam_ \partial_j(\tilde{J_j}^{k + 1/2}) (tau_M/\rho)(\eta/Re)\partial_j{\partial_j{v_i}})
							+ (fe.N(a) * Ctemam_ * Coe_massFlux * div_J_explicit_avg * (tauM/implicitAvgDensity) * 0.5 * Coe_diff *
							   diffJ) * detJxW
							///(1/Pe)(w_i, Ctemam_ \partial_j(\tilde{J_j}^{k + 1/2}) (tau_M/\rho) (1/Re)\gamma\partial_j{\phi}\partial_j{v_i})
							/// since diffJCrossterm = \partial_j{ \phi } \partial_j{ \delta v_i } for a fixed i
							+ (fe.N(a) * Ctemam_ * Coe_massFlux * div_J_explicit_avg * (tauM/implicitAvgDensity) * 0.5 * gamma *
							   diffJCrossterm) * detJxW;
				}

				//debug: test for NAN and inf
				//validateMatrix (Ae);
			}
			///---------------------------------Done with Elemental operator Matrix----------------------------------------///
		}
	}

	///****************************************************************************************************************///
	///******************Implementations with custom Theta scheme for the NS block*************************************///
	///****************************************************************************************************************///
	/** This is a variational multiscale solver for CHNS with a custom IMEX scheme for time integration, using the
	 * variable density momentum residual.
	 * We have proven this scheme to be unconditionally energy stable.
	 * @param fe FEMElm object for looping over elements and gauss points
	 * @param Ae Empty Ae matrix to be filled in the method
	 * @param dt timestep
	 * @param t current time
	 * @return
	 */
	void CHNSsemiImplicitThetaSecondOrder_ConsvEqAdded_ImplicitDensitybe(const FEMElm &fe, ZEROARRAY<double> &be, const
	                                                                   double dt, const double t) {

		//validateVector(be);
		//PrintInfo ("position = ", fe.position (), ", iso_position = ", fe.itg_pt (), ", num_pt = ", fe
		//		.n_itg_pts (), ", current_itg_pt = ", fe.cur_itg_pt_num ());
		//PrintInfo ("Printing be");
		//if (GetMPIRank () == 0) {
		//	for (int i = 0; i < be.size (); i++) {
		//		std::cout << be (i) << " ";
		//		std::cout << "\n";
		//	}
		//}

		double dt_ = dt;
		double t_ = t + dt_; ///< current time is elapsed + dt
		int degOfFreedom = DIM;
		int nsd = DIM;
		double Ci_f = this->input_data_->Ci_f;
		int n_basis_functions = fe.nbf (); // # of basis functions
		double detJxW = fe.detJxW ();

		double Re = this->input_data_->Re; ///< Reynolds number
		double Pe = this->input_data_->Pe; /// Diffusional Peclet number
		double We = this->input_data_->We; /// Weber number
		double Cn = this->input_data_->Cn; /// Cahn number
		double Fr = this->input_data_->Fr; /// Froude number

		/// Physical properties
		double rhoH = input_data_->rhoH;
		double rhoL = input_data_->rhoL;
		double etaH = input_data_->etaH;
		double etaL = input_data_->etaL;

		/// calculate alpha and beta (coefficients in mixture density)
		double alpha = (rhoH - rhoL) / 2.0;
		double beta = (rhoH + rhoL) / 2.0;
		/// calculate gamma and kappa (coefficients in mixture density)
		double gamma = (etaH - etaL) / 2.0;
		double kappa = (etaH + etaL) / 2.0;

		/// Get phi to calculate mixture density of the form given in Abels et al.
		double phi = this->p_data_->valueFEM (fe, CHNSNodeData::PHI);
		double phiPrev1 = this->p_data_->valueFEM (fe, CHNSNodeData::PHI_PRE1);
		double phiPrev2 = this->p_data_->valueFEM (fe, CHNSNodeData::PHI_PRE2);
		std::vector<double> phi_pre_list = {phiPrev1, phiPrev2};
		double phiExplicitAvg = calcExplicitApproxField(phi_pre_list);


		double phiForPropertyCalculation = 0.0;
		if (phi >= 1.0) {
			phiForPropertyCalculation = 1.0;
		} else if (phi <= -1.0) {
			phiForPropertyCalculation = -1.0;
		} else {
			phiForPropertyCalculation = phi;
		}

		/// Make the bounds on phi used for evaluation for mixture density and viscosity
		double phiForPropertyCalculationPrev = 0.0;
		if (phiPrev1 >= 1.0){
			phiForPropertyCalculationPrev = 1.0;
		} else if (phiPrev1 <= -1.0){
			phiForPropertyCalculationPrev = -1.0;
		} else{
			phiForPropertyCalculationPrev = phiPrev1;
		}

		double rhoMix,rhoMixPrev;
		if (input_data_->caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS && input_data_->manufacSolType == 2) {
			rhoMix = ((rhoH - rhoL) / 2) * phi + ((rhoH + rhoL) / 2);
			rhoMixPrev = ((rhoH - rhoL) / 2) * phiPrev1 + ((rhoH + rhoL) / 2);
		} else {
			rhoMix = ((rhoH - rhoL) / 2) * phiForPropertyCalculation + ((rhoH + rhoL) / 2);
			rhoMixPrev = ((rhoH - rhoL) / 2) * phiForPropertyCalculationPrev + ((rhoH + rhoL) / 2);
		}

		double implicitAvgDensity = 0.5 * (rhoMix + rhoMixPrev);

		/// Mixture viscosity based on last SNES iteration
		double etaMix = ((etaH - etaL) / 2) * phiForPropertyCalculation + ((etaH + etaL) / 2);
		double etaMixPrev = ((etaH - etaL) / 2) * phiForPropertyCalculationPrev + ((etaH + etaL) / 2);
		double implicitAvgViscosity = 0.5 * (etaMix + etaMixPrev);

		/// Calculate the explicit approximations of averaged Density and viscosity
		//double explicitAvgDensity = calcExplicitDensity(phi_pre_list);
		/// Mixture density explicit
		double explicitAvgDensity = 1.0;
		//double rhoExplicitAvg = calcExplicitDensity(phi_list);
		if (input_data_->caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS && input_data_->manufacSolType == 2) {
			explicitAvgDensity = calcExplicitDensityNoPullBack(phi_pre_list);
		} else {
			explicitAvgDensity = calcExplicitDensity(phi_pre_list);
		}
		double explicitAvgViscosity = calcExplicitViscosity(phi_pre_list);

		//double Ctemam = input_data_->Ctemam;

		/// Calculate bunch of coeff
		double Coe_diff = 0.0;
		double Coe_diff_without_eta = 0.0;
		double Coe_conv = 0.0;
		double Coe_crossTerm1 = 0.0;
		double Coe_massFlux = 0.0;
		double Coe_fineScaleMassFlux = 0.0;
		double Coe_pressure = 0.0;
		double Coe_gravity = 0.0;
		double Coe_forcing_equiv = 0.0;
		double Coe_forcing = 0.0;
		if (Re >= 1) {
			Coe_diff = implicitAvgViscosity / Re;
			Coe_diff_without_eta = 1.0 / Re;
			Coe_conv = implicitAvgDensity;
			Coe_crossTerm1 = 0.5;
			Coe_massFlux = 1.0 / (Cn * Pe);
			Coe_fineScaleMassFlux = 0.5 / (Cn * Pe * implicitAvgDensity);
			if (input_data_->rescale_pressure) {
				Coe_pressure = 1.0;
			} else {
				Coe_pressure = 1.0 / We;
			}
			Coe_forcing_equiv = 1.0 / (Cn * We);
			Coe_forcing = Cn/We;
			Coe_gravity = 1.0;
		} else if (Re < 1) {
			Coe_diff = implicitAvgViscosity;
			Coe_diff_without_eta = 1 / Re;
			Coe_conv = implicitAvgDensity * Re;
			Coe_crossTerm1 = Re;
			Coe_massFlux = Re / (Cn * Pe);
			Coe_fineScaleMassFlux = Re / (Cn * Pe * implicitAvgDensity);
			if (input_data_->rescale_pressure) {
				Coe_pressure = Re;
			} else {
				Coe_pressure = Re / We;
			}
			Coe_forcing_equiv = Re / (Cn * We);
			Coe_forcing = (Re * Cn)/ We;
			Coe_gravity = Re;
		}


		/// field variables at the previous timestep (level k), where k+1 is current level.


		ZEROPTV u_pre1, u_pre2;
		for (int i = 0; i < nsd; i++) {
			/// u_pre here is solenoidal and VEL_X_SOL, .. are accessed here
			u_pre1 (i) = p_data_->valueFEM (fe, (CHNSNodeData::VEL_X_PRE1 + i));
			/// u_pre2 here is solenoidal and VEL_X_SOL, .. are accessed here
			u_pre2 (i) = p_data_->valueFEM (fe, (CHNSNodeData::VEL_X_PRE2 + i));
		}
		/// explicit approximation of u^{k+\half}
		std::vector<ZEROPTV> vel_pre_list = {u_pre1, u_pre2};
		ZEROPTV uExplicitAvgVelocity = calcExplicitVelocity (vel_pre_list);

		/// Define velocity gradient tensor
		ZeroMatrix<double> duPrev1, duPrev2;
		duPrev1.redim (nsd, nsd);
		duPrev2.redim (nsd, nsd);
		for (int i = 0; i < nsd; i++) {
			for (int j = 0; j < nsd; j++) {
				///both of these are solenoidal
				duPrev1 (i, j) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::VEL_X_PRE1 + i, j);//< timelevel k
				duPrev2 (i, j) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::VEL_X_PRE2 + i, j);//< timelevel k-1
			}
		}



		/// Get gradient of phasefield
		ZEROPTV dphi, dphiPrev1, dphiPrev2, dphiExact;
		for (int i = 0; i < nsd; i++) {
			dphi (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PHI, i);
			dphiPrev1 (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PHI_PRE1, i);
			dphiPrev2 (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PHI_PRE2, i);
		}
		std::vector<ZEROPTV> gradPhi_prev_list = {dphiPrev1, dphiPrev2};
		ZEROPTV gradPhiExplicitAvg = calcExplicitVelocity (gradPhi_prev_list);

		/// ----------------------------------Conservation terms calculation ------------------------------------------///
		double div_u_prev1 = 0.0;
		double div_u_prev2 = 0.0;
		for (int i = 0; i < nsd; i ++ ){
			div_u_prev1 += duPrev1 (i, i);
			div_u_prev2 += duPrev2 (i, i);
		}
		std::vector<double>div_u_prev_list = {div_u_prev1, div_u_prev2};
		double div_u_explicitAvg = calcExplicitField(div_u_prev_list);
		///( \tilde{rho}^{k + 1/2} * \partial_j( \tilde{v_j^{k + 1/2}} ) )
		double rhoMultipliedWithDiv_u_explicitAvg = implicitAvgDensity * div_u_explicitAvg;

		///( \tilde{v_j^{k + 1/2}} * \partial_j(\tilde{rho}^{k + 1/2}) )
		double div_rhoExplicitAvgDottedWithExplicitVelocity = 0.0;
		for (int i = 0; i < nsd; i++) {
			div_rhoExplicitAvgDottedWithExplicitVelocity += alpha * gradPhiExplicitAvg(i) * uExplicitAvgVelocity(i);
		}

		/**
		 * \partial_j(\tilde{rho}^{k + 1/2} v_j^{k + 1/2}) =
		 * ( \tilde{rho}^{k + 1/2} * \partial_j( \tilde{v_j^{k + 1/2}} ) ) + ( \tilde{v_j^{k + 1/2}} * \partial_j(\tilde{rho}^{k + 1/2}) )
		 */
		double divOfRhoExplicitMultipliedWithExplicitVelocity = div_rhoExplicitAvgDottedWithExplicitVelocity +
		                                                        rhoMultipliedWithDiv_u_explicitAvg;
		///-------------------------------------------------------------------------------------------------------------///

		/// Calculate the laplacian of velocity
		ZEROPTV d2uPrev1;
		/// PS: This assumes the first three degrees are always velocity, it is wise to keep it that way
		for (int dir1 = 0; dir1 < nsd; dir1++) {
			/// Summing over three directions of velocity as it is laplacian
			for (int dir2 = 0; dir2 < nsd; dir2++) {
				d2uPrev1 (dir1) += p_data_->value2DerivativeFEM (fe, CHNSNodeData::VEL_X_PRE1 + dir1, dir2, dir2);
			}
		}


		double p_prev = p_data_->valueFEM (fe, CHNSNodeData::PRESSURE_PRE1);
		double p_prev2 = p_data_->valueFEM (fe, CHNSNodeData::PRESSURE_PRE2);
		/// See general formulation given in equation 3.13, 3.14 in Guermond et al. 2006
		/// This controls how many time points of pressure are use to approximate grad{p} term in the velocity predictor.
		double pStar = p_extrap_c[1] * p_prev + p_extrap_c[2] * p_prev2;
		//pStar = this->p_data_->valueFEM (fe, CHNSNodeData::PRESSURE_MANUFAC);

		/// Get gradient of pressure at previous timestep and calculate the gradient of pressure according to 3.14 in
		// Guermond 2006
		ZEROPTV dpPrev, dpPrev2, dpStar;
		for (int i = 0; i < nsd; i++) {
			dpPrev (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PRESSURE_PRE1, i);
			dpPrev2 (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PRESSURE_PRE2, i);
			dpStar (i) = p_extrap_c[1] * dpPrev (i) + p_extrap_c[2] * dpPrev2 (i);
			//dpStar(i) = this->p_data_->valueDerivativeFEM(fe, CHNSNodeData::PRESSURE_MANUFAC, i);
		}


		/// Calculate the Diffusional fluxes from Cahn Hilliard (J in Abels et al.)
		double mu = p_data_->valueFEM (fe, CHNSNodeData::MU);
		ZEROPTV JExplicitAvg;
		ZEROPTV gradMu, gradmuPrev1, gradmuPrev2;
		double laplacianMuPrev1 = 0.0;
		double laplacianMuPrev2 = 0.0;
		for (int i = 0; i < nsd; i++) {
			gradMu (i) = p_data_->valueDerivativeFEM (fe, CHNSNodeData::MU, i);
			gradmuPrev1 (i) = p_data_->valueDerivativeFEM (fe, CHNSNodeData::MU_PRE1, i);
			gradmuPrev2 (i) = p_data_->valueDerivativeFEM (fe, CHNSNodeData::MU_PRE2, i);

			laplacianMuPrev1 += p_data_->value2DerivativeFEM (fe, CHNSNodeData::MU_PRE1, i, i);
			laplacianMuPrev2 += p_data_->value2DerivativeFEM (fe, CHNSNodeData::MU_PRE2, i, i);
		}
		std::vector<ZEROPTV> gradMu_prev_list = {gradmuPrev1, gradmuPrev2};
		ZEROPTV gradMuExplicitAvg = calcExplicitVelocity (gradMu_prev_list);
		/// We can compute explicit approximation of J^{k+\half} similar to velocity
		for (int i = 0; i < nsd; i++) {
			JExplicitAvg (i) = ((rhoL - rhoH) / 2) * mobility * gradMuExplicitAvg (i);
		}
		double divJprev1 = ((rhoL - rhoH) / 2) * mobility * laplacianMuPrev1;
		double divJprev2 = ((rhoL - rhoH) / 2) * mobility * laplacianMuPrev2;
		std::vector<double> divJ_prevList = {divJprev1, divJprev2};
		//// \partial_j{\tilde(J^{k + 1/2})}
		double div_J_explicit_avg = calcExplicitField(divJ_prevList);


		/// -------------------------------------body forces ------------------------------------------------------///

		/// Contribution of gravity in terms of Froude number
		ZEROPTV gravity (0.0, 0.0, 0.0);
		gravity.x () = 0.0;
		/// if Fr is specified to be zero in the config file turn gravity off
		if (Fr < 1e-10) {
			gravity.y () = 0;
		} else {
			gravity.y () = -(Coe_gravity / Fr);
		}
		gravity.z () = 0.0;

		/// External forcing: In the current implementation this term is non-zero
		/// only in the case of Manufactured solutions test of CHNS and periodic
		/// channel case
		ZEROPTV forcingNS (0.0, 0.0, 0.0);
		if (input_data_->caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS) {
			forcingNS = calcForcingManufacturedSolutionNS (fe, t_);
		} else if (input_data_->caseTypeForNavierStokes == CHNSInputData::PERIODIC_CHANNEL_FLOW) {
			throw TALYException () << "Not yet implemented forcing for periodic pressure driven flow needs to be added";
		} else {
			forcingNS = {0.0, 0.0, 0.0};
		}

		/// External forcing at t = k-1: In the current implementation this term is non-zero
		/// only in the case of Manufactured solutions test of CHNS and periodic
		/// channel case
		ZEROPTV forcingNSprev1 (0.0, 0.0, 0.0);
		if (input_data_->caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS) {
			forcingNSprev1 = calcForcingManufacturedSolutionNS (fe, t_ - dt_);
		} else if (input_data_->caseTypeForNavierStokes == CHNSInputData::PERIODIC_CHANNEL_FLOW) {
			throw TALYException () << "Not yet implemented forcing for periodic pressure driven flow needs to be added";
		} else {
			forcingNS = {0.0, 0.0, 0.0};
		}


		///---------------------------------VMS parameter settings---------------------------------------------------///
		VMSParams vms_params_current = calc_tau_vel (fe, uExplicitAvgVelocity, JExplicitAvg, phiExplicitAvg,
		                                             implicitAvgDensity, implicitAvgViscosity, Ci_f, dt);
		double tauM = vms_params_current.tauM;
		///---------------------------------VMS parameters calculated------------------------------------------------///

		double pureNSflag = 1.0;
		if (input_data_->pureNS) {
			pureNSflag = 0.0;
		}

		for (int a = 0; a < n_basis_functions; a++) {
			///-------------------------------------Calculating the Elemental operator-------------------------------------///

			/// Auxiliary term useful for calculating things in jacobian (\partial_j{w_i}, v_j)
			double crossTermVelocityPart = 0.0;
			for (int j = 0; j < nsd; j++) {
				crossTermVelocityPart += fe.dN (a, j) * uExplicitAvgVelocity (j);
			}
			double massFluxCrossContrib = 0.0;
			for (int j = 0; j < nsd; j++) {
				massFluxCrossContrib += fe.dN (a, j) * JExplicitAvg (j);
			}


			/// crosstermPrefix =  0.5 * \partial_j{w_i} \tilde{v_j}^{k+\half} \tau_M :  summed over j for each i
			double crosstermPrefix = Coe_crossTerm1 * crossTermVelocityPart * tauM;

			/// massFluxCrossTermPrefix = Coe_fineScaleMassFlux * \partial_j{w_i} \tilde{J_j}^{k+\half} \tau_M :
			/// summed over j for each i
			/// Where, Coe_fineScaleMassFlux = 0.5/(Pe \tilde{\rho}^{k+1/2})
			///        massFluxCrossContrib  = \partial_j{w_i} \tilde{J_j}^{k+\half} = fe.dN(a, j) * JExplicitAvg(j)
			double massFluxCrossTermPrefix = Coe_fineScaleMassFlux * massFluxCrossContrib * tauM;

			///----------------------------------------------Start be------------------------------------------------------///
			/// All the terms involved in the equation
			ZEROPTV convFine, massFluxFine, normal_NS, NScontrib, conservationTermsNScoarse;
			ZEROPTV conservationTermsNSfineMomentum, conservationTermsNSfineMassFlux;


			ZEROPTV convectionCoarse;
			ZEROPTV massFluxCoarse;
			ZEROPTV diff;
			ZEROPTV diffChainTerm;
			ZEROPTV CHNSCouple;
			for (int i = 0; i < nsd; i++) {
				convectionCoarse (i) = 0.0;
				diff (i) = 0.0;
				for (int j = 0; j < nsd; j++) {
					convectionCoarse(i) += Coe_conv * uExplicitAvgVelocity(j) * 0.5 * duPrev1(i, j); ///< coarse advection
					//convectionCoarse (i) += fe.dN (a, j) * Coe_conv * uExplicitAvgVelocity (j) * 0.5 * u_pre1 (i);
					massFluxCoarse(i) += Coe_massFlux * (JExplicitAvg(j) * 0.5 * duPrev1(i, j)); ///< coarse massflux
					//massFluxCoarse (i) += Coe_massFlux * fe.dN (a, j) * JExplicitAvg (j) * 0.5 * u_pre1 (i);
					diff (i) += fe.dN (a, j) * Coe_diff * 0.5 * duPrev1 (i, j);///< Diffusion term
					diffChainTerm (i) += (Coe_diff_without_eta * gamma * 0.5 * gradPhiExplicitAvg (j) * duPrev1 (i, j));
					//CHNSCouple(i) +=  -((Cn/We) * fe.dN(a, j) * 0.5 * (dphi(i) + dphiPrev1(i)) * 0.5 * (dphi(j) + dphiPrev1(j)
					//		))* detJxW;
					//CHNSCouple(i) +=  -((Cn/We) * fe.dN(a, j) * dphiExact(i) * dphiExact(j))* detJxW;
				}
				/// (w_i, \tilde{phi}^{k+1/2} \partial_i(mu^{k+1/2}))
				//CHNSCouple (i) += (Coe_forcing_equiv * pureNSflag * fe.N (a) * phiExplicitAvg * gradMu(i)) * detJxW;
				//CHNSCouple (i) += (Coe_forcing_equiv * pureNSflag * fe.N (a) * 0.5 * (phi + phiPrev1) * gradMu(i)) * detJxW;
			}
			if(input_data_->ifdphidphiForcing) {
				for (int i = 0; i < nsd; i++) {
					CHNSCouple(i) = 0.0;
					for (int j = 0; j < nsd; j++) {
						CHNSCouple(i) +=
								-(Coe_forcing * fe.dN(a, j) * pureNSflag * 0.5 * (dphi(i) + dphiPrev1(i)) * 0.5 * (dphi(j) +dphiPrev1(j)))* detJxW;
//						CHNSCouple(i) +=
//								-0.5 * (Coe_forcing * fe.dN(a, j) * pureNSflag * gradPhiExplicitAvg(i) * 0.5 * (dphi(j) + dphiPrev1(j))) * detJxW
//								- 0.5 * ( Coe_forcing * fe.dN(a, j) * pureNSflag * 0.5 * (dphi(i) +dphiPrev1(i)) * gradPhiExplicitAvg(j) ) * detJxW;
//						CHNSCouple(i) +=
//								-(Coe_forcing * fe.dN(a, j) * pureNSflag * gradPhiExplicitAvg(i) * gradPhiExplicitAvg(j)) * detJxW;
					}
				}
			} else {
				for (int i = 0; i < nsd; i++) {
					CHNSCouple (i) += (Coe_forcing_equiv * pureNSflag * fe.N (a) * phiExplicitAvg * gradMu(i)) * detJxW;
//					for (int j = 0; j < nsd; j++) {
//						CHNSCouple (i) +=
//								Coe_forcing_equiv * pureNSflag * fe.dN(a, i) * mu * 0.5 * (phi  + phiPrev1) * detJxW
//								- Coe_forcing * pureNSflag * 0.5 * fe.dN(a, i) * 0.25 * (dphi(j) + dphiPrev1(j)) * (dphi(j) + dphiPrev1(j))*detJxW;
//					}
//					CHNSCouple (i) += Coe_forcing_equiv * pureNSflag * fe.N(a) *
//					                  ( pow((0.5 * (phi + phiPrev1)), 3) - (0.5 *(phi +phiPrev1)) ) * 0.5 * (dphi(i) + dphiPrev1(i)) * detJxW;
				}
			}



			/// Terms of Normal Navier Stokes without any additional VMS terms
			for (int i = 0; i < nsd; i++) { /// "i" is the row number
				normal_NS (i) =
						///(w_i, \tilde{\rho}^{k+1/2} \partial_t{v_i})
						(fe.N (a) * Coe_conv * (u_pre1 (i) / dt_)) * detJxW
						//+ (fe.N(a) * uExplicitAvgVelocity(i) * alpha * (phiPrev1 / dt_)) * detJxW
						///(w_i, \rho v_j \partial_j{v_i})
						- (fe.N(a) * convectionCoarse(i)) * detJxW
						//+ (convectionCoarse (i)) * detJxW
						///1/Pe(w_i, J_j \partial_j{v_i})
						- (fe.N(a) * massFluxCoarse(i)) * detJxW
						//+ (massFluxCoarse (i)) * detJxW
						///1/We(w_i, \partial_i{pStar}) : see Guermond et al. 2006 : eq 3.14 for details
						+ (Coe_pressure * (fe.dN (a, i) * pStar)) * detJxW
						///1/Re(\partial_j{w_i}, \eta \partial_j{v_i})
						- ((diff (i)) * detJxW)
						///(w_i, \rho {gravity{i}})
						+ (fe.N (a) * implicitAvgDensity * gravity (i)) * detJxW
						///(w_i, forcingNS(i))
						+ (fe.N (a) * 0.5 * (forcingNS (i) + forcingNSprev1 (i))) * detJxW;

				conservationTermsNScoarse(i) =
						(pureNSflag * fe.N(a) * Ctemam_ * ((phiForPropertyCalculation - phiForPropertyCalculationPrev)/dt_) * 0.5
						* u_pre1(i)) * detJxW
						+ (fe.N(a) * Ctemam_ * Coe_massFlux * div_J_explicit_avg * 0.5 * u_pre1(i)) * detJxW
						+ (fe.N(a) * Ctemam_ * divOfRhoExplicitMultipliedWithExplicitVelocity * 0.5 * u_pre1(i)) * detJxW;
				/// convection fine scale terms
				convFine (i) =
						///0.5 * (\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M \rho \partial_t{v_i^{k}} ),
						(crosstermPrefix * Coe_conv * (u_pre1 (i) / dt_)) * detJxW
						///0.5 * (\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M convAvg(i)),
						/// where convAvg = 0.5 * \tilde{v_j}^{k+\half} \partial_j{v_i^{k}} summer over j for a fixed i
						- (crosstermPrefix * convectionCoarse (i)) * detJxW
						///0.5 * (\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M massFlux(i)),
						/// where massFlux = 0.5 * \tilde{J_j}^{k+\half} \partial_j(v_i) summer over j for a fixed i
						- (crosstermPrefix * massFluxCoarse (i)) * detJxW
						///0.5 * (\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M (1/We * \partial_i(p^k) ))
						- (crosstermPrefix * (Coe_pressure * dpStar (i))) * detJxW
						///0.5 * (\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M (1/Re) \tilde{\eta}^{k+1/2}
						///                                                                  \partial_j(\partial_j (v_i^k)) )
						+ (crosstermPrefix * (Coe_diff * 0.5 * d2uPrev1 (i))) * detJxW
						///0.5 * (\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M (\gamma/Re)\partial_j(\tilde{\eta}^{k+1/2})
						///                                                                                  \partial_j (v_i^k)))
						+ (crosstermPrefix * diffChainTerm (i)) * detJxW
						///0.5 * (\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M (1/Fr) * \tilde{\rho}^{k+1/2} g_i)
						+ (crosstermPrefix * implicitAvgDensity * gravity (i)) * detJxW
						///0.5 * (\partial_j{w_i}, \tilde{v_j}^{k+\half} tau_M f_i^{k+1/2})
						+ (crosstermPrefix * 0.5 * (forcingNS (i) + forcingNSprev1 (i))) * detJxW;

				/// mass flux fine scale terms
				massFluxFine (i) =
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M  \rho\partial_t{v_i^{k}} ),
						(massFluxCrossTermPrefix * Coe_conv * (u_pre1 (i) / dt_)) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M convAvg(i)),
						/// where convAvg = 0.5 * \tilde{v_j}^{k+\half} \partial_j{v_i^{k}} summer over j for a fixed i
						- (massFluxCrossTermPrefix * convectionCoarse (i)) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M massFlux(i)),
						/// where massFlux = 0.5 * \tilde{J_j}^{k+\half} \partial_j(v_i) summer over j for a fixed i
						- (massFluxCrossTermPrefix * massFluxCoarse (i)) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M (1/We * \partial_i(p^k) ))
						- (massFluxCrossTermPrefix * (Coe_pressure * dpStar (i))) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M (1/Re) \tilde{\eta}^{k+1/2}
						///                                                                  0.5 *\partial_j(\partial_j(v_i^k)) )
						+ (massFluxCrossTermPrefix * (Coe_diff * 0.5 * d2uPrev1 (i))) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M (\gamma/Re)\partial_j(\tilde{\eta}^{k+1/2})
						///                                                                           0.5 * \partial_j(v_i^k)))
						+ (massFluxCrossTermPrefix * diffChainTerm (i)) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M (1/Fr) * \tilde{\rho}^{k+1/2} g_i)
						+ (massFluxCrossTermPrefix * implicitAvgDensity * gravity (i)) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M f_i^{k+1/2})
						+ (massFluxCrossTermPrefix * 0.5 * (forcingNS (i) + forcingNSprev1 (i))) * detJxW;

				conservationTermsNSfineMomentum(i) = ///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M  \rho\partial_t{v_i^{k}} ),
						- (fe.N(a) * Ctemam_ * divOfRhoExplicitMultipliedWithExplicitVelocity
						   * (tauM/implicitAvgDensity) * 0.5 * Coe_conv * (u_pre1 (i) / dt_)) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M convAvg(i)),
						/// where convAvg = 0.5 * \tilde{v_j}^{k+\half} \partial_j{v_i^{k}} summer over j for a fixed i
						+ (fe.N(a) * Ctemam_ * divOfRhoExplicitMultipliedWithExplicitVelocity
						   * (tauM/implicitAvgDensity) * 0.5 * convectionCoarse (i)) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M massFlux(i)),
						/// where massFlux = 0.5 * \tilde{J_j}^{k+\half} \partial_j(v_i) summer over j for a fixed i
						+ (fe.N(a) * Ctemam_ * divOfRhoExplicitMultipliedWithExplicitVelocity
						   * (tauM/implicitAvgDensity) * 0.5 * massFluxCoarse (i)) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M (1/We * \partial_i(p^k) ))
						+ (fe.N(a) * Ctemam_ * divOfRhoExplicitMultipliedWithExplicitVelocity
						   * (tauM/implicitAvgDensity) * 0.5 * (Coe_pressure * dpStar (i))) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M (1/Re) \tilde{\eta}^{k+1/2}
						///                                                                  0.5 *\partial_j(\partial_j(v_i^k)) )
						- (fe.N(a) * Ctemam_ * divOfRhoExplicitMultipliedWithExplicitVelocity
						   * (tauM/implicitAvgDensity) * 0.5 * (Coe_diff * 0.5 * d2uPrev1 (i))) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M (\gamma/Re)\partial_j(\tilde{\eta}^{k+1/2})
						///                                                                           0.5 * \partial_j(v_i^k)))
						- (fe.N(a) * Ctemam_ * divOfRhoExplicitMultipliedWithExplicitVelocity
						   * (tauM/implicitAvgDensity) * 0.5 * diffChainTerm (i)) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M (1/Fr) * \tilde{\rho}^{k+1/2} g_i)
						- (fe.N(a) * Ctemam_ * divOfRhoExplicitMultipliedWithExplicitVelocity
						   * (tauM/implicitAvgDensity) * 0.5 * implicitAvgDensity * gravity (i)) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M f_i^{k+1/2})
						- (fe.N(a) * Ctemam_ * divOfRhoExplicitMultipliedWithExplicitVelocity
						   * (tauM/implicitAvgDensity) * 0.5 * 0.5 * (forcingNS (i) + forcingNSprev1 (i))) * detJxW;

				conservationTermsNSfineMassFlux(i) = ///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M\rho\partial_t{v_i^{k}} ),
						- (fe.N(a) * Ctemam_ * Coe_massFlux * div_J_explicit_avg
						   * (tauM/implicitAvgDensity) * 0.5 * Coe_conv * (u_pre1 (i) / dt_)) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M convAvg(i)),
						/// where convAvg = 0.5 * \tilde{v_j}^{k+\half} \partial_j{v_i^{k}} summer over j for a fixed i
						+ (fe.N(a) * Ctemam_ * Coe_massFlux * div_J_explicit_avg
						   * (tauM/implicitAvgDensity) * 0.5 * convectionCoarse (i)) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M massFlux(i)),
						/// where massFlux = 0.5 * \tilde{J_j}^{k+\half} \partial_j(v_i) summer over j for a fixed i
						+ (fe.N(a) * Ctemam_ * Coe_massFlux * div_J_explicit_avg
						   * (tauM/implicitAvgDensity) * 0.5 * massFluxCoarse (i)) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M (1/We * \partial_i(p^k) ))
						+ (fe.N(a) * Ctemam_ * Coe_massFlux * div_J_explicit_avg
						   * (tauM/implicitAvgDensity) * 0.5 * (Coe_pressure * dpStar (i))) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M (1/Re) \tilde{\eta}^{k+1/2}
						///                                                                  0.5 *\partial_j(\partial_j(v_i^k)) )
						- (fe.N(a) * Ctemam_ * Coe_massFlux * div_J_explicit_avg
						   * (tauM/implicitAvgDensity) * 0.5 * (Coe_diff * 0.5 * d2uPrev1 (i))) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M (\gamma/Re)\partial_j(\tilde{\eta}^{k+1/2})
						///                                                                           0.5 * \partial_j(v_i^k)))
						- (fe.N(a) * Ctemam_ * Coe_massFlux * div_J_explicit_avg
						   * (tauM/implicitAvgDensity) * 0.5 * diffChainTerm (i)) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M (1/Fr) * \tilde{\rho}^{k+1/2} g_i)
						- (fe.N(a) * Ctemam_ * Coe_massFlux * div_J_explicit_avg
						   * (tauM/implicitAvgDensity) * 0.5 * implicitAvgDensity * gravity (i)) * detJxW
						///(0.5/(Pe*\rho))*(\partial_j{w_i}, \tilde{J_j}^{k+\half} tau_M f_i^{k+1/2})
						- (fe.N(a) * Ctemam_ * Coe_massFlux * div_J_explicit_avg
						   * (tauM/implicitAvgDensity) * 0.5 * 0.5 * (forcingNS (i) + forcingNSprev1 (i))) * detJxW;

				//NScontrib(i) =    normal_NS(i) + convFine(i) + massFluxFine(i) + CHNSCouple(i);
				NScontrib (i) = normal_NS (i)
				                + convFine (i)
				                + massFluxFine (i)
				                + conservationTermsNScoarse(i)
				                + conservationTermsNSfineMomentum (i)
				                + conservationTermsNSfineMassFlux (i)
				                - CHNSCouple (i);
			}
			//validateVector (be);

			/// Adding all the calculated terms to the vector
			for (int i = 0; i < nsd; i++) {
				be ((degOfFreedom * a) + i) += NScontrib (i);
			}
		}
//		PrintInfo ("position = ", fe.position (), ", iso_position = ", fe.itg_pt (), ", num_pt = ", fe
//				.n_itg_pts (), ", current_itg_pt = ", fe.cur_itg_pt_num ());
//		PrintInfo ("Printing be");
//		if (GetMPIRank () == 0) {
//			for (int i = 0; i < be.size (); i++) {
//				std::cout << be (i) << " ";
//				std::cout << "\n";
//			}
//		}
	}

#pragma mark Projection-integrands

	/** Helmholtz decomposition projection step. It updates pressure
	 * @param fe FEMElm object for looping over elements and gauss points
	 * @param Ae Empty Ae matrix to be filled in the method
	 * @param dt timestep
	 * @param t current time
	 * @return
	 */
	void NSIntegrandsPressurePoissonAe(const FEMElm &fe, ZeroMatrix<double> &Ae, const double dt, const double t) {
		int degOfFreedom = 1;
		double dt_ = dt;
		double t_ = t + dt_;

		int nsd = DIM;
		int n_basis_functions = fe.nbf (); // # of basis functions
		double detJxW = fe.detJxW ();

		/// Physical properties
		double rhoH = input_data_->rhoH;
		double rhoL = input_data_->rhoL;
		double etaH = input_data_->etaH;
		double etaL = input_data_->etaL;

		double We = input_data_->We; /// Weber number

		/// Get phi to calculate mixture density of the form given in Abels et al.
		double phi = this->p_data_->valueFEM(fe, CHNSNodeData::PHI);
		double phiPrev1 = this->p_data_->valueFEM(fe, CHNSNodeData::PHI_PRE1);
		double phiPrev2 = this->p_data_->valueFEM(fe, CHNSNodeData::PHI_PRE2);
		std::vector <double> phi_list = {phiPrev1, phiPrev2};

		/// Make the bounds on phi used for evaluation for mixture density and viscosity
		double phiForPropertyCalculation = 0.0;
		if (phi >= 1.0){
			phiForPropertyCalculation = 1.0;
		} else if (phi <= -1.0){
			phiForPropertyCalculation = -1.0;
		} else{
			phiForPropertyCalculation = phi;
		}

		/// Make the bounds on phi used for evaluation for mixture density and viscosity
		double phiForPropertyCalculationPrev = 0.0;
		if (phiPrev1 >= 1.0){
			phiForPropertyCalculationPrev = 1.0;
		} else if (phiPrev1 <= -1.0){
			phiForPropertyCalculationPrev = -1.0;
		} else{
			phiForPropertyCalculationPrev = phiPrev1;
		}

		double phiAvg = 0.5 * (phi + phiPrev1);
		double phiAvgPullback = 0.5 * (phiForPropertyCalculation + phiForPropertyCalculationPrev);
		/// calculate alpha and beta (coefficients in mixture density)
		double alpha = (input_data_->rhoH - input_data_->rhoL) / 2.0;
		double beta = (input_data_->rhoH + input_data_->rhoL) / 2.0;
		double rhoImplicitAvg = 1.0;
		if (input_data_->caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS && input_data_->manufacSolType == 2) {
			rhoImplicitAvg = (alpha * phiAvg) + beta;
		} else {
			rhoImplicitAvg = (alpha * phiAvgPullback) + beta;
		}


		/// Mixture density explicit
		double rhoExplicitAvg = 1.0;
		//double rhoExplicitAvg = calcExplicitDensity(phi_list);
		if (input_data_->caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS && input_data_->manufacSolType == 2) {
			rhoExplicitAvg = calcExplicitDensityNoPullBack (phi_list);
		} else {
			rhoExplicitAvg = calcExplicitDensity (phi_list);
		}


		double Coeff_pressure_poisson = 0.0;
		if (input_data_->ifRhoNuImplicit){ /// if the velocity block is linear, rho is explicit
			if (input_data_->rescale_pressure){
				Coeff_pressure_poisson = (1.0/rhoImplicitAvg);
			} else {
				Coeff_pressure_poisson = (1.0/(rhoImplicitAvg * We));
			}
		} else {
			if (input_data_->rescale_pressure){
				Coeff_pressure_poisson = (1.0/rhoExplicitAvg); //(1.0/rhoExplicitAvg);// (1.0/rhoMix);//(1.0/rhoExplicitAvg);
			} else {
				Coeff_pressure_poisson = (1.0/(rhoExplicitAvg * We));
			}
		}

		for (int a = 0; a < n_basis_functions; a++) {
			///---------------Calculating the Elemental operator------------///
			for (int b = 0; b < n_basis_functions; b++) {
				for (int i = 0; i < nsd; i++) {
					Ae ((degOfFreedom * a) + 0, (degOfFreedom * b) + 0) +=
							///(\partial_i(w), \partial_i(p))
							(Coeff_pressure_poisson * fe.dN (a, i) * fe.dN (b, i)) * detJxW;
				}
			}
		}
	}

	/** Helmholtz decomposition projection step. It updates pressure
	 * @param fe FEMElm object for looping over elements and gauss points
	 * @param be Empty be vector to be filled in the method
	 * @param dt timestep
	 * @param t current time
	 * @return
	 */
	void NSIntegrandsPressurePoissonbe(const FEMElm &fe, ZEROARRAY<double> &be, const double dt, const double t) {
		double dt_ = dt;
		double t_ = t + dt_; ///< current time is elapsed + dt
		int degOfFreedom = 1;

		double Re = this->input_data_->Re; ///< Reynolds number
		double Pe = this->input_data_->Pe; /// Diffusional Peclet number
		double We = this->input_data_->We; /// Weber number
		double Cn = this->input_data_->Cn; /// Cahn number
		double Fr = this->input_data_->Fr; /// Froude number

		int nsd = DIM;
		double Ci_f = this->input_data_->Ci_f;
		int n_basis_functions = fe.nbf(); // # of basis functions
		double detJxW = fe.detJxW();

		/// Physical properties
		double rhoH = input_data_->rhoH;
		double rhoL = input_data_->rhoL;
		double etaH = input_data_->etaH;
		double etaL = input_data_->etaL;


		/// Get phi to calculate mixture density of the form given in Abels et al.
		double phi = this->p_data_->valueFEM(fe, CHNSNodeData::PHI);
		double phiPrev1 = this->p_data_->valueFEM(fe, CHNSNodeData::PHI_PRE1);
		double phiPrev2 = this->p_data_->valueFEM(fe, CHNSNodeData::PHI_PRE2);
		std::vector <double> phi_list = {phiPrev1, phiPrev2};
		double phiExplicitAvg = calcExplicitApproxField(phi_list);

		/// Make the bounds on phi used for evaluation for mixture density and viscosity
		double phiForPropertyCalculation = 0.0;
		if (phi >= 1.0){
			phiForPropertyCalculation = 1.0;
		} else if (phi <= -1.0){
			phiForPropertyCalculation = -1.0;
		} else{
			phiForPropertyCalculation = phi;
		}

		/// Make the bounds on phi used for evaluation for mixture density and viscosity
		double phiForPropertyCalculationPrev = 0.0;
		if (phiPrev1 >= 1.0){
			phiForPropertyCalculationPrev = 1.0;
		} else if (phiPrev1 <= -1.0){
			phiForPropertyCalculationPrev = -1.0;
		} else{
			phiForPropertyCalculationPrev = phiPrev1;
		}

		double phiAvg = 0.5 * (phi + phiPrev1);
		double phiAvgPullback = 0.5 * (phiForPropertyCalculation + phiForPropertyCalculationPrev);
		/// calculate alpha and beta (coefficients in mixture density)
		double alpha = (input_data_->rhoH - input_data_->rhoL) / 2.0;
		double beta = (input_data_->rhoH + input_data_->rhoL) / 2.0;
		double rhoImplicitAvg = 1.0;
		if (input_data_->caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS && input_data_->manufacSolType == 2) {
			rhoImplicitAvg = (alpha * phiAvg) + beta;
		} else {
			rhoImplicitAvg = (alpha * phiAvgPullback) + beta;
		}

		/// calculate gamma and kappa (coefficients in mixture density)
		double gamma = (etaH - etaL) / 2.0;
		double kappa = (etaH + etaL) / 2.0;

		/// Mixture viscosity based on last SNES iteration
		double etaMix = ((etaH - etaL) / 2) * phiForPropertyCalculation + ((etaH + etaL) / 2);
		double etaMixPrev = ((etaH - etaL) / 2) * phiForPropertyCalculationPrev + ((etaH + etaL) / 2);

		double viscosityImplicitAvg = 0.5 * (etaMix + etaMixPrev);

		/// Mixture density explicit
		double rhoExplicitAvg = 1.0;
		//double rhoExplicitAvg = calcExplicitDensity(phi_list);
		if (input_data_->caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS && input_data_->manufacSolType == 2) {
			rhoExplicitAvg = calcExplicitDensityNoPullBack (phi_list);
		} else {
			rhoExplicitAvg = calcExplicitDensity (phi_list);
		}

		/// Mixture viscosity based on last SNES iteration
		double viscosityExplicitAvg = calcExplicitViscosity (phi_list);

		/// This helps when Re is extremely small, being in the denominator can make the solution blowup
		double Coe_diff = 0.0;
		double Coe_diff_avg = 0.0;
		double Coe_diff_without_eta = 0.0;
		double Coe_conv = 0.0;
		double Coe_conv_avg = 0.0;
		double Coe_crossTerm1 = 0.0;
		double Coe_crossTerm2 = 0.0;
		double Coe_ReynoldsStressTerm = 0.0;
		double Coe_massFlux = 0.0;
		double Coe_pressure = 0.0;
		double Coe_finescalePressure = 0.0;
		double Coe_finescaleContinuity = 0.0;
		double Coe_gravity = 0.0;
		double Coe_forcing = 0.0;
		double Coe_forcing_equiv = 0.0;
		if (Re > 1) {
			Coe_diff_without_eta = 1.0/Re;
			if (input_data_->ifRhoNuImplicit) {
				Coe_diff = viscosityImplicitAvg / Re;
				Coe_conv = rhoImplicitAvg * 1.0;
			} else {
				Coe_diff = viscosityExplicitAvg / Re;
				Coe_conv = rhoExplicitAvg * 1.0;
			}
			Coe_massFlux = 1.0 / (Cn * Pe);
			if (input_data_->rescale_pressure){
				Coe_pressure = 1.0;
			} else {
				Coe_pressure = 1.0 / We;
			}
			Coe_gravity = 1.0;
			Coe_forcing_equiv = 1.0/(Cn * We);
		} else if (Re <= 1) {
			if (input_data_->ifRhoNuImplicit) {
				Coe_diff = viscosityImplicitAvg ;
				Coe_conv = rhoImplicitAvg * Re;
			} else {
				Coe_diff = viscosityExplicitAvg ;
				Coe_conv = rhoExplicitAvg * Re;
			}
			Coe_diff_without_eta = 1/Re;
			Coe_massFlux = Re / (Cn * Pe);
			if (input_data_->rescale_pressure) {
				Coe_pressure = Re;
			} else {
				Coe_pressure = Re / We;
			}
			Coe_gravity = Re;
			Coe_forcing_equiv = Re/(Cn * We);
		}

		ZEROPTV u, u_pre1, u_pre2, uExplicitAvg;
		for (int i = 0; i < nsd; i++) {
			u(i) = this->p_data_->valueFEM(fe, i);
			/// u_pre here is solenoidal and VEL_X_SOL, .. are accessed here
			u_pre1(i) = p_data_->valueFEM(fe, (CHNSNodeData::VEL_X_PRE1 + i));
			u_pre2(i) = p_data_->valueFEM(fe, (CHNSNodeData::VEL_X_PRE2 + i));
		}
		std::vector<ZEROPTV> vel_list = {u_pre1, u_pre2};
		uExplicitAvg = calcExplicitVelocity (vel_list);

		/// Define velocity gradient tensor
		ZeroMatrix<double> du,duPrev1;
		du.redim(nsd, nsd);
		duPrev1.redim(nsd, nsd);
		for (int i = 0; i < nsd; i++) {
			for (int j = 0; j < nsd; j++) {
				du(i, j) = this->p_data_->valueDerivativeFEM(fe, i, j);
				duPrev1(i, j) = this->p_data_->valueDerivativeFEM(fe, CHNSNodeData::VEL_X_PRE1 + i, j);
			}
		}

		/// Get gradient of phasefield
		ZEROPTV dphi,dphiPrev1,dphiPrev2;
		for (int i = 0; i < nsd; i++) {
			dphi(i) = this->p_data_->valueDerivativeFEM(fe, CHNSNodeData::PHI, i);
			dphiPrev1(i) = this->p_data_->valueDerivativeFEM(fe, CHNSNodeData::PHI_PRE1, i);
			dphiPrev2(i) = this->p_data_->valueDerivativeFEM(fe, CHNSNodeData::PHI_PRE2, i);
		}
		std::vector<ZEROPTV> gradPhi_prev_list = {dphiPrev1, dphiPrev2};
		ZEROPTV gradPhiExplicitAvg = calcExplicitVelocity (gradPhi_prev_list);

		/// Calculate the laplacian of velocity This is required for course scale residual of Navier-Stokes for diffusion
		/// term
		ZEROPTV d2u,d2uPrev1;
		/// loop for three directions of velocity (MAX NSD is no of velocity directions in the problem)
		/// PS: This assumes the first three degrees are always velocity, it is wise to keep it that way
		for (int dir1 = 0; dir1 < nsd; dir1++) {
			/// Summing over three directions of velocity as it is laplacian
			for (int dir2 = 0; dir2 < nsd; dir2++) {
				d2u(dir1) += p_data_->value2DerivativeFEM(fe, dir1, dir2, dir2);
				d2uPrev1(dir1) += p_data_->value2DerivativeFEM(fe, (CHNSNodeData::VEL_X_PRE1 + dir1), dir2, dir2);
			}
		}

		/// Pressure fields setup
		double p = p_data_->valueFEM(fe, CHNSNodeData::PRESSURE);
		double p_prev1 = p_data_->valueFEM(fe, CHNSNodeData::PRESSURE_PRE1);
		double p_prev2 = p_data_->valueFEM(fe, CHNSNodeData::PRESSURE_PRE2);
		double pStar = p_extrap_c[1] * p_prev1 + p_extrap_c[2] * p_prev2;

		/// Get gradient of pressure at current and previous timestep
		ZEROPTV dp, dpPrev1, dpPrev2, dpStar;
		for (int i = 0; i < nsd; i++) {
			dp(i) = this->p_data_->valueDerivativeFEM(fe, CHNSNodeData::PRESSURE, i);
			dpPrev1(i) = p_data_->valueDerivativeFEM(fe, CHNSNodeData::PRESSURE_PRE1, i);
			dpPrev2(i) = p_data_->valueDerivativeFEM(fe, CHNSNodeData::PRESSURE_PRE2, i);
			dpStar(i) = p_extrap_c[1] * dpPrev1(i) + p_extrap_c[2] * dpPrev2(i);
		}

		/// Calculate the Diffusional fluxes from Cahn Hilliard (J in Abels et al.)
		ZEROPTV J, JPrev, JExplicitAvg;
		ZEROPTV gradmu, gradmuPrev1, gradmuPrev2;
		for (int i = 0; i < nsd; i++) {
			gradmu(i) = p_data_->valueDerivativeFEM(fe, CHNSNodeData::MU, i);
			gradmuPrev1(i) = p_data_->valueDerivativeFEM(fe, CHNSNodeData::MU_PRE1, i);
			gradmuPrev2(i) = p_data_->valueDerivativeFEM(fe, CHNSNodeData::MU_PRE2, i);
			J(i) = ((rhoL - rhoH) / 2) * mobility * gradmu(i);
			JPrev(i) = ((rhoL - rhoH) / 2) * mobility * gradmuPrev1(i);
		}
		std::vector<ZEROPTV> gradMu_prev_list = {gradmuPrev1, gradmuPrev2};
		ZEROPTV gradMuExplicitAvg = calcExplicitVelocity (gradMu_prev_list);
		/// We can compute explicit approximation of J^{k+\half} similar to velocity
		for (int i = 0; i < nsd; i++) {
			JExplicitAvg(i) = ((rhoL - rhoH) / 2) * mobility * gradMuExplicitAvg(i);
		}

		/// Contribution of gravity in terms of Froude number
		ZEROPTV gravity;
		gravity.x() = 0.0;
		/// if Fr is specified to be zero in the config file turn gravity off
		if (Fr == 0.0) {
			gravity.y() = 0;
		} else {
			gravity.y() = -(Coe_gravity / Fr);
		}
		gravity.z() = 0.0;

		/// External forcing: In the current implementation this term is non-zero
		/// only in the case of Manufactured solutions test of CHNS and periodic
		/// channel case
		ZEROPTV forcingNS, forcingNSPrev1;
		if (input_data_->caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS) {
			forcingNS = calcForcingManufacturedSolutionNS(fe, t_);
			forcingNSPrev1 = calcForcingManufacturedSolutionNS (fe, t_ - dt_);
		} else {
			forcingNS = {0.0, 0.0, 0.0};
			forcingNSPrev1 = {0.0, 0.0, 0.0};
		}

		///---------------------------------VMS parameter settings---------------------------------------------------///
		VMSParams vms_params_current;
		if (input_data_->ifRhoNuImplicit) {
			vms_params_current = calc_tau_vel(fe, u, J, phiExplicitAvg, rhoImplicitAvg,
			                                  viscosityImplicitAvg, Ci_f, dt);
		} else {
			vms_params_current = calc_tau_vel(fe, u, J, phiExplicitAvg, rhoExplicitAvg,
			                                  viscosityExplicitAvg, Ci_f, dt);
		}
		double tauM = vms_params_current.tauM;
		//double tauM = 0.0;
		///---------------------------------VMS parameters calculated------------------------------------------------///
		/** We define the convection of Navier Stokes here from
		* using inner product of gradient tensor and fields we acquired
		* above.
		*/
		ZEROPTV convectionPreviousNewtonIt;
		ZEROPTV diffusionPreviousNewtonItTerm1;
		ZEROPTV massFluxMuResidPreviousNewtonIt;
		ZEROPTV convectionPreviousNewtonItNoCoeff;
		ZEROPTV diffusionPreviousNewtonItTerm2;
		for (int i = 0; i < nsd; i++) {
			convectionPreviousNewtonIt(i) = 0.0;
			diffusionPreviousNewtonItTerm1(i) += Coe_diff * 0.5 * (d2u(i) + d2uPrev1(i));
			for (int j = 0; j < nsd; j++) {
				convectionPreviousNewtonIt(i) += Coe_conv * 0.5 * (u(j) + u_pre1(j)) * 0.5 * (du(i, j) + duPrev1(i, j));
				//convectionPreviousNewtonIt(i) += Coe_conv * uExplicitAvg(j) * 0.5 * (du(i, j) + duPrev1(i, j));
				convectionPreviousNewtonItNoCoeff(i) += 0.5 * (u(j) + u_pre1(j)) * 0.5 * (du(i, j) + duPrev1(i, j));
				//convectionPreviousNewtonItNoCoeff(i) += uExplicitAvg(j) * 0.5 * (du(i, j) + duPrev1(i, j));
				/// Chain Rule
				massFluxMuResidPreviousNewtonIt(i) += Coe_massFlux * 0.5 * (J(j) + JPrev(j)) * 0.5 * (du(i, j) + duPrev1(i,j));
				//massFluxMuResidPreviousNewtonIt(i) += Coe_massFlux * JExplicitAvg(j) * 0.5 * (du(i, j) + duPrev1(i,j));
				///
				diffusionPreviousNewtonItTerm2(i) +=
						Coe_diff_without_eta * gamma * gradPhiExplicitAvg(j) * 0.5 * (du(i,j) + duPrev1(i,j));
			}
		}

		ZEROPTV timeDerivative;
		for (int i = 0; i < nsd; i++) {
			timeDerivative(i) = (u(i) - u_pre1(i)) / dt_;
		}

		/** Construct the Navier Stokes equation without diffusion
		* Here diffusion is not present as its contribution to stabilizers
		* for
		* linear basis functions is zero
		* Diffusion term is added for higher order basis functions
		* Equation 61 of Bazilevs et al. (2007)
		*/
		ZEROPTV momentumResidual;
		for (int i = 0; i < nsd; i++) {
			if (input_data_->ifRhoNuImplicit) {
				momentumResidual(i) = (Coe_conv * timeDerivative(i))
				                      + convectionPreviousNewtonIt(i)
				                      + (massFluxMuResidPreviousNewtonIt(i))
				                      + (Coe_pressure * dpStar(i))
				                      - diffusionPreviousNewtonItTerm1(i)
				                      - diffusionPreviousNewtonItTerm2(i)
				                      - (rhoImplicitAvg * gravity(i))
				                      - 0.5 * (forcingNS(i) + forcingNSPrev1(i))
				                      - (Coe_forcing_equiv * phiExplicitAvg * gradmu(i));
			} else {
				momentumResidual(i) = (Coe_conv * timeDerivative(i))
				                      + convectionPreviousNewtonIt(i)
				                      + (massFluxMuResidPreviousNewtonIt(i))
				                      + (Coe_pressure * dpStar(i))
				                      - diffusionPreviousNewtonItTerm1(i)
				                      - diffusionPreviousNewtonItTerm2(i)
				                      - (rhoExplicitAvg * gravity(i))
				                      - 0.5 * (forcingNS(i) + forcingNSPrev1(i))
				                      - (Coe_forcing_equiv * phiExplicitAvg * 0.5 * (gradmu(i) + gradmuPrev1(i)));
			}
		}

		ZEROPTV u_fine;
		for (int i = 0; i < nsd; i++) {
			if (input_data_->ifRhoNuImplicit) {
				u_fine(i) = -tauM * (1.0/rhoImplicitAvg) * momentumResidual(i);
			} else {
				u_fine(i) = -tauM * (1.0/rhoExplicitAvg) * momentumResidual(i);
			}
		}

		double time_coeff = 1.0/2.0;
		double Coeff_velPred = 0.0;
		double Coeff_pressure_poisson = 0.0;
		if (input_data_->ifRhoNuImplicit){ /// if the velocity block is linear, rho is explicit
			Coeff_velPred = (1.0 / (time_coeff * dt_));
			if (input_data_->rescale_pressure){
				Coeff_pressure_poisson = (1.0/rhoImplicitAvg);
			} else {
				Coeff_pressure_poisson = (1.0/(rhoImplicitAvg * We));
			}
		} else {
			Coeff_velPred = (1.0 /(time_coeff * dt_));
			if (input_data_->rescale_pressure){
				Coeff_pressure_poisson = (1.0/rhoExplicitAvg); //(1.0/rhoExplicitAvg);// (1.0/rhoMix);//(1.0/rhoExplicitAvg);
			} else {
				Coeff_pressure_poisson = (1.0/(rhoExplicitAvg * We));
			}
		}


		//std::cout << "Gauss point: " << fe.position().x() << ", " << fe.position().y() << std::endl;

		for (int a = 0; a < n_basis_functions; a++) {
			///----------------------------------Done with Elemental operator Matrix---------------------------------------///

			double pressurePoissonRHS1 = 0.0;
			double pressurePoissonRHS2 = 0.0;
			double pressurePoissonRHS3 = 0.0;
			for (int i = 0; i < nsd; i++) {
				/// Coeff_velPred = 2/dt
				pressurePoissonRHS1 += -(fe.N(a) * Coeff_velPred * du(i, i));
				/// Coeff_velPred = 2/dt
				pressurePoissonRHS2 += (fe.dN(a, i) * Coeff_velPred * u_fine(i));
				/// Coeff_pressure_poisson = 1.0/rho
				pressurePoissonRHS3 += (fe.dN(a, i) * Coeff_pressure_poisson * dpStar(i));
			}
			/// Contribution of this term goes to last element of the vector
			double contContrib = (pressurePoissonRHS1 * detJxW) + (pressurePoissonRHS2 * detJxW)
			                     + (pressurePoissonRHS3 * detJxW);
			be((degOfFreedom * a) + 0) += contContrib;
		}
	}


	#pragma mark Velocity update-integrands
	/** Helmholtz decomposition projection step. It updates pressure and velocity
	 * @param fe FEMElm object for looping over elements and gauss points
	 * @param Ae Empty Ae matrix to be filled in the method
	 * @param dt timestep
	 * @param t current time
	 * @return
	 */
	void NSIntegrandsVelocityUpdateAe(const FEMElm &fe, ZeroMatrix<double> &Ae, const double dt, const double t) {
		double dt_ = dt;
		double t_ = t + dt_; ///< current time is elapsed + dt
		int degOfFreedom = DIM; ///3 degrees of freedom

		int nsd = DIM;
		double Ci_f = this->input_data_->Ci_f;
		int n_basis_functions = fe.nbf(); // # of basis functions
		double detJxW = fe.detJxW();


		/// Physical properties
		double rhoH = input_data_->rhoH;
		double rhoL = input_data_->rhoL;
		double etaH = input_data_->etaH;
		double etaL = input_data_->etaL;


		/// Get phi to calculate mixture density of the form given in Abels et al.
		double phi = this->p_data_->valueFEM(fe, CHNSNodeData::PHI);
		double phiPrev1 = this->p_data_->valueFEM(fe, CHNSNodeData::PHI_PRE1);
		double phiPrev2 = this->p_data_->valueFEM(fe, CHNSNodeData::PHI_PRE2);
		std::vector <double> phi_list = {phiPrev1, phiPrev2};

		/// Make the bounds on phi used for evaluation for mixture density and viscosity
		double phiForPropertyCalculation = 0.0;
		if (phi >= 1.0){
			phiForPropertyCalculation = 1.0;
		} else if (phi <= -1.0){
			phiForPropertyCalculation = -1.0;
		} else{
			phiForPropertyCalculation = phi;
		}

		/// Make the bounds on phi used for evaluation for mixture density and viscosity
		double phiForPropertyCalculationPrev = 0.0;
		if (phiPrev1 >= 1.0){
			phiForPropertyCalculationPrev = 1.0;
		} else if (phiPrev1 <= -1.0){
			phiForPropertyCalculationPrev = -1.0;
		} else{
			phiForPropertyCalculationPrev = phiPrev1;
		}

		double phiAvg = 0.5 * (phi + phiPrev1);
		double phiAvgPullback = 0.5 * (phiForPropertyCalculation + phiForPropertyCalculationPrev);
		/// calculate alpha and beta (coefficients in mixture density)
		double alpha = (input_data_->rhoH - input_data_->rhoL) / 2.0;
		double beta = (input_data_->rhoH + input_data_->rhoL) / 2.0;
		double rhoImplicitAvg = 1.0;
		if (input_data_->caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS && input_data_->manufacSolType == 2) {
			rhoImplicitAvg = (alpha * phiAvg) + beta;
		} else {
			rhoImplicitAvg = (alpha * phiAvgPullback) + beta;
		}

		/// Mixture density explicit
		double rhoExplicitAvg = 1.0;
		//double rhoExplicitAvg = calcExplicitDensity(phi_list);
		if (input_data_->caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS && input_data_->manufacSolType == 2) {
			rhoExplicitAvg = calcExplicitDensityNoPullBack (phi_list);
		} else {
			rhoExplicitAvg = calcExplicitDensity (phi_list);
		}

		double Coeff_vel_update = 0.0;
		if (input_data_->ifRhoNuImplicit){
			Coeff_vel_update = rhoImplicitAvg;
		} else {
			Coeff_vel_update = rhoExplicitAvg;
		}


		for (int a = 0; a < n_basis_functions; a++) {
			for (int b = 0; b < n_basis_functions; b++) {
				/// All the terms which does not contain any fine scale corrections for the first three rows of the elemental
				/// matrix
				for (int i = 0; i < nsd; i++) {
					Ae((degOfFreedom * a) + i, (degOfFreedom * b) + i) +=
							///(w_i, \rho(\tilde{\phi}^{k+1/2}) v_i^{k+1})
							(fe.N(a) * Coeff_vel_update * fe.N(b)) * detJxW;
				}
			}
			///----------------------------------Done with Elemental operator Matrix---------------------------------------///
		}
	}

	/** Helmholtz decomposition velocity update step. It updates velocity to the solenoidal variation
	 * @param fe FEMElm object for looping over elements and gauss points
	 * @param be Empty be vector to be filled in the method
	 * @param dt timestep
	 * @param t current time
	 * @return
	 */
	void NSIntegrandsVelocityUpdatebe(const FEMElm &fe, ZEROARRAY<double> &be, const double dt, const double t) {
		double dt_ = dt;
		double t_ = t + dt_; ///< current time is elapsed + dt
		int degOfFreedom = DIM; ///4 degrees of freedom

		double Re = this->input_data_->Re; ///< Reynolds number
		double Pe = this->input_data_->Pe; /// Diffusional Peclet number
		double We = this->input_data_->We; /// Weber number
		double Cn = this->input_data_->Cn; /// Cahn number
		double Fr = this->input_data_->Fr; /// Froude number

		int nsd = DIM;
		double Ci_f = this->input_data_->Ci_f;
		int n_basis_functions = fe.nbf(); // # of basis functions
		double detJxW = fe.detJxW();


		/// Physical properties
		double rhoH = input_data_->rhoH;
		double rhoL = input_data_->rhoL;
		double etaH = input_data_->etaH;
		double etaL = input_data_->etaL;


		/// Get phi to calculate mixture density of the form given in Abels et al.
		double phi = this->p_data_->valueFEM(fe, CHNSNodeData::PHI);
		double phiPrev1 = this->p_data_->valueFEM(fe, CHNSNodeData::PHI_PRE1);
		double phiPrev2 = this->p_data_->valueFEM(fe, CHNSNodeData::PHI_PRE2);
		std::vector <double> phi_list = {phiPrev1, phiPrev2};
		double phiExplicitAvg = calcExplicitApproxField(phi_list);

		/// Make the bounds on phi used for evaluation for mixture density and viscosity
		double phiForPropertyCalculation = 0.0;
		if (phi >= 1.0){
			phiForPropertyCalculation = 1.0;
		} else if (phi <= -1.0){
			phiForPropertyCalculation = -1.0;
		} else{
			phiForPropertyCalculation = phi;
		}

		/// Make the bounds on phi used for evaluation for mixture density and viscosity
		double phiForPropertyCalculationPrev = 0.0;
		if (phiPrev1 >= 1.0){
			phiForPropertyCalculationPrev = 1.0;
		} else if (phiPrev1 <= -1.0){
			phiForPropertyCalculationPrev = -1.0;
		} else{
			phiForPropertyCalculationPrev = phiPrev1;
		}

		double phiAvg = 0.5 * (phi + phiPrev1);
		double phiAvgPullback = 0.5 * (phiForPropertyCalculation + phiForPropertyCalculationPrev);
		/// calculate alpha and beta (coefficients in mixture density)
		double alpha = (input_data_->rhoH - input_data_->rhoL) / 2.0;
		double beta = (input_data_->rhoH + input_data_->rhoL) / 2.0;
		double rhoImplicitAvg = 1.0;
		if (input_data_->caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS && input_data_->manufacSolType == 2) {
			rhoImplicitAvg = (alpha * phiAvg) + beta;
		} else {
			rhoImplicitAvg = (alpha * phiAvgPullback) + beta;
		}

		/// calculate gamma and kappa (coefficients in mixture density)
		double gamma = (etaH - etaL) / 2.0;
		double kappa = (etaH + etaL) / 2.0;

		/// Mixture viscosity based on last SNES iteration
		double etaMix = ((etaH - etaL) / 2) * phiForPropertyCalculation + ((etaH + etaL) / 2);
		double etaMixPrev = ((etaH - etaL) / 2) * phiForPropertyCalculationPrev + ((etaH + etaL) / 2);

		double viscosityImplicitAvg = 0.5 * (etaMix + etaMixPrev);

		/// Mixture density explicit
		double rhoExplicitAvg = 1.0;
		//double rhoExplicitAvg = calcExplicitDensity(phi_list);
		if (input_data_->caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS && input_data_->manufacSolType == 2) {
			rhoExplicitAvg = calcExplicitDensityNoPullBack (phi_list);
		} else {
			rhoExplicitAvg = calcExplicitDensity (phi_list);
		}

		/// Mixture viscosity based on last SNES iteration
		double viscosityExplicitAvg = calcExplicitViscosity (phi_list);

		/// This helps when Re is extremely small, being in the denominator can make the solution blowup
		double Coe_diff = 0.0;
		double Coe_diff_avg = 0.0;
		double Coe_diff_without_eta = 0.0;
		double Coe_conv = 0.0;
		double Coe_conv_avg = 0.0;
		double Coe_crossTerm1 = 0.0;
		double Coe_crossTerm2 = 0.0;
		double Coe_ReynoldsStressTerm = 0.0;
		double Coe_massFlux = 0.0;
		double Coe_fineScaleMassFlux = 0.0;
		double Coe_pressure = 0.0;
		double Coe_finescalePressure = 0.0;
		double Coe_finescaleContinuity = 0.0;
		double Coe_gravity = 0.0;
		double Coe_forcing = 0.0;
		double Coe_forcing_equiv = 0.0;
		if (Re > 1) {
			Coe_diff_without_eta = 1.0/Re;
			if (input_data_->ifRhoNuImplicit) {
				Coe_diff = viscosityImplicitAvg / Re;
				Coe_conv = rhoImplicitAvg * 1.0;
			} else {
				Coe_diff = viscosityExplicitAvg / Re;
				Coe_conv = rhoExplicitAvg * 1.0;
			}
			Coe_massFlux = 1.0 / (Cn * Pe);
			if (input_data_->rescale_pressure){
				Coe_pressure = 1.0;
			} else {
				Coe_pressure = 1.0 / We;
			}
			Coe_gravity = 1.0;
			Coe_forcing_equiv = 1.0/(Cn * We);
		} else if (Re <= 1) {
			if (input_data_->ifRhoNuImplicit) {
				Coe_diff = viscosityImplicitAvg ;
				Coe_conv = rhoImplicitAvg * Re;
			} else {
				Coe_diff = viscosityExplicitAvg ;
				Coe_conv = rhoExplicitAvg * Re;
			}
			Coe_diff_without_eta = 1/Re;
			Coe_massFlux = Re / (Cn * Pe);
			if (input_data_->rescale_pressure) {
				Coe_pressure = Re;
			} else {
				Coe_pressure = Re / We;
			}
			Coe_gravity = Re;
			Coe_forcing_equiv = Re/(Cn * We);
		}

		ZEROPTV u, u_pre1, u_pre2, uExplicitAvg;
		for (int i = 0; i < nsd; i++) {
			u(i) = this->p_data_->valueFEM(fe, i);
			/// u_pre here is solenoidal and VEL_X_SOL, .. are accessed here
			u_pre1(i) = p_data_->valueFEM(fe, (CHNSNodeData::VEL_X_PRE1 + i));
			u_pre2(i) = p_data_->valueFEM(fe, (CHNSNodeData::VEL_X_PRE2 + i));
		}
		std::vector<ZEROPTV> vel_list = {u_pre1, u_pre2};
		uExplicitAvg = calcExplicitVelocity (vel_list);

		/// Define velocity gradient tensor
		ZeroMatrix<double> du,duPrev1;
		du.redim(nsd, nsd);
		duPrev1.redim(nsd, nsd);
		for (int i = 0; i < nsd; i++) {
			for (int j = 0; j < nsd; j++) {
				du(i, j) = this->p_data_->valueDerivativeFEM(fe, i, j);
				duPrev1(i, j) = this->p_data_->valueDerivativeFEM(fe, CHNSNodeData::VEL_X_PRE1 + i, j);
			}
		}

		/// Get gradient of phasefield
		ZEROPTV dphi,dphiPrev1,dphiPrev2;
		for (int i = 0; i < nsd; i++) {
			dphi(i) = this->p_data_->valueDerivativeFEM(fe, CHNSNodeData::PHI, i);
			dphiPrev1(i) = this->p_data_->valueDerivativeFEM(fe, CHNSNodeData::PHI_PRE1, i);
			dphiPrev2(i) = this->p_data_->valueDerivativeFEM(fe, CHNSNodeData::PHI_PRE2, i);
		}
		std::vector<ZEROPTV> gradPhi_prev_list = {dphiPrev1, dphiPrev2};
		ZEROPTV gradPhiExplicitAvg = calcExplicitVelocity (gradPhi_prev_list);

		/// Calculate the laplacian of velocity This is required for course scale residual of Navier-Stokes for diffusion
		/// term
		ZEROPTV d2u,d2uPrev1;
		/// loop for three directions of velocity (MAX NSD is no of velocity directions in the problem)
		/// PS: This assumes the first three degrees are always velocity, it is wise to keep it that way
		for (int dir1 = 0; dir1 < nsd; dir1++) {
			/// Summing over three directions of velocity as it is laplacian
			for (int dir2 = 0; dir2 < nsd; dir2++) {
				d2u(dir1) += p_data_->value2DerivativeFEM(fe, dir1, dir2, dir2);
				d2uPrev1(dir1) += p_data_->value2DerivativeFEM(fe, (CHNSNodeData::VEL_X_PRE1 + dir1), dir2, dir2);
			}
		}

		/// Pressure fields setup
		double p = p_data_->valueFEM(fe, CHNSNodeData::PRESSURE);
		double p_prev1 = p_data_->valueFEM(fe, CHNSNodeData::PRESSURE_PRE1);
		double p_prev2 = p_data_->valueFEM(fe, CHNSNodeData::PRESSURE_PRE2);
		double pStar = p_extrap_c[1] * p_prev1 + p_extrap_c[2] * p_prev2;

		/// Get gradient of pressure at current and previous timestep
		ZEROPTV dp, dpPrev1, dpPrev2, dpStar;
		for (int i = 0; i < nsd; i++) {
			dp(i) = this->p_data_->valueDerivativeFEM(fe, CHNSNodeData::PRESSURE, i);
			dpPrev1(i) = p_data_->valueDerivativeFEM(fe, CHNSNodeData::PRESSURE_PRE1, i);
			dpPrev2(i) = p_data_->valueDerivativeFEM(fe, CHNSNodeData::PRESSURE_PRE2, i);
			dpStar(i) = p_extrap_c[1] * dpPrev1(i) + p_extrap_c[2] * dpPrev2(i);
		}

		/// Calculate the Diffusional fluxes from Cahn Hilliard (J in Abels et al.)
		ZEROPTV J, JPrev, JExplicitAvg;
		ZEROPTV gradmu, gradmuPrev1, gradmuPrev2;
		for (int i = 0; i < nsd; i++) {
			gradmu(i) = p_data_->valueDerivativeFEM(fe, CHNSNodeData::MU, i);
			gradmuPrev1(i) = p_data_->valueDerivativeFEM(fe, CHNSNodeData::MU_PRE1, i);
			gradmuPrev2(i) = p_data_->valueDerivativeFEM(fe, CHNSNodeData::MU_PRE2, i);
			J(i) = ((rhoL - rhoH) / 2) * mobility * gradmu(i);
			JPrev(i) = ((rhoL - rhoH) / 2) * mobility * gradmuPrev1(i);
		}
		std::vector<ZEROPTV> gradMu_prev_list = {gradmuPrev1, gradmuPrev2};
		ZEROPTV gradMuExplicitAvg = calcExplicitVelocity (gradMu_prev_list);
		/// We can compute explicit approximation of J^{k+\half} similar to velocity
		for (int i = 0; i < nsd; i++) {
			JExplicitAvg(i) = ((rhoL - rhoH) / 2) * mobility * gradMuExplicitAvg(i);
		}

		/// Contribution of gravity in terms of Froude number
		ZEROPTV gravity;
		gravity.x() = 0.0;
		/// if Fr is specified to be zero in the config file turn gravity off
		if (Fr == 0.0) {
			gravity.y() = 0;
		} else {
			gravity.y() = -(Coe_gravity / Fr);
		}
		gravity.z() = 0.0;

		/// External forcing: In the current implementation this term is non-zero
		/// only in the case of Manufactured solutions test of CHNS and periodic
		/// channel case
		ZEROPTV forcingNS, forcingNSPrev1;
		if (input_data_->caseTypeForNavierStokes == CHNSInputData::MANUFACTURED_SOL_TEST_NS) {
			forcingNS = calcForcingManufacturedSolutionNS(fe, t_);
			forcingNSPrev1 = calcForcingManufacturedSolutionNS (fe, t_ - dt_);
		} else {
			forcingNS = {0.0, 0.0, 0.0};
			forcingNSPrev1 = {0.0, 0.0, 0.0};
		}

		///---------------------------------VMS parameter settings---------------------------------------------------///
		VMSParams vms_params_current;
		if (input_data_->ifRhoNuImplicit) {
			vms_params_current = calc_tau_vel(fe, u, J, phiExplicitAvg, rhoImplicitAvg,
			                                  viscosityImplicitAvg, Ci_f, dt);
		} else {
			vms_params_current = calc_tau_vel(fe, u, J, phiExplicitAvg, rhoExplicitAvg,
			                                  viscosityExplicitAvg, Ci_f, dt);
		}
		double tauM = vms_params_current.tauM;
		//double tauM = 0.0;
		///---------------------------------VMS parameters calculated------------------------------------------------///
		/** We define the convection of Navier Stokes here from
		* using inner product of gradient tensor and fields we acquired
		* above.
		*/
		ZEROPTV convectionPreviousNewtonIt;
		ZEROPTV diffusionPreviousNewtonItTerm1;
		ZEROPTV massFluxMuResidPreviousNewtonIt;
		ZEROPTV convectionPreviousNewtonItNoCoeff;
		ZEROPTV diffusionPreviousNewtonItTerm2;
		for (int i = 0; i < nsd; i++) {
			convectionPreviousNewtonIt(i) = 0.0;
			diffusionPreviousNewtonItTerm1(i) += Coe_diff * 0.5 * (d2u(i) + d2uPrev1(i));
			for (int j = 0; j < nsd; j++) {
				convectionPreviousNewtonIt(i) += Coe_conv * 0.5 * (u(j) + u_pre1(j)) * 0.5 * (du(i, j) + duPrev1(i, j));
				//convectionPreviousNewtonIt(i) += Coe_conv * uExplicitAvg(j) * 0.5 * (du(i, j) + duPrev1(i, j));
				convectionPreviousNewtonItNoCoeff(i) += 0.5 * (u(j) + u_pre1(j)) * 0.5 * (du(i, j) + duPrev1(i, j));
				//convectionPreviousNewtonItNoCoeff(i) += uExplicitAvg(j) * 0.5 * (du(i, j) + duPrev1(i, j));
				/// Chain Rule
				massFluxMuResidPreviousNewtonIt(i) += Coe_massFlux * 0.5 * (J(j) + JPrev(j)) * 0.5 * (du(i, j) + duPrev1(i,j));
				//massFluxMuResidPreviousNewtonIt(i) += Coe_massFlux * JExplicitAvg(j) * 0.5 * (du(i, j) + duPrev1(i,j));
				///
				diffusionPreviousNewtonItTerm2(i) +=
						Coe_diff_without_eta * gamma * gradPhiExplicitAvg(j) * 0.5 * (du(i,j) + duPrev1(i,j));
			}
		}

		ZEROPTV timeDerivative;
		for (int i = 0; i < nsd; i++) {
			timeDerivative(i) = (u(i) - u_pre1(i)) / dt_;
		}

		/** Construct the Navier Stokes equation without diffusion
		* Here diffusion is not present as its contribution to stabilizers
		* for
		* linear basis functions is zero
		* Diffusion term is added for higher order basis functions
		* Equation 61 of Bazilevs et al. (2007)
		*/
		ZEROPTV momentumResidual;
		for (int i = 0; i < nsd; i++) {
			if (input_data_->ifRhoNuImplicit) {
				momentumResidual(i) = (Coe_conv * timeDerivative(i))
				                      + convectionPreviousNewtonIt(i)
				                      + (massFluxMuResidPreviousNewtonIt(i))
				                      + (Coe_pressure * dpStar(i))
				                      - diffusionPreviousNewtonItTerm1(i)
				                      - diffusionPreviousNewtonItTerm2(i)
				                      - (rhoImplicitAvg * gravity(i))
				                      - 0.5 * (forcingNS(i) + forcingNSPrev1(i))
				                      - (Coe_forcing_equiv * phiExplicitAvg * gradmu(i));
			} else {
				momentumResidual(i) = (Coe_conv * timeDerivative(i))
				                      + convectionPreviousNewtonIt(i)
				                      + (massFluxMuResidPreviousNewtonIt(i))
				                      + (Coe_pressure * dpStar(i))
				                      - diffusionPreviousNewtonItTerm1(i)
				                      - diffusionPreviousNewtonItTerm2(i)
				                      - (rhoExplicitAvg * gravity(i))
				                      - 0.5 * (forcingNS(i) + forcingNSPrev1(i))
				                      - (Coe_forcing_equiv * phiExplicitAvg * 0.5 * (gradmu(i) + gradmuPrev1(i)));
			}
		}

		ZEROPTV u_fine;
		for (int i = 0; i < nsd; i++) {
			if (input_data_->ifRhoNuImplicit) {
				u_fine(i) = -tauM * (1.0/rhoImplicitAvg) * momentumResidual(i);
			} else {
				u_fine(i) = -tauM * (1.0/rhoExplicitAvg) * momentumResidual(i);
			}
		}

		double time_coeff = 1.0/2.0;
		double Coeff_pressure = 0.0;
		double Coeff_vel_update = 0.0;
		if (input_data_->rescale_pressure){
			Coeff_pressure = (time_coeff * dt_);
		} else {
			Coeff_pressure = (time_coeff * dt_)/We;
		}
		if (input_data_->ifRhoNuImplicit){ /// if the velocity block is linear, rho is explicit
			Coeff_vel_update = rhoImplicitAvg;
		} else {
			Coeff_vel_update = rhoExplicitAvg;//rhoExplicitAvg; //rhoMix; //rhoExplicitAvg; //rhoMix; //1.0;
		}


		for (int a = 0; a < n_basis_functions; a++) {
			///Start be
			ZEROPTV rhs;
			/// Terms of Normal Navier Stokes without any additional VMS terms
			for (int i = 0; i < nsd; i++) {
				rhs(i) =
						///(w_i, \rho(\tilde{\phi}^{k+1/2}) \tilde{v_i}^{c,k+1}) for each i
						///Coeff_vel_update = rho
						(fe.N(a) * Coeff_vel_update * u(i)) * detJxW
						/// Corresponding fine scale velocity
						+ (fe.N(a) * Coeff_vel_update * u_fine(i)) * detJxW
						/// Coeff_pressure = dt/2
						- (fe.N(a) * Coeff_pressure * (dp(i) - dpStar(i))) * detJxW;
			}

			/// Adding all the calculated terms to the vector
			for (int i = 0; i < nsd; i++) {
				be((degOfFreedom * a) + i) += rhs(i);
			}
		}
	}

#pragma mark CH-Integrands

	/** This is a Crank-Nicholson implementation of the conservative
	 * Cahn-Hilliard equation with const mobility.
	 * @param fe FEElm object
	 * @param Ae Initialized empty matrix for Ae
	 * @param dt timestep
	 * @param free_energy: Pointer to the type of free energy
	 * @param t: elapsed time
	 */
	void CHNSIntegrandsCHConsvCrankNicholsonAe(
			const FEMElm &fe, ZeroMatrix<double> &Ae, const double dt, CHNSFreeEnergy *free_energy, const double t) {

		/// Get all the non -dimensional numbers for the CH Equations
		double Pe = this->input_data_->Pe; /// Diffusional Peclet number
		double Cn = this->input_data_->Cn; /// Cahn number

		const double dt_ = dt;
		const double t_ = t + dt_; /// current time is elapsed + dt
		const int nsd = DIM;
		const int nbf = fe.nbf ();
		const double detJxW = fe.detJxW ();


		/// getting the value of phase field from NODEData
		double phi = this->p_data_->valueFEM (fe, CHNSNodeData::PHI);

		/// Remember that the position of u_n is starting from
		/// CHNSNodeData::NUM_VARS + CHNSNodeData::MANUFAC_SOL_VARS
		double phiPrev = this->p_data_->valueFEM (fe, CHNSNodeData::PHI_PRE1);
		double phiPrev2 = this->p_data_->valueFEM (fe, CHNSNodeData::PHI_PRE2);
		double mu = this->p_data_->valueFEM (fe, CHNSNodeData::MU);
		double mu_prev = this->p_data_->valueFEM (fe, CHNSNodeData::MU_PRE1);

		std::vector<double> phi_prev_list = {phiPrev, phiPrev2};
		double phiExplicitAvg = calcExplicitApproxField(phi_prev_list);

		/// Calculating the forcing for Phi at current timestep
		double forcingPhiCurr;
		if (input_data_->caseTypeCHinit == CHNSInputData::MANUFACTURED_SOL_TEST_CH) {
			forcingPhiCurr = calcForcingManufacturedSolutionPhi (fe, t_);
			// forcingPhi = 0.0;
		} else {
			forcingPhiCurr = 0.0;
		}

		/// Calculating the forcing for Phi at previous timestep
		double forcingPhiPre;
		if (input_data_->caseTypeCHinit == CHNSInputData::MANUFACTURED_SOL_TEST_CH) {
			forcingPhiPre = calcForcingManufacturedSolutionPhi (fe, (t_ - dt_));
			// forcingPhi = 0.0;
		} else {
			forcingPhiPre = 0.0;
		}

		/// Calculating the forcing for Mu at current timestep
		double forcingMu;
		if (input_data_->caseTypeCHinit == CHNSInputData::MANUFACTURED_SOL_TEST_CH) {
			forcingMu = calcForcingManufacturedSolutionMu (fe, t_);
		} else {
			forcingMu = 0.0;
		}

		/// Calculating the forcing for Mu for previous timestep
		double forcingMuPre;
		if (input_data_->caseTypeCHinit == CHNSInputData::MANUFACTURED_SOL_TEST_CH) {
			forcingMuPre = calcForcingManufacturedSolutionMu (fe, (t_ - dt_));
		} else {
			forcingMuPre = 0.0;
		}

		/// Calculating the derivatives of the phase field, mu, and velocity from
		/// Navier Stokes
		ZEROPTV dmu;
		ZEROPTV dphi;
		ZEROPTV vel;
		ZEROPTV velPre;
		ZEROPTV dphiPrev1;
		ZEROPTV dphiPrev2;
		ZEROPTV dmuPrev;
		for (int i = 0; i < nsd; i++) {
			dmu (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::MU, i);
			dphi (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PHI, i);
			vel (i) = this->p_data_->valueFEM (fe, CHNSNodeData::VEL_X + i); /// As first three elements
			/// on NodeData are always velocity
			velPre (i) = this->p_data_->valueFEM (fe, (CHNSNodeData::VEL_X_PRE1 + i));
			dphiPrev1 (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PHI_PRE1, i);
			dphiPrev2(i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PHI_PRE2, i);
			dmuPrev (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::MU_PRE1, i);
		}


		double advection_flag = 1.0;
		if (input_data_->pureCH) {
			advection_flag = 0.0;
		}

		double K, S, R;
		for (int a = 0; a < nbf; a++) {
			for (int b = 0; b < nbf; b++) {

				K = 0.0;
				/// Diffusion term for both mu equation and phi equation
				for (int k = 0; k < nsd; k++) {
					K += fe.dN (a, k) * fe.dN (b, k) * detJxW;
				}
				/// The mass term(unsteady term)
				double M = (fe.N (a)) * fe.N (b) * detJxW;

				R = M * free_energy->d2fdphi2 (phi);
				mobility = 1.0;
				/// Phi terms: unsteady term and advection term of phi
				Ae (CHNSNodeData::CH_DOF * a, CHNSNodeData::CH_DOF * b) +=
						//M + (0.5 * advecPhi * dt_);
						M;
				/// Laplacian of mu in phi equation
				Ae (CHNSNodeData::CH_DOF * a, CHNSNodeData::CH_DOF * b + 1) +=
						(mobility / (Pe * Cn)) * K * dt_;
				/// phi terms in mu equation: term 1 is df/d phi, term 2: laplacian
				/// of phi
				Ae (CHNSNodeData::CH_DOF * a + 1, CHNSNodeData::CH_DOF * b) +=
						0.5 * ((Cn * Cn * K) + R) * dt_;
				/// mu term in mu equation
				Ae (CHNSNodeData::CH_DOF * a + 1, CHNSNodeData::CH_DOF * b + 1) -=
						M * dt_;
			}
		}
//		PrintInfo("position = ", fe.position(), ", iso_position = ", fe.itg_pt(), ", num_pt = ", fe.n_itg_pts(), ", current_itg_pt = ", fe.cur_itg_pt_num());
//		PrintInfo("Printing Ae");
//		if (GetMPIRank()==0) {
//			for (int i = 0; i < Ae.nx(); i++) {
//				for (int j = 0; j < Ae.ny(); j++) {
//					std::cout << Ae(i, j) << " ";
//				}
//				std::cout << "\n";
//			}
//		}
	}

	/** This is a Crank-Nicholson implementation of the conservative Cahn-Hilliard equation with const mobility.
	 * @param fe FEElm object
	 * @param be Initialized empty vector for Be
	 * @param dt timestep
	 * @param free_energy: Pointer to the type of free energy
	 * @param t: elapsed time
	 */
	void CHNSIntegrandsCHConsvCrankNicholsonbe(
			const FEMElm &fe, ZEROARRAY<double> &be,
			const double dt, CHNSFreeEnergy *free_energy, const double t) {

		/// Get all the non -dimensional numbers for the CH Equations
		double Pe = this->input_data_->Pe; /// Diffusional Peclet number
		double Cn = this->input_data_->Cn; /// Cahn number

		const double dt_ = dt;
		const double t_ = t + dt_; /// current time is elapsed + dt
		const int nsd = DIM;
		const int nbf = fe.nbf ();
		const double detJxW = fe.detJxW ();

		/// getting the value of phase field from NODEData
		double phi = this->p_data_->valueFEM (fe, CHNSNodeData::PHI);

		/// Remember that the position of u_n is starting from
		/// CHNSNodeData::NUM_VARS + CHNSNodeData::MANUFAC_SOL_VARS
		double phiPrev = this->p_data_->valueFEM (fe, CHNSNodeData::PHI_PRE1);
		double phiPrev2 = this->p_data_->valueFEM (fe, CHNSNodeData::PHI_PRE2);
		double mu = this->p_data_->valueFEM (fe, CHNSNodeData::MU);
		double mu_prev = this->p_data_->valueFEM (fe, CHNSNodeData::MU_PRE1);

		std::vector<double> phi_prev_list = {phiPrev, phiPrev2};
		double phiExplicitAvg = calcExplicitApproxField(phi_prev_list);

		/// Calculating the forcing for Phi at current timestep
		double forcingPhiCurr;
		if (input_data_->caseTypeCHinit == CHNSInputData::MANUFACTURED_SOL_TEST_CH) {
			forcingPhiCurr = calcForcingManufacturedSolutionPhi (fe, t_);
			// forcingPhi = 0.0;
		} else {
			forcingPhiCurr = 0.0;
		}

		/// Calculating the forcing for Phi at previous timestep
		double forcingPhiPre;
		if (input_data_->caseTypeCHinit == CHNSInputData::MANUFACTURED_SOL_TEST_CH) {
			forcingPhiPre = calcForcingManufacturedSolutionPhi (fe, (t_ - dt_));
			// forcingPhi = 0.0;
		} else {
			forcingPhiPre = 0.0;
		}

		/// Calculating the forcing for Mu at current timestep
		double forcingMu;
		if (input_data_->caseTypeCHinit == CHNSInputData::MANUFACTURED_SOL_TEST_CH) {
			forcingMu = calcForcingManufacturedSolutionMu (fe, t_);
		} else {
			forcingMu = 0.0;
		}

		/// Calculating the forcing for Mu for previous timestep
		double forcingMuPre;
		if (input_data_->caseTypeCHinit == CHNSInputData::MANUFACTURED_SOL_TEST_CH) {
			forcingMuPre = calcForcingManufacturedSolutionMu (fe, (t_ - dt_));
		} else {
			forcingMuPre = 0.0;
		}

		/// Calculating the derivatives of the phase field, mu, and velocity from
		/// Navier Stokes
		ZEROPTV dmu;
		ZEROPTV dphi;
		ZEROPTV vel;
		ZEROPTV velPre;
		ZEROPTV dphiPrev1;
		ZEROPTV dphiPrev2;
		ZEROPTV dmuPrev;
		for (int i = 0; i < nsd; i++) {
			dmu (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::MU, i);
			dphi (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PHI, i);
			vel (i) = this->p_data_->valueFEM (fe, CHNSNodeData::VEL_X + i); /// As first three elements
			/// on NodeData are always velocity
			velPre (i) = this->p_data_->valueFEM (fe, (CHNSNodeData::VEL_X_PRE1 + i));
			dphiPrev1 (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PHI_PRE1, i);
			dphiPrev2(i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PHI_PRE2, i);
			dmuPrev (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::MU_PRE1, i);
		}


		double advection_flag = 1.0;
		if (input_data_->pureCH) {
			advection_flag = 0.0;
		}

		double K, S, R;
		for (int a = 0; a < nbf; a++) {
			/// Assembling elemental 'b' vector -- 'phi' equation
			/// mass term (unsteady term) - forcing term
			be (CHNSNodeData::CH_DOF * a) +=
					(fe.N (a)) * (phi - phiPrev) * detJxW -
					//    ((fe.N(a) + PG.SUPG(a)) * (forcingPhiCurr) * detJxW * dt_);
					((fe.N (a)) * ((forcingPhiCurr + forcingPhiPre) / 2.0) * detJxW * dt_);

			/// Stiffness term and advective term
			for (int k = 0; k < nsd; k++) {
				/// Laplacian of mu
				be (CHNSNodeData::CH_DOF * a) += (mobility / (Pe * Cn)) * fe.dN (a, k) * (dmu (k)) * detJxW * dt_;
				/// Advective term
				//be (CHNSNodeData::CH_DOF * a) -= advection_flag * (fe.dN (a, k)) * (0.5 * (vel (k) + velPre (k)) ) * (0.5 *
				//		(phi + phiPrev)) *detJxW * dt_;
				be (CHNSNodeData::CH_DOF * a) -= advection_flag * (fe.dN (a, k) * (0.5 * (vel (k) + velPre (k)) ) * phiExplicitAvg)*detJxW * dt_;
			}

			/// Assembling elemental 'b' vector -- 'mu' equation

			be (CHNSNodeData::CH_DOF * a + 1) -=
					(fe.N (a)) * (mu) * detJxW * dt_; ///
			/// mass
			/// term
			for (int k = 0; k < nsd; k++) {
				/// Laplacian of phi(weakened)
				be (CHNSNodeData::CH_DOF * a + 1) += Cn * Cn * fe.dN (a, k) * (0.5 * (dphi (k) + dphiPrev1 (k)) ) * detJxW *dt_;
			}
			S = ((fe.N (a)) *
			     (free_energy->dfdphi ( 0.5 * (phi + phiPrev ) ) ) * detJxW * dt_) +
			    ///
			    (((fe.N (a)) * (0.5 * (forcingMu + forcingMuPre)) * detJxW * dt_));

			be (CHNSNodeData::CH_DOF * a + 1) += S;
		}
//		PrintInfo("position = ", fe.position(), ", iso_position = ", fe.itg_pt(), ", num_pt = ", fe.n_itg_pts(), ", current_itg_pt = ", fe.cur_itg_pt_num());
//		PrintInfo("Printing be");
//		if (GetMPIRank()==0) {
//			for (int i = 0; i < be.size(); i++) {
//				std::cout << be(i) << " ";
//				std::cout << "\n";
//			}
//		}
	}

	/** This is a Crank-Nicholson implementation of the conservative Cahn-Hilliard equation with const mobility. With a
	 * fully implicit advection term
	 * @param fe FEElm object
	 * @param Ae Initialized empty matrix for Ae
	 * @param dt timestep
	 * @param free_energy: Pointer to the type of free energy
	 * @param t: elapsed time
	 */
	void CHNSIntegrandsCHConsvCrankNicholsonFullyImplicitAe(
			const FEMElm &fe, ZeroMatrix<double> &Ae, const double dt, CHNSFreeEnergy *free_energy, const double t) {

		/// Get all the non -dimensional numbers for the CH Equations
		double Pe = this->input_data_->Pe; /// Diffusional Peclet number
		double Cn = this->input_data_->Cn; /// Cahn number

		const double dt_ = dt;
		const double t_ = t + dt_; /// current time is elapsed + dt
		const int nsd = DIM;
		const int nbf = fe.nbf ();
		const double detJxW = fe.detJxW ();


		/// getting the value of phase field from NODEData
		double phi = this->p_data_->valueFEM (fe, CHNSNodeData::PHI);

		/// Remember that the position of u_n is starting from
		/// CHNSNodeData::NUM_VARS + CHNSNodeData::MANUFAC_SOL_VARS
		double phiPrev = this->p_data_->valueFEM (fe, CHNSNodeData::PHI_PRE1);
		double phiPrev2 = this->p_data_->valueFEM (fe, CHNSNodeData::PHI_PRE2);
		double mu = this->p_data_->valueFEM (fe, CHNSNodeData::MU);
		double mu_prev = this->p_data_->valueFEM (fe, CHNSNodeData::MU_PRE1);

		std::vector<double> phi_prev_list = {phiPrev, phiPrev2};
		double phiExplicitAvg = calcExplicitApproxField(phi_prev_list);

		/// Calculating the forcing for Phi at current timestep
		double forcingPhiCurr;
		if (input_data_->caseTypeCHinit == CHNSInputData::MANUFACTURED_SOL_TEST_CH) {
			forcingPhiCurr = calcForcingManufacturedSolutionPhi (fe, t_);
			// forcingPhi = 0.0;
		} else {
			forcingPhiCurr = 0.0;
		}

		/// Calculating the forcing for Phi at previous timestep
		double forcingPhiPre;
		if (input_data_->caseTypeCHinit == CHNSInputData::MANUFACTURED_SOL_TEST_CH) {
			forcingPhiPre = calcForcingManufacturedSolutionPhi (fe, (t_ - dt_));
			// forcingPhi = 0.0;
		} else {
			forcingPhiPre = 0.0;
		}

		/// Calculating the forcing for Mu at current timestep
		double forcingMu;
		if (input_data_->caseTypeCHinit == CHNSInputData::MANUFACTURED_SOL_TEST_CH) {
			forcingMu = calcForcingManufacturedSolutionMu (fe, t_);
		} else {
			forcingMu = 0.0;
		}

		/// Calculating the forcing for Mu for previous timestep
		double forcingMuPre;
		if (input_data_->caseTypeCHinit == CHNSInputData::MANUFACTURED_SOL_TEST_CH) {
			forcingMuPre = calcForcingManufacturedSolutionMu (fe, (t_ - dt_));
		} else {
			forcingMuPre = 0.0;
		}

		/// Calculating the derivatives of the phase field, mu, and velocity from
		/// Navier Stokes
		ZEROPTV dmu;
		ZEROPTV dphi;
		ZEROPTV vel;
		ZEROPTV velPre;
		ZEROPTV dphiPrev1;
		ZEROPTV dphiPrev2;
		ZEROPTV dmuPrev;
		for (int i = 0; i < nsd; i++) {
			dmu (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::MU, i);
			dphi (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PHI, i);
			vel (i) = this->p_data_->valueFEM (fe, CHNSNodeData::VEL_X + i); /// As first three elements
			/// on NodeData are always velocity
			velPre (i) = this->p_data_->valueFEM (fe, (CHNSNodeData::VEL_X_PRE1 + i));
			dphiPrev1 (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PHI_PRE1, i);
			dphiPrev2(i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PHI_PRE2, i);
			dmuPrev (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::MU_PRE1, i);
		}


		double advection_flag = 1.0;
		if (input_data_->pureCH) {
			advection_flag = 0.0;
		}

		double K, S, R;
		for (int a = 0; a < nbf; a++) {
			for (int b = 0; b < nbf; b++) {

				K = 0.0;
				/// Diffusion term for both mu equation and phi equation
				for (int k = 0; k < nsd; k++) {
					K += fe.dN (a, k) * fe.dN (b, k) * detJxW;
				}
				/// The mass term(unsteady term)
				double M = (fe.N (a)) * fe.N (b) * detJxW;

				/// Advection term for phi equation
				double advecPhi = 0.0;
				for (int k = 0; k < nsd; k++) {
					advecPhi -=
							advection_flag * (fe.dN (a, k)) * ((vel (k) + velPre (k)) / 2.0) * fe.N (b) * detJxW;
				}

				R = M * free_energy->d2fdphi2 (phi);
				mobility = 1.0;
				/// Phi terms: unsteady term and advection term of phi
				Ae (CHNSNodeData::CH_DOF * a, CHNSNodeData::CH_DOF * b) +=
						M + (0.5 * advecPhi * dt_);
						//M;
				/// Laplacian of mu in phi equation
				Ae (CHNSNodeData::CH_DOF * a, CHNSNodeData::CH_DOF * b + 1) +=
						(mobility / (Pe * Cn)) * K * dt_;
				/// phi terms in mu equation: term 1 is df/d phi, term 2: laplacian
				/// of phi
				Ae (CHNSNodeData::CH_DOF * a + 1, CHNSNodeData::CH_DOF * b) +=
						0.5 * ((Cn * Cn * K) + R) * dt_;
				/// mu term in mu equation
				Ae (CHNSNodeData::CH_DOF * a + 1, CHNSNodeData::CH_DOF * b + 1) -=
						M * dt_;
			}
		}
	}

	/** This is a Crank-Nicholson implementation of the conservative Cahn-Hilliard equation with const mobility. With a
	 * fully implicit advection term
	 * @param fe FEElm object
	 * @param be Initialized empty vector for Be
	 * @param dt timestep
	 * @param free_energy: Pointer to the type of free energy
	 * @param t: elapsed time
	 * */
	void CHNSIntegrandsCHConsvCrankNicholsonFullyImplicitbe(
			const FEMElm &fe, ZEROARRAY<double> &be,
			const double dt, CHNSFreeEnergy *free_energy, const double t) {

		/// Get all the non -dimensional numbers for the CH Equations
		double Pe = this->input_data_->Pe; /// Diffusional Peclet number
		double Cn = this->input_data_->Cn; /// Cahn number

		const double dt_ = dt;
		const double t_ = t + dt_; /// current time is elapsed + dt
		const int nsd = DIM;
		const int nbf = fe.nbf ();
		const double detJxW = fe.detJxW ();

		/// getting the value of phase field from NODEData
		double phi = this->p_data_->valueFEM (fe, CHNSNodeData::PHI);

		/// Remember that the position of u_n is starting from
		/// CHNSNodeData::NUM_VARS + CHNSNodeData::MANUFAC_SOL_VARS
		double phiPrev = this->p_data_->valueFEM (fe, CHNSNodeData::PHI_PRE1);
		double phiPrev2 = this->p_data_->valueFEM (fe, CHNSNodeData::PHI_PRE2);
		double mu = this->p_data_->valueFEM (fe, CHNSNodeData::MU);
		double mu_prev = this->p_data_->valueFEM (fe, CHNSNodeData::MU_PRE1);

		std::vector<double> phi_prev_list = {phiPrev, phiPrev2};
		double phiExplicitAvg = calcExplicitApproxField(phi_prev_list);

		/// Calculating the forcing for Phi at current timestep
		double forcingPhiCurr;
		if (input_data_->caseTypeCHinit == CHNSInputData::MANUFACTURED_SOL_TEST_CH) {
			forcingPhiCurr = calcForcingManufacturedSolutionPhi (fe, t_);
			// forcingPhi = 0.0;
		} else {
			forcingPhiCurr = 0.0;
		}

		/// Calculating the forcing for Phi at previous timestep
		double forcingPhiPre;
		if (input_data_->caseTypeCHinit == CHNSInputData::MANUFACTURED_SOL_TEST_CH) {
			forcingPhiPre = calcForcingManufacturedSolutionPhi (fe, (t_ - dt_));
			// forcingPhi = 0.0;
		} else {
			forcingPhiPre = 0.0;
		}

		/// Calculating the forcing for Mu at current timestep
		double forcingMu;
		if (input_data_->caseTypeCHinit == CHNSInputData::MANUFACTURED_SOL_TEST_CH) {
			forcingMu = calcForcingManufacturedSolutionMu (fe, t_);
		} else {
			forcingMu = 0.0;
		}

		/// Calculating the forcing for Mu for previous timestep
		double forcingMuPre;
		if (input_data_->caseTypeCHinit == CHNSInputData::MANUFACTURED_SOL_TEST_CH) {
			forcingMuPre = calcForcingManufacturedSolutionMu (fe, (t_ - dt_));
		} else {
			forcingMuPre = 0.0;
		}

		/// Calculating the derivatives of the phase field, mu, and velocity from
		/// Navier Stokes
		ZEROPTV dmu;
		ZEROPTV dphi;
		ZEROPTV vel;
		ZEROPTV velPre;
		ZEROPTV dphiPrev1;
		ZEROPTV dphiPrev2;
		ZEROPTV dmuPrev;
		for (int i = 0; i < nsd; i++) {
			dmu (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::MU, i);
			dphi (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PHI, i);
			vel (i) = this->p_data_->valueFEM (fe, CHNSNodeData::VEL_X + i); /// As first three elements
			/// on NodeData are always velocity
			velPre (i) = this->p_data_->valueFEM (fe, (CHNSNodeData::VEL_X_PRE1 + i));
			dphiPrev1 (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PHI_PRE1, i);
			dphiPrev2(i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PHI_PRE2, i);
			dmuPrev (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::MU_PRE1, i);
		}


		double advection_flag = 1.0;
		if (input_data_->pureCH) {
			advection_flag = 0.0;
		}

		double K, S, R;
		for (int a = 0; a < nbf; a++) {
			/// Assembling elemental 'b' vector -- 'phi' equation
			/// mass term (unsteady term) - forcing term
			be (CHNSNodeData::CH_DOF * a) +=
					(fe.N (a)) * (phi - phiPrev) * detJxW -
					//    ((fe.N(a) + PG.SUPG(a)) * (forcingPhiCurr) * detJxW * dt_);
					((fe.N (a)) * ((forcingPhiCurr + forcingPhiPre) / 2.0) * detJxW * dt_);

			/// Stiffness term and advective term
			for (int k = 0; k < nsd; k++) {
				/// Laplacian of mu
				be (CHNSNodeData::CH_DOF * a) += (mobility / (Pe * Cn)) * fe.dN (a, k) * (dmu (k)) * detJxW * dt_;
				/// Advective term
				be (CHNSNodeData::CH_DOF * a) -= advection_flag * (fe.dN (a, k)) * (0.5 * (vel (k) + velPre (k)) ) * (0.5 *
						(phi + phiPrev)) *detJxW * dt_;
				//be (CHNSNodeData::CH_DOF * a) -= advection_flag * (fe.dN (a, k) * (0.5 * (vel (k) + velPre (k)) ) *
				//		phiExplicitAvg)*detJxW * dt_;
			}

			/// Assembling elemental 'b' vector -- 'mu' equation

			be (CHNSNodeData::CH_DOF * a + 1) -=
					(fe.N (a)) * (mu) * detJxW * dt_; ///
			/// mass
			/// term
			for (int k = 0; k < nsd; k++) {
				/// Laplacian of phi(weakened)
				be (CHNSNodeData::CH_DOF * a + 1) += Cn * Cn * fe.dN (a, k) * (0.5 * (dphi (k) + dphiPrev1 (k)) ) * detJxW *dt_;
			}
			S = ((fe.N (a)) *
			     (free_energy->dfdphi ( 0.5 * (phi + phiPrev ) ) ) * detJxW * dt_) +
			    ///
			    (((fe.N (a)) * (0.5 * (forcingMu + forcingMuPre)) * detJxW * dt_));

			be (CHNSNodeData::CH_DOF * a + 1) += S;
		}
	}

	/** This is a BDF2 implementation of the conservative
	 * Cahn-Hilliard equation with const mobility.
	 * @param fe FEElm object
	 * @param Ae Initialized empty matrix for Ae
	 * @param be Initialized empty vector for Be
	 * @param dt timestep
	 * @param free_energy: Pointer to the type of free energy
	 * @param t: elapsed time
	 */
	void CHNSIntegrandsCHConsvBDF2Ae(
			const FEMElm &fe, ZeroMatrix<double> &Ae, const double dt, CHNSFreeEnergy *free_energy, const double t) {

		/// Get all the non -dimensional numbers for the CH Equations
		double Pe = this->input_data_->Pe; /// Diffusional Peclet number
		double Cn = this->input_data_->Cn; /// Cahn number

		const double dt_ = dt;
		const double t_ = t + dt_; /// current time is elapsed + dt
		const int nsd = DIM;
		const int nbf = fe.nbf ();
		const double detJxW = fe.detJxW ();


		/// getting the value of phase field from NODEData
		double phi = this->p_data_->valueFEM (fe, CHNSNodeData::PHI);

		/// Remember that the position of u_n is starting from
		/// CHNSNodeData::NUM_VARS + CHNSNodeData::MANUFAC_SOL_VARS
		double phiPrev = this->p_data_->valueFEM (fe, CHNSNodeData::PHI_PRE1);
		double phiPrev2 = this->p_data_->valueFEM (fe, CHNSNodeData::PHI_PRE2);
		double mu = this->p_data_->valueFEM (fe, CHNSNodeData::MU);
		double mu_prev = this->p_data_->valueFEM (fe, CHNSNodeData::MU_PRE1);

		/// Calculating the forcing for Phi at current timestep
		double forcingPhiCurr;
		if (input_data_->caseTypeCHinit == CHNSInputData::MANUFACTURED_SOL_TEST_CH) {
			forcingPhiCurr = calcForcingManufacturedSolutionPhi (fe, t_);
			// forcingPhi = 0.0;
		} else {
			forcingPhiCurr = 0.0;
		}

		/// Calculating the forcing for Phi at previous timestep
		double forcingPhiPre;
		if (input_data_->caseTypeCHinit == CHNSInputData::MANUFACTURED_SOL_TEST_CH) {
			forcingPhiPre = calcForcingManufacturedSolutionPhi (fe, (t_ - dt_));
			// forcingPhi = 0.0;
		} else {
			forcingPhiPre = 0.0;
		}

		/// Calculating the forcing for Mu at current timestep
		double forcingMu;
		if (input_data_->caseTypeCHinit == CHNSInputData::MANUFACTURED_SOL_TEST_CH) {
			forcingMu = calcForcingManufacturedSolutionMu (fe, t_);
		} else {
			forcingMu = 0.0;
		}

		/// Calculating the forcing for Mu for previous timestep
		double forcingMuPre;
		if (input_data_->caseTypeCHinit == CHNSInputData::MANUFACTURED_SOL_TEST_CH) {
			forcingMuPre = calcForcingManufacturedSolutionMu (fe, (t_ - dt_));
		} else {
			forcingMuPre = 0.0;
		}

		/// Calculating the derivatives of the phase field, mu, and velocity from
		/// Navier Stokes
		ZEROPTV dmu;
		ZEROPTV dphi;
		ZEROPTV vel;
		ZEROPTV velPre;
		ZEROPTV dphiPrev;
		ZEROPTV dmuPrev;
		for (int i = 0; i < nsd; i++) {
			dmu (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::MU, i);
			dphi (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PHI, i);
			vel (i) = this->p_data_->valueFEM (fe, CHNSNodeData::VEL_X + i); /// As first three elements
			/// on NodeData are always velocity
			velPre (i) = this->p_data_->valueFEM (fe, (CHNSNodeData::VEL_X_PRE1 + i));
			dphiPrev (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PHI_PRE1, i);
			dmuPrev (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::MU_PRE1, i);
		}

		double K, S, R;
		for (int a = 0; a < nbf; a++) {
			for (int b = 0; b < nbf; b++) {

				K = 0.0;
				/// Diffusion term for both mu equation and phi equation
				for (int k = 0; k < nsd; k++) {
					K += fe.dN (a, k) * fe.dN (b, k) * detJxW;
				}
				/// The mass term(unsteady term)
				double M = (fe.N (a)) * fe.N (b) * detJxW;

				/// Advection term for phi equation
				double advecPhi = 0.0;
				for (int k = 0; k < nsd; k++) {
					advecPhi -=
							(fe.dN (a, k)) * (vel (k)) * fe.N (b) * detJxW;
				}

				R = M * free_energy->d2fdphi2 (phi);
				mobility = 1.0;
				/// Phi terms: unsteady term and advection term of phi
				Ae (CHNSNodeData::CH_DOF * a, CHNSNodeData::CH_DOF * b) +=
						(bdf_c[0] * M) + (advecPhi * dt_);
				/// Laplacian of mu in phi equation
				Ae (CHNSNodeData::CH_DOF * a, CHNSNodeData::CH_DOF * b + 1) +=
						(mobility / (Pe * Cn)) * K * dt_;
				/// phi terms in mu equation: term 1 is df/d phi, term 2: laplacian
				/// of phi
				Ae (CHNSNodeData::CH_DOF * a + 1, CHNSNodeData::CH_DOF * b) +=
						((Cn * Cn * K) + R) * dt_;
				/// mu term in mu equation
				Ae (CHNSNodeData::CH_DOF * a + 1, CHNSNodeData::CH_DOF * b + 1) -=
						M * dt_;
			}

		}
	}

	/** This is a BDF2 implementation of the conservative Cahn-Hilliard equation with const mobility.
	 * @param fe FEElm object
	 * @param Ae Initialized empty matrix for Ae
	 * @param be Initialized empty vector for Be
	 * @param dt timestep
	 * @param free_energy: Pointer to the type of free energy
	 * @param t: elapsed time
	 */
	void CHNSIntegrandsCHConsvBDF2be(
			const FEMElm &fe, ZEROARRAY<double> &be, const double dt, CHNSFreeEnergy *free_energy, const double t) {

		/// Get all the non -dimensional numbers for the CH Equations
		double Pe = this->input_data_->Pe; /// Diffusional Peclet number
		double Cn = this->input_data_->Cn; /// Cahn number

		const double dt_ = dt;
		const double t_ = t + dt_; /// current time is elapsed + dt
		const int nsd = DIM;
		const int nbf = fe.nbf ();
		const double detJxW = fe.detJxW ();


		/// getting the value of phase field from NODEData
		double phi = this->p_data_->valueFEM (fe, CHNSNodeData::PHI);

		/// Remember that the position of u_n is starting from
		/// CHNSNodeData::NUM_VARS + CHNSNodeData::MANUFAC_SOL_VARS
		double phiPrev = this->p_data_->valueFEM (fe, CHNSNodeData::PHI_PRE1);
		double phiPrev2 = this->p_data_->valueFEM (fe, CHNSNodeData::PHI_PRE2);
		double mu = this->p_data_->valueFEM (fe, CHNSNodeData::MU);
		double mu_prev = this->p_data_->valueFEM (fe, CHNSNodeData::MU_PRE1);

		/// Calculating the forcing for Phi at current timestep
		double forcingPhiCurr;
		if (input_data_->caseTypeCHinit == CHNSInputData::MANUFACTURED_SOL_TEST_CH) {
			forcingPhiCurr = calcForcingManufacturedSolutionPhi (fe, t_);
			// forcingPhi = 0.0;
		} else {
			forcingPhiCurr = 0.0;
		}

		/// Calculating the forcing for Phi at previous timestep
		double forcingPhiPre;
		if (input_data_->caseTypeCHinit == CHNSInputData::MANUFACTURED_SOL_TEST_CH) {
			forcingPhiPre = calcForcingManufacturedSolutionPhi (fe, (t_ - dt_));
			// forcingPhi = 0.0;
		} else {
			forcingPhiPre = 0.0;
		}

		/// Calculating the forcing for Mu at current timestep
		double forcingMu;
		if (input_data_->caseTypeCHinit == CHNSInputData::MANUFACTURED_SOL_TEST_CH) {
			forcingMu = calcForcingManufacturedSolutionMu (fe, t_);
		} else {
			forcingMu = 0.0;
		}

		/// Calculating the forcing for Mu for previous timestep
		double forcingMuPre;
		if (input_data_->caseTypeCHinit == CHNSInputData::MANUFACTURED_SOL_TEST_CH) {
			forcingMuPre = calcForcingManufacturedSolutionMu (fe, (t_ - dt_));
		} else {
			forcingMuPre = 0.0;
		}

		/// Calculating the derivatives of the phase field, mu, and velocity from
		/// Navier Stokes
		ZEROPTV dmu;
		ZEROPTV dphi;
		ZEROPTV vel;
		ZEROPTV velPre;
		ZEROPTV dphiPrev;
		ZEROPTV dmuPrev;
		for (int i = 0; i < nsd; i++) {
			dmu (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::MU, i);
			dphi (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PHI, i);
			vel (i) = this->p_data_->valueFEM (fe, CHNSNodeData::VEL_X + i); /// As first three elements
			/// on NodeData are always velocity
			velPre (i) = this->p_data_->valueFEM (fe, (CHNSNodeData::VEL_X_PRE1 + i));
			dphiPrev (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::PHI_PRE1, i);
			dmuPrev (i) = this->p_data_->valueDerivativeFEM (fe, CHNSNodeData::MU_PRE1, i);
		}

		double K, S, R;
		for (int a = 0; a < nbf; a++) {
			/// Assembling elemental 'b' vector -- 'phi' equation

			/// mass term (unsteady term) - forcing term
			be (CHNSNodeData::CH_DOF * a) +=
					(fe.N (a)) * (bdf_c[0] * phi + bdf_c[1] * phiPrev + bdf_c[2] * phiPrev2) * detJxW -
					((fe.N (a)) * forcingPhiCurr * detJxW * dt_);

			/// Stiffness term and advective term
			for (int k = 0; k < nsd; k++) {
				/// Laplacian of mu
				be (CHNSNodeData::CH_DOF * a) += (mobility / (Pe * Cn)) * fe.dN (a, k) * (dmu (k)) * detJxW * dt_;
				/// Advective term
				be (CHNSNodeData::CH_DOF * a) -= (fe.dN (a, k)) * (vel (k) * phi ) * detJxW * dt_;
			}

			/// Assembling elemental 'b' vector -- 'mu' equation

			be (CHNSNodeData::CH_DOF * a + 1) -=
					(fe.N (a)) * (mu) * detJxW * dt_; ///
			/// mass
			/// term
			for (int k = 0; k < nsd; k++) {
				/// Laplacian of phi(weakened)
				be (CHNSNodeData::CH_DOF * a + 1) += Cn * Cn * fe.dN (a, k) * dphi (k) * detJxW * dt_;
			}

			/// S = fe.N(a) * dfdphi_poly(phi) * detJxW;
			/// S = fe.N(a) * dfdphi_FH(phi) * detJxW;
			/// contribution of df/dphi to be
			S = ((fe.N (a)) * free_energy->dfdphi (phi)  * detJxW * dt_) +
			    /// The forcing term for mu equation
			    (((fe.N (a)) * forcingMu * detJxW * dt_));
			be (CHNSNodeData::CH_DOF * a + 1) += S;
		}
	}


#pragma mark Manufactured solutions helper

	/**Function to calculate the external forcing in manufactured solutions case for Navier Stokes
	 * @param fe FEMElm object for positions
	 * @param t current time
	 * @return forcing due to manufactured solutions
	 */
	ZEROPTV calcForcingManufacturedSolutionNS(const FEMElm &fe, const double &t) {
		ZEROPTV forcingNS;
		ZEROPTV location = fe.position ();

		forcingNS.x () = forcingNSXdir (location, t);
		forcingNS.y () = forcingNSYdir (location, t);
		forcingNS.z () = 0.0;
		return forcingNS;
	}

	/**Function to calculate the external forcing in manufactured solutions case for Cahn Hilliard
	 * @param fe FEMElm object for positions
	 * @param t current time
	 * @return forcing due to manufactured solutions
	 */
	double calcForcingManufacturedSolutionPhi(const FEMElm &fe, const double &t) {
		ZEROPTV location = fe.position ();

		// double forcing = forcingCHtwoPhasePhi(location, t);
		double forcing = forcingCHtwoPhasePhiphiScaled(location, t);
		return forcing;
	}

	/**Function to calculate the external forcing in manufactured solutions case for Cahn Hilliard
	 * @param fe FEMElm object for positions
	 * @param t current time
	 * @return forcing due to manufactured solutions
	 */
	double calcForcingManufacturedSolutionMu(const FEMElm &fe, const double &t) {
		ZEROPTV location = fe.position ();
		// double forcing = forcingCHtwoPhaseMu(location, t);
		double forcing = forcingCHtwoPhaseMuphiScaled (location, t);
		return forcing;
	}

	/// Function which calculate forcing for manufactured solutions case for
	/// Navier-Stokes in X direction
	double forcingNSXdir(const ZEROPTV &location, const double &t) {
		double time = t;
		/// physical parameters
		double rhoH = input_data_->rhoH;
		double rhoL = input_data_->rhoL;
		double alpha = ((rhoH - rhoL) / 2);
		double beta = ((rhoH + rhoL) / 2);

		/// Get non dimensional numbers
		double Re = this->input_data_->Re;
		double Pe = this->input_data_->Pe; /// Diffusional Peclet number
		double We = this->input_data_->We; /// Weber number
		double Cn = this->input_data_->Cn; /// Cahn number
		double Fr = this->input_data_->Fr; /// Froude number

		/// Get the reference velocity and lenghtscale
		// double U = input_data_->referenceVelocityScaleFreeEnergy;
		// double L = input_data_->referenceLengthScaleCn;
		// double Re_Mass = input_data_->ReMass;
		// Forcing in x
		double forcingX = 0.0;
		if (input_data_->manufacSolType == 1) {
			double t2 = location.y() * M_PI;
			double t3 = cos(t2);
			double t4 = t3 * t3;
			double t5 = location.x() * M_PI;
			double t6 = sin(t5);
			double t7 = t6 * t6;
			double t8 = sin(time);
			double t9 = t8 * t8;
			double t10 = cos(time);
			double t11 = cos(t5);
			double t12 = sin(t2);
			double t13 = 1.0 / We;
			double t14 = M_PI * M_PI;
			double t15 = t11 * t11;
			double t16 = t12 * t12;
			double t17 = 1.0 / Pe;
			forcingX = beta * t3 * t6 * t10 + t10 * t11 * t12 * t13 * M_PI +
			           (t3 * t6 * t8 * t14 * 2.0) / Re +
			           alpha * t4 * t6 * t8 * t10 * t11 * 2.0 +
			           beta * t4 * t6 * t9 * t11 * M_PI +
			           beta * t6 * t9 * t11 * t16 * M_PI +
			           alpha * t4 * t6 * t9 * t11 * t14 * t17 +
			           alpha * t6 * t9 * t11 * t14 * t16 * t17 +
			           Cn * t4 * t6 * t9 * t11 * t13 * t14 * M_PI * 3.0 -
			           Cn * t6 * t9 * t11 * t13 * t14 * t16 * M_PI -
			           alpha * t3 * t4 * t6 * t7 * t8 * t9 * M_PI +
			           alpha * t3 * t4 * t6 * t8 * t9 * t15 * M_PI +
			           alpha * t3 * t6 * t8 * t9 * t15 * t16 * M_PI * 2.0;
		} else if (input_data_->manufacSolType == 2) {
//			if (input_data_->solverLinearOrNot){ /// true if linear
			/// linear solver uses a different surface tension forcing: phi\grad(mu)
			if (input_data_->pureNS) {
				assert(rhoH == rhoL);
				double t2 = sin (t);
				double t3 = location.x () * M_PI;
				double t4 = location.y () * M_PI;
				double t5 = M_PI * M_PI;
				double t6 = cos (t3);
				double t7 = cos (t4);
				double t8 = sin (t3);
				double t9 = sin (t4);
				double t10 = t8 * t8;
				forcingX = (t9 * M_PI * (-Re * t2 * t8 + We * t2 * t5 * t7 * t10 * 1.2E+1 + Re * We * t7 * t10 * cos (t) * 2.0
				                         - We * t2 * t5 * (t6 * t6) * t7 * 4.0
				                         + Re * We * (t2 * t2) * t5 * t6 * (t8 * t8 * t8) * t9 * 4.0)) / (Re * We);
			}else {
				if (input_data_->ifdphidphiForcing) { /// div (grad phi grad phi) forcing
					double t2 = cos (t);
					double t3 = sin (t);
					double t4 = location.x () * M_PI;
					double t5 = location.y () * M_PI;
					double t6 = M_PI * M_PI * M_PI;
					double t13 = 1.0 / Pe;
					double t14 = 1.0 / Re;
					double t15 = 1.0 / We;
					double t7 = t3 * t3;
					double t8 = t3 * t3 * t3;
					double t9 = cos (t4);
					double t10 = cos (t5);
					double t11 = sin (t4);
					double t12 = sin (t5);
					double t16 = t9 * t9;
					double t17 = t10 * t10;
					double t18 = t11 * t11;
					double t19 = t11 * t11 * t11;
					double t20 = t12 * t12;
					forcingX = -t3 * t11 * t12 * t15 * M_PI + beta * t6 * t7 * t9 * t19 * t20 * 4.0
					           - t3 * t6 * t10 * t12 * t14 * t16 * 4.0 + t3 * t6 * t10 * t12 * t14 * t18 * 1.2E+1
					           - alpha * t6 * t8 * (t10 * t10 * t10) * (t11 * t11 * t11 * t11 * t11) * t20 * 4.0
					           + beta * t2 * t10 * t12 * t18 * M_PI * 2.0 + Cn * t6 * t7 * t9 * t11 * t15 * t17 * 3.0
					           - Cn * t6 * t7 * t9 * t11 * t15 * t20 - alpha * t6 * t7 * t9 * t12 * t13 * t18 * 2.0
					           + alpha * t6 * t8 * t10 * t16 * t19 * t20 * 4.0
					           + alpha * t6 * t8 * t10 * t16 * t19 * (t20 * t20) * 4.0
					           + alpha * t2 * t3 * t9 * t12 * t17 * t18 * M_PI * 4.0
					           + alpha * t6 * t7 * t9 * t12 * t13 * t17 * t18 * 1.2E+1;
				} else { /// phi grad(mu) forcing
					double t2 = cos (t);
					double t3 = sin (t);
					double t4 = location.x() * M_PI;
					double t5 = location.y() * M_PI;
					double t6 = M_PI * M_PI * M_PI;
					double t13 = 1.0 / Pe;
					double t14 = 1.0 / Re;
					double t15 = 1.0 / We;
					double t7 = t3 * t3;
					double t8 = t3 * t3 * t3;
					double t9 = cos (t4);
					double t10 = cos (t5);
					double t11 = sin (t4);
					double t12 = sin (t5);
					double t16 = t9 * t9;
					double t17 = t10 * t10;
					double t18 = t11 * t11;
					double t19 = t11 * t11 * t11;
					double t20 = t12 * t12;
					forcingX =
							-t3 * t11 * t12 * t15 * M_PI + beta * t6 * t7 * t9 * t19 * t20 * 4.0 - t3 * t6 * t10 * t12 * t14 * t16 * 4.0
							+ t3 * t6 * t10 * t12 * t14 * t18 * 1.2E+1 - alpha * t6 * t8 * (t10 * t10 * t10) * (t11 * t11 * t11 * t11 * t11) * t20 * 4.0
							+ beta * t2 * t10 * t12 * t18 * M_PI * 2.0 - alpha * t6 * t7 * t9 * t12 * t13 * t18 * 2.0
							+ alpha * t6 * t8 * t10 * t16 * t19 * t20 * 4.0 - (t7 * t9 * t11 * t15 * t17 * M_PI) / Cn
							+ alpha * t6 * t8 * t10 * t16 * t19 * (t20 * t20) * 4.0 + alpha * t2 * t3 * t9 * t12 * t17 * t18 * M_PI * 4.0
							+ alpha * t6 * t7 * t9 * t12 * t13 * t17 * t18 * 1.2E+1;
				}
			}
		} else {
			throw TALYException() << "manufacCase not implemented";
		}


		return forcingX;
	}

	/// Function which calculate forcing for manufactured solutions case for
	/// Navier-Stokes in Y direction
	double forcingNSYdir(const ZEROPTV &location, const double &t) {
		double time = t;
		/// physical parameters
		double rhoH = input_data_->rhoH;
		double rhoL = input_data_->rhoL;
		double alpha = ((rhoH - rhoL) / 2);
		double beta = ((rhoH + rhoL) / 2);

		/// Get non dimensional numbers
		double Re = this->input_data_->Re;
		double Pe = this->input_data_->Pe; /// Diffusional Peclet number
		double We = this->input_data_->We; /// Weber number
		double Cn = this->input_data_->Cn; /// Cahn number
		double Fr = this->input_data_->Fr; /// Froude number

		double forcingY = 0.0;

		// Forcing in y
		double forcingX = 0.0;
		if (input_data_->manufacSolType == 1) {
			double t2 = location.x() * M_PI;
			double t3 = cos(t2);
			double t4 = t3 * t3;
			double t5 = location.y() * M_PI;
			double t6 = sin(t5);
			double t7 = t6 * t6;
			double t8 = sin(time);
			double t9 = t8 * t8;
			double t10 = cos(time);
			double t11 = 1.0 / Fr;
			double t12 = cos(t5);
			double t13 = sin(t2);
			double t14 = 1.0 / We;
			double t15 = M_PI * M_PI;
			double t16 = t12 * t12;
			double t17 = t13 * t13;
			double t18 = 1.0 / Pe;
			forcingY =
					beta * t11 - beta * t3 * t6 * t10 + t10 * t12 * t13 * t14 * M_PI +
					alpha * t3 * t8 * t11 * t12 - (t3 * t6 * t8 * t15 * 2.0) / Re -
					alpha * t4 * t6 * t8 * t10 * t12 * 2.0 +
					beta * t4 * t6 * t9 * t12 * M_PI + beta * t6 * t9 * t12 * t17 * M_PI -
					alpha * t4 * t6 * t9 * t12 * t15 * t18 -
					alpha * t6 * t9 * t12 * t15 * t17 * t18 +
					Cn * t4 * t6 * t9 * t12 * t14 * t15 * M_PI * 3.0 -
					Cn * t6 * t9 * t12 * t14 * t15 * t17 * M_PI -
					alpha * t3 * t4 * t6 * t7 * t8 * t9 * M_PI +
					alpha * t3 * t4 * t6 * t8 * t9 * t16 * M_PI +
					alpha * t3 * t6 * t8 * t9 * t16 * t17 * M_PI * 2.0;
		} else if (input_data_->manufacSolType == 2) {
//			if (input_data_->solverLinearOrNot){ /// true if linear
			/// linear solver uses a different surface tension forcing: phi\grad(mu)
			if (input_data_->pureNS){
				assert(rhoH == rhoL);
				double t2 = sin (t);
				double t3 = location.x () * M_PI;
				double t4 = location.y () * M_PI;
				double t5 = M_PI * M_PI * M_PI;
				double t10 = 1.0 / Re;
				double t6 = cos (t3);
				double t7 = cos (t4);
				double t8 = sin (t3);
				double t9 = sin (t4);
				double t11 = t9 * t9;
				forcingY = 1.0 / Fr + (t2 * t2) * t5 * t7 * (t8 * t8) * (t9 * t9 * t9) * 4.0 + (t2 * t6 * t7 * M_PI) / We
				           - t6 * t8 * t11 * M_PI * cos (t) * 2.0 - t2 * t5 * t6 * t8 * t10 * t11 * 1.2E+1
				           + t2 * t5 * t6 * (t7 * t7) * t8 * t10 * 4.0;
			} else {
				if (input_data_->ifdphidphiForcing) { /// div (grad phi grad phi) forcing
					double t2 = cos (t);
					double t3 = sin (t);
					double t4 = location.x () * M_PI;
					double t5 = location.y () * M_PI;
					double t6 = M_PI * M_PI * M_PI;
					double t13 = 1.0 / Fr;
					double t14 = 1.0 / Pe;
					double t15 = 1.0 / Re;
					double t16 = 1.0 / We;
					double t7 = t3 * t3;
					double t8 = t3 * t3 * t3;
					double t9 = cos (t4);
					double t10 = cos (t5);
					double t11 = sin (t4);
					double t12 = sin (t5);
					double t17 = t9 * t9;
					double t18 = t10 * t10;
					double t19 = t11 * t11;
					double t20 = t12 * t12;
					double t21 = t12 * t12 * t12;
					forcingY = beta * t13 + t3 * t9 * t10 * t16 * M_PI + alpha * t3 * t9 * t10 * t13
					           + beta * t6 * t7 * t10 * t19 * t21 * 4.0 + t3 * t6 * t9 * t11 * t15 * t18 * 4.0
					           - t3 * t6 * t9 * t11 * t15 * t20 * 1.2E+1
					           - alpha * t6 * t8 * (t9 * t9 * t9) * (t12 * t12 * t12 * t12 * t12) * t19 * 4.0
					           - beta * t2 * t9 * t11 * t20 * M_PI * 2.0 + Cn * t6 * t7 * t10 * t12 * t16 * t17 * 3.0
					           - Cn * t6 * t7 * t10 * t12 * t16 * t19 + alpha * t6 * t7 * t10 * t11 * t14 * t20 * 2.0
					           + alpha * t6 * t8 * t9 * t18 * t19 * t21 * 4.0
					           + alpha * t6 * t8 * t9 * t18 * (t19 * t19) * t21 * 4.0
					           - alpha * t2 * t3 * t10 * t11 * t17 * t20 * M_PI * 4.0
					           - alpha * t6 * t7 * t10 * t11 * t14 * t17 * t20 * 1.2E+1;
				} else { /// phi grad(mu) forcing
					double t2 = cos (t);
					double t3 = sin (t);
					double t4 = location.x() * M_PI;
					double t5 = location.y() * M_PI;
					double t6 = M_PI * M_PI * M_PI;
					double t13 = 1.0 / Fr;
					double t14 = 1.0 / Pe;
					double t15 = 1.0 / Re;
					double t16 = 1.0 / We;
					double t7 = t3 * t3;
					double t8 = t3 * t3 * t3;
					double t9 = cos (t4);
					double t10 = cos (t5);
					double t11 = sin (t4);
					double t12 = sin (t5);
					double t17 = t9 * t9;
					double t18 = t10 * t10;
					double t19 = t11 * t11;
					double t20 = t12 * t12;
					double t21 = t12 * t12 * t12;
					forcingY =
							beta * t13 + t3 * t9 * t10 * t16 * M_PI + alpha * t3 * t9 * t10 * t13 + beta * t6 * t7 * t10 * t19 * t21 * 4.0
							+ t3 * t6 * t9 * t11 * t15 * t18 * 4.0 - t3 * t6 * t9 * t11 * t15 * t20 * 1.2E+1
							- alpha * t6 * t8 * (t9 * t9 * t9) * (t12 * t12 * t12 * t12 * t12) * t19 * 4.0
							- beta * t2 * t9 * t11 * t20 * M_PI * 2.0 + alpha * t6 * t7 * t10 * t11 * t14 * t20 * 2.0
							+ alpha * t6 * t8 * t9 * t18 * t19 * t21 * 4.0 - (t7 * t9 * t11 * t16 * t18 * M_PI) / Cn
							+ alpha * t6 * t8 * t9 * t18 * (t19 * t19) * t21 * 4.0 - alpha * t2 * t3 * t10 * t11 * t17 * t20 * M_PI * 4.0
							- alpha * t6 * t7 * t10 * t11 * t14 * t17 * t20 * 1.2E+1;
				}
			}
		} else {
			throw TALYException() << "manufacCase not implemented";
		}
		return forcingY;
	}

	/// Function which calculate forcing for manufactured solutions case for
	/// Cahn-Hilliard for phi with 1/Cn from mu equation in phi equation
	double forcingCHtwoPhasePhiphiScaled(const ZEROPTV &location,
	                                     const double &t) {
		double time = t;
		/// physical parameters
		double rhoH = input_data_->rhoH;
		double rhoL = input_data_->rhoL;
		double alpha = ((rhoH - rhoL) / 2);
		double beta = ((rhoH + rhoL) / 2);

		/// Get non dimensional numbers
		double Pe = this->input_data_->Pe; /// Diffusional Peclet number
		double Cn = this->input_data_->Cn; /// Cahn number
		// double L = this->input_data_->referenceLengthScaleCn; /// Global Scaling
		/// length for interface thickness
		// double U = this->input_data_->referenceVelocityScaleFreeEnergy; ///
		// Global
		// double Re_Mass = this->input_data_->ReMass;
		/// scaling velocity for freeEnergy

		double forcingPhi = 0.0;
		if (input_data_->manufacSolType == 1) {
			double t2 = location.x() * M_PI;
			double t3 = cos(t2);
			double t4 = location.y() * M_PI;
			double t5 = sin(t4);
			double t6 = sin(t);
			double t7 = cos(t4);
			double t8 = sin(t2);
			double t9 = t6 * t6;
			forcingPhi = t3 * t7 * cos(t) + (t3 * t3) * (t5 * t5) * t9 * M_PI -
			             (t7 * t7) * (t8 * t8) * t9 * M_PI +
			             (t3 * t6 * t7 * (M_PI * M_PI) * 2.0) / (Cn * Pe);
		} else if (input_data_->manufacSolType == 2) {
			if (input_data_->pureCH) {
				forcingPhi = (cos (location.x () * M_PI) * cos (location.y () * M_PI)
				              * ((M_PI * M_PI) * sin (t) * 2.0 + Cn * Pe * cos (t))) / (Cn * Pe);
			} else {
				double t2 = sin (t);
				double t3 = location.x () * M_PI;
				double t4 = location.y () * M_PI;
				double t5 = M_PI * M_PI;
				double t6 = t2 * t2;
				double t7 = cos (t3);
				double t8 = cos (t4);
				double t9 = sin (t3);
				double t10 = sin (t4);
				forcingPhi = t7 * t8 * cos (t) + t5 * t6 * (t7 * t7) * t9 * (t10 * t10 * t10) * 2.0
				             - t5 * t6 * (t8 * t8) * (t9 * t9 * t9) * t10 * 2.0 + (t2 * t5 * t7 * t8 * 2.0) / (Cn * Pe);
			}
		} else {
			throw TALYException() << "manufacCase not implemented";
		}

		return forcingPhi;
	}

	/// Function which calculate forcing for manufactured solutions case for
	/// Cahn-Hilliard with 1/Cn from mu equation in phi equation
	double forcingCHtwoPhaseMuphiScaled(const ZEROPTV &location,
	                                    const double &t) {
		double time = t;
		/// physical parameters
		double rhoH = input_data_->rhoH;
		double rhoL = input_data_->rhoL;
		double alpha = ((rhoH - rhoL) / 2);
		double beta = ((rhoH + rhoL) / 2);

		/// Get non dimensional numbers
		double Pe = this->input_data_->Pe; /// Diffusional Peclet number
		double Cn = this->input_data_->Cn; /// Cahn number
		// double Re_Mass = this->input_data_->ReMass;
		/// scaling Re for freeEnergy

		// Forcing for Mu equation
		double forcingMu = 0.0;
		if (input_data_->manufacSolType == 1) {
			double t2 = location.x() * M_PI;
			double t3 = cos(t2);
			double t4 = t3 * t3;
			double t5 = location.y() * M_PI;
			double t6 = cos(t5);
			double t7 = t6 * t6;
			double t8 = sin(time);
			double t9 = t8 * t8;
			forcingMu = t3 * t6 * t8 * 2.0 -
			            (Cn * Cn) * t3 * t6 * t8 * (M_PI * M_PI) * 2.0 -
			            t3 * t4 * t6 * t7 * t8 * t9;
		} else if (input_data_->manufacSolType == 2) {
			if (input_data_->pureCH) {
				double t2 = sin (t);
				double t3 = location.x () * M_PI;
				double t4 = location.y () * M_PI;
				double t5 = cos (t3);
				double t6 = cos (t4);
				forcingMu = t2 * t5 * t6 * 2.0 - (t2 * t2 * t2) * (t5 * t5 * t5) * (t6 * t6 * t6)
				            - (Cn * Cn) * t2 * t5 * t6 * (M_PI * M_PI) * 2.0;
			} else {
				double t2 = sin (t);
				double t3 = location.x () * M_PI;
				double t4 = location.y () * M_PI;
				double t5 = cos (t3);
				double t6 = cos (t4);
				forcingMu = t2 * t5 * t6 * 2.0 - (t2 * t2 * t2) * (t5 * t5 * t5) * (t6 * t6 * t6)
				            - (Cn * Cn) * t2 * t5 * t6 * (M_PI * M_PI) * 2.0;
			}
		} else {
			throw TALYException() << "manufacCase not implemented";
		}

		return forcingMu;
	}

};
