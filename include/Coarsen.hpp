//
// Created by makrand on 3/17/21.
//

#ifndef LINNS_PSPG_PNP_NSCOARSEN_HPP
#define LINNS_PSPG_PNP_NSCOARSEN_HPP
#include <Traversal/Refinement.h>
class Coarsen : public Refinement {

	const DENDRITE_REAL * sphereRegion_;
	const DENDRITE_UINT coarseLevel_;
	const DENDRITE_UINT wallCoarseLevel_;
	const DomainExtents & domainExtents_;
 public:
	Coarsen(DA *da, const std::vector<TREENODE> & treePart, const DomainExtents &domainInfo, const size_t numLocalElements, const DENDRITE_UINT coarseLevel,
	        const DENDRITE_UINT wallCoarseLevel, const DENDRITE_REAL * sphereRegion);



	ot::OCT_FLAGS::Refine getRefineFlags(TALYFEMLIB::FEMElm &fe, const std::vector<TALYFEMLIB::ZEROPTV> &coords) override;


};

Coarsen::Coarsen(DA *da, const std::vector<TREENODE> & treePart, const DomainExtents &domainInfo, const size_t numLocalElements,
                 const DENDRITE_UINT coarseLevel, const DENDRITE_UINT wallCoarseLevel, const DENDRITE_REAL * sphereRegion)
		:sphereRegion_(sphereRegion),coarseLevel_(coarseLevel),wallCoarseLevel_(wallCoarseLevel),Refinement(da,treePart,domainInfo),domainExtents_(domainInfo)
{
	this->initRefinement();
}

ot::OCT_FLAGS::Refine
Coarsen::getRefineFlags(TALYFEMLIB::FEMElm &fe, const std::vector<TALYFEMLIB::ZEROPTV> &coords) {

    const DENDRITE_UINT npe = this->m_octDA->getNumNodesPerElement();
    bool isPositive = false;
    for(int dim = 0; dim < DIM; dim++){
        if((coords[npe - 1].data()[dim] >= domainExtents_.physicalDADomain.max[dim])
           or (FEQUALS(coords[npe - 1].data()[dim], domainExtents_.physicalDADomain.max[dim]))){
           isPositive = true;
        }
    }

    bool ifSphereRegion = ((coords[0].z() > sphereRegion_[0] and coords[7].z() < sphereRegion_[1]));
	if(m_level > coarseLevel_ and not(m_BoundaryOctant) and not(ifSphereRegion)){
		return ot::OCT_FLAGS::Refine::OCT_COARSEN;
	}
	else if(m_level > wallCoarseLevel_ and (m_BoundaryOctant) and not(isPositive) and not(ifSphereRegion)){
        return ot::OCT_FLAGS::Refine::OCT_COARSEN;
	}
	else{
		return ot::OCT_FLAGS::Refine::OCT_NO_CHANGE;
	}
}
#endif //LINNS_PSPG_PNP_NSCOARSEN_HPP
